export default {
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'CÔNG TY CỔ PHẦN MINH PHÚC HOLDING',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'ant-design-vue/dist/antd.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/antd-ui',
    '@/plugins/api.js',
    '@/plugins/core.js',
    '@/plugins/products.js',
    '@/plugins/infiniteScroll.js',
    '@/plugins/axios.js',
    '@/plugins/vueliate.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',

  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {

  },

  server: {
    port: 6600,
    host: 'localhost'
  },

  axios: {
    baseURL: process.env.BASE_URL,
    // proxy: true,
    headers: { //optional
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }
  },
  auth: {
    rewriteRedirects: true,
    watchLoggedIn: true,
    strategies: {
      local: {
        token: {
          property: 'data.access_token',
          maxAge: 86400,
          global: true,
          type: 'Bearer'
        },
        user: {
          property: 'user',
          autoFetch: true
        },
        endpoints: {
          login: { url: '/login', method: 'post', propertyName: 'data.access_token'},
          refresh: { url: '/refresh', method: 'post' },
          user: { url: '/me', method: 'get' },
          logout: { url: '/logout', method: 'post'}
        },
        autoLogout: true
      }
    },
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      home: '/',
    },
  },

  env: {
    CLIENT_BASE_URL: process.env.CLIENT_BASE_URL,
    BASE_URL: process.env.BASE_URL,
    PREFIX_SELLER: process.env.PREFIX_SELLER,
    HOST: process.env.HOST,
  }
}
