
export default (context, inject) => {
  inject('buildSlug', buildSlug)
  inject('buildCategorySlug', buildCategorySlug)
}

function buildSlug(e) {
  return '/'+e.slug+'-pid'+(!e.product_id ? e.id : e.product_id)
}

function buildCategorySlug(e) {
  return '/mart/'+e.slug
}

function isEmpty(obj) {
  for(var prop in obj) {
    if(obj.hasOwnProperty(prop))
      return false;
  }
  return true;
}
