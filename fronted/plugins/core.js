
export default (context, inject) => {
    inject('getCategories', context.store.dispatch("categories/getAll"))
    inject('moneyFormat', moneyFormat)
    inject('getImageUrl', getImageUrl)
    inject('loadCart', loadCart)
    inject('loadProduct', loadProduct)
    inject('discount', discount)
    inject('calcDiscount', calcDiscount)
    inject('isEmpty', isEmpty)
    inject('number_format', number_format)
    inject('number_replace_dot', number_replace_dot)
    inject('getName', getName)
    inject('getLocation', getLocation)
    inject('copyURL', copyURL)
    inject('filterOption', filterOption)
    inject('urlSeller', urlSeller)
}

function getName(e) {
    return e == null ? '---' : typeof e.name !== "undefined" ? e.name : typeof e.full_name !== "undefined" ? e.full_name : '---'
}

function urlSeller(e) {
    return process.env.PREFIX_SELLER+e
}

function filterOption(input, option) {
  return (
    option.componentOptions.children[0].text.toLowerCase().indexOf(input.toLowerCase()) >= 0
  );
}

function getLocation(e) {
  let str = ''
  if (!isEmpty(e.ward)) {
    str += e.ward.name + ', ';
  }

  if (!isEmpty(e.district)) {
    str += e.district.name + ', ';
  }

  if (!isEmpty(e.city)) {
    str += e.city.name;
  }
  return str
}

function discount(product) {
    return '-'+calcDiscount(product)+ '%'
}

function calcDiscount(product) {
    let price = product.price
    let price_sale = product.price_sale
    return Math.round(100 - (price_sale/price*100))
}

function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
    return true;
}

function getImageUrl(e) {
    e = !isEmpty(e) ? e[0] : null
    return e == null ? '/img/no-image.png' : typeof e.url !== "undefined" ? e.url : '/img/no-image.png'
}

async function loadCart(cartItems) {
    if (cartItems && cartItems.length > 0) {
        let query = {};
        cartItems.forEach((item, key) => {
            query['products[' + key + '][id]'] = item.id
            query['products[' + key + '][quantity]'] = item.quantity
        });

        await this.$store.dispatch('carts/getCartProducts', query);
        return query;
    }
}

async function loadProduct(product) {
    let payload = {
        'quantity': product.quantity,
        'slug': product.slug+'-pid'+product.product_id
    }
    let response = await this.$store.dispatch('product/getProductByQuantityAndId', payload);
    return response;
}

function moneyFormat(number) {
    return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(number)
}

function number_format(Num) {
    Num = Num.toString().replace(/^0+/, "").replace(/\./g, "").replace(/,/g, "");
    Num = "" + parseInt(Num);
    var temp1 = "";
    var temp2 = "";
    if (Num == 0 || Num == undefined || Num == '0' || Num == '' || isNaN(Num)) {
        return '';
    } else {
        var count = 0;
        for (var k = Num.length - 1; k >= 0; k--) {
            var oneChar = Num.charAt(k);
            if (count == 3) {
                temp1 += ".";
                temp1 += oneChar;
                count = 1;
                continue;
            } else {
                temp1 += oneChar;
                count++;
            }
        }
        for (var k = temp1.length - 1; k >= 0; k--) {
            var oneChar = temp1.charAt(k);
            temp2 += oneChar;
        }
        return temp2;
    }
}
function number_replace_dot(string) {
    return string.toString().replace(/\D/g, '')
}

function copyURL(text) {
  try {
    navigator.clipboard.writeText(text);
    this.$message.success('Đã sao chép vào bộ nhớ đệm')
  } catch($e) {
    this.$message.error('Sao chép lỗi')
  }
}
