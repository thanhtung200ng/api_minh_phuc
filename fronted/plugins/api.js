import createApi from '~/api/index'


export const serializeQuery = query => {
  return Object.keys(query)
      .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(query[key])}`)
      .join('&');
};

export default (ctx, inject) => {

  inject('api', createApi(ctx.$axios))
}

