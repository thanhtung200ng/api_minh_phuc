import Vue from 'vue'
import lightgallery from 'lightgallery'
import 'lightgallery/dist/css/lightgallery.min.css'

Vue.use(lightgallery)
