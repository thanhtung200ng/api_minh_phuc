  export default () => ({
  object: {},
  variant: {
    name: '---',
    price: '---',
    price_sale: '---',
    images: []
  },
  cart: [],
  list:[],
  loading: false,
  total: 0,
  count: 20,
  page: 0
})
