export default {
  async getAll({ commit, state, dispatch }, payload) {
    dispatch('setPage', state.page)
    let pal = {
      count: state.count,
      page: state.page,
      categories: $nuxt.$route.params.slug, keyword: $nuxt.$route.query.q,
      ...payload
    };

    const res = await this.$api.products.all(pal);
    const { success } = res;
    if (success === true) {
      const { value } = res;
      commit('SET_TOTAL', value.total);
      commit('SET_LIST', value.products);
      commit('SET_LOADING', false)
    } else {
      const { errors } = res;
      commit('SET_LOADING', false)
    }
    return res
  },

  async getDetail({ commit, state }, payload) {
    let pal = {
      ...payload
    }
    commit('SET_LOADING', true)
    const res = await this.$api.products.show(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_OBJECT', value.product)
      commit('SET_LOADING', false)
    } else {
      const { errors } = res
      commit('SET_LOADING', false)
      // Handle error here
    }
    return res
  },

  setListEmpty({ commit, dispatch }, payload) {
    commit('SET_LIST_EMPTY', payload)
  },

  setPage({ commit, state }, payload) {
    commit('SET_PAGE', payload)
  },
  setLoading({ commit, state }, payload) {
    commit('SET_LOADING', payload)

  }
}
