export default {
  async getAll({ commit, dispatch }) {
    const res = await this.$api.categories.all()
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_CATEGORIES', value.categories)
      dispatch('setObject')
    } else {
      const { errors } = res
      // Handle error here
    }
  },

  setObject({ commit, state }, payload) {
    if (this.app.context.route.params.slug) {
      let selected = state.list.find(e => e.slug === this.app.context.route.params.slug)
      if (selected) {
        commit('SET_OBJECT', selected)
      }
    }

  },

}
