export default {
  SET_CATEGORIES(state, value) {
    state.list = value;
  },
  SET_OBJECT(state, value) {
    state.object = value;
  }
}
