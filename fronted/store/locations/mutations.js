export default {
  SET_CITIES(state, newValue) {
    state.cities = newValue
  },
  SET_DISTRICTS(state, newValue) {
    state.districts = newValue
  },
  SET_WARDS(state, newValue) {
    state.wards = newValue
  },

  SET_CITY(state, new_value) {
    state.city_id = new_value
  },
  SET_DISTRICT(state, new_value) {
    state.district_id = new_value
  },
  SET_WARD(state, new_value) {
    state.ward_id = new_value
  },
  SET_STREET(state, new_value) {
    state.street = new_value
  },

}
