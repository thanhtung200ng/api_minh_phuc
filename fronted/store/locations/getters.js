export default {
  city: (state) => {
    return state.city_id
  },
  district: (state) => {
    return state.district_id
  },
  ward: (state) => {
    return state.ward_id
  },
  street: (state) => {
    return state.street
  },
}
