export default () => ({
  cities: [],
  districts: [],
  wards: [],
  city: null,
  district: null,
  ward: null,
  city_id: '',
  district_id: '',
  ward_id: '',
})
