export default {
  async getCities({ commit, dispatch }, payload) {
    const res = await this.$api.locations.allCities()
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_CITIES', value.locations)
    } else {
      const { errors } = res
      // Handle error here
    }
  },
  async getDistricts({ commit }, payload) {
    const res = await this.$api.locations.allDistricts(payload.city_id)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_DISTRICTS', value.locations)
    } else {
      const { errors } = res
      // Handle error here
    }
  },
  async getWards({ commit }, payload) {
    const res = await this.$api.locations.allWards(payload.district_id)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_WARDS', value.locations)
    } else {
      const { errors } = res
      // Handle error here
    }
  },


  setCity ({ commit, dispatch }, payload) {
    commit('SET_CITY', payload)
    commit('SET_DISTRICT', null)
    commit('SET_WARD', null)
    dispatch('getDistricts', {city_id: payload})
  },
  setDistrict ({ commit, dispatch }, payload) {
    commit('SET_DISTRICT', payload)
    commit('SET_WARD', null)
    dispatch('getWards', {district_id: payload})
  },
  setWard ({ commit }, payload) {
    commit('SET_WARD', payload)
  },
  setStreet ({ commit }, payload) {
    commit('SET_STREET', payload)
  },
}
