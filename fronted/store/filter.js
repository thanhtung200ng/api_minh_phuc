export const state = () => ({
  visible: false
});

export const mutations = {

  CHANGE_VISIBLE(state, status) {
    state.visible = status;
  },
};

export const actions = {

  onOff({ commit }, status = true ) {
    commit("CHANGE_VISIBLE", status);
  }
};

// export { state, mutations, actions };
