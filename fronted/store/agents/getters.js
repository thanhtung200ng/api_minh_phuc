export default {
  name: (state) => {
    return state.address.full_name
  },
  mobile: (state) => {
    return state.address.mobile
  },
  email: (state) => {
    return state.address.email
  },
  city: (state) => {
    return state.address.city_id
  },
  district: (state) => {
    return state.address.district_id
  },
  ward: (state) => {
    return state.address.ward_id
  },
  street: (state) => {
    return state.address.street
  },
  default: (state) => {
    return state.address.default
  },
}
