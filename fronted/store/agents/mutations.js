export default {
  SET_LIST(state, new_value) {
    state.list = new_value
  },
  SET_OBJECT(state, new_value) {
    state.address = new_value
  },
  SET_AGENT_SELECTED(state, new_value) {
    state.agentSelected = new_value
  },
  SET_NAME(state, new_value) {
    state.address.full_name = new_value
  },
  SET_MOBILE(state, new_value) {
    state.address.mobile = new_value
  },
  SET_EMAIL(state, new_value) {
    state.address.email = new_value
  },
  SET_CITY(state, new_value) {
    state.address.city_id = new_value
  },
  SET_DISTRICT(state, new_value) {
    state.address.district_id = new_value
  },
  SET_WARD(state, new_value) {
    state.address.ward_id = new_value
  },
  SET_STREET(state, new_value) {
    state.address.street = new_value
  },
  SET_PLACE_API(state, new_value) {
    state.listPlaceApi = new_value
  },
  SET_DEFAULT(state, new_value) {
    state.address.default = new_value
  },
  SET_LOADING(state, value) {
    state.loading = value;
  },
  SET_LOADING_API(state, value) {
    console.log(value)
    state.loadingAPI = value;
  },

}
