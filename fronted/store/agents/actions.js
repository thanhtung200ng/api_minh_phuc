export default {


  async getAll({ commit, state, dispatch }, payload) {
    let pal = {
      ...payload
    }
    commit('SET_LOADING', true)
    const res = await this.$api.agent.all(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_LIST', value.agent)
      let selected = value.agent.find(e => e.default)
      if (selected) {
        dispatch('setAddressSelected', selected)
      }

      commit('SET_LOADING', false)
    } else {
      const { errors } = res
      // Handle error here
      commit('SET_LOADING', false)
    }
    return res
  },

  async getDetail({ commit, state, dispatch }, payload) {
    let pal = {
      ...payload
    }
    commit('SET_LOADING', true)
    const res = await this.$api.agent.show(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      if (!$nuxt.$isEmpty(value.address)) {
        commit('SET_OBJECT', value.address)
      }
      if (!state.agent.id) {
        $nuxt.$router.push($nuxt.$route.path)
      }
      commit('SET_LOADING', false)
    } else {
      const { errors } = res
      commit('SET_LOADING', false)
      // Handle error here
    }
    return res
  },

  setLoading ({ commit }, payload) {
    commit('SET_LOADING', payload)
  },
  setAgentSelected ({ commit }, payload) {
    commit('SET_AGENT_SELECTED', payload)
  },
  setName ({ commit }, payload) {
    commit('SET_NAME', payload)
  },
  setMobile ({ commit }, payload) {
    commit('SET_MOBILE', payload)
  },
  setEmail ({ commit }, payload) {
    commit('SET_EMAIL', payload)
  },
  setCity ({ commit, dispatch }, payload) {
    commit('SET_CITY', payload)
    commit('SET_DISTRICT', null)
    commit('SET_WARD', null)
    commit('SET_STREET', '')
  },
  setDistrict ({ commit, dispatch }, payload) {
    commit('SET_DISTRICT', payload)
    commit('SET_WARD', null)
    commit('SET_STREET', '')
  },
  setWard ({ commit }, payload) {
    commit('SET_WARD', payload)
    commit('SET_STREET', '')
  },
  setPlaceApi ({ commit, state }, payload) {
    commit('SET_LOADING_API', true)
    commit('SET_PLACE_API', [])
    if (state.agent.ward_id) {

      let pay = {
        keyword: state.agent.street,
        city_id: state.agent.city_id,
        district_id: state.agent.district_id,
        ward_id: state.agent.ward_id,
      }
      this.$api.locations.allPlace(pay).then(function (res) {
        if (res.success === true) {
          if (!res.value.locations.length) {
            $nuxt.$message.error('Không tìm thấy địa chỉ của bạn trên google map. Vui lòng tìm kiếm lại lần nữa')
          }
          commit('SET_PLACE_API', res.value.locations)
        } else {
          $nuxt.$message.error(res.message)
        }
        commit('SET_LOADING_API', false)
      }).catch(function (error) {
        if (!$nuxt.$isEmpty(error.response.data.errors)) {
          error.response.data.errors.forEach(function (el) {
            $nuxt.$message.error(el.message)
          })
        } else {
          $nuxt.$message.error('Có lỗi xảy ra. Vui lòng liên hệ quản trị viên để được hướng dẫn.')
        }
        commit('SET_LOADING_API', false)

      })
    }else {
      $nuxt.$message.warn('Vui lòng nhập đầy đủ Tỉnh/Huyện/Xã')
    }
    commit('SET_STREET', '')
  },
  setStreet ({ commit, state }, payload) {
    commit('SET_STREET', payload)
    commit('SET_PLACE_API', [])
  },

  setDefault ({ commit }, payload) {
    commit('SET_DEFAULT', payload)
  },


}
