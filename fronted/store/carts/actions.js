export default {
  async getAll({ commit, state }, payload) {
    let pal = {
      ...payload
    }

    const res = await this.$api.cart.all(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_CARTS', value.carts)
    } else {
      const { errors } = res
      // Handle error here
    }
    return res
  },

  async getDetail({ commit, state }, payload) {
    let pal = {
      ...payload
    }

    const res = await this.$api.cart.all(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_CARTS', value.carts)
    } else {
      const { errors } = res
      // Handle error here
    }
    return res
  },
  setLoading({ commit, state, dispatch }, payload) {
    commit('SET_LOADING', {loading: payload})
  },
  async getItemCart({ commit, state, dispatch }, payload) {
    let pal = {
      ...payload
    }
    const vm = $nuxt
    commit('SET_LOADING', {loading: true})
    const res = await this.$api.cart.show(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_CART', value.cart)
      commit('SET_LOADING', {loading: false})
      dispatch('setDistanceMatrix', {distance: value.distance, shippingFee: value.shippingFee, shippingFeeDiscount: value.shippingFeeDiscount})
    } else {
      const { errors } = res
      if (payload.notice) {
        if (!vm.$isEmpty(errors)) {
          errors.forEach(function (el) {
            vm.$message.error(el.message)
          })
        } else {
          vm.$message.error(res.message)
        }
      }

    }
    return res
  },

  async addItemToCart({ commit, state, dispatch }, payload) {
    let pal = {
      ...payload
    }
    commit('SET_LOADING', {loading: true, id: pal.id})
    const res = await this.$api.cart.addTo(pal)
    const { success } = res
    if (success === true) {
      const { message, value } = res
      $nuxt.$message.success(message)
      commit('SET_LOADING', {loading: false})
      if ($nuxt.$route.params.slug && $nuxt.$route.name === 'carts-slug') {
        dispatch('getItemCart', {code: $nuxt.$route.params.slug})
      }else {
        dispatch("getItemCart", {code: 'wholesale'})
      }
    } else {
      const { errors } = res
      if (!$nuxt.$isEmpty(errors)) {
        errors.forEach(function (el) {
          $nuxt.$message.error(el.message)
        })
      } else {
        $nuxt.$message.warning('Hệ thống đang chuyển đổi. Vui lòng quay lại sau vài phút nữa.')
      }
      commit('SET_LOADING', {loading: false})
      // Handle error here
    }
    return res
  },

  async getPaymentMethod({ commit, state, dispatch }, payload) {
    let pal = []
    commit('SET_LOADING_PAYMENT_METHOD', true)
    let resGetItemCart = dispatch('getItemCart', {code: $nuxt.$route.params.code})
    resGetItemCart.then(async (response) => {
      if (response.success === true) {
        state.cart.items.forEach((item, key) => {
          pal.push(item.variant.id)
        })
        const res = await this.$api.cart.paymentMethod({products: pal})
        const {success} = res
        console.log(res)
        if (success === true) {
          const {value} = res
          commit('SET_LIST_PAYMENT_METHOD', value.wallets)
          commit('SET_LOADING_PAYMENT_METHOD', false)
        } else {
          const {errors} = res
          // Handle error here
        }
        return res
      }
    })
  },

  async setDistanceMatrix({ commit, state }, payload) {
    commit('SET_DISTANCE_MATRIX', payload.distance)
    commit('SET_SHIPPING_FEE', payload.shippingFee)
    commit('SET_SHIPPING_FEE_DISCOUNT', payload.shippingFeeDiscount)
  },

  setCustomerWalletSelected ({ commit }, payload) {
    commit('SET_CUSTOMER_WALLET_SELECTED', payload)
  },

  setCart ({ commit }, payload) {
    commit('SET_CART', payload)
  },
  async deleteItemToCart({ commit, state, dispatch }, payload) {
    let pal = {
      id:payload.id,
      ...payload
    }
    commit('SET_LOADING', {loading: true, id: pal.id})
    const res = await this.$api.cart.deleteTo(pal)
    const { success } = res
    if (success === true) {
      const { message, value } = res
      $nuxt.$message.success(message)
      commit('SET_LOADING', {loading: false})
      if ($nuxt.$route.params.slug) {
        dispatch('getItemCart', {code: $nuxt.$route.params.slug})
      }
    } else {
      const { errors } = res
      if (!$nuxt.$isEmpty(errors)) {
        errors.forEach(function (el) {
          $nuxt.$message.error(el.message)
        })
      } else {
        $nuxt.$message.warning('Hệ thống đang chuyển đổi. Vui lòng quay lại sau vài phút nữa.')
      }
      commit('SET_LOADING', {loading: false})
      // Handle error here
    }
    return res
  },
}
