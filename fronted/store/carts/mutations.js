export default {
  SET_CARTS(state, value) {
    state.carts = value;
    let amount = 0;
    value.forEach((item) => {
      amount += parseInt(item.items_sum_quantity);
    })
    state.amount = amount
  },
  SET_CART(state, value) {
    state.cart = value;
  },
  SET_LIST_PAYMENT_METHOD(state, value) {
    state.listPaymentMethod = value;
  },

  SET_TRANSFER_PAYMENT_METHOD(state, value) {
    state.transferPayment = value;
  },

  SET_CUSTOMER_WALLET_SELECTED(state, value) {
    state.customerWalletSelected = value;
  },

  SET_LOADING_PAYMENT_METHOD(state, value) {
    state.loadingPayment = value;
  },
  SET_PAYMENT_METHOD_SELECTED(state, value) {
    state.paymentMethodSelected = value;
  },
  SET_OBJECT(state, value) {
    state.object = value;
    if (!$nuxt.$isEmpty(value.variants[0])) {
      state.variant = value.variants[0];
    }
  },
  SET_AMOUNT(state, value) {
    state.amount = value;
  },

  SET_DISTANCE_MATRIX(state, value) {
    state.distanceMatrix = value;
  },

  SET_SHIPPING_FEE(state, value) {
    state.shippingFee = value;
  },

  SET_SHIPPING_FEE_DISCOUNT(state, value) {
    state.shippingFeeDiscount= value;
  },
  SET_PAGE(state) {
    state.page = state.page+1;
  },
  SET_LOADING(state, value) {
    state.loading = value.loading;
    if (value.loading && value.id) {
      state.variantIdCheck = value.id;
    }else {
      state.variantIdCheck = 0
    }
  },

}
