export default {
  totalEachCart: (state) => {
    let total = 0;
    if (!$nuxt.$isEmpty(state.cart.items)) {
      state.cart.items.forEach((item) => {
        total += parseInt(item.quantity) * item.variant.price_sale
      })
    }else {
      /*state.carts.items.forEach((item) => {
        total += parseInt(item.quantity) * item.variant.price_sale
      })*/
    }
    return total
  },
}
