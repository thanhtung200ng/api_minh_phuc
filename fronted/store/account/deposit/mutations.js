export default {
  SET_LIST_EMPTY(state) {
    state.list = [];
    state.page = 0;
    state.total = 0;
    console.log(state)
  },
  SET_LIST(state, value) {
    state.list = value;
  },
  SET_TOTAL(state, value) {
    state.total = value;
  },
  SET_TRANSACTION_LIST(state, value) {
    state.transaction_list = value;
  },
  SET_TRANSACTION_TOTAL(state, value) {
    state.transaction_total = value;
  },
  SET_TRANSACTION_LOADING(state, value) {
    state.loadingTransaction = value;
  },
  SET_OBJECT(state, value) {
    state.object = value;
    if (!$nuxt.$isEmpty(value.variants[0])) {
      state.variant = value.variants[0];
    }
  },
  SET_PAGE(state, payload) {
    state.page = payload+1;
  },
  SET_OPEN(state, payload) {
    state.open = payload;
  },
  SET_TYPE(state, payload) {
    state.type = payload;
  },
  SET_WALLET(state, payload) {
    state.wallet_id = payload;
  },
  SET_MONEY(state, payload) {
    state.money = payload;
  },
  SET_LOADING(state, value) {
    state.loading = value;
  },
  SET_RESPONSE(state, value) {
    state.response = value;
  }
}
