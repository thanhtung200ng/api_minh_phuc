export default () => ({
  object: {},
  response: {},
  cart: [],
  list: [],
  name: 'Nạp điểm',
  loading: false,
  open: false,
  money: 0,
  type: 0,
  total: 0,
  transaction_total: 0,
  transaction_list: [],
  loadingTransaction: false,
  count: 20,
  page: 0
})
