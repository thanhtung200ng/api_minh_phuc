export default {
  async getAll({ commit, state, dispatch }, payload) {
    dispatch('setPage', state.page)
    let pal = {
      count: state.count,
      page: state.page,
      ...payload
    }

    const res = await this.$api.deposit.all(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_TOTAL', value.total)
      commit('SET_LIST', value.deposits)
    } else {
      const { errors } = res
      // Handle error here
    }
    return res
  },

  async getAllTransaction({ commit, state, dispatch }, payload) {
    dispatch('setTransactionLoading', true)
    dispatch('setPage', state.page)
    let pal = {
      count: state.count,
      page: state.page,
      ...payload
    }

    const res = await this.$api.transaction.all(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_TRANSACTION_TOTAL', value.total)
      commit('SET_TRANSACTION_LIST', value.transactions)
      dispatch('setTransactionLoading', false)
    } else {
      const { errors } = res
      // Handle error here
      dispatch('setTransactionLoading', false)
    }
    return res
  },

  async getDetail({ commit, state }, payload) {
    let pal = {
      ...payload
    }
    commit('SET_LOADING', true)
    const res = await this.$api.orders.show(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_OBJECT', value.product)
      commit('SET_LOADING', false)
    } else {
      const { errors } = res
      commit('SET_LOADING', false)
      // Handle error here
    }
    return res
  },

  setType({ commit, dispatch }, payload) {
    commit('SET_TYPE', payload)
  },

  setTransactionLoading({ commit, dispatch }, payload) {
    commit('SET_TRANSACTION_LOADING', payload)
  },

  setWallet({ commit, dispatch }, payload) {
    commit('SET_WALLET', payload)
  },
  setListEmpty({ commit, dispatch }, payload) {
    commit('SET_LIST_EMPTY', payload)
  },

  setTransactionListEmpty({ commit, dispatch }) {
    dispatch('setPage', -1)
    commit('SET_TRANSACTION_LIST', [])
  },

  setOpen({ commit, state }, payload) {
    commit('SET_OPEN', false)
    commit('SET_OPEN', payload)
    commit('SET_RESPONSE', {})
  },
  setPage({ commit, state }, payload) {
    commit('SET_PAGE', payload)
  },
  setMoney({ commit, state }, payload) {
    commit('SET_MONEY', payload)
  },
  setLoading({ commit, state }, payload) {
    commit('SET_LOADING', payload)
  },
  setResponse({ commit, state }, payload) {
    commit('SET_RESPONSE', payload)
  },

  handleDeposit: async function ({ commit, state, dispatch }) {
    const vm = $nuxt
    dispatch('setLoading', true)
    let keygen = vm.$auth.user
    let tokenDeposit = ''
    Object.entries(vm.$auth.user).forEach(([key, value]) => {
      keygen = key.indexOf("$2a$12$");
      if (keygen >= 0) {
        keygen = key;
        return tokenDeposit = vm.$auth.user[key]
      }
    });
    await this.$api.deposit.create({
      money: parseInt(state.money),
      from_customer_id: vm.$auth.user.id,
      to_customer_id: vm.$auth.user.id,
      to_customer_wallet_id: state.wallet_id,
      transaction_code: 1,
      type: state.type,
      [keygen]: tokenDeposit,
    }).then(function (res) {

      if (res.success === true) {
        dispatch('setResponse', res.value)
      } else {
        if (!vm.$isEmpty(res.errors)) {
          res.errors.forEach(function (el) {
            vm.$message.error(el.message)
          })
        } else {
          vm.$message.error(res.message)
        }
      }
      dispatch('setLoading', false)
    }).catch(function (error) {
      if (typeof error.response !== "undefined" && error.response.data.errors != null) {
        error.response.data.errors.forEach(function (el) {
          vm.$message.error(el.message)
        })
      } else {
        $nuxt.$message.warning('Hệ thống đang chuyển đổi. Vui lòng quay lại sau vài phút nữa.')
      }
      dispatch('setLoading', false)
    })
  }
}
