export default {
  async getAll({ commit, state, dispatch }, payload) {
    dispatch('setPage', state.page)
    dispatch('setLoading', true)
    let pal = {
      count: state.count,
      page: state.page,
      categories: $nuxt.$route.params.slug, keyword: $nuxt.$route.query.q,
      ...payload
    };
    if(payload !== undefined){
        pal = payload
        console.log(pal)
    }
    const res = await this.$api.orders.all(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_TOTAL', value.total)
      commit('SET_LIST', value.orders)
      dispatch('setLoading', false)
    } else {
      const { errors } = res
      // Handle error here
      dispatch('setLoading', false)
    }
    return res
  },

  async getDetail({ commit, state }, payload) {
    let pal = {
      ...payload
    }
    commit('SET_LOADING', true)
    const res = await this.$api.orders.show(pal)
    const { success } = res
    if (success === true) {
      const { value } = res
      commit('SET_OBJECT', value.product)
      commit('SET_LOADING', false)
    } else {
      const { errors } = res
      commit('SET_LOADING', false)
      // Handle error here
    }
    return res
  },

  setLoading({ commit, dispatch }, payload) {
    commit('SET_LOADING', payload)
  },

  setListEmpty({ commit, dispatch }, payload) {
    commit('SET_LIST_EMPTY', payload)
  },

  setPage({ commit, state }, payload) {
    commit('SET_PAGE', payload)
  }
}
