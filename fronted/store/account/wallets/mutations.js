export default {
  SET_LIST_EMPTY(state) {
    state.list = [];
    state.page = 0;
    state.total = 0;
    console.log(state)
  },
  SET_LIST(state, value) {
    state.list = value;
  },
  SET_TOTAL(state, value) {
    state.total = value;
  },
  SET_OBJECT(state, value) {
    state.object = value;
    if (!$nuxt.$isEmpty(value.variants[0])) {
      state.variant = value.variants[0];
    }
  },
  SET_PAGE(state, payload) {
    state.page = payload+1;
  },
  SET_LOADING(state, value) {
    state.loading = value;
  }
}
