const resource = 'delivery-address'
export default ($axios) => ({

    all(payload) {
        return $axios.$get(`${resource}`, {
            params: payload,
        })
    },

    show(payload) {
        return $axios.$get(`${resource}/show`, {
            params: payload,
        })
    },

    create(payload) {
        return $axios.$post(`${resource}/create`, payload)
    },

    update(id, payload) {
        return $axios.$put(`${resource}/${id}`, payload)
    },

    delete(id) {
        return $axios.$delete(`${resource}/${id}`)
    },

    agent(payload) {
        return $axios.$get(`${resource}/agent`, {
            params: payload,
        })
    },
})
