const resource = 'bank'
export default ($axios) => ({
  all() {
    return $axios.$get(`${resource}`)
  },
  bank() {
    return $axios.$get(`${resource}/bank`)
  },
  show(id) {
    return $axios.$get(`${resource}/show/${id}`)
  },
  update(id, payload) {
    return $axios.$put(`${resource}/${id}`, payload)
  },

  create(payload) {
    return $axios.$post(`${resource}/create`, payload)
  },

})
