const resource = 'cart'
export default ($axios) => ({

  all(payload) {
    return $axios.$get(`/${resource}`, {
      params: payload,
    })
  },

  show(payload) {
    return $axios.$get(`/${resource}/show`, {
      params: payload,
    })
  },

  checkout(payload) {
    return $axios.$post(`/${resource}/checkout`, payload)
  },

  distanceMatrix(payload) {
    return $axios.$get(`../locations/distance-matrix`, {
      params: payload,
    })
  },

  paymentMethod(payload) {
    return $axios.$get(`/${resource}/payment-method`, {
      params: payload,
    })
  },

  addTo(payload) {
    return $axios.$post(`/${resource}/create`, payload)
  },

  deleteTo(payload) {
    return $axios.$post(`/${resource}/delete`, payload)
  },

  update(id, payload) {
    return $axios.$put(`/${resource}/${id}`, payload)
  },
})
