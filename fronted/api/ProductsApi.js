const resource = 'products'
export default ($axios) => ({
  all(payload) {
    return $axios.$get(`/${resource}`, {
      params: payload,
    })
  },
  show(payload) {
    return $axios.$get(`/${resource}/show/${payload.slug}`, {
      params: payload,
    })
  },
  filters(payload) {
    return $axios.$get(`/${resource}/filters`, {
      params: payload,
    })
  },
})
