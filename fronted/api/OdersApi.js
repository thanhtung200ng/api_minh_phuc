const resource = 'orders'
export default ($axios) => ({
  all(payload) {
    return $axios.$get(`/${resource}`, {
      params: payload,
    })
  },
})
