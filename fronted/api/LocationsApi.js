const resource = '../locations'

export default ($axios) => ({
  allCities() {
    return $axios.$get(`${resource}`)
  },

  allDistricts(id) {
    return $axios.$get(`${resource}?type=districts&filters[city_id][value]=${id}&filters[city_id][operand]=IsEqualTo`)
  },

  allWards(id) {
    return $axios.$get(`${resource}?type=wards&filters[district_id][value]=${id}&filters[district_id][operand]=IsEqualTo`)
  },

  allPlace(payload) {
    return $axios.$get(`${resource}/place`, {
      params: payload
    })
  },
  distanceMatrix(payload) {
    return $axios.$get(`${resource}/distance-matrix`, {
      params: payload
    })
  },


})
