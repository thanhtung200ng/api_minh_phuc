const resource = 'withdraw'
export default ($axios) => ({
  all() {
    return $axios.$get(`${resource}`)
  },
  getWallet() {
    return $axios.$get(`${resource}/wallet`)
  },
  create(payload) {
    return $axios.$post(`${resource}/create`, payload)
  },
})
