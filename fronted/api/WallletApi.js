const resource = 'wallets'
export default ($axios) => ({
  getWalletDeposit() {
    return $axios.$get(`${resource}/deposit`)
  },

  all(payload) {
    return $axios.$get(`/${resource}`, {
      params: payload,
    })
  },
  wallets_share_active(payload) {
    return $axios.$post(`/${resource}/wallets_share_active`, payload)
  },
})
