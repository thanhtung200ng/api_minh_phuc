const resource = 'customers'
export default ($axios) => ({
  register(payload) {
    return $axios.$post('register', payload)
  },
  update(id, payload) {
    return $axios.$put(`/${resource}/${id}`, payload)
  },
  changePassword(payload) {
    return $axios.$post(`/${resource}/password`, payload)
  },
});
