const resource = process.env.PREFIX_SELLER+'transactions'
export default ($axios) => ({

  all(payload) {
    return $axios.$get(`${resource}`, {
      params: payload,
    })
  },

  create(payload) {
    return $axios.$post(`${resource}/create`, payload)
  },

  approved(id) {
    return $axios.$put(`${resource}/approved/${id}`)
  },

  delete(id) {
    return $axios.$delete(`${resource}/${id}`)
  },

})
