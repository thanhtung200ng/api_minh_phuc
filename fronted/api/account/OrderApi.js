const resource = 'order'
export default ($axios) => ({
  all(payload) {
    return $axios.$get(`/${resource}`, {
      params: payload,
    })
  },
  cancel(id) {
    return $axios.$put(`/${resource}/approved`, {
      id: id,
      next:4
    })
  },
  cancelTransfer(id) {
    return $axios.$post(`/${resource}/cancel`, {
      id: id,
    })
  },
  filters(payload) {
    return $axios.$get(`/${resource}/filters`, {
      params: payload,
    })
  },
  show(id) {
    return $axios.$get(`${resource}/show/${id}`)
  },

})
