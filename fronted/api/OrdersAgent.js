const resource = 'orders_agent'
export default ($axios) => ({
  all(payload) {
    return $axios.$get(`/${resource}`, {
      params: payload,
    })
  },
})
