export default ($axios) => ({

  index(payload) {
    return $axios.$get(payload.uri, {
      params: payload,
    })
  },

  export(uri, payload) {
    return $axios.$get(uri, {
      params: payload,
    })
  },

})
