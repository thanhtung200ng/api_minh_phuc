import CategoriesApi from "~/api/CategoriesApi";
import ProductsApi from "~/api/ProductsApi";
import CartApi from "~/api/CartApi";
import LocationsApi from "~/api/LocationsApi";
import DeliveryAddress from "~/api/DeliveryAddress";
import AgentApi from "@/api/AgentApi";
import OrderApi from "@/api/account/OrderApi";
import CustomerApi from "@/api/account/CustomerApi";
import banksApi from "@/api/BanksApi";
import WidthDrawMoneyApi from "@/api/WidthDrawMoneyApi";
import DepositApi from "@/api/DepositApi";
import WallletApi from "@/api/WallletApi";
import OrdersAgent from "@/api/OrdersAgent";
import TransactionApi from "@/api/account/TransactionApi";
import FilterApi from "@/api/FilterApi";

export default ($axios) => ({
  filters: FilterApi($axios),
  categories: CategoriesApi($axios),
  products: ProductsApi($axios),
  cart: CartApi($axios),
  locations: LocationsApi($axios),
  deliveryAddress: DeliveryAddress($axios),
  agent: AgentApi($axios),
  orders: OrderApi($axios),
  customer: CustomerApi($axios),
  bank: banksApi($axios),
  widthDrawMoney : WidthDrawMoneyApi($axios),
  deposit : DepositApi($axios),
  transaction : TransactionApi($axios),
  wallets : WallletApi($axios),
  orderAgent : OrdersAgent($axios)
})


