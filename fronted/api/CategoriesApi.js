const resource = 'categories'
export default ($axios) => ({
  all(payload) {
    return $axios.$get(`/${resource}`, {
      params: payload,
    })
  },
  filters(payload) {
    return $axios.$get(`/${resource}/filters`, {
      params: payload,
    })
  },
})
