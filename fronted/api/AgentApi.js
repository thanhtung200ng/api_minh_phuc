const resource = 'agent'
export default ($axios) => ({
  all(payload) {
    return $axios.$get(`${resource}`, {
      params: payload,
    })
  },

  show(id) {
    return $axios.$get(`${resource}/show/${id}`)
  },
})
