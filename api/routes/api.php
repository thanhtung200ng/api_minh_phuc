<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('search', ['uses' => 'SearchController@index']);
Route::post('login', ['as' => 'login', 'uses' => 'Auth\Members\LoginController@login']);
Route::post('register', ['as' => 'register', 'uses' => 'Auth\Members\RegisterController@register']);
Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\Members\LoginController@logout']);
Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('me', ['uses' => 'Auth\Members\LoginController@me']);
    Route::post('media', ['uses' => 'Media\MediaController@upload']);
    Route::group(['prefix' => 'members'], function () {
        Route::get('/', ['uses' => 'Members\MemberController@index']);
        Route::get('filters', ['uses' => 'Members\MemberController@filters']);
        Route::post('create', ['uses' => 'Members\MemberController@store']);
        Route::get('show/{id}', ['uses' => 'Members\MemberController@show']);
        Route::put('{id}', ['uses' => 'Members\MemberController@update']);
        Route::delete('{id}', ['uses' => 'Members\MemberController@destroy']);
    });

    Route::group(['prefix' => 'customers'], function () {
        Route::get('/', ['uses' => 'Customers\CustomerController@index']);
        Route::post('create', ['uses' => 'Customers\CustomerController@store']);
        Route::get('show/{id}', ['uses' => 'Customers\CustomerController@show']);
        Route::put('{id}', ['uses' => 'Customers\CustomerController@update']);
        Route::get('getAllCustomerByCty', ['uses' => 'Customers\CustomerController@getAllCustomerByCty']);
        Route::post('updateRanksTotalRevenue', ['uses' => 'Customers\CustomerController@updateRankTotalRevenue']);
        Route::post('createAllWalletCustomer', ['uses' => 'Customers\CustomerController@createWalletAllCustomer']);
        Route::post('storeWalletsById/{id}', ['uses' => 'Customers\CustomerController@storeWalletsById']);
        Route::get('getListWalletCanStoreById/{id}', ['uses' => 'Customers\CustomerController@getListWalletCanStoreById']);
        Route::post('updateRankMpmart', ['uses' => 'Customers\CustomerController@updateRankMpmart']);
        Route::get('filters', ['uses' => 'Customers\CustomerController@filters']);
        Route::get('children/node/{id}', ['uses' => 'Customers\CustomerController@children']);
        Route::get('filters/export', ['uses' => 'Customers\CustomerController@excel_export']);
        Route::post('wallet_customer', ['uses' => 'Customers\CustomerController@getByIdWallet']);
    });



});
