<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => '_FrontEnd'],
    function (){
        Route::get('me', ['uses' => 'Auth\Customers\LoginController@me']);
        Route::post('login', ['uses' => 'Auth\Customers\LoginController@login']);
        Route::post('refresh', ['uses' => 'Auth\Customers\LoginController@refresh']);
        Route::post('register', ['uses' => 'Auth\Customers\RegisterController@register']);
        Route::post('logout', ['uses' => 'Auth\Customers\LoginController@logout']);
        Route::get('search/{action_name?}', ['uses' => 'Filters\SearchController@index']);

        Route::get('{service}/oauth/callback', ['uses' => 'Auth\Customers\SocialAuthFacebookController@auth']);

        Route::group(['prefix' => 'auth'], function (){
            Route::get('{service}/redirect', ['uses' => 'Auth\Customers\SocialAuthFacebookController@redirect']);
            Route::get('{service}/callback', ['uses' => 'Auth\Customers\SocialAuthFacebookController@callback']);
            Route::put('{service}/create', ['middleware' => 'auth:api_customer', 'uses' => 'Auth\Customers\SocialAuthFacebookController@store']);
        });


        Route::group(['prefix' => 'customers', 'middleware' => 'auth:api_customer'], function (){
            Route::put('{id}', ['uses' => 'Customers\CustomerController@update']);
            Route::post('password', ['uses' => 'Customers\CustomerController@changePassword']);
            Route::get('getAll', ['uses' => 'Customers\CustomerController@index']);
            Route::put('affiliate-marketing/{id}', ['uses' => 'Customers\CustomerController@updateParentReferralCode']);
            Route::put('affiliate-marketing-seller/{id}', ['uses' => 'Customers\CustomerController@updateSellerParentReferralCode']);
            Route::get('wallet-products', ['middleware' => 'auth:api_customer', 'uses' => 'Customers\CustomerController@getWalletCustomerByProducts']);
            Route::get('children/{id}', ['uses' => 'Customers\CustomerController@children']);
        });


//    lấy ra các sản phẩm mới được ra mắt
        Route::group(['prefix' => 'new_arrivals'], function (){
            Route::get('/', ['uses' => 'NewArrivalsProducts\NewArrivalsProductsController@index']);
            Route::get('show/{id}', ['uses' => 'NewArrivalsProducts\NewArrivalsProductsController@show']);
        });

//    lấy ra các danh mục cha con
        Route::group(['prefix' => 'category_parent_children'], function (){
            Route::get('/', ['uses' => 'Categories\CategoryController@index']);
        });
        //    route dành cho các sản phẩm có danh mục hot
        Route::group(['prefix' => 'hot_sale'], function (){
            Route::get('/', ['uses' => 'Home\ProductsController@index']);
            Route::get('/hot_sale', ['uses' => 'Home\ProductsController@showHotSaleProducts']);
            Route::get('/cty_new', ['uses' => 'Home\ProductsController@showProductsHotOfCty']);

        });
        // route dành cho Agency
        Route::group(['prefix' => 'agency'], function (){
            Route::get('/', ['uses' => 'Home\AgencyCotroller@index']);
            Route::get('/all', ['uses' => 'Home\AgencyCotroller@getAllAgency']);
        });

        Route::group(['prefix' => 'box_show_products'], function (){
            Route::get('/', ['uses' => 'BoxShowByCate\BoxShowByCateController@index']);
        });

        Route::group(['prefix' => 'products'], function (){
            Route::get('/', ['uses' => 'Products\ProductController@index']);
            Route::get('/cart', ['uses' => 'Products\ProductController@cart']);
            Route::get('show/{slug}-pid{id}', ['uses' => 'Products\ProductController@show'])->where(['slug' => '^(?!((.*/)|(pid))).*\D+.*$']);
            Route::get('/featured', ['uses' => 'Home\HomeController@index']);
            Route::get('/filters', ['uses' => 'Products\ProductController@filters']);
            Route::get('/create-product', ['uses' => 'Products\ProductController@productsByKeyword']);
            Route::post('evaluate', ['uses' => 'Products\ProductController@evaluateAndComment']);
            Route::get('getprice', ['uses' => 'Products\ProductController@getPriceByQuantity']);
            Route::get('getlistprice', ['uses' => 'Products\ProductController@getListPriceByQuantity']);
        });
        Route::group(['prefix' => 'products-seller'], function (){
            Route::get('/', ['uses' => 'Products\ProductSellerController@index']);
            Route::post('create', ['uses' => 'Products\ProductSellerController@store']);
            Route::get('show/{id}', ['uses' => 'Products\ProductSellerController@show']);
            Route::put('{id}', ['uses' => 'Products\ProductSellerController@update']);
            Route::delete('deleted', ['uses' => 'Products\ProductSellerController@destroy']);
        });


        Route::group(['prefix' => 'categories'], function (){
            Route::get('/', ['uses' => 'Categories\CategoryController@index']);
            Route::get('/show-cate', ['uses' => 'Categories\CategoryController@showlist']);
            Route::get('filters', ['uses' => 'Categories\CategoryController@filters']);
        });
        //api lấy ra các ví có chức năng mua hàng
        Route::group(['prefix' => 'wallet_role'], function (){
            Route::get('/', ['uses' => 'Wallets\WalletController@index']);
        });

        Route::group(['prefix' => 'banner'], function (){
            Route::get('/', ['uses' => 'Banner\BannerController@index']);
        });

        Route::group(['prefix' => 'tag'], function (){
            Route::get('/', ['uses' => 'Tag\TagsController@index']);
        });

        Route::group(['prefix' => 'withdraw'], function (){
            Route::get('/', ['uses' => 'WithdrawMoney\WithdrawMoneyFrontEndController@index']);
            Route::get('/filters', ['uses' => 'WithdrawMoney\WithdrawMoneyFrontEndController@filters']);
            Route::post('create', ['uses' => 'WithdrawMoney\WithdrawMoneyFrontEndController@store']);
            Route::post('createV1', ['uses' => 'WithdrawMoney\WithdrawMoneyFrontEndController@storeV1']);
            Route::get('wallet', ['uses' => 'WithdrawMoney\WithdrawMoneyFrontEndController@getWallet']);
            Route::delete('{id}', ['uses' => 'WithdrawMoney\WithdrawMoneyFrontEndController@approved']);
            Route::delete('{id}', ['uses' => 'WithdrawMoney\WithdrawMoneyFrontEndController@delete']);
        });

        Route::group(['prefix' => 'warehouse'], function (){
            Route::get('/', ['uses' => 'Warehouse\WarehouseController@index']);
            Route::post('updateQuantity', ['uses' => 'Warehouse\WarehouseController@updateQuantityWarehouse']);
            Route::get('show', ['uses' => 'Warehouse\WarehouseController@show']);
        });

        Route::group(['prefix' => 'sales_booth'], function (){
            Route::post('create', ['uses' => 'SalesBoothModel\SalesBoothController@store']);
            Route::put('{id}', ['uses' => 'SalesBoothModel\SalesBoothController@update']);
            Route::get('{id}', ['uses' => 'SalesBoothModel\SalesBoothController@show']);
        });
        Route::group(['prefix' => 'favorite_product'], function (){
            Route::post('create', ['uses' => 'FavoriteProduct\FavoriteProductController@store']);
            Route::put('{id}', ['uses' => 'FavoriteProduct\FavoriteProductController@update']);
        });

        Route::group(['prefix' => 'subscriber'], function (){
            Route::post('create', ['uses' => 'Subscriber\SubscriberController@CreateAndUpdateSubscriber']);
        });

        Route::group(['prefix' => 'settings'], function (){
            Route::get('show', ['uses' => 'Settings\ConfigureWebController@show']);
        });

        Route::group(['prefix' => 'news'], function (){
            Route::get('/', ['uses' => 'News\PostController@index']);
            Route::get('show/{slug}', ['uses' => 'News\PostController@show']);
        });

        Route::group(['middleware' => 'auth:api_customer'], function (){
            Route::group(['prefix' => 'business-contract'], function (){
                Route::post('create', ['uses' => 'BusinessContracts\BusinessContractController@store']);
            });
            Route::group(['prefix' => 'delivery-address', 'middleware' => 'auth:api_customer'], function (){
                Route::get('/', ['uses' => 'AddressDelivery\AddressDeliveryController@index']);
                Route::post('create', ['uses' => 'AddressDelivery\AddressDeliveryController@store']);
                Route::get('/agent', ['uses' => 'Agent\AgentController@showByDeliveryAddress']);
                Route::get('/show', ['uses' => 'AddressDelivery\AddressDeliveryController@show']);
                Route::put('{id}', ['uses' => 'AddressDelivery\AddressDeliveryController@update']);
                Route::delete('{id}', ['uses' => 'AddressDelivery\AddressDeliveryController@delete']);
            });

            Route::group(['prefix' => 'deposit'], function (){
                Route::get('/', ['uses' => 'Deposits\DepositController@index']);
                Route::get('/filters', ['uses' => 'Deposits\DepositController@filters']);
                Route::post('create', ['uses' => 'Deposits\DepositController@store']);
            });

            Route::group(['prefix' => 'order', 'middleware' => 'auth:api_customer'], function () {
                Route::get('/', ['uses' => 'PurchaseOrder\PurchaseOrdersController@index']);
                Route::post('create', ['uses' => 'PurchaseOrder\PurchaseOrdersController@store']);
                Route::get('show/{id}', ['uses' => 'PurchaseOrder\PurchaseOrdersController@show']);
                Route::post('cancel', ['uses' => 'PurchaseOrder\PurchaseOrdersController@cancelOrder']);
                Route::put('approved', ['uses' => 'PurchaseOrder\PurchaseOrdersController@approved']);
                Route::get('/filters', ['uses' => 'PurchaseOrder\PurchaseOrdersController@filters']);
            });

            Route::group(['prefix' => 'agent'], function () {
                Route::get('/', ['uses' => 'Agent\AgentController@index']);
            });

            Route::group(['prefix' => 'bank'], function () {
                Route::get('/', ['uses' => 'Banks\BanksController@showCustomerBank']);
                Route::get('show/{id}', ['uses' => 'Banks\BanksController@showCustomerBankByBankId']);
                Route::get('bank', ['uses' => 'Banks\BanksController@index']);
                Route::put('{id}', ['uses' => 'Banks\BanksController@update']);
                Route::delete('{id}', ['uses' => 'Banks\BanksController@delete']);
                Route::post('create', ['uses' => 'Banks\BanksController@store']);
            });

            Route::group(['prefix' => 'cart'], function () {
                Route::get('/', ['uses' => 'Cart\CartController@index']);
                Route::get('/payment-method', ['uses' => 'Cart\CartController@payment_method']);
                Route::post('create', ['uses' => 'Cart\CartController@store']);
                Route::post('delete', ['uses' => 'Cart\CartController@delete']);
                Route::post('checkout', ['uses' => 'Cart\CartController@checkout']);
                Route::get('show', ['uses' => 'Cart\CartController@show']);
                Route::put('{id}', ['uses' => 'Cart\CartController@update']);
                Route::delete('{id}', ['uses' => 'Cart\CartController@destroy']);
            });

            Route::group(['prefix' => 'wallets'], function (){
                Route::get('/', ['uses' => 'Wallets\WalletController@index']);
                Route::get('/all', ['uses' => 'Wallets\WalletController@all']);
                Route::get('/deposit', ['uses' => 'Wallets\WalletController@getWalletDeposit']);
                Route::post('/wallets_share_active', ['uses' => 'Wallets\WalletController@wallets_share_active']);
            });

            Route::group(['prefix' => 'invest'], function (){
                Route::get('single', ['uses' => 'Invest\InvestController@getSingleType']);
            });

            Route::group(['prefix' => 'customer_wallet'], function (){
                Route::post('transfers', ['uses' => 'CustomerWallet\CustomerWalletController@walletGiftPointsToCooperate']);
            });
        });
        Route::get('/order/tracking/{id}', ['uses' => 'PurchaseOrder\PurchaseOrdersController@tracking']);
    });
