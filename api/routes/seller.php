<?php

Route::group(['namespace' => '_Seller'],
    function () {
        Route::post('login', ['uses' => 'Auth\Customers\LoginController@login']);
        Route::post('logout', ['uses' => 'Auth\Customers\LoginController@logout']);
        Route::group(['middleware' => ['auth:api_customer']],
            function () {
                Route::get('me', ['uses' => 'Auth\Customers\LoginController@me']);
                Route::group(['prefix' => 'agents'], function () {
                    Route::post('create', ['uses' => 'Agents\AgentController@store']);
                    Route::put('{id}', ['uses' => 'Agents\AgentController@update']);
                    Route::get('show/{id}', ['uses' => 'Agents\AgentController@show']);
                });
                Route::group(['prefix' => 'salesbooth'], function () {
                    Route::post('create', ['uses' => 'SalesBooth\SalesBoothController@store']);
                    Route::put('{id}', ['uses' => 'SalesBooth\SalesBoothController@update']);
                    Route::get('show', ['uses' => 'SalesBooth\SalesBoothController@show']);
                });

                Route::group(['prefix' => 'customers'], function () {
                    Route::get('/', ['uses' => 'Customers\CustomerController@index']);
                    Route::get('show/{id}', ['uses' => 'Customers\CustomerController@show']);
                    Route::get('children/node/{id}', ['uses' => 'Customers\CustomerController@children']);
                    Route::put('{id}', ['uses' => 'Customers\CustomerController@update']);
                });

                Route::group(['prefix' => 'order'], function () {
                    Route::get('filters', ['uses' => 'PurchaseOrder\PurchaseOrdersController@filters']);
                    Route::get('/', ['uses' => 'PurchaseOrder\PurchaseOrdersController@index']);
                    Route::put('approved', ['uses' => 'PurchaseOrder\PurchaseOrdersController@approved']);
                    Route::get('show/{id}', ['uses' => 'PurchaseOrder\PurchaseOrdersController@show']);
                    Route::get('steps', ['uses' => 'PurchaseOrder\PurchaseOrdersController@steps']);
                });

                Route::group(['prefix' => 'warehouse'], function () {
                    Route::get('inventory', ['uses' => 'Warehouse\WarehouseController@inventory']);
                    Route::post('updateQuantity', ['uses' => 'Warehouse\WarehouseController@updateQuantityWarehouse']);
                });
                Route::group(['prefix' => 'wallet'], function () {
                    Route::post('transfers', ['uses' => 'Wallets\WalletsController@TransfersWallet']);

                });

                Route::group(['prefix' => 'withdraw'], function () {
                    Route::post('create', ['uses' => 'WithdrawMoney\WithdrawMoneySellerController@store']);
                    Route::get('showTransaction/{wallet_id}', ['uses' => 'WithdrawMoney\WithdrawMoneySellerController@showTrasactionByWallet']);
                    Route::get('createWithdrawError', ['uses' => 'WithdrawMoney\WithdrawMoneySellerController@createWithdrawError']);
                });

                Route::group(['prefix' => 'categories'], function (){
                    Route::get('/', ['uses' => 'Categories\CategoryController@index']);
                    Route::get('/show-cate', ['uses' => 'Categories\CategoryController@showlist']);
                    Route::get('filters', ['uses' => 'Categories\CategoryController@filters']);
                });

                Route::group(['prefix' => 'products'], function (){
                    Route::get('/', ['uses' => 'Products\ProductController@index']);
                    Route::post('create', ['uses' => 'Products\ProductController@store']);
                    Route::get('show/{id}', ['uses' => 'Products\ProductController@show']);
                    Route::put('{id}', ['uses' => 'Products\ProductController@update']);
                    Route::delete('{id}', ['uses' => 'Products\ProductController@destroy']);
                    Route::get('filters', ['uses' => 'Products\ProductController@filters']);
                });

                Route::group(['prefix' => 'inventory'], function (){
                    Route::get('filters', ['uses' => 'Inventory\InventoryController@filters']);
                    Route::get('products', ['uses' => 'Inventory\InventoryController@products']);
                    Route::post('updateOrInsert', ['uses' => 'Inventory\InventoryController@updateOrInsert']);
                    Route::post('export', ['uses' => 'Inventory\InventoryController@export']);
                });
                Route::group(['prefix' => 'transactions'], function () {
                    Route::get('/', ['uses' => 'Transactions\TransactionController@index']);
                    Route::get('/filters', ['uses' => 'Transactions\TransactionController@filters']);
                });

                Route::group(['prefix' => 'deposit'], function () {
                    Route::get('filters', ['uses' => 'Deposits\DepositController@filters']);
                    Route::get('/', ['uses' => 'Deposits\DepositController@index']);
                });

                Route::group(['prefix' => 'banks'], function () {
                    Route::get('/', ['uses' => 'Banks\BankController@index']);
                    Route::get('/customerBanks', ['uses' => 'Banks\BankController@showCustomerBank']);
                    Route::post('create', ['uses' => 'Banks\BankController@storeCustomerBank']);
                });
            });
    });
