<?php

Route::group(['namespace' => '_Jobs'],
    function (){
        Route::group(['prefix' => 'profit'], function () {
            Route::post('/', ['uses' => 'Repay\RepayController@profit']);
            Route::post('/return_money', ['uses' => 'Repay\RepayController@return_money']);
            Route::post('/exceptions', ['uses' => 'Repay\RepayExceptionController@profit']);
        });

        Route::any('/transaction/{action_name?}', ['uses' => 'ProcessTransactionController@index']);
        Route::any('/deposit/{action_name?}', ['uses' => 'Deposit\DepositExceptionController@index']);
        Route::any('/product/{action_name?}', ['uses' => 'Product\ProductExceptionController@index']);


    });
