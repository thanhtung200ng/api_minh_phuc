    <?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locations', ['as' => 'locations', 'uses' => 'App\Http\Controllers\LocationController@locations']);
Route::get('locations/place', ['as' => 'locations', 'uses' => 'App\Http\Controllers\LocationController@place']);
Route::get('locations/distance-matrix', ['as' => 'locations', 'uses' => 'App\Http\Controllers\LocationController@distanceMatrix']);
Route::post('media', ['uses' => 'Media\MediaController@upload']);
Route::get('get-file/{token}', ['as' => 'getFile', 'uses' => 'App\Http\Controllers\FileController@get']);
Route::get('banks', ['as' => 'Bank', 'uses' => 'App\Http\Controllers\_System\BankController@index']);
