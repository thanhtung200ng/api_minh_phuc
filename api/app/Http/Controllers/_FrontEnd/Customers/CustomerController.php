<?php


namespace App\Http\Controllers\_FrontEnd\Customers;


use App\Elibs\Date;
use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Customer;
use App\Http\Repositories\Entities\Member;
use App\Http\Repositories\Entities\Response;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Http\Repositories\Enums\TransactionCode;
use App\Http\Repositories\Factories\FilterFactory;
use App\Http\Repositories\Factories\OrderFactory;
use App\Http\Repositories\Resources\Customers\CustomerResources;
use App\Http\Repositories\Resources\Wallets\WalletsResources;
use App\Http\Repositories\Resources\Customers\CustomerResourcesCollection;
use App\Http\Repositories\Services\CustomerService;
use App\Http\Repositories\Services\WalletBuyProductVariantServices;
use App\Http\Repositories\Services\CustomerWalletService;
use App\Http\Repositories\ServicesEloquent\CustomerServiceEloquent;
use App\Http\Repositories\ServicesEloquent\CustomerWalletServiceEloquent;
use App\Http\Repositories\ServicesEloquent\ProductVariantsServiceEloquent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $response = app(Response::class);
        $page = $request->get('page', 1);
        $count = $request->get('count', 200);
        try {
            $offset = ($page - 1) * $count;
            $qEloquent = CustomerServiceEloquent::newQuery()->with(['city', 'district', 'ward']);
            if (checkRequestInputFieldEmptyOrNot($request, 'keyword')) {
                $keyword = $request->get('keyword');
                $qEloquent = $qEloquent->where('full_name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('email', 'LIKE', '%'.$keyword.'%')->orWhere('account', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('mobile', 'LIKE', '%'.$keyword.'%');
            }
            $qEloquent = $qEloquent->orderBy('id', 'DESC');
            $total = $qEloquent->count();
            if ($count) {
                $qEloquent->offset($offset);
                $qEloquent->limit($count);
            }
            $customers = $qEloquent->get();
            $response->value->add('total', $total);
            $response->value->add('customers', $customers->toArray());
            return $response->json(['message' => 'Lấy danh sách thông tin khách hàng thành công!', 'success' => true], HttpStatusCode::OK);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Lấy danh sách thông tin khách hàng thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     ** @param int $id
     ** @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $response = new Response();

        try {
            $validator = $this->vali($request, $id);

            $user = CustomerServiceEloquent::getById($id);

            if (!$user) {
                $response->error->add('user_null', 'Không tìm thấy khách hàng #' . $id);
            }
            if ($validator->fails()) {
                $response->error->addValidator($validator);
                return $response->json(['message' => 'Cập nhật khách hàng thất bại!', 'success' => false], HttpStatusCode::UNPROCESSABLE_ENTITY);
            }
            $user->full_name = $request->get('full_name');
            $user->city_id = $request->get('city_id');
            $user->district_id = $request->get('district_id');
            $user->ward_id = $request->get('ward_id');

            if (checkRequestInputFieldEmptyOrNot($request, 'street')) {
                $user->street = $request->get('street');
            }

            $user = CustomerServiceEloquent::update($user, $user->getOriginal());
            $response->value->add('customer', $user->toArray());
            return $response->json(['message' => 'Cập nhật khách hàng thành công!', 'success' => true], HttpStatusCode::CREATE);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Cập nhật khách hàng thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function vali(Request $request, int $id = 0)
    {
        $rules = [
            'first_name' => 'required|string|min:1|max:64',
            'last_name' => 'required|string|min:1|max:64',
            'full_name' => 'required|string|min:1|max:64',
            'mobile' => 'required|min:10|max:13|unique:ui_customers,mobile,' . $id,
            'bio' => 'nullable|min:10|max:250',
            'email' => 'email|min:3|max:64|unique:ui_customers,email,' . $id,
            'account' => 'required|string|min:3|max:64|unique:ui_customers,account,' . $id,
//            'referral_code' => 'min:6|unique:ui_customers,referral_code,'.$id,
            'city_id' => 'required|exists:__cities,id',
            'district_id' => 'required|exists:__districts,id',
            'ward_id' => 'required|exists:__wards,id',
            'street' => 'required|max:250',

        ];

        return $this->makeValidator($request, $rules, [
            'required' => ':attribute không được để trống.',
            'unique' => ':attribute đã tồn tại.',
        ], [
            'full_name' => 'Họ và tên',
            'mobile' => 'Số điện thoại',
            'email' => 'Email',
            'account' => 'Tài khoản đăng nhập',
            'city_id' => 'Thành phố',
            'district_id' => 'Quận, huyện',
            'ward_id' => 'Phường, xã',
            'street' => 'Địa chỉ',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     ** @param int $id
     ** @param Request $request
     * @return JsonResponse
     */
    public function updateParentReferralCode(Request $request, int $id): JsonResponse
    {
        $response = new Response();
        $validator = $this->makeValidator($request, [
            'parent_referral_code' => [
                'required',
                Rule::exists('ui_customers', 'referral_code')
                    ->whereNot('id', $id),
            ],
        ], [
            'required' => ':attribute không được để trống',
            'exists' => ':attribute không tồn tại',
        ], [
            'parent_referral_code' => 'Mã người giới thiệu',
        ]);
        if ($validator->fails()) {
            $response->error->addValidator($validator);
            return $response->json(['message' => 'Cập nhật mã người giới thiệu thất bại!', 'success' => false], HttpStatusCode::UNPROCESSABLE_ENTITY);
        }

        try {
            $user = CustomerServiceEloquent::getById($id);
            if ($user->getParentReferralCode()) {
                return $response->json(['message' => 'Không thể thay đổi mã người giới thiệu', 'success' => false], HttpStatusCode::OK);
            }
            $user->parent_referral_code = $request->get('parent_referral_code');
            $user = CustomerServiceEloquent::update($user, $user->getOriginal());
            $response->value->add('customer',$user->getOriginal());
            return $response->json(['message' => 'Cập nhật mã người giới thiệu thành công!', 'success' => true], HttpStatusCode::CREATE);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Cập nhật mã người giới thiệu thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    public function updateSellerParentReferralCode(Request $request, int $id): JsonResponse
    {
        $response = new Response();

        $validator = $this->makeValidator($request, [
            'seller_parent_referral_code' => [
                'required',
                Rule::exists('ui_customers', 'seller_referral_code')
                    ->whereNot('id', $id),
            ],
        ], [
            'required' => ':attribute không được để trống',
            'exists' => ':attribute không tồn tại',
        ], [
            'seller_parent_referral_code' => 'Mã người giới thiệu',
        ]);
        if ($validator->fails()) {
            $response->error->addValidator($validator);
            return $response->json(['message' => 'Cập nhật mã người giới thiệu thất bại!', 'success' => false], HttpStatusCode::UNPROCESSABLE_ENTITY);
        }

        try {
            $user = CustomerServiceEloquent::getById($id);
            if ($user->seller_parent_referral_code) {
                return $response->json(['message' => 'Không thể thay đổi mã người giới thiệu', 'success' => false], HttpStatusCode::OK);
            }
            $user->seller_parent_referral_code = $request->get('seller_parent_referral_code');
            if (!$user->seller_referral_code) {
                $user->seller_referral_code = \App\Models\Customer::genSellerRef(auth('api_customer')->user());
            }
            $user = CustomerServiceEloquent::update($user, $user->getOriginal());

            $response->value->add('customer', $user->toArray());
            return $response->json(['message' => 'Cập nhật mã người giới thiệu thành công!', 'success' => true], HttpStatusCode::CREATE);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Cập nhật mã người giới thiệu thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    public function valiWallet(Request $request)
    {
        $rules = [
            'customer_id' => 'required|exists:ui_customers,id',
        ];
        return $this->makeValidator($request, $rules, [
            'required' => ':attribute không được để trống',
            'exists' => ':attribute không tồn tại'
        ], [
            'customer_id' => 'ID Khách hàng',
        ]);
    }

    function getWalletCustomerByProducts(Request $request)
    {
        $response = new Response();
        try {
            $productIds = $request->get('products');
            if (!$productIds) {
                $response->error->add('exists', 'Không tìm thấy sản phẩm phù hợp');
                return $response->json(['message' => 'Không tìm thấy sản phẩm phù hợp', 'success' => false], HttpStatusCode::OK);
            }
            $customer = auth('api_customer')->user();
            $products = ProductVariantsServiceEloquent::newQuery()->with('allowPayWallet')->select('id', 'name', 'status', 'product_id', 'sales_booth_id', 'is_choose_agent')->whereIn('id', $productIds)->get();
            $allowPayWallet = [];
            foreach ($products as $product) {
                $allowPayWallet = array_merge($product->allowPayWallet->pluck('wallet_id')->toArray(), $allowPayWallet);
            }
            $customerWallets = CustomerWalletServiceEloquent::getListByGroupWalletId($customer->id, $allowPayWallet);

            $response->value->add('wallets', $customerWallets);
            return $response->json(['message' => 'Lấy danh sách ví thành công!', 'success' => true], HttpStatusCode::OK);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Lấy danh sách ví thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    public function valiApproved($listProducts, $customer_id, &$customer, &$walletBuy, &$walletBuyProducts, &$response)
    {

        if (empty($listProducts)) {
            $response->error->add('products', 'Bạn cần chọn sản phẩm');
        } else {
            $customer = CustomerServiceEloquent::getById($customer_id);
            if (!$customer) {
                $response->error->add('customer', 'không tìm thấy thông tin khách hàng');
            } else {
                if (!$customer->isActive()) {
                    $response->error->add('status', 'Khách hàng này chưa được kích hoạt hặc đã tạm khóa!');
                } else {
                    $walletBuy = $customer->getAllWalletBuyProducts();
                    if (!$walletBuy) {
                        $response->error->add('wallet', 'Ví của bạn không có quyền mua sản phẩm!');
                    } else {
                        if ($customer_id) {

                        }
                        $walletIds =
                        $walletBuyProducts = WalletBuyProductVariantServices::getListWalletBuyProduct($listProducts, $customer_id, $walletIds);
                        if ($walletBuyProducts->isEmpty()) {
                            $response->error->add('producWallet', 'Không tìm thấy ví thanh toán phù hợp cho sản phẩm #' . implode(",", $listProducts) . '');
                        }
                    }
                }
            }
        }
    }

    public function changePassword(Request $request) {
        $response = app(Response::class);
        $user = auth('api_customer')->user();
        $pass = $request->get('password');
        $newPass = $request->get('newPass');
        $confirmNewPass = $request->get('confirmPass');
        try {
            if (empty($pass)){
                return $response->json(['message' => 'Vui lòng nhập nhật khẩu cũ', 'success' => false], HttpStatusCode::OK);
            }elseif (empty($newPass)){
                return $response->json(['message' => 'Vui lòng nhập nhật khẩu mới', 'success' => false], HttpStatusCode::OK);
            }elseif (empty($confirmNewPass)) {
                return $response->json(['message' => 'Vui lòng nhập nhật lại khẩu mới', 'success' => false], HttpStatusCode::OK);
            }elseif ($newPass !== $confirmNewPass){
                return $response->json(['message' => 'Nhập lại mật khẩu không khớp', 'success' => false], HttpStatusCode::OK);
            }else{
                $password = $user->password;
                if(!Hash::check($pass, $password)){
                    return $response->json(['message' => 'Mật khẩu cũ chưa chính xác', 'success' => false], HttpStatusCode::OK);
                }
                $user->password = Member::passwordHash($newPass);
                CustomerServiceEloquent::getInstance()->update($user, $user->getOriginal());
                return $response->json(['message' => 'Đổi mật khẩu thành công!', 'success' => true], HttpStatusCode::OK);
            }
        }catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Thất bại !!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    /**
     * Lấy danh sách cấp dưới
     * @return JsonResponse
     */
    public function children(int $id): JsonResponse
    {
        $response = new Response();
        $customer = CustomerServiceEloquent::getById($id);
        if ($customer) {
            $customers = CustomerServiceEloquent::getChildren($customer->referral_code);
        }
        $response->value->add('customers', @$customers ? $customers->toArray() : []);
        return $response->json(['message' => 'Lấy thông tin khách hàng thành công!', 'success' => true], HttpStatusCode::OK);
    }


}
