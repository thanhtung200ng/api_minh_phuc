<?php

namespace App\Http\Controllers\_FrontEnd\Auth\Customers;

use App\Http\Repositories\Entities\Response;
use App\Http\Repositories\Entities\Wallets;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Http\Repositories\Factories\FilterFactory;
use App\Http\Repositories\Factories\OrderFactory;
use App\Http\Repositories\Resources\SalesBooth\SaleBoothResources;
use App\Http\Repositories\Services\CustomerWalletService;
use App\Http\Repositories\Services\SalesBoothService;
use App\Http\Repositories\Services\WalletsService;
use App\Http\Repositories\ServicesEloquent\WalletServiceEloquent;
use App\Models\Customer;
use App\Models\DepositModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Elibs\Helper;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api_customer', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $response = new Response();
        $validator = $this->makeValidator($request, [
            'account' => 'required|string',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $response->error->addValidator($validator);
            return $response->json(['message' => 'Đăng nhập thất bại!', 'success' => false], HttpStatusCode::UNPROCESSABLE_ENTITY);
        }

        $credentials = $request->only(['account', 'password']);
        if (! $token = auth('api_customer')->attempt($credentials)) {
            return $response->json(['message' => 'Đăng nhập thất bại!', 'success' => false], HttpStatusCode::UNAUTHORIZED);
        }

        /*if(!auth('api_customer')->user()->is_seller && !auth('api_customer')->user()->is_mpmart) {
            return $response->json(['message' => 'Bạn chưa đăng ký làm nhà bán hàng với chúng tôi', 'success' => false], HttpStatusCode::UNAUTHORIZED);
        }*/
        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(): \Illuminate\Http\JsonResponse
    {

        $user = $this->user();
        return response()->json(['success' => true, 'user' => $user]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth('api_customer')->logout();

        return response()->json(['message' => 'Đăng xuất thành công', 'success' => true]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api_customer')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api_customer')->factory()->getTTL() * 60,
            'user' => $this->user(),
            'success' => true
        ]);
    }

    function user(): array
    {
        $user = Customer::with('city', 'district', 'ward', 'wallets.wallet', 'social', 'sales_booth')->whereHas('wallets.wallet', function ($q) {
            $q->whereStatus(WalletServiceEloquent::getStatusActive());
        })->find(auth('api_customer')->id())->toArray();
        $tokenGen = Customer::genToken($user); // b7a7ab6cd14c0c343c0b96a50a74d9d7
        $user['mp_token'] = $tokenGen;
        $tokenGenDeposit = DepositModel::genToken($user);
        $keyGenTokenDeposit = DepositModel::genKeyToken($user);
        $user[$keyGenTokenDeposit] = $tokenGenDeposit;
        return $user;
    }

}
