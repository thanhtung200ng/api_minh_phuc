<?php

namespace App\Http\Controllers\_FrontEnd\Auth\Customers;

use App\Elibs\Zalo;
use App\Exceptions\Handler;
use App\Http\Repositories\Entities\Response;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Models\Customer;
use App\Models\CustomerSocialModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Two\InvalidStateException;
use Tymon\JWTAuth\JWTAuth;

class SocialAuthFacebookController extends Controller
{
    protected $auth;

    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
        $this->middleware('social');
    }

    public function redirect(Request $request, $service)
    {
        dd('Next');
        return Socialite::driver($service)->stateless()->redirect();
    }

    public function callback($service)
    {
        dd('Next');
        try {
            $serviceUser = Socialite::driver($service)->stateless()->user();
        } catch (\Exception $e) {
            return redirect(env('CLIENT_BASE_URL') . 'account/profile?error=Unable to login using ' . $service . '. Please try again' . '&origin=login');
        }

        $email = $serviceUser->getEmail();

        return redirect(env('CLIENT_BASE_URL') . 'account/profile?social_conn=true&service='.$service.'&token=' . CustomerSocialModel::genToken(['social_id' => $serviceUser->getId()]));
    }

    public function needsToCreateSocial(Customer $user, $service): bool
    {
        return !$user->hasSocialLinked($service);
    }

    public function getExistingUser($serviceUser, $email, $service)
    {
        /*if ((env('RETRIEVE_UNVERIFIED_SOCIAL_EMAIL') == 0) && ($service != 'google')) {
            $CustomerSocialModel = CustomerSocialModel::where('social_id', $serviceUser->getId())->first();
            return $CustomerSocialModel ? $CustomerSocialModel->user : null;
        }*/
        return Customer::where('email', $email)->first();
    }

    public function auth(Request $request, $service)
    {
        $response = app(Response::class);
        if(!checkRequestInputFieldEmptyOrNot($request, 'state') && !checkRequestInputFieldEmptyOrNot($request, 'code')) {
            $response->error->add('invalid', 'Kết nối thất bại!');
            return $response->json(['message' => 'Kết nối siêu thất bại!', 'success' => false], HttpStatusCode::OK);
        }
        $codes = Zalo::generate_pkce_codes();
        // Store the request state to be checked in auth.php
        // Store the code verifier to be used in auth.php
        $data =  http_build_query(array(
            "app_id" => env('ZALO_CLIENT_ID'),
            "secret_key" => env('ZALO_CLIENT_SECRET'),
            "code" => $request->code,
            "code_verifier" => $codes["verifier"],
            "grant_type" => "authorization_code"
        ));

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('ZALO_ACCESS_TOKEN_URL'),
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "secret_key: " . env('ZALO_CLIENT_SECRET')
            ),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_FAILONERROR => true,
        ) );
        $response = curl_exec($curl);
        curl_close($curl);
        $auth = json_decode( $response, true );
        dd($auth);
        // store the Access Token in a temporary PHP session variable
        $_SESSION["zalo_access_token"] = array(
            "access_token" => $auth["access_token"],
            "expires_in"   => $auth["expires_in"]
        );
        // store the Refresh Token in a secured HTTP only cookie
        $expr = time() + (3*30*24*60*60); // 3 months?
        setcookie( "zalo_refresh_token", $auth["refresh_token"],
            $expr, "/refresh",
            $_SERVER['HTTP_HOST'], true, true );
        // clean up the one-time-use state variables
        unset( $_SESSION["zalo_auth_state"] );
        unset( $_SESSION["zalo_code_verifier"] );
        //$auth = Zalo::graphApi(env('ZALO_ACCESS_TOKEN_URL'), 'POST', $data);
        dd($auth);
    }

    public function store(Request $request, $service) {
        $response = app(Response::class);
        try {
            $customer = auth('api_customer')->user();
            if (!checkRequestInputFieldEmptyOrNot($request, 'token')) {
                $response->error->add('invalid', 'Kết nối thất bại!');
                return $response->json(['message' => 'Kết nối siêu thất bại!', 'success' => false], HttpStatusCode::OK);
            }
            $token = Crypt::decrypt($request->token);
            $decrypt = explode('__', $token);
            if (!CustomerSocialModel::validToken($token, ['email' => $customer['email'], 'social_id' => $decrypt[2]])) {
                $response->error->add('_invalid', 'Kết nối thất bại!');
                return $response->json(['message' => 'Kết nối thất bại!', 'success' => false], HttpStatusCode::OK);
            }

            if ($this->needsToCreateSocial($customer, $service)) {
                CustomerSocialModel::create([
                    'service' => $service,
                    'social_id' => $decrypt[2],
                    'customer_id' => $customer->id,
                    'created_at' => time()
                ]);
                return $response->json(['message' => 'Kết nối thành công!', 'success' => true], HttpStatusCode::OK);
            }
            return $response->json(['message' => 'Đã kết nối thành công!', 'success' => true], HttpStatusCode::OK);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Kết nối thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }
}
