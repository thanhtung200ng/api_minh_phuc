<?php


namespace App\Http\Controllers\_FrontEnd\Auth\Customers;


use App\Elibs\TeleNotice;
use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Http\Repositories\Entities\Member;
use App\Http\Repositories\Entities\Response;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Http\Repositories\Resources\Customers\CustomerResources;
use App\Http\Repositories\Services\CustomerService;
use App\Http\Repositories\ServicesEloquent\CustomerServiceEloquent;
use App\Http\Repositories\ServicesEloquent\CustomerWalletServiceEloquent;
use App\Http\Repositories\ServicesEloquent\WalletServiceEloquent;
use App\Models\Customer;
use App\Models\CustomerWalletModel;
use App\Models\WalletModel;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {

        $response = new Response();
        $validator = $this->makeValidator($request, [
            'first_name' => 'required|string|min:1|max:64',
            'last_name' => 'required|string|min:1|max:64',
            'mobile' => 'required|min:3|max:64|unique:ui_customers',
            'email' => 'nullable|email|min:3|max:64|unique:ui_customers',
            'account' => 'required|regex:/^[a-z0-9]+$/u|min:3|max:64|unique:ui_customers',
            'password' => 'required|min:4',
            'parent_referral_code' => 'nullable|exists:ui_customers,referral_code',
            'seller_parent_referral_code' => 'nullable|exists:ui_customers,seller_referral_code',
        ], [
            'required' => ':attribute không được bỏ trống',
            'unique' => ':attribute đã tồn tại',
            'exists' => ':attribute không tồn tại',
            'regex' => ':attribute không đúng định dạng a-z0-9',
            'email' => ':attribute không đúng định dạng',
        ], [
            'first_name' => 'Họ tên',
            'last_name' => 'Họ tên',
            'mobile' => 'Số điện thoại',
            'email' => 'Email',
            'account' => 'Tài khoản đăng nhập',
            'password' => 'Mật khẩu đăng nhập',
            'parent_referral_code' => 'Mã người giới thiệu',
            'seller_parent_referral_code' => 'Mã người giới thiệu gian hàng',
        ]);

        if ($validator->fails()) {
            $response->error->addValidator($validator);
            return $response->json(['message' => 'User Registration Failed!', 'success' => false], HttpStatusCode::UNPROCESSABLE_ENTITY);
        }

        try {

            DB::beginTransaction();
            $objToSave = [
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'full_name' => $request->get('fullName'),
                'account' => Str::lower(trim($request->get('account'))),
                'password' => Member::passwordHash($request->get('password')),
                'mobile' => $request->get('mobile'),
                'email' => $request->get('email'),
                'status' => Customer::getStatusActive(),
            ];
            $objToSave['is_mpmart'] = 1;
            $objToSave['is_buyer'] = 1;
            $objToSave['parent_referral_code'] = trim($request->get('parent_referral_code'));
            if (!$objToSave['parent_referral_code']) {
                $objToSave['parent_referral_code'] = 'leviettrung';
            }
            $objToSave['referral_code'] = $objToSave['account'];
            /*$tokenGen = Customer::genToken($objToSave); // b7a7ab6cd14c0c343c0b96a50a74d9d7

            if ($tokenGen != $request->get('mp_token')) {
                DB::rollBack();
                $response->error->add('failed', 'Link giới thiệu không không hợp lệ.');
                return $response->json(['message' => 'User Registration Failed!', 'success' => false], HttpStatusCode::CREATE);
            }*/


            if (checkRequestInputFieldEmptyOrNot($request, 'seller_parent_referral_code')) {
                $objToSave['seller_parent_referral_code'] = $request->get('seller_parent_referral_code');
                $objToSave['seller_referral_code'] = Customer::genSellerRef($objToSave);
            }

            $id = CustomerServiceEloquent::create($objToSave);
            $wallets = [];
            $wallet = [
                'wallet_id' => app(WalletModel::class)->getViTieuDung(),
                'customer_id' => $id,
                'current_balance' => 0,
                'created_at' => time(),
                'created_by' => $id,
                'status' => CustomerWalletModel::getStatusActive(),
            ];
            $wallets[] = $wallet;
            if (isset($objToSave['referral_code']) && $objToSave['referral_code']) {
                CustomerWalletServiceEloquent::getWalletToSaveForCustomerMPMart($wallets, $id, $id);
            }
            foreach ($wallets as $wallet) {
                if ($wallet['wallet_id'] === WalletModel::getInstance()->getViTangDiem()) {
                    $wallet['current_balance'] = 5000000;
                }
                CustomerWalletServiceEloquent::create($wallet);
            }
            $objToSave['id'] = $id;
            unset($objToSave['password']);
            $response->value->add('customer', $objToSave);
            TeleNotice::customer('Có tài khoản đăng ký mới', $objToSave);
            DB::commit();
            return $response->json(['message' => 'Đăng ký thành công!', 'success' => true], HttpStatusCode::CREATE);
        } catch (\Exception $e) {
            DB::rollBack();
            app(Handler::class)->report($e);
            return $response->json(['message' => 'User Registration Failed!', 'success' => false], HttpStatusCode::CONFLICT);
        }

    }
}
