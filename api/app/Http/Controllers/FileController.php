<?php


namespace App\Http\Controllers;

use App\Http\Repositories\Enums\HttpStatusCode;
use App\Models\CustomerSocialModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class FileController extends Controller
{

    function get(Request $request, $token) {
        $file = Crypt::decryptString($token);
        $pathFile = public_path($file);
        if (file_exists($pathFile)) {
            return redirect(asset($file));
        }
        die('Không tồn tại');
    }
}
