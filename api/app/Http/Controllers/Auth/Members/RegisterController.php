<?php


namespace App\Http\Controllers\Auth\Members;


use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Http\Repositories\Entities\Error;
use App\Http\Repositories\Entities\Member;
use App\Http\Repositories\Entities\Response;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Http\Repositories\Resources\Customers\CustomerResources;
use App\Http\Repositories\Services\MemberService;
use \Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $response = new Response();
        $validator = $this->makeValidator($request, [
            'first_name' => 'required|string|min:1|max:64',
            'last_name' => 'required|string|min:1|max:64',
            'mobile' => 'required|min:3|max:64|unique:ui_members',
            'email' => 'email|min:3|max:64|unique:ui_members',
            'account' => 'required|string|min:3|max:64|unique:ui_members',
            'password' => 'required|min:4',
        ]);

        if ($validator->fails()) {
            $response->error->addValidator($validator);
            return $response->json(['message' => 'User Registration Failed!', 'success' => false], HttpStatusCode::UNPROCESSABLE_ENTITY);
        }

        try {

            $user = new Member();
            $user->setFirstName($request->get('first_name'));
            $user->setLastName($request->get('last_name'));
            $user->setAccount($request->get('account'));
            $user->setMobile($request->get('mobile'));
            if($request->get('email') !== null) {
                $user->setEmail($request->get('email'));
            }
            $user->setPassword(Member::passwordHash($request->get('password')));
            $user->setStatus(0);

            $user = MemberService::create($user);

            $response->value->add('members', CustomerResources::toArray($user));
            return $response->json(['message' => 'User Registration Successful!', 'success' => true], HttpStatusCode::CREATE);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'User Registration Failed!', 'success' => false], HttpStatusCode::CONFLICT);
        }

    }
}
