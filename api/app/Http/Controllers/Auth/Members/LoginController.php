<?php

namespace App\Http\Controllers\Auth\Members;

use App\Http\Repositories\Entities\Response;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Models\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth:admin', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $response = new Response();
        $validator = $this->makeValidator($request, [
            'account' => 'required|string|min:3|max:64',
            'password' => 'required|min:4',
        ]);

        if ($validator->fails()) {
            $response->error->addValidator($validator);
            return $response->json(['message' => 'User Login Failed!', 'success' => false], HttpStatusCode::UNPROCESSABLE_ENTITY);
        }
        $credentials = $request->only(['account', 'password']);
        if (! $token = Auth::guard('admin')->attempt($credentials)) {
            return $response->json(['message' => 'Unauthorized!', 'success' => false], HttpStatusCode::UNAUTHORIZED);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(): \Illuminate\Http\JsonResponse
    {
        return response()->json(['success' => true, 'user' => auth()->user()]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out', 'success' => true]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => Auth::guard('admin')->user(),
            'expires_in' => auth()->factory()->getTTL() * 60,
            'success' => true
        ]);
    }
}
