<?php


namespace App\Http\Controllers\Customers;


use App\Elibs\Excel;
use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Wallets\WalletsController;
use App\Http\Repositories\Entities\Member;
use App\Http\Repositories\Entities\Response;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Http\Repositories\Services\CustomerService;
use App\Http\Repositories\Services\CustomerWalletService;
use App\Http\Repositories\Services\WalletsService;
use App\Http\Repositories\ServicesEloquent\CustomerServiceEloquent;
use App\Http\Repositories\ServicesEloquent\CustomerWalletServiceEloquent;
use App\Http\Repositories\ServicesEloquent\MoneyRankMpMartServiceEloquent;
use App\Models\Customer;
use App\Models\CustomerBank;
use App\Models\CustomerWalletModel;
use App\Models\MoneyRankMpMartModel;
use App\Models\MoneyRankMpMartTotalRevenueModel;
use App\Models\RankMpMartModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $response = app(Response::class);
        $page = $request->get('page', 1);
        $count = $request->get('count', 100);
        try {
            $offset = ($page - 1) * $count;
            $qEloquent = CustomerServiceEloquent::newQuery()->with(['wallets.wallet', 'city', 'district', 'ward', 'moneyRankMpMartTotalRevenue.rank']);
            $qEloquent = $qEloquent->orderBy('id', 'DESC');
            $total = $qEloquent->count();
            $this->_query_filter($request,$qEloquent);
            if ($count) {
                $qEloquent->offset($offset);
                $qEloquent->limit($count);
            }
            $customers = $qEloquent->get();
            $response->value->add('total', (int)$total);
            $response->value->add('pageSize', (int)$count);
            $response->value->add('customers', $customers->toArray());
            return $response->json(['message' => 'Lấy danh sách thông tin khách hàng thành công!', 'success' => true], HttpStatusCode::OK);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Lấy danh sách thông tin khách hàng thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $response = new Response();
        $validator = $this->vali($request);
        if ($validator->fails()) {
            $response->error->addValidator($validator);
            return $response->json(['message' => 'Tạo mới khách hàng thất bại!', 'success' => false], HttpStatusCode::OK);
        }

        try {
            $user = new Customer();
            if (!$user->parent_referral_code) {
                $parent_referral_code = trim($request->get('parent_referral_code'));
                if ($parent_referral_code) {
                    $user->parent_referral_code = $parent_referral_code;
                }else {
                    $response->error->add('null','Mã giới thiệu không được bỏ trống');
                    return $response->json(['message' => 'Tạo mới khách hàng thất bại!', 'success' => false], HttpStatusCode::OK);
                }
            }
            $user->account = trim($request->get('account'));
            $user->referral_code = trim($request->get('account'));

            if ($request->has('first_name')) {
                $user->first_name = $request->get('first_name');
            }
            if ($request->has('last_name')) {
                $user->last_name = $request->get('last_name');
            }
            $user->full_name = $user->first_name .' '.$user->last_name;
            if ($request->has('bio')) {
                $user->bio = $request->get('bio');
            }
            if ($request->has('birthday')) {
                $user->birthday = $request->get('birthday');
            }
            if ($request->has('status')) {
                $user->status = $request->get('status');
            }
            if ($request->has('is_buyer')) {
                $user->is_buyer = $request->get('is_buyer');
            }
            if ($request->has('is_seller')) {
                $user->is_seller = $request->get('is_seller');
            }


            if ($request->has('is_mpmart')) {
                $user->is_mpmart = $request->get('is_mpmart');
            }
            if ($request->has('mpmart_rank')) {
                $rank_id = $request->get('mpmart_rank');
                $rank = MoneyRankMpMartModel::with('rank')->where('rank_id', $rank_id)->first();
                if ($rank) {
                    $user->mpmart_rank = $rank->toArray();
                }else {
                    $user->mpmart_rank = null;
                }
            }
            if ($request->has('mpmart_rank_total_revenue')) {
                $rank_id = $request->get('mpmart_rank_total_revenue');
                $rank = MoneyRankMpMartTotalRevenueModel::where('rank_total_revenue_id', $rank_id)->first();
                if ($rank) {
                    $user->mpmart_rank_total_revenue = $rank->id;
                }else {
                    $user->mpmart_rank = null;
                }
            }
            if ($request->has('city_id')) {
                $user->city_id = $request->get('city_id');
            }
            if ($request->has('district_id')) {
                $user->district_id = $request->get('district_id');
            }
            if ($request->has('ward_id')) {
                $user->ward_id = $request->get('ward_id');
            }
            if ($request->has('password') && $request->get('password')) {
                $user->password = Member::passwordHash($request->get('password'));
            }
            if ($request->has('street')) {
                $user->street = $request->get('street');
            }
            $user = CustomerServiceEloquent::update($user, $user->getOriginal());
            $response->value->add('customer', $user->toArray());
            return $response->json(['message' => 'Tạo mới khách hàng thành công!', 'success' => true], HttpStatusCode::OK);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Tạo mới khách hàng thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $response = new Response();
        $customer = CustomerServiceEloquent::getByIdAndWith($id);

        $response->value->add('customer', $customer ? $customer->toArray() : []);
        return $response->json(['message' => 'Lấy thông tin khách hàng thành công!', 'success' => true], HttpStatusCode::OK);
    }


    public function updateRankMpmart(Request $request) {  // update chức danh (đại lý , ctv , mpmart, tong kho, ..)
        $response = new Response();
        $customerId = $request->get('customer_id');
        $RankId = $request->get('rank_id');
        $rules = [
            'customer_id' => 'required|numeric|exists:ui_customers,id',
            'rank_id' => 'required|numeric|exists:__ranks_mpmart,id'
        ];
        $validator = $this->makeValidator($request, $rules, [
            'required' => ':attribute không được để trống',
            'numeric' => ':attribute không đúng định dạng',
            'exists' => ':attribute không tồn tại'
        ], [
            'customer_id' => 'Khách hàng',
            'rank_id' => 'Chức danh',
        ]);
        if ($validator->fails()) {
            $response->error->addValidator($validator);
            return $response->json(['message' => 'Cập nhật thông tin chức danh thất bại!', 'success' => false], HttpStatusCode::UNPROCESSABLE_ENTITY);
        }
        try {
            $ranks = MoneyRankMpMartServiceEloquent::getByIdRanks($RankId);
            if(empty($ranks)){
                return $response->json(['message' => 'Chức danh không tồn tại!', 'success' => true], HttpStatusCode::OK);
            }
            $updateRanksCustomer = CustomerServiceEloquent::updateRankMpmat($ranks,$customerId);
            $response->value->add('RankCustomer', $updateRanksCustomer->toArray());
            return $response->json(['message' => 'Cập nhập chức danh thành công!', 'success' => true], HttpStatusCode::OK);
        }catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Cập nhập chức danh thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     ** @param int $id
     ** @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request, int $id)
    {
//        dd($request->all());
        $response = new Response();
        $validator = $this->vali($request, $id);
        /**
         * @var Customer $user
         * */
        $user = CustomerServiceEloquent::getById($id);
        if (!$user) {
            $response->error->add('user_null', 'Không tìm thấy khách hàng #'.$id);
        } else {
            if ($user->parent_referral_code) {
                $parent_referral_code = trim($request->get('parent_referral_code'));
                if ($user->parent_referral_code != $parent_referral_code) {
                    $response->error->add('user_null', 'Tài khoản này đã có mã giới thiệu cấp trên. Không thể thay đổi mã cấp trên');
                }
            }
        }

        if ($validator->fails() || $response->error->count()) {
            $response->error->addValidator($validator);
            return $response->json(['message' => 'Tạo mới khách hàng thất bại!', 'success' => false], HttpStatusCode::UNPROCESSABLE_ENTITY);
        }
        try {
            if ($request->has('first_name')) {
                $user->first_name = $request->get('first_name');
            }
            if ($request->has('last_name')) {
                $user->last_name = $request->get('last_name');
            }
            $user->full_name = $user->first_name . ' '.$user->last_name;

            if ($request->has('bio')) {
                $user->bio = $request->get('bio');
            }
            if ($request->has('birthday')) {
                $user->birthday = $request->get('birthday');
            }
            if ($request->has('status')) {
                $user->status = $request->get('status');
            }
            if ($request->has('is_buyer')) {
                $user->is_buyer = $request->get('is_buyer');
            }
            if ($request->has('is_seller')) {
                $user->is_seller = $request->get('is_seller');
            }
            if (!$user->parent_referral_code) {
                if ($request->has('parent_referral_code')) {
                    $user->parent_referral_code = trim($request->get('parent_referral_code'));
                }
            }

            if ($request->has('is_mpmart')) {
                $user->is_mpmart = $request->get('is_mpmart');
            }
            if ($request->has('mpmart_rank')) {
                $rank_id = $request->get('mpmart_rank');
                $rank = MoneyRankMpMartModel::with('rank')->where('rank_id', $rank_id)->first();
                if ($rank) {
                    $user->mpmart_rank = $rank->toArray();
                }else {
                    $user->mpmart_rank = null;
                }
            }
            if ($request->has('mpmart_rank_total_revenue')) {
                $rank_id = $request->get('mpmart_rank_total_revenue');
                $rank = MoneyRankMpMartTotalRevenueModel::where('rank_total_revenue_id', $rank_id)->first();
                if ($rank) {
                    $user->mpmart_rank_total_revenue = $rank->id;
                }else {
                    $user->mpmart_rank = null;
                }
            }
            if ($request->has('city_id')) {
                $user->city_id = $request->get('city_id');
            }
            if ($request->has('district_id')) {
                $user->district_id = $request->get('district_id');
            }
            if ($request->has('ward_id')) {
                $user->ward_id = $request->get('ward_id');
            }
            if ($request->has('password') && $request->get('password')) {
                $user->password = Member::passwordHash($request->get('password'));
            }
            if ($request->has('street')) {
                $user->street = $request->get('street');
            }
            $user = CustomerServiceEloquent::update($user, $user->getOriginal());
            $response->value->add('customer', $user->toArray());
            return $response->json(['message' => 'Cập nhật khách hàng thành công!', 'success' => true], HttpStatusCode::OK);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Tạo mới khách hàng thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function getAllCustomerByCty(){
        $response = new Response();
        $listCustomer = CustomerServiceEloquent::getAllCustomerByCty();
        $response->value->add('listCustomer', $listCustomer);
        return $response->json(['message' => 'Lấy thông tin khách hàng thành công!', 'success' => true], HttpStatusCode::OK);
    }


    public function updateRankTotalRevenue(Request $request){  //  c1 - c6
        $response = new Response();
        $id = $request->get('id');
        $rankId =  $request->get('rankId');
        $rules =  [
            'id' => 'required|exists:ui_customers,id',
            'rankId' => 'required|exists:__ranks_mpmart_total_revenue,id',
        ];
        $validator = $this->makeValidator($request, $rules, [
            'required' => ':attribute không được để trống',
            'exists' => ':attribute không tồn tại',
        ], [
            'rankId' => 'Ranks',
            'id' => 'Khách hàng '
        ]);
        if ($validator->fails()) {
            $response->error->addValidator($validator);
            return $response->json(['message' => 'updated cấp bậc thất bại!', 'success' => false], HttpStatusCode::UNPROCESSABLE_ENTITY);
        }
        try {
            $customer = CustomerService::updateRankTotalRevenue($id, $rankId);
            $response->value->add('updateRank', $customer);
            return $response->json(['message' => 'Cập nhập cấp bậc thành công!', 'success' => true], HttpStatusCode::OK);
        }catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Tạo mới khách hàng thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    public function createWalletAllCustomer(){
        $response = new Response();
        try {
            $listCustomerId = CustomerService::getAllCustomerId();  // lấy ra all customer
            if(empty($listCustomerId)){
                return $response->json(['message' => 'Danh sách khách hàng không tồn tại!', 'success' => false], HttpStatusCode::OK);
            }
            $listWalletCustomer = CustomerWalletService::getAllWalletByCustomer($listCustomerId);
            $countWallet = WalletsService::countWallet();
            if($listWalletCustomer->isEmpty()){  // đây là ko có cái del gì lên tạo all ví cho all customer
                foreach ($listCustomerId as $value){
                    WalletsController::create_customer_wallet($value);
                }

            }else{
                foreach ($listWalletCustomer as $key => $value){  // những thằng nào thiếu ví thì tạo all cí cho nó chs
                    if($value->count() < $countWallet){
                        WalletsController::create_customer_wallet($key);
                    }
                }
            }
        }catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Tạo mới khách hàng thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    public function storeWalletsById(int $id,Request $request){
        $response = new Response();
        DB::beginTransaction();
        try {
            $lsWallets = $request->get('wallet_id');
            if (empty($lsWallets)){
                return $response->json(['message' => 'Vui lòng chọn ví cần tạo ', 'success' => false], HttpStatusCode::OK);
            }
            $check_customer = CustomerService::newQuery()->find($id);
            if (empty($check_customer)){
                return $response->json(['message' => 'Không tìm thấy thông tin khách hàng', 'success' => false], HttpStatusCode::OK);
            }
            $check_wallets = CustomerWalletServiceEloquent::newQuery()
                ->where('customer_id',$id)
                ->whereIn('wallet_id',$lsWallets);
            if ($check_wallets->count() >= count($lsWallets)){
                return $response->json(['message' => 'Khách hàng đã có ví này rồi vui lòng kiểm tra lại', 'success' => false], HttpStatusCode::OK);
            }
            $save = [];
            foreach ($lsWallets as $item){
                $objToSave = [
                    'wallet_id' => $item,
                    'customer_id' => $check_customer->id,
                    'status' => CustomerWalletServiceEloquent::getStatusActive(),
                    'created_at' => time()
                ];
                $save[] = CustomerWalletServiceEloquent::create($objToSave);
            }
            $response->value->add('wallets',$save);
            DB::commit();
            return $response->json(['message' => 'Tạo mới ví của khách hàng thành công', 'success' => true], HttpStatusCode::OK);
        }
        catch (\Exception $e){
            DB::rollBack();
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Tạo mới ví của khách hàng thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    public function getListWalletCanStoreById(int $id){
        $response = app(Response::class);
        try {
            $customer = CustomerService::newQuery()->find($id);
            if (empty($customer)){
                return $response->json(['message' => 'Không tìm thấy thông tin khách hàng', 'success' => false], HttpStatusCode::OK);
            }
            $listWallets = WalletsService::getWalletPermissionBuyProducts();
            $listWalletCustomer = CustomerWalletService::newQuery()
                ->where('customer_id','=',$id)
                ->pluck('wallet_id')->toArray();
            $arrWallet = [];
            foreach ($listWallets as $item){
                if (isset($item->id) && !in_array($item->id,$listWalletCustomer)){
                    $arrWallet[$item->id] = $item;
                }
            }
            if (empty($arrWallet)){
                return $response->json(['message' => 'Khách hàng này đã có đủ loại ví ', 'success' => false], HttpStatusCode::OK);
            }
            $response->value->add('wallets',$arrWallet);
            return $response->json(['message' => 'Lấy danh sách ví có thể tạo thành công', 'success' => true], HttpStatusCode::OK);
        }
        catch (\Exception $e){
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Tạo mới ví của khách hàng thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }


    public function vali(Request $request, int $id = 0) {
        $rules = [
            'first_name' => 'required|string|min:1|max:64',
            'last_name' => 'required|string|min:1|max:64',
            'mobile' => 'required|min:10|max:13|unique:ui_customers,mobile,'.$id,
            'bio' => 'nullable|min:10|max:250',
            'email' => 'nullable|email|min:3|max:64|unique:ui_customers,email,'.$id,
            'account' => 'required|alpha_num|min:3|max:64|unique:ui_customers,account,'.$id,
            'password' => 'nullable|min:6',
            'parent_referral_code' => 'exists:ui_customers,referral_code',
            'mpmart_rank_total_revenue' => 'required|exists:__money_rank_mpmart_total_revenue,id',
            'street' => 'nullable|max:250',
            'ward_id' => 'nullable|exists:__wards,id',
            'district_id' => 'nullable|exists:__districts,id',
            'city_id' => 'nullable|exists:__cities,id',
        ];
        if($id) {
            foreach (['email', 'account', 'password', 'mobile'] as $item) {
                unset($rules[$item]);
            }
        }
        return $this->makeValidator($request, $rules, [
            'required' => ':attribute không được để trống',
            'email' => ':attribute không đúng định dạng',
            'string' => ':attribute định dạng là ký tự chữ và số',
            'alpha_num' => ':attribute định dạng phải là các ký tự chữ và số',
            'min' => ':attribute tối thiểu',
            'max' => ':attribute tối đa',
            'unique' => ':attribute đã tồn tại',
            'exists' => ':attribute không tồn tại',
        ], [
            'first_name' => 'Họ',
            'last_name' => 'Tên',
            'mobile' => 'Số điện thoại',
            'email' => 'Email',
            'parent_referral_code' => 'Mã giới thiệu cấp trên',
            'mpmart_rank_total_revenue' => 'Hoa hồng kết nối',
            'account' => 'Tài khoản đăng nhập',
            'password' => 'Mật khẩu',
            'city_id' => 'Thành phố',
            'district_id' => 'Quận, huyện',
            'ward_id' => 'Phường, xã',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function filters(Request $request): JsonResponse
    {
        $response = app(Response::class);
        try {
            $response->value->add('filters', $this->columns());
            return $response->json(['message' => 'Lấy bộ lọc thành công!', 'success' => true], HttpStatusCode::OK);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Lấy bộ lọc thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }

    function _query_filter(Request $request, &$query)
    {
//        dd($request->all(),$query->get()[6]);
        if (checkRequestInputFieldEmptyOrNot($request, 'name')) {
            $query = $query->where('full_name', 'LIKE', '%'.$request->get('name').'%');
        }
        if (checkRequestInputFieldEmptyOrNot($request, 'account')) {
            $query = $query->where('account', 'LIKE', '%'.$request->get('account').'%');
        }
        if (checkRequestInputFieldEmptyOrNot($request, 'phone')) {
            $query = $query->where('mobile', 'LIKE', '%'.$request->get('phone').'%');
        }
        if (checkRequestInputFieldEmptyOrNot($request, 'email')) {
            $query = $query->where('email', 'LIKE', '%'.$request->get('email').'%');
        }
//        if (checkRequestInputFieldEmptyOrNot($request, 'rank')) {
//            $num = (int)$request->get('rank');
//            $query = $query->whereHas('mpmart_rank', function ($query) {
//                $query->where('rank.id', '=', $num);
//            });
//            dd($query->get());
//
//            $query = $query->where('mpmart_rank.rank.id', 'LIKE', '%'.(int)$request->get('rank').'%');
//        }
        if (checkRequestInputFieldEmptyOrNot($request, 'status')) {
            $status = convertRequestStringToArray($request, 'status');
            $query = $query->whereIn('status', $status);
        }
    }

    public function columns(): array
    {
        return [
            ['name' => 'Nhập thông tin tìm kiếm ', 'id' => 'name', 'placeholder' => 'Tên khách hàng, Số thuê bao, Email ...', 'type' => 'text', 'icon' => [
                'prefix' => 'user',
                'suffix' => 'info-circle',
            ]],
            ['name' => 'Nhập tài khoản', 'id' => 'account', 'placeholder' => 'Nhập tài khoản', 'type' => 'text', 'icon' => [
                'prefix' => 'user',
                'suffix' => 'info-circle',
            ]],
            ['name' => 'Nhập Số điện thoại', 'id' => 'phone', 'placeholder' => 'Nhập số điện thoại', 'type' => 'text', 'icon' => [
                'prefix' => 'user',
                'suffix' => 'info-circle',
            ]],
            ['name' => 'Nhập email', 'id' => 'email', 'placeholder' => 'Nhập số email', 'type' => 'text', 'icon' => [
                'prefix' => 'user',
                'suffix' => 'info-circle',
            ]],
            ['name' => 'Lựa chọn cấp bậc', 'id' => 'rank', 'type' => 'select',  'placeholder' => 'Cấp bậc', 'values' => RankMpMartModel::getRank(), 'icon' => [
                'prefix' => 'user',
                'suffix' => 'info-circle',
            ]],
            ['name' => 'Lựa chọn trạng thái', 'id' => 'status', 'type' => 'select', 'mode' => 'multiple', 'placeholder' => 'Trạng thái', 'values' => CustomerBank::getStatus(), 'icon' => [
                'prefix' => 'user',
                'suffix' => 'info-circle',
            ]],

        ];
    }
    public function excel_export(Request $request){
        {
            $response = app(Response::class);
            try {
                $query = CustomerServiceEloquent::newQuery()->with(['wallets.wallet', 'city', 'district', 'ward', 'moneyRankMpMartTotalRevenue.rank']);
                $query = $query->orderBy('id', 'DESC');
                $this->_query_filter($request,$query);
                $query = $query->orderBy('id', 'DESC');
                $deposits = $query->get()->toArray();
                $cells = [
                    'account' => 'ID',
                    'full_name' => 'Tên khách hàng',
                    'mobile' => 'Số điện thoại',
                    'email' => 'Email',
                    'rank' => 'Rank',
                    'discount' => '% chiết khấu',
                ];
                $value = [];
                foreach ($deposits as $index => $deposit) {
                    $value[$index] = [
                        'account' => $deposit['account'],
                        'full_name' => $deposit['full_name'],
                        'mobile' => $deposit['mobile'],
                        'email' => $deposit['email'],
                        'rank' => !empty($deposit['mpmart_rank']) ? $deposit['mpmart_rank']['rank']->name : 'Chưa cập nhật',
                        'discount' => !empty($deposit['mpmart_rank']) ? $deposit['mpmart_rank']['discount'] : 'Chưa cập nhật',
                    ];
                }
                $fileName = '[' . \date('d-m-Y H:i:s') . '] - Danh sách khách hàng.xlsx';
                $excel = Excel::getInstance()->export($fileName, $cells, $value);
                $response->value->add('file', $excel);
                return $response->json(['message' => 'Xuất danh sách thông tin khách hàng thành công!', 'success' => true], HttpStatusCode::OK);
            } catch (\Exception $e) {
                app(Handler::class)->report($e);
                return $response->json(['message' => 'Xuất danh sách thông tin khách hàng thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
            }
        }
    }
    /**
     * Lấy danh sách cấp dưới
     * @return JsonResponse
     */
    public function children(int $id): JsonResponse
    {
        $response = new Response();
        $customer = CustomerServiceEloquent::getById($id);
        if ($customer) {
            $customers = CustomerServiceEloquent::getChildren($customer->referral_code);
        }
        $response->value->add('customers', @$customers ? $customers->toArray() : []);
        return $response->json(['message' => 'Lấy thông tin khách hàng thành công!', 'success' => true], HttpStatusCode::OK);
    }

    public function getByIdWallet(Request $request):JsonResponse
    {
        $response = new Response();
        try {
            $id_customer = $request->get('id');
            $get_wallet_by_id_customer = CustomerWalletModel::whereIn('customer_id', [$id_customer])->with(['wallet:id,name'])->get();
            $response->value->add('customer_wallets', @$get_wallet_by_id_customer ? $get_wallet_by_id_customer->toarray() : []);
            return $response->json(['message' => 'Lấy danh sách ví theo account thành công!', 'success' => true], HttpStatusCode::OK);
        }
        catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Lấy danh sách ví theo account thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }
}
