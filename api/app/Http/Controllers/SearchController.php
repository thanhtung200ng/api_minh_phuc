<?php


namespace App\Http\Controllers;


use App\Exceptions\Handler;
use App\Http\Repositories\Entities\Response;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Http\Repositories\ServicesEloquent\ProductServiceEloquent;
use App\Http\Repositories\ServicesEloquent\ProductVariantsServiceEloquent;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $response = app(Response::class);
        $page = $request->get('page', 1);
        $count = (int)$request->get('count', 10);
        try {
            $offset = ($page - 1) * $count;
            $qEloquent = ProductVariantsServiceEloquent::newQuery()->select(['name', 'id', 'sku', 'images']);
            if (checkRequestInputFieldEmptyOrNot($request, 'keyword')) {
                $qEloquent = $qEloquent->where('name', 'LIKE', '%'.$request->get('keyword').'%');
            }
            $qEloquent = $qEloquent->orderBy('id', 'DESC');
            $total = $qEloquent->count();
            if ($count) {
                $qEloquent->offset($offset);
                $qEloquent->limit($count);
            }
            $products = $qEloquent->get();
            $response->value->add('total', $total);
            $response->value->add('pageSize', $count);
            $response->value->add('products', $products->toArray());
            return $response->json(['message' => 'Lấy danh sách sản phẩm thành công!', 'success' => true], HttpStatusCode::OK);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Lấy danh sách sản phẩm dịch thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }
    }
}
