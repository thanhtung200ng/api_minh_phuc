<?php


namespace App\Http\Controllers;


use App\Elibs\ApiHelper;
use App\Elibs\Zalo;
use App\Exceptions\Handler;
use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Position;
use App\Http\Repositories\Entities\Response;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Http\Repositories\Enums\RankMpMartCode;
use App\Http\Repositories\Factories\FilterFactory;
use App\Http\Repositories\Factories\OrderFactory;
use App\Http\Repositories\Resources\LocationResourcesCollection;
use App\Http\Repositories\Resources\Positions\PositionResources;
use App\Http\Repositories\Resources\Positions\PositionResourcesCollection;
use App\Http\Repositories\Services\LocationService;
use App\Http\Repositories\Services\PositionService;
use App\Http\Repositories\ServicesEloquent\AgentServiceEloquent;
use App\Models\City;
use App\Models\District;
use App\Models\Ward;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function locations(Request $request): JsonResponse
    {
        $response = app(Response::class);
        $type = $request->get('type', 'cities');

        try {
            $allows = ['id', 'name', 'code'];
            if ($type === 'districts') {
                $allows[] = 'city_id';
            }else if ($type === 'wards') {
                $allows[] = 'district_id';
            }
            $filters = FilterFactory::makeCollectionFromRequest($request, $allows);
            $orders = OrderFactory::makeCollectionFromRequest($request, $allows);
            $total = 0;
            $locations = LocationService::getAll($type, $total, $orders, $filters);
            $response->value->add('total', $total);
            $response->value->add('locations', LocationResourcesCollection::toArray($locations));

            return $response->json(['message' => 'Lấy danh sách địa chỉ thành công!','success' => true], HttpStatusCode::OK);
        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Lấy danh sách địa chỉ thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }

    }

    /**
     * Display a listing place of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function place(Request $request): JsonResponse
    {
        $response = app(Response::class);
        $city_id = $request->get('city_id');
        $district_id = $request->get('district_id');
        $ward_id = $request->get('ward_id');
        $keyword = $request->get('keyword');

        try {
            if ($city_id) {
                $city = City::find($city_id);
                if ($city) {
                    $district = District::where([
                        'id' => $district_id,
                        'city_id' => $city_id,
                    ])->first();
                    if ($district) {
                        $ward = Ward::where([
                            'id' => $ward_id,
                            'district_id' => $district_id,
                        ])->first();
                        if ($ward) {
                            //dd(env('GOOGLE_API_KEY'), 1232131);
                            $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?key='.env('GOOGLE_API_KEY').'&query='.$keyword.', '. $ward->name. ', '. $district->name. ', '. $city->name;
                            $locations = Http::get($url);
                            $response->value->add('results', $locations->json());
                            $response->value->add('locations', $locations->json()['results']);

                            return $response->json(['message' => 'Lấy danh sách địa chỉ thành công!','success' => true], HttpStatusCode::OK);
                        }
                    }
                }
            }
            return $response->json(['message' => 'Lấy danh sách địa chỉ thất bại!', 'success' => false], HttpStatusCode::CONFLICT);

        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Lấy danh sách địa chỉ thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }

    }
    public function distanceMatrix(Request $request): JsonResponse
    {
        $response = app(Response::class);
        try {
            $origin = $request->get('origin');
            $destination = $request->get('destination');
            $agent_id = $request->get('agent_id');
            if ($origin && $destination) {

                $distanceMatrix = $this->calcShippingFee($origin, $destination, $agent_id);
                $response->value->add('distancematrix', $distanceMatrix);
                return $response->json(['message' => 'Lấy danh sách địa chỉ thành công!','success' => true], HttpStatusCode::OK);
            }



        } catch (\Exception $e) {
            app(Handler::class)->report($e);
            return $response->json(['message' => 'Lấy danh sách địa chỉ thất bại!', 'success' => false], HttpStatusCode::CONFLICT);
        }

    }

    function calcShippingFee($origin, $destination, $agent_id): array
    {
        $agent = AgentServiceEloquent::getById($agent_id);
        if ($agent && $agent->customer && $agent->customer->isCty() && 1 == 2) {
            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?key='.env('GOOGLE_API_KEY').'&origins='.$origin.'&destinations='.$destination;
            $distanceMatrix = Http::get($url);
            $distanceMatrix = $distanceMatrix->json();
            if (isset($distanceMatrix['rows'][0]['elements'][0]) && $distanceMatrix['rows'][0]['elements'][0]['status'] === 'OK') {
                $distance = $distanceMatrix['rows'][0]['elements'][0]['distance']['value'];
                $fee = 0;
                if (auth('api_customer')->check()) {
                    $customer = auth('api_customer')->user();
                    $total = \request('amount');
                    if (isset($customer->mpmart_rank['rank_id'])) {
                        if(RankMpMartCode::isDaiLy($customer->mpmart_rank['rank_id'])){
                            if ($total < 10000000) {
                                if ($distance > 10000) {
                                    $fee = ($distance - 10000);
                                }
                            }
                        }elseif (RankMpMartCode::isSieuThi($customer->mpmart_rank['rank_id'])){
                            if ($total < 15000000) {
                                if ($distance > 10000) {
                                    $fee = ($distance - 10000);
                                }
                            }
                        }elseif (RankMpMartCode::isTongKho($customer->mpmart_rank['rank_id'])){
                            if ($total < 15000000) {
                                if ($distance > 10000) {
                                    $fee = ($distance - 10000);
                                }
                            }
                        }
                        elseif (RankMpMartCode::isCTV($customer->mpmart_rank['rank_id'])){
                            if ($total < 3000000) {
                                if ($distance > 10000) {
                                    $fee = ($distance - 10000);
                                }
                            }
                        }
                    }else {
                        if ($total < 3000000) {
                            if ($distance > 10000) {
                                $fee = ($distance - 10000);
                            }
                        }
                    }

                }

                $distanceMatrix['rows'][0]['elements'][0]['distance']['money'] = $fee * 0.5;

            }
        }
        else {
            $distanceMatrix['rows'][0]['elements'][0]['status'] = 'OK';
            $distanceMatrix['rows'][0]['elements'][0]['distance']['money'] = 0;
        }


        return $distanceMatrix;
    }

}
