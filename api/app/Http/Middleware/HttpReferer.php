<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HttpReferer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request);
        if(!isset($_SERVER['HTTP_REFERER']) || !in_array($_SERVER['HTTP_REFERER'], ['https://mocquyhuong.com/', 'https://2x.zouk.mocquyhuong.com/'])) {
            return response(['message' => 'Not Access', 'success' => false], 401);
        }
        return $next($request);
    }
}
