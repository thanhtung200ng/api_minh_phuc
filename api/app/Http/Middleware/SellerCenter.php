<?php


namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;

class SellerCenter
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param  Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth('api_customer')->user();
        if (!@$user->is_seller && !@$user->is_mpmart) {
            auth('api_customer')->logout(true);
            return response(['message' => 'Bạn không có quyền truy cập hệ thống bán hàng của chúng tôi.', 'success' => false], 403);
        }
        return $next($request);
    }
}
