<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Models\MoneyRankMpMartTotalRevenueModel;
use Illuminate\Database\Eloquent\Model;

class MoneyRankMpMartTotalRevenueServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new MoneyRankMpMartTotalRevenueModel();
        parent::__construct($model);
    }

    public static function getById($id)
    {
        $query = self::newQuery();

        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query = $query->with(['rank']);
        return $query->get();
    }

    public static function getByMaxMinMoney(float $money)
    {
        $query = self::newQuery();
        $query = $query->with('rank')->has('rank')
            ->where('min_money', '<=', $money)
            ->where('max_money', '>', $money)
            ->where('status', self::getStatusActive());
        return $query->first();
    }
    public static function getRankAndDiscount()
    {
        $query = self::newQuery();
        dd($query->get()->toArray());
        return $query->get();
    }
}
