<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class BaseServiceEloquent
{
    public $model;
    protected static $instances = [];

    function __construct($model = false)
    {
        $cls = static::class;
        if(!empty($model)) {
            $this->model = $model;
        }
        self::$instances[$cls] = &$this;

    }

    public static function &getInstance()
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
        }

        return self::$instances[$cls];
    }

    /**
     * @return QueryBuilder
     */
    public static function newQueryBuilder(): QueryBuilder
    {
        $instance = self::getInstance();
        return app('db')->table($instance->model->getTable());
    }


    public static function newQuery()
    {
        return self::getInstance()->model->query();
    }

    /**
     * @param  $query
     * @param Order[]|Collection $orders
     * @return  $query
     */
    protected static function processOrder($query, $orders)
    {
        foreach ($orders as $order) {
            $query->orderBy($order->getColumnName(), $order->getValue());
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param Filter[]|Collection $filters
     * @return  $query
     */
    protected static function processFilter(Builder $query, $filters)
    {
        foreach ($filters as $filter) {
            switch ($filter->getOperand()) {
                case 'IsEqualTo':
                    $query->having($filter->getColumnName(), '=', $filter->getValue());
                    break;
                case 'IsEqualToOrNull':
                    $query->where(function ($query) use ($filter) {
                        /** @var Builder $query */
                        $query->having($filter->getColumnName(), '=', $filter->getValue())
                            ->orWhereNull($filter->getColumnName());
                    });
                    break;
                case 'IsNull':
                    $query->whereNull($filter->getColumnName());
                    break;
                case 'IsNotEqualTo':
                    $query->having($filter->getColumnName(), '<>', $filter->getValue());
                    break;
                case 'IsNotNull':
                    $query->whereNotNull($filter->getColumnName());
                    break;
                case 'StartWith':
                    $query->having($filter->getColumnName(), 'LIKE', $filter->getValue() . '%');
                    break;
                case 'DoesNotContains':
                    $query->having($filter->getColumnName(), 'NOT LIKE', '%' . $filter->getValue() . '%');
                    break;
                case 'Contains':
                    $query->having($filter->getColumnName(), 'LIKE', '%' . $filter->getValue() . '%');
                    break;
                case 'EndsWith':
                    $query->having($filter->getColumnName(), 'LIKE', '%' . $filter->getValue());
                    break;
                case 'In':
                    $query->whereIn($filter->getColumnName(), $filter->getValue());
                    break;
                case 'NotIn':
                    $query->whereNotIn($filter->getColumnName(), $filter->getValue());
                    break;
                case 'Between':
                    $query->havingBetween($filter->getColumnName(), $filter->getValue());
                    break;
                case 'IsAfterThanOrEqualTo':
                case 'IsGreaterThanOrEqualTo':
                    $query->having($filter->getColumnName(), '>=', $filter->getValue());
                    break;
                case 'IsGreaterThanOrNull':
                    $query->where(function ($query) use ($filter) {
                        /** @var Builder $query */
                        $query->having($filter->getColumnName(), '>', $filter->getValue())
                            ->orWhereNull($filter->getColumnName());
                    });
                    break;
                case 'IsAfterThan':
                case 'IsGreaterThan':
                    $query->having($filter->getColumnName(), '>', $filter->getValue());
                    break;
                case 'IsBeforeThanOrEqualTo':
                case 'IsLessThanOrEqualTo':
                    $query->having($filter->getColumnName(), '<=', $filter->getValue());
                    break;
                case 'IsBeforeThan':
                case 'IsLessThan':
                    $query->having($filter->getColumnName(), '<', $filter->getValue());
                    break;
            }
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param array $selects
     * @return Builder $query
     */
    protected static function processSelect(Builder $query, array $selects = []): Builder
    {
        $query->select($selects);
        return $query;
    }

    public static function getStatusActive(): int
    {
        return 1;
    }

    /**
     * @return int
     */

    public static function getStatusInActive(): int
    {
        return 0;
    }

    /**
     * @return int
     */

    public static function getStatusDeleted(): int
    {
        return -4;
    }

    /**
     * @return int
     */

    protected static function getStatusDisabled(): int
    {
        return -1;
    }

    static function getCode($selected = false, $return = false): array {
        $listStatus = [
            ['id' => self::getStatusActive(), 'style' => 'success', 'name' => 'Đã duyệt',],
            ['id' => self::getStatusInActive(), 'style' => 'primary', 'name' => 'Đơn mới'],
            ['id' => self::getStatusDeleted(), 'style' => 'danger', 'name' => 'Đã xoá'],
        ];
        if (is_numeric($selected)) {
            foreach ($listStatus as $status) {
                if ($status['id'] === $selected) {
                    $status['checked'] = 'checked';
                    if($return) {
                        return $status;
                    }
                }
            }
        }

        return $listStatus;
    }

    public static function statisticInDate($option , $query, $column)
    {
        switch ($option){
            case 'today':
                $start = Carbon::now()->startOfDay()->timestamp;
                $end = Carbon::now()->endOfDay()->timestamp;
                $query->whereBetween($column , array($start,$end));
                break;
            case 'week':
                $start = Carbon::now()->startOfYear()->timestamp;
                $end = Carbon::now()->endOfYear()->timestamp;
//                $query->whereBetween($column , array($start,$end));

                $a = $query->select(
                    'created_at',
                    DB::raw("DATE_FORMAT(DATE((FROM_UNIXTIME(created_at))),'%m-%Y') as month"),
                    DB::raw("COUNT(*) as count"),
                    DB::raw("SUM(money) as sum"));
                dd($a->groupBy('month')->get());

                dd($query->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->format('d');
                }));


                break;
            case 'month':
                $start = Carbon::now()->startOfMonth()->timestamp;
                $end = Carbon::now()->endOfMonth()->timestamp;
                $query->whereBetween($column , array($start,$end));
                break;
            case 'year':
                $start = Carbon::now()->startOfYear()->timestamp;
                $end = Carbon::now()->endOfYear()->timestamp;
                $query->whereBetween($column , array($start,$end));
                break;
            default:
                return $query->whereBetween($column , $option);
        }
        return $query;
    }

}
