<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Models\BaseModel;
use App\Models\WalletModel;
use Illuminate\Database\Eloquent\Model;

class WalletServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new WalletModel();
        parent::__construct($model);
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $selects = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $query = self::processSelect($query, $selects);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function getAllActive()
    {
        $query = self::newQuery();
        $query = $query->where('status', WalletModel::getStatusActive());
        return $query->get();
    }
    public static function getAllActive_Role()
    {
        $query = self::newQuery();
        $query = $query->where('status', WalletModel::getStatusActive())->whereIn('id', [1,2,4,7,8]);
        return $query->get();
    }

    public static function getListWallet($listWallet){
        $query = self::newQuery();
        $query = $query->where('status', BaseModel::getStatusActive())->whereIn('id', $listWallet);
        return $query->get();
    }

    public static function checkWalletCanWithdraw(int $walletId){
        return in_array($walletId, [2,4]);
    }

    public static function WalletCanSendMoney(int $walletId){
        return in_array($walletId, [
            WalletModel::getInstance()->getViHoaHong(),
            WalletModel::getInstance()->getViLoiNhuan(),
        ]);
    }

    public static function WalletCanReceiveMoney(int $walletId){
        return in_array($walletId, [
            WalletModel::getInstance()->getViKyQuy(),
        ]);
    }
}
