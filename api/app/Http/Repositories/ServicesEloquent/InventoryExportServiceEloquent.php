<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\InventoryExportModel;
use Illuminate\Database\Eloquent\Model;

class InventoryExportServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new InventoryExportModel();
        parent::__construct($model);
    }

    public static function create($obj){
        $obj['created_at'] = time();
        $id = self::newQuery()->insertGetId($obj);
        $obj['id'] = $id;
        LogAdminService::create($id, ResourceCode::CREATE, self::getInstance()->model->getTable(), $obj, [], $obj);
        return $obj;
    }

    public static function update(InventoryExportModel $obj, $before = null): InventoryExportModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }
}
