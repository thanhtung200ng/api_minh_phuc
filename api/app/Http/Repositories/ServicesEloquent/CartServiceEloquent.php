<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Elibs\Helper;
use App\Http\Repositories\Enums\CartCode;
use App\Http\Repositories\Enums\PurchaseOrderCode;
use App\Http\Repositories\Enums\RankMpMartCode;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\CartNameModel;
use App\Models\ProductVariantModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\CartModel as THIS;

class CartServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new THIS();
        parent::__construct($model);
    }

    public static function getCartBigCorpAuth(int $type) {
        $where = [
            ['customer_id', '=', auth('api_customer')->id()],
            ['status', '=', CartCode::STATUS_SHOPPING],
            ['type', '=', $type]
        ];
        return self::newQuery()->withSum('items', 'quantity')->with('customer','items.variant', 'customerWallet.wallet', 'deliveryAddress.city', 'deliveryAddress.district', 'deliveryAddress.ward',
            'agent.city', 'agent.district', 'agent.ward'
        )->where($where)->first();
    }


    public static function getListNameCart() {
        $where = [
            ['status', '=', self::getStatusActive()],
        ];
        return CartNameModel::where($where)->get();
    }

    public static function update(THIS $obj, $before = null): THIS
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }

    public static function getGroupCartBigCorpAuth() {
        $where = [
            ['customer_id', '=', auth('api_customer')->id()],
            ['status', '=', CartCode::STATUS_SHOPPING],
        ];
        return self::newQuery()->with('items')->where($where)->get()->groupBy('type');
    }

    public static function getGroupCartWithSumBigCorpAuth() {
        $where = [
            ['customer_id', '=', auth('api_customer')->id()],
            ['status', '=', CartCode::STATUS_SHOPPING],
        ];
        return self::newQuery()->withSum('items', 'quantity')->with('type')->where($where)->get();
    }

    public static function create($objToSave)
    {
        $items = $objToSave['items'];
        unset($objToSave['items']);
        $id = self::newQuery()->insertGetId($objToSave);
        $objToSave['id'] = $id;

        foreach ($items as $k => $item) {
            $items[$k]['cart_id'] = $id;
        }

        foreach ($items as $k => $item) {
            $id = CartItemsServiceEloquent::create($item);
            $items[$k]['id'] = $id;
        }
        $objToSave['items'] = $items;
        return $objToSave;
    }

    public static function getAll(Request $request)
    {
        $cartItems = $request->get('products');
        $whereIn = array_column($cartItems, 'id');
        $variants = ProductVariantModel::with('wallets','salesBooth')->where('status', ProductVariantModel::getStatusActive())
            ->whereIn('id', $whereIn)->get();
        $choseAgents = $variants->groupBy('is_choose_agent');
        $groupSalesBooth = [];
        $groupAgents = [
            'name' => env('COMPANY_NAME'),
            'customer_wallet' => [],
            'agent' => [],
            'products' => [],
        ];
        foreach ($choseAgents as $key => $products) {
            if (!$key) {    // key !== 1 thì là gian hàng
                foreach ($products as $product) {
                    if (!isset($groupSalesBooth[$product->salesBooth->id])) {
                        $groupSalesBooth[$product->salesBooth->id] = [
                            'id' => $product->salesBooth->id,
                            'name' => $product->salesBooth->name,
                            'customer_wallet' => [],
                            'products' => [],
                        ];
                    }
                    $groupSalesBooth[$product->salesBooth->id]['products'][] = $product->toArray();
                }
            }else {  // key === 1 thì là chọn đại lý trả hàng
                foreach ($products as $product) {
                    $groupAgents['products'][] = $product->toArray();
                }
            }

        }
        return [
            'choose_agent' => $groupAgents, 'sales_booth' => $groupSalesBooth, 'cartItems' => $variants->toArray()
        ];
    }

    static function checkWallet(&$response, $cart, $total) {
        if (!$cart->customerWallet) {
            $response->error->add('customer_wallet_0', 'Không tìm thấy ví thanh toán phù hợp');
        }else {
            $payWallet = $cart->customerWallet;
            if ($payWallet->customer->getAccount() === $cart->customer->getAccount()) {
                foreach ($cart->items as $variant) {
                    if (!$variant->variant->wallets) {
                        return $response->error->add('customer_wallet' . $payWallet->getKey(), 'Không tìm thấy ví thanh toán phù hợp cho sản phẩm "#' . $variant->variant->product_id .' '.$variant->variant->getName().'"');
                    }else {
                        $allowWallet = $variant->variant->wallets->toArray();
                        if (!in_array($payWallet->wallet_id, array_column($allowWallet, 'id'))) {
                            return $response->error->add('customer_wallet' . $payWallet->getKey(), '' . $payWallet->getName() . ' không được phép thanh toán cho sản phẩm ' . $variant->variant->getName());
                        } else {
                            if (!$payWallet->isActive()) {
                                return $response->error->add('customer_wallet' . $payWallet->getKey(), '' . $payWallet->getName() . '" Chưa được kích hoạt hoặc đã tạm khoá');
                            }
                        }
                    }

                }
                if ($cart->customerWallet->current_balance < $total) {
                    $response->error->add('customer_wallet_'.$cart->customerWallet->id, 'Số dư ví "' . $cart->customerWallet->getName() . ('" không đủ. Bạn cần nạp thêm ' . ( Helper::formatMoney($total - $cart->customerWallet->current_balance))) .' thực hiện giao dịch này.');
                }
            } else {
                $response->error->add('customer_wallet', '' . $payWallet->getName() . '" không được phép truy cập.');
            }
        }
    }

    static function calcWallet(&$response, $cart, $total) {
        $payWallet = $cart->customerWallet;
        $payWallet->current_balance -= $total;
        CustomerWalletServiceEloquent::update($payWallet, $payWallet->getOriginal());
    }

    static function checkQuantity(&$response, &$cart, &$total) {
        $variants = $cart->items;
        foreach ($variants as $variant) {
            if (!$variant->variant->salesBooth) {
                $response->error->add('product_'.$variant->id, 'Không tìm thấy gian hàng của sản phẩm "' . $variant->variant->getName() . '". Vui lòng lựa chọn sản phẩm khác.');
            }else {
                if (!$variant->variant->salesBooth->isActive()) {
                    $response->error->add('product_'.$variant->id, 'Gian hàng của sản phẩm "' . $variant->variant->getName() . '" chưa hoạt động. Vui lòng lựa chọn sản phẩm khác.');
                }else {
                    if ($variant->quantity > $variant->variant->quantity) {
                        $response->error->add('product_'.$variant->id, '' . $variant->variant->getName() . '" chỉ còn tối đa ' . $variant->variant->getQuantity() . ' sản phẩm trong kho.');
                    }else {
                        $variant->variant->customerWallet = $cart->customerWallet;
                        //$variant->variant->quantity_purchased = $variant->quantity;
                        $total += $variant->quantity * $variant->variant->price_sale;
                    }
                }
            }
        }
    }

    static function calcPriceSaleDiscount(&$response, &$cart, &$total) {
        $variants = $cart->items;
        foreach ($variants as $variant) {
            $variant->variant->customerWallet = $cart->customerWallet;
            $variant->variant->quantity_purchased = $variant->quantity;
            $total += $variant->quantity * $variant->variant->getPriceSaleDiscount();
        }
    }

    static function checkAgent(&$response, $cart) {
        $agent = $cart->agent;
        $deliveryAddress = $cart->deliveryAddress;
        if ($agent === null) {
            $response->error->add('agent_null', 'Không tìm thấy Siêu thị/đại lý trả hàng');
        } else {
            if (!$agent->isActive()) {
                $response->error->add('agent_inactive', 'Cơ sở trả hàng "'.$agent->getName().'" chưa được kích hoạt hoặc đã tạm khoá');
            } else {

                if ($agent->customer->getId() == $deliveryAddress->customer->getId()) {
                    $response->error->add('agent_match', 'Bạn không thể chọn chính mình trả hàng. Vui lòng chọn lại đại lý trả hàng');
                } else {
                    // kiểm tra MPMart chỉ được đặt tại công ty
                    if (!isset($deliveryAddress->customer->mpmart_rank['rank_id'])) {
                        $response->error->add('rank_undefined', 'Bạn chưa có chức danh trong hệ thống. Vui lòng liên hệ quản trị viên để biết thêm chi tiết');
                    } else {
                        if (!isset($agent->customer->mpmart_rank['rank_id'])) {
                            $response->error->add('rank_undefined', 'Đại lý trả hàng chưa có chức danh trong hệ thống. Vui lòng lựa chọn Siêu thị/đại lý khác');
                        } else {
                            if (!$agent->customer->isCty()) { // nếu đại lý trả hàng không phải công ty
                                if (RankMpMartCode::isSieuThi($deliveryAddress->customer->mpmart_rank['rank_id']) && !RankMpMartCode::isTongKho($agent->customer->mpmart_rank['rank_id'])) {
                                    $response->error->add('agent_undefined', 'Bạn chỉ có thể chọn '.env('COMPANY_NAME').' hoặc Tổng kho trả hàng');
                                } elseif (RankMpMartCode::isDaiLy($deliveryAddress->customer->mpmart_rank['rank_id'])
                                    && !RankMpMartCode::isSieuThi($agent->customer->mpmart_rank['rank_id']) && !RankMpMartCode::isTongKho($agent->customer->mpmart_rank['rank_id']) ) {
                                    $response->error->add('agent_undefined', 'Bạn chỉ có thể chọn '.env('COMPANY_NAME').' hoặc các Siêu Thị MpMart trả hàng');
                                } elseif (RankMpMartCode::isCTV($deliveryAddress->customer->mpmart_rank['rank_id']) && RankMpMartCode::isCTV($agent->customer->mpmart_rank['rank_id'])) {
                                    $response->error->add('agent_undefined', 'Bạn chỉ có thể chọn '.env('COMPANY_NAME').' hoặc các Siêu Thị/Đại lý MpMart trả hàng');
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    static function checkDeliveryAddress(&$response, $cart) {
        $deliveryAddress = $cart->deliveryAddress;
        if (!$deliveryAddress || !$deliveryAddress->isActive()) {
            $response->error->add('delivery_address_isset', 'Địa chỉ nhận hàng chưa được kích hoạt hoặc tạm khoá.');
        }
    }
}
