<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Models\BaseModel;
use App\Models\WithdrawMoneyModel;
use Illuminate\Support\Facades\DB;

class WithdrawMoneyServicesEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new WithdrawMoneyModel();
        parent::__construct($model);
    }
    public static function getById($id){
        $query = self::newQuery();
        $query = $query->where('id',$id);
        $query = $query->with(['customer', 'wallet.wallet' => function($q){
            $q->select('id', 'name' ,'status' ,'slug');
        } ,'bank.bank', 'wallet.customer' => function($q){
            $q->select('id','account' ,'status');
        }
        ]);
        return $query->first();
    }
    public static function getAllActive()
    {
        $query = self::newQuery();
        $query = $query->where('status', self::getStatusActive());
        return $query->get();
    }
    public static function getByListId($listId){
        $query = self::newQuery();
        $query = $query->whereIn('id',$listId)->where('status', BaseModel::getStatusInActive());
        $query = $query->with('customer', 'wallet.wallet','bank');
        return $query->get();
    }

    public static function deduction($item,$money){
        $totalMoney = $item->getCurrentBalance() - $money;
        DB::table('ui_customer_wallets')->where('id', $item->getId())->update([
            'current_balance' => $totalMoney
        ]);
        $item->setCurrentBalance($totalMoney);
        return $item;
    }
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $selects = []){
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query = $query->with(['customer', 'wallet.wallet' => function($q){
            $q->select('id', 'name' ,'status' ,'slug');
        } ,'bank.bank', 'wallet.customer' => function($q){
            $q->select('id','account' ,'status');
        }
        ]);
        $query = $query->orderBy('id','DESC');

        return $query->get();
    }

    public function update($id ,$save){

        $query = self::newQuery();
        $query->where('id', $id)->update($save);
        return $query->where('id', $id)->first();
    }

    public function create($obj){
        $query = self::newQuery();
        $id = $query->insertGetId($obj);
        return $query->where('id', $id)->first();
    }

    public function minWithdrawMoney(): int
    {
        return 50000;
    }
    public function walletLining(): int
    {
        return 50000;
    }
    public function transactionFee($money): int
    {
        if($money <= 3000000 ){
            return 2000;
        }if($money < 10000000){
        return 5000;
    }if($money < 25000000){
        return 10000;
    }
        return 15000;
    }

    public function incomeTax(): float
    {
        return 0.1;
    }
    public static function getByCustomerIdAndWalletId($customerId, $walletId){
        $query = self::newQuery();
        $query = $query->where(['customer_id'=> $customerId , 'wallet_id' => $walletId])->orderBy('id','DESC');
        return $query->get();
    }

    public static function getByCustomer($customerId){
        $query = self::newQuery();
        $query = $query->where('customer_id', $customerId)->orderBy('id','DESC');
        $query = $query->with(['bank.bank']);
        return $query;
    }
}
