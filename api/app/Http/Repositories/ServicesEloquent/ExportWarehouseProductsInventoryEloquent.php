<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\ExportWarehouseProductsInventoryModel;
use App\Models\WarehouseProductsInventoryModel;
use Illuminate\Database\Eloquent\Model;

class ExportWarehouseProductsInventoryEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new ExportWarehouseProductsInventoryModel();
        parent::__construct($model);
    }

    public static function create($objToSave){
        $id = self::newQuery()->insertGetId($objToSave);
        LogAdminService::create($id, ResourceCode::CREATE, 'ui_export_warehouse_log', $objToSave, [], $objToSave);
        return $id;
    }
}
