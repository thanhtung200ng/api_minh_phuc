<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\NewsCategoriesModel;
use App\Models\BaseModel;
use App\Models\CustomerWalletModel;
use Illuminate\Database\Eloquent\Model;

class NewsCategoriesServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new NewsCategoriesModel();
        parent::__construct($model);
    }

    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function create($obj){
        $id = self::newQuery()->insertGetId($obj);
        LogAdminService::create($id, ResourceCode::CREATE, 'ui_news_categories', $obj, [], $obj);
        return $id;
    }

    public static function update(NewsCategoriesModel $obj, $before = null): NewsCategoriesModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }

    public static function delete(NewsCategoriesModel $obj, $before = null): NewsCategoriesModel
    {
        $obj->status = ResourceCode::DELETE;
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }

}
