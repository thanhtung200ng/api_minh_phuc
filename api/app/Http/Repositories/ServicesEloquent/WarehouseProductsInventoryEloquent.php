<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\WarehouseProductsInventoryModel;

class WarehouseProductsInventoryEloquent extends BaseServiceEloquent
{

    public function __construct()
    {
        $model = new WarehouseProductsInventoryModel();
        parent::__construct($model);
    }

    public static function update(WarehouseProductsInventoryModel $obj, $before = null): WarehouseProductsInventoryModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        ExportWarehouseProductsInventoryEloquent::create([
            'quantity' => $before['quantity'] - $after['quantity'],
            'warehouse_product_id' => $obj->warehouse_product_id,
            'type' => 0,
            'created_at' => time(),
        ]);
        return $obj;
    }
    public static function getByWarehouseProductsId($id){
        $query = self::newQuery();
        $query = $query->where('warehouse_product_id', $id);
        return $query->first();
    }

    public static function updateQuantity($id, $data){
        $query = self::newQuery();
        $query = $query->where('id', $id);
        $query->update([
            'quantity' => $data->get('quantity'),
            'status' => $data->get('status'),
        ]);
        return $query->where('id', $id)->first();
    }

    public static function updatedQuantity($id, $data){
        $query = self::newQuery();
        $query = $query->where('id', $id);
        $query->update($data);
        return $query->where('id', $id)->first();
    }

    public static function createQuantity($warehouseProductsId, $data){
        $query = self::newQuery();
        $id = $query->insertGetId([
            'quantity' => $data->get('quantity'),
            'status' => $data->get('status'),
            'warehouse_product_id' => $warehouseProductsId
        ]);
        return $query->where('id', $id)->first();
    }

    public static function createdQuantity($data){
        $query = self::newQuery();
        $id = $query->insertGetId($data);
        return $query->where('id', $id)->first();
    }
}
