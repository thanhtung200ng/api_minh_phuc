<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Http\Repositories\Enums\PurchaseOrderCode;
use App\Http\Repositories\Enums\ResourceCode;
use App\Models\CartItemsModel as THIS;

class CartItemsServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new THIS();
        parent::__construct($model);
    }

    static function getByVoucherGroupIdBigCorpId($bigCorpId, $voucherGroupId) {
        return self::newQuery()->whereHas('cart', function ($q) use ($bigCorpId) {
            $q->where('bigcorp_id', '=', $bigCorpId)->where('status', '=', PurchaseOrderCode::getStatusShopping());
        })->where([
            ['voucher_group_id', '=', $voucherGroupId],
        ])->first();
    }

    public static function create($objToSave)
    {
        $objToSave['created_at'] = time();
        $id = self::newQuery()->insertGetId($objToSave);
        //log
        return $id;
    }

    public static function update(THIS $obj, $before = null): THIS
    {
        //$objToSave = $obj->getDirty();
        $obj->save();
        //$after = $obj->toArray();
        //LogServiceEloquent::getInstance()->create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $objToSave, $before, $after, null, auth('api_bigcorp')->id());
        return $obj;
    }

    public static function delete(int $id): int
    {
        $instance = self::getInstance();
        $before = self::newQuery()->find($id);
        return $before->delete();
    }
}
