<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Models\MoneyRankMpMartModel;
use Illuminate\Database\Eloquent\Model;

class MoneyRankMpMartServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new MoneyRankMpMartModel();
        parent::__construct($model);
    }

    public static function getById($id, $selects = [])
    {
        $query = self::newQuery();
        if ($selects) {
            $query = self::processSelect($query, $selects);
        }
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getByIdRanks($id)
    {
        $query = self::newQuery();
        $query = $query->where('rank_id', $id);
        $query = $query->with('rank');
        return $query->first();
    }

    public static function getByMaxMinMoney(float $money)
    {
        $query = self::newQuery();
        $query = $query->with('rank')->has('rank')
            ->where('min_money', '<=', $money)
            ->where('max_money', '>', $money)
            ->where('status', self::getStatusActive());
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $selects = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $query = self::processSelect($query, $selects);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }
}
