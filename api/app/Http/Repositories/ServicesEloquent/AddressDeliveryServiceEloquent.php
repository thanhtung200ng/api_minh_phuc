<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\AgentModel;
use App\Models\DeliveryAddressModel as THIS;

class AddressDeliveryServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new THIS();
        parent::__construct($model);
    }

    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getByIdAndWith($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->with('city', 'district', 'ward', 'customer')->where('id', $id);
        return $query->first();
    }

    public static function delete(THIS $obj): THIS
    {
        $before = $obj->getOriginal();
        $obj->status = self::getStatusDeleted();
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::DELETE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }
}
