<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\Customer;
use App\Models\InventoryImportModel;
use Illuminate\Database\Eloquent\Model;

class InventoryImportServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new InventoryImportModel();
        parent::__construct($model);
    }

    public static function create($obj){
        $obj['created_at'] = time();
        $id = self::newQuery()->insertGetId($obj);
        $obj['id'] = $id;
        LogAdminService::create($id, ResourceCode::CREATE, self::getInstance()->model->getTable(), $obj, [], $obj);
        return $obj;
    }

    public static function update(InventoryImportModel $obj, $before = null): InventoryImportModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }
}
