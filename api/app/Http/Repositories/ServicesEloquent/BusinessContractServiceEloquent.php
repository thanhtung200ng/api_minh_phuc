<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\BusinessContractModel;
use App\Models\Customer;

class BusinessContractServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new BusinessContractModel();
        parent::__construct($model);
    }

    public static function getById($id)
    {
        $query = self::newQuery();
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function create($objToSave){
        $objToSave['created_at'] = time();
        return self::newQuery()->insertGetId($objToSave);
    }

    public static function update(BusinessContractModel $obj, $before = null): BusinessContractModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }
}
