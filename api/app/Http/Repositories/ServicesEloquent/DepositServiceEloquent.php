<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Elibs\Date;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\BaseModel;
use App\Models\DepositModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DepositServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new DepositModel();
        parent::__construct($model);
    }

    public static function calcTotalRevenueThreeMonth($customerIdArray = [], $status = false) {
        if (!$status) {
            $status = self::getStatusActive();
        }
        $select = [
            DB::raw('SUM(ui_deposits.money) as sum'), DB::raw('COUNT(ui_deposits.id) as total'), 'ui_deposits.status', 'ui_deposits.to_customer_id', 'ui_deposits.to_customer_wallet_id',
        ];
        $three_month_ago = strtotime("-3 month"); // 3 tháng trước
        $where = [
            ['member_approved_at', '>=', $three_month_ago],
        ];
        return self::newQuery()->select($select)->where($where)->whereHas('toCustomerWallet', function ($q) {
            $q->where('wallet_id', WalletServiceEloquent::getInstance()->model->getViHopTac());
        })->whereIn('to_customer_id', $customerIdArray)->groupBy(['to_customer_id', 'status', 'to_customer_wallet_id'])->get();

    }

    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->with(['fromCustomer.moneyRankMpMartTotalRevenue.rank', 'toCustomer.moneyRankMpMartTotalRevenue.rank', 'fromCustomerWallet.wallet', 'toCustomerWallet.wallet']);
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = $query->with(['fromCustomer.moneyRankMpMartTotalRevenue.rank', 'toCustomer.moneyRankMpMartTotalRevenue.rank', 'fromCustomerWallet.wallet', 'toCustomerWallet.wallet']);
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function create($deposit){
        $deposit['created_at'] = time();
        $id = self::newQuery()->insertGetId($deposit);
        LogAdminService::create($id, ResourceCode::CREATE, DepositModel::getInstance()->getTable(), $deposit, [], $deposit);
        return $id;
    }

    public static function update(DepositModel $obj, $before = null): DepositModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }

    public static function deleted($id){
       $query =  self::newQuery();
       $query = $query->where('id', $id);
       $before = $query->first();
       $query->update([
           'status' => BaseModel::getStatusDeleted(),
           'member_deleted_by' => Auth()->user()->id,
           'deleted_at' => time()
       ]);
       $after = $query->first();
       LogAdminService::create($id, ResourceCode::DELETE, DepositModel::getInstance()->getTable() ,$after->status,$before,$after);
       return $after;
    }

    public static function restoreDeletedState($id){
        $query =  self::newQuery();
        $query = $query->where('id', $id);
        $before = $query->first();
        $query->update(['status' => BaseModel::getStatusInActive()]);
        $after = $query->first();
        LogAdminService::create($id, ResourceCode::RESTORE, DepositModel::getInstance()->getTable() ,$after->status,$before,$after);
        return $after;
    }


}
