<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\PostModel as THIS;

class PostServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new THIS();
        parent::__construct($model);
    }

    public static function create($obj)
    {
        $id = self::newQuery()->insertGetId($obj);
        LogAdminService::getInstance()->create($id, ResourceCode::CREATE, self::getInstance()->model->getTable(), $obj, [], $obj);
        return $id;
    }

    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function update(THIS $obj, $before = null): THIS
    {
        $objToSave = $obj->getDirty();
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::getInstance()->create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $objToSave, $before, $after);
        return $obj;
    }

    public static function delete(int $id): int
    {
        $instance = self::getInstance();
        $BeforeDelete = self::newQuery()
            ->find($id);
        LogAdminService::create($id, ResourceCode::UPDATE, $instance->model->getTable(),[], $BeforeDelete, Auth()->user()->id);
        return self::newQuery()
            ->where('id', $id)
            ->update([
                'status' => self::getStatusDeleted()
            ]);
    }
}
