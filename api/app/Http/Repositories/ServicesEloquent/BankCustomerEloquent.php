<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\AgentModel;
use App\Models\BankCustomerModel;
use App\Models\BaseModel;

class BankCustomerEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new BankCustomerModel();
        parent::__construct($model);
    }

    public static function create($obj){
        $id = self::newQuery()->insertGetId($obj);
        LogAdminService::create($id, ResourceCode::CREATE, 'ui_customer_bank', $obj, [], $obj);
        return $id;
    }

    public static function update($obj, $before = null){
        $obj->save();
        return $obj;
    }

    public static function getByCustomerId($customerId){
        $query = self::newQuery();
        $query = $query->where(['status' => BaseModel::getStatusActive(), 'customer_id' => $customerId]);
        $query = $query->with('bank');
        return $query->get();
    }
    public static function getBankByCustomerId($customerId){
        $query = self::newQuery();
        $query = $query->where(['status' => BaseModel::getStatusActive(), 'customer_id' => $customerId]);
        return $query->get();
    }
    public static function getByIdAndCustomerId($bankId, $customerId){
        $query = self::newQuery();
        $query = $query->where(['status' => BaseModel::getStatusActive(),'customer_id' => $customerId, 'id' => $bankId ]);
        $query = $query->with('bank');
        return $query->first();
    }

    public static function getBankByAccountNumber($account){
        $query = self::newQuery();
        $query = $query->where('account_number',$account);
        return $query->first();
    }

}
