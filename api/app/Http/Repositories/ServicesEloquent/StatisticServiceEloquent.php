<?php


namespace App\Http\Repositories\ServicesEloquent;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class StatisticServiceEloquent extends BaseServiceEloquent
{


    public static function statisticDeposit($filter){

        $query = DB::table('ui_deposits');

        $query = self::statisticInDate('week',$query,'created_at');
    }
}
