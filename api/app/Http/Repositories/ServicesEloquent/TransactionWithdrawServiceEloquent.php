<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\TransactionWithdrawModel;

class TransactionWithdrawServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new TransactionWithdrawModel();
        parent::__construct($model);
    }

    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->where('id', $id);
        return $query->first();
    }


    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = $query->with(['fromCustomer.moneyRankMpMartTotalRevenue.rank',
            'toCustomer.moneyRankMpMartTotalRevenue.rank',
            'fromCustomerWallet.wallet', 'toCustomerWallet.wallet', 'deposit']);
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    /**
     * @param array $transaction
     * @return int
     */
    public static function create(array $transaction): int
    {
        return self::newQuery()->insertGetId([
            'withdraw_id' => $transaction['withdraw_id'],
            'before_money' => $transaction['before_money'],
            'after_money' => $transaction['after_money'],
            'before_balance_from_customer_wallet' => $transaction['before_balance_from_customer_wallet'],
            'after_balance_from_customer_wallet' => $transaction['after_balance_from_customer_wallet'],
            'before_balance_to_customer_wallet' => $transaction['before_balance_to_customer_wallet'],
            'after_balance_to_customer_wallet' => $transaction['after_balance_to_customer_wallet'],
            'fee' => $transaction['fee'],
            'tax' => $transaction['tax'],
            'from_customer_id' => $transaction['from_customer_id'],
            'to_customer_id' => $transaction['to_customer_id'],
            'from_customer_wallet_id' => $transaction['from_customer_wallet_id'],
            'to_customer_wallet_id' => $transaction['to_customer_wallet_id'],
            'status' => $transaction['status'],
            'member_created_at' => $transaction['member_created_at'],
            'member_created_by' => $transaction['member_created_by'],
            'transaction_code' => $transaction['transaction_code'],
        ]);
    }

    public static function update($id, $save = null): TransactionWithdrawModel
    {
        $query = self::newQuery();
        $query->where('withdraw_id', $id)->update($save);
        return $query->where('withdraw_id', $id)->first();
    }

    public static function approved(TransactionWithdrawModel $obj, $before = null): TransactionWithdrawModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }
}
