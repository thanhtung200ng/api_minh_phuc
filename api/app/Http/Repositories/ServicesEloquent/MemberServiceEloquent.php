<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\MemberFactory;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\Member;

class MemberServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new Member();
        parent::__construct($model);
    }
    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->where('id', $id);
        return $query->first();
    }
    public function getByEmail($email) {
        $query = self::newQuery();
        $query = $query->where('email', $email);
        return $query->first();
    }
    public function getByPhone($phone) {
        $query = self::newQuery();
        $query = $query->where('mobile', $phone);
        return $query->first();
    }

    public static function getByIdAndWith($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->with('city', 'district', 'ward', 'customer')->where('id', $id);
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function getAllActive()
    {
        $query = self::newQuery();
        return $query->get();
    }

    public static function getByCustomer($id){
        $query = self::newQuery();
        $query = $query->where('customer_id', $id);
        return $query->first();
    }

    public static function create($obj){
        $id = self::newQuery()->insertGetId($obj);
        LogAdminService::create($id, ResourceCode::CREATE, 'ui_agents', $obj, [], $obj);
        return $id;
    }

    public static function update(Member $obj, $before = null): Member
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }

}
