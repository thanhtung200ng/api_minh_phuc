<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\WarehouseProductsModel;

class WarehouseProductsEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new WarehouseProductsModel();
        parent::__construct($model);
    }

    public function create ($obj) {
        $query = self::newQuery();
        $id = $query->insertGetId($obj);
        $after = $query->where('id', $id)->first();
        LogAdminService::create($id, ResourceCode::CREATE, 'ui_warehouse_products', $obj, [], $after);
        return $after;
    }
}
