<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Models\City;
use App\Models\District;
use App\Models\LocationModel;
use App\Models\Ward;
use Illuminate\Database\Eloquent\Model;

class LocationServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        if(isset($entity->district_id)) {
            $model = new Ward();
        }elseif (isset($entity->city_id)) {
            $model = new District();
        }else {
            $model = new City();
        }
        parent::__construct($model);
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = $query->with(['fromCustomer.moneyRankMpMartTotalRevenue.rank', 'toCustomer.moneyRankMpMartTotalRevenue.rank', 'fromCustomerWallet.wallet', 'toCustomerWallet.wallet']);
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }
}
