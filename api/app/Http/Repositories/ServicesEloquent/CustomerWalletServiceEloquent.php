<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\BaseModel;
use App\Models\CustomerWalletModel;
use App\Models\WalletModel;
use App\Models\WithdrawMoneyModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class CustomerWalletServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new CustomerWalletModel();
        parent::__construct($model);
    }

    public static function initNewModel(&$obj, $customerId) {
        $obj = new CustomerWalletModel();
        $obj->wallet_id = app(WalletModel::class)->getViTangDiem();
        $obj->customer_id = $customerId;
        $obj->created_by = $customerId;
        $obj->current_balance = 0;
        $obj->created_at = time();
        $obj->status = CustomerWalletModel::getStatusActive();

        if (auth()->check()) {
            $obj->member_approved_by = auth()->id();
            $obj->member_approved_at = time();
        }
        CustomerWalletServiceEloquent::update($obj, $obj->getOriginal());
    }

    public static function getById($id, $selects = [])
    {
        $query = self::newQuery();
        if ($selects) {
            $query = self::processSelect($query, $selects);
        }
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $selects = [])
    {
        $query = self::newQuery();
        if ($orders) {
            $query = self::processOrder($query, $orders);
        }
        $query = self::processFilter($query, $filters);
        if ($selects) {
            $query = self::processSelect($query, $selects);
        }
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function create($wallet){
        $id = self::newQuery()->insertGetId($wallet);
        LogAdminService::create($id, ResourceCode::CREATE, 'ui_customer_wallets', $wallet, [], $wallet);
        return $id;
    }

    public static function update(CustomerWalletModel $obj, $before = null): CustomerWalletModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }


    /** Lấy danh sách ví của khách hàng theo danh sách wallet_id
     * @param int $customerId
     * @param array $walletIds
     * @return mixed
     */
    public static function getListByGroupWalletId(int $customerId, array $walletIds) {
        $query = self::newQuery();
        $query = $query->with('wallet')->whereIn('wallet_id', $walletIds);
        $query = $query->where('customer_id', $customerId);
        return $query->get();
    }

    /** Lấy ví của khách hàng theo code
     * @param int $customerId
     * @param int|string $code
     * @return mixed
     */
    public static function getByCustomerIdAndTransactionCode(int $customerId, $code) {
        $query = self::newQuery();
        $query = $query->where('customer_id', $customerId);
        $query = $query->where('group_transaction_code', 'LIKE', '%'.$code.'%');
        return $query->first();
    }

    /** Lấy danh sách ví để tạo cho khách hàng đăng kí phát triển hệ thống (PTHT)
     * @param $wallets
     * @param int $customerId
     * @param int|null $created_by
     * @param int|null $member_approved_by
     * @return array
     */
    public static function getWalletToSaveForCustomerMPMart(&$wallets, int $customerId, int $created_by = null, int $member_approved_by = null) : array {
        $walletIds = [app(WalletModel::class)->getViHoaHong(), app(WalletModel::class)->getViKhoDiem(),
            app(WalletModel::class)->getViHopTac(), app(WalletModel::class)->getViSi(),
            app(WalletModel::class)->getViLoiNhuan(), app(WalletModel::class)->getViTongDoanhThu(), app(WalletModel::class)->getViTangDiem(),
        ];
        $currentWallet = self::newQuery()->whereIn('wallet_id', $walletIds)->where('customer_id', $customerId)->get()->keyBy('wallet_id');
        foreach ($walletIds as $wallet_id) {
            if (!isset($currentWallet[$wallet_id])) {
                $wallet = [
                    'wallet_id' => $wallet_id,
                    'customer_id' => $customerId,
                    'current_balance' => 0,
                    'created_at' => time(),
                    'created_by' => $created_by,
                    'member_approved_by' => $member_approved_by,
                    'status' => CustomerWalletModel::getStatusActive(),
                ];
                if ($member_approved_by) {
                    $wallet['member_approved_at'] = time();
                }

                $wallets[] = $wallet;
            }
        }
        return $wallets;
    }

    /** Lấy danh sách ví để tạo cho khách hàng đăng kí phát triển hệ thống (PTHT)
     * @param $wallets
     * @param int $customerId
     * @param int|null $created_by
     * @param int|null $member_approved_by
     * @return array
     */
    public static function getWalletToSaveForCustomerSeller(&$wallets, int $customerId, int $created_by = null, int $member_approved_by = null) : array {
        $walletIds = [app(WalletModel::class)->getViHoaHong(), app(WalletModel::class)->getViKyQuy(),
            app(WalletModel::class)->getViLoiNhuan(),
        ];
        $currentWallet = self::newQuery()->whereIn('wallet_id', $walletIds)->where('customer_id', $customerId)->get()->keyBy('wallet_id');
        foreach ($walletIds as $wallet_id) {
            if (!isset($currentWallet[$wallet_id])) {
                $wallet = [
                    'wallet_id' => $wallet_id,
                    'customer_id' => $customerId,
                    'current_balance' => 0,
                    'created_at' => time(),
                    'created_by' => $created_by,
                    'member_approved_by' => $member_approved_by,
                    'status' => CustomerWalletModel::getStatusActive(),
                ];
                if ($member_approved_by) {
                    $wallet['member_approved_at'] = time();
                }

                $wallets[] = $wallet;
            }
        }
        return $wallets;
    }
    public static function getWalletToSaveForCustomerInvestor(&$wallets, int $customerId, int $created_by = null, int $member_approved_by = null) : array {
        $walletIds = [app(WalletModel::class)->getViDauTu(), app(WalletModel::class)->getViLoiNhuan(),
            app(WalletModel::class)->getViLoiNhuan(),
        ];
        $currentWallet = self::newQuery()->whereIn('wallet_id', $walletIds)->where('customer_id', $customerId)->get()->keyBy('wallet_id');
        foreach ($walletIds as $wallet_id) {
            if (!isset($currentWallet[$wallet_id])) {
                $wallet = [
                    'wallet_id' => $wallet_id,
                    'customer_id' => $customerId,
                    'current_balance' => 0,
                    'created_at' => time(),
                    'created_by' => $created_by,
                    'member_approved_by' => $member_approved_by,
                    'status' => CustomerWalletModel::getStatusActive(),
                ];
                if ($member_approved_by) {
                    $wallet['member_approved_at'] = time();
                }

                $wallets[] = $wallet;
            }
        }
        return $wallets;
    }

    /** Lấy danh sách ví mua hàng
     * @param $wallets
     * @param int $customerId
     * @return array
     */
    public static function getWalletPurchaseByAuth(int $customerId) {
        $walletIds = [app(WalletModel::class)->getViHoaHong(), app(WalletModel::class)->getViKhoDiem(),
            app(WalletModel::class)->getViHopTac(), app(WalletModel::class)->getViTieuDung(),
            app(WalletModel::class)->getViLoiNhuan(),
        ];
        return self::newQuery()->with('wallet')
            ->whereHas('wallet', function ($q) {
                $q->whereStatus(WalletServiceEloquent::getStatusActive());
            })
            ->whereIn('wallet_id', $walletIds)->where('customer_id', $customerId)->get()->keyBy('wallet_id');
    }

    /** Lấy danh sách ví
     * @param int $customerId
     * @return mixed
     */
    public static function getWalletByAuth(int $customerId)
    {
        return self::newQuery()->with('wallet')->where([
            ['status', '=', self::getStatusActive()],
            ['customer_id', '=', $customerId],
        ])->get();
    }



    public static function getWalletAllByAuth(int $customerId) {

        return self::newQuery()->with('wallet')
            ->whereHas('wallet', function ($q) {
                $q->whereStatus(WalletServiceEloquent::getStatusActive());
            })->where('customer_id', $customerId)->get()->keyBy('wallet_id');
    }

    public static function updateBalanceCustomerWallet($before, $after , $obj): CustomerWalletModel
    {
        self::newQuery()->where('id', $obj->id)->update([
            'current_balance' => $after,
        ]);
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }

    public static function updateMoneyToCustomer ($obj) {
        $query = self::newQuery();
        $obj['updated_at'] = time();
        $before = $query->where('id', $obj['id'])->first();
        $query->where('id', $obj['id'])->update($obj);
        $after = $query->where('id', $obj['id'])->first();
        LogAdminService::create($obj['id'], ResourceCode::UPDATE, 'ui_customer_wallets', $obj, $before, $after);
    }

    public static function updateBalanceCustomerWallet_v2($beforeMoney, $afterMoney , $obj)
    {
        $query = self::newQuery();
        $before = $query->where('id', $obj->id)->first();
        $query->where('id', $obj->id)->update([
            'current_balance' => $afterMoney,
        ]);
        $after = $query->where('id', $obj->id)->first();
        LogAdminService::create($obj->id, ResourceCode::UPDATE,'ui_customer_wallets', $obj, $before, $after);
        return $after;
    }

    public function getWalletWidthDraw($customerId) {
        $query = self::newQuery();
        $query = $query->where('status', BaseModel::getInstance()->getStatusActive());
        $query = $query->where('customer_id', $customerId);
        $query = $query->whereIn('wallet_id', WithdrawMoneyModel::getInstance()->walletWidthDraw())->orderBy('id','DESC');
        $query = $query->with('wallet');
        return $query->get();
    }


    public function getWalletDeposit($customerId)
    {
        $query = self::newQuery();
        $query = $query->where('status', BaseModel::getInstance()->getStatusActive());
        $query = $query->where('customer_id', $customerId);
        $query = $query->whereIn('wallet_id', WithdrawMoneyModel::getInstance()->walletDeposit())->orderBy('id', 'DESC');
        $query = $query->with('wallet');
        return $query->get();
    }


        public static function getByCustomerIdAndWalletId(int $customerId,$walletId) {
        $query = self::newQuery();
        $query = $query->where(['customer_id' => $customerId , 'id' => $walletId ]);
        return $query->first();
    }
}
