<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Models\BankModel;
use App\Models\BaseModel;

class BankEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new BankModel();
        parent::__construct($model);
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $selects = [])
    {

        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function getBankById($bankId){
        $query = self::newQuery();
        $query = $query->where( 'id' ,'=',$bankId);
        return $query->get();
    }


}
