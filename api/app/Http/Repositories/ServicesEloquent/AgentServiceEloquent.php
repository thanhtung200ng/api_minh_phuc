<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\AgentModel;
use App\Models\BaseModel;
use App\Models\CustomerWalletModel;
use Illuminate\Database\Eloquent\Model;

class AgentServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new AgentModel();
        parent::__construct($model);
    }

    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getByIdAndWith($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->with('city', 'district', 'ward', 'customer')->where('id', $id);
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }
    public static function getByCustomer($id){
        $query = self::newQuery();
        $query = $query->where('customer_id', $id);
        return $query->first();
    }

    public static function getByListCustomer($listId){
        $query = self::newQuery();
        $query = $query->whereIn('customer_id', $listId);
        return $query->get();
    }

    public static function create($obj){
        $id = self::newQuery()->insertGetId($obj);
        LogAdminService::create($id, ResourceCode::CREATE, 'ui_agents', $obj, [], $obj);
        return $id;
    }

    public static function createAgency($obj){
        $query = self::newQuery();
        $obj['created_at'] = time();
        $id = $query->insertGetId($obj);
        $after = $query->where('id', $id)->first();
        LogAdminService::create($id, ResourceCode::CREATE, 'ui_agents', $obj, [], $after);
        return $after;
    }

    public static function updateAgency($obj){
        $query = self::newQuery();
        $obj['updated_at'] = time();
        $before = $query->where('id', $obj['id'])->first();
        $query->where('id', $obj['id'])->update($obj);
        $after = $query->where('id', $obj['id'])->first();
        LogAdminService::create($obj['id'], ResourceCode::UPDATE, 'ui_agents', $obj, $before, $after);
        return $after;
    }

    public static function update(AgentModel $obj, $before = null): AgentModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }

}
