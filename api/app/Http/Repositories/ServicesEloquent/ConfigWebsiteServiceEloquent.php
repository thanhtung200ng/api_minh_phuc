<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\ConfigWebsiteModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\DocBlock\Tags\See;

class ConfigWebsiteServiceEloquent extends BaseServiceEloquent
{
    protected $web ;
    public function __construct()
    {
        $model = new ConfigWebsiteModel();
        $this->web = $model->newQuery()->first();
        parent::__construct($model);
    }


    public function getHotline(): string {
        return $this->web->hotline ?? '';
    }

    public function getCompanyName(): string {
        return $this->web->company_name ?? '';
    }

    public function getAccountHolder(): string {
        return $this->web->company_bank_account ?? '';
    }

    public function getBank(): string {
        return $this->web->bank ?? '';
    }

    public function getStkBank(): string {
        return $this->web->company_bank ?? '';
    }

    static function getCompanyBank(): array {
        return [
            [
                'name' => self::getInstance()->getBank(),
                'account_number' => self::getInstance()->getStkBank(),
                'account_name' => self::getInstance()->getAccountHolder()
            ]
        ];
    }

    static function getTransferContent($msg): string {
        return 'QHMART NAPDIEM ' .$msg;
    }

    static function getTransferContentForDeposit($msg): string {
        return 'QHMART NAPDIEM ' .$msg;
    }


    static function getTransferContentForPo($msg): string {
        return 'QHMART MUAHANG ' .$msg;
    }

    public static function get()
    {
        return self::newQuery()->first();
    }

    public static function create($config)
    {
        $query = self::newQuery();
        $id = $query->insertGetId($config);
        $after = $query->where('id', $id)->first();
        LogAdminService::create($id, ResourceCode::CREATE, 'ui_warehouse_products', $config, [], $after);
        return $after;
    }

    public static function update($config)
    {
        $query = self::newQuery();
        $query = $query->where('id', 1);
        $query->update($config);
        $after = $query->where('id', 1)->first();
        LogAdminService::create(1, ResourceCode::CREATE, 'ui_warehouse_products', $config, [], $after);
        return $after;
    }

    /**
     * @makeBy @kayn
     * @param int $parentId
     * @return Collection
     */
    public static function getAllSubCategories(int $parentId): Collection
    {
        $query = self::newQuery();
        return $query
            ->where('parent_id', $parentId)->where('status', self::getStatusActive())
            ->get();
    }

    public static function getAllFeatured($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $selects = [])
    {
        $query = self::newQuery();
        $query = $query->with(['products' => function ($p) {
            return $p->select('id', 'status', 'category_id', 'views')->with('variants')->
            whereHas('variants', function($query) {
                $query->where('is_featured', 1);
            })->orderBy('id', 'DESC');
        }])->has('products')->where('is_featured', 1);
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        if ($selects) {
            $query = self::processSelect($query, $selects);
        }
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get()->map(function($request) {
            $request->setRelation('products', $request->products->take(21));
            return $request;
        });
    }
}
