<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Entities\Product;
use App\Http\Repositories\Entities\ProductVariants;
use App\Http\Repositories\Factories\ProductFactory;
use App\Http\Repositories\Factories\ProductVariantsFactory;
use App\Models\BaseModel;
use App\Models\ProductVariantModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductVariantsServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new ProductVariantModel();
        parent::__construct($model);
    }

    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = $query->with('salesBooth');
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function getAllByCty()
    {
        $query = self::newQuery();
        $where = [
            'status' => BaseModel::getInstance()->getStatusActive(),
            'sales_booth_id' => 1  // @todo nvcong
        ];
        $query->where($where);
        return $query->get();
    }

    public static function delete($id)
    {
        $query = self::newQuery();
        $query->where('id', $id)->update(
            [
                'status' => self::getStatusDeleted(),
                'updated_at' => time(),
            ]
        );
    }

    public static function getAllProductItem($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get()->groupBy('product_id');
        return $entities;
    }


    public static function getAllProductHotSale($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query->where('ui_product_items.is_hot_sale', 1)->join('ui_products', 'ui_products.id' , '=' ,'ui_product_items.product_id');
        $entities = $query->get();
        return $entities;
    }

        public static function getAllProductHotOfCty($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query->where('status', 1)->orderBy('created_at', 'desc');
        $entities = $query->get();
        return $entities;
    }

    public static function create($variant){
        $id = self::newQuery()->insertGetId([
            'product_id' => $variant['product_id'],
            'sku' => $variant['sku'],
            'name' => $variant['name'],
            'slug' => Str::slug($variant['name']),
            'price' => $variant['price'],
            'status' => request('status', 0),
            'price_sale' => $variant['price_sale'],
            'sales_booth_id' => $variant['sales_booth_id'],
            'quantity' => $variant['quantity'],
            'images' => json_encode($variant['images']),
            'ranks' => @$variant['ranks'] ? json_encode($variant['ranks']) : '',
            'created_at' => time(),
            'is_choose_agent' => $variant['is_choose_agent'],
            'discount_product_by_quantity' => $variant['discount_product_by_quantity'],
            //'is_mpmart' => (int)isset($variant['is_mpmart']),
            'is_discount' => (int)isset($variant['is_discount']) ?: 0,
            'is_featured' => (int)isset($variant['is_featured']) ?: 0,
            'is_hot_sale' => (int)isset($variant['is_hot_sale']) ?:0,
            'avatar' => isset($variant['avatar']) ? json_encode($variant['avatar']) : 0,
            'property_item_id' => isset($variant['property_item_id']) ? json_encode($variant['property_item_id']) : 0,
            'gtin' => (int)isset($variant['gtin']) ?? 0,
        ]);
        return self::newQuery()->where('id',$id )->first();
    }

    public static function update($variant,$id){
        $query = self::newQuery();
        $query->where([['product_id', $id],['id', $variant['id']]])->update([
            'product_id' => $id,
            'sku' => $variant['sku'],
            'name' => $variant['name'],
            'price' => $variant['price'],
            'status' => request('status', 0),
            'price_sale' => $variant['price_sale'],
            'sales_booth_id' => $variant['sales_booth_id'],
            'quantity' => $variant['quantity'],
            'images' => json_encode($variant['images']),
            'ranks' => json_encode($variant['ranks']),
            'created_at' => time(),
            'is_choose_agent' => $variant['is_choose_agent'],
            'discount_product_by_quantity' => $variant['discount_product_by_quantity'],
            //'is_mpmart' => (int)isset($variant['is_mpmart']),
            'is_discount' => (int)$variant['is_discount'],
            'is_featured' => (int)$variant['is_featured'],
            'avatar' => isset($variant['avatar']) ? json_encode($variant['avatar']) : null,
            'property_item_id' => isset($variant['property_item_id']) ? json_encode($variant['property_item_id']) : 0,
            'gtin' => (int)isset($variant['gtin']) ?? 0,
        ]);
        return $query->where('id', $variant['id'])->first();
    }

    public static function getAllProductVariants($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []): \Illuminate\Support\Collection
    {
        $query = self::newQuery();
        $query = $query->leftJoin('ui_product_items as pi', function ($join) {
            $join->on('p.id', '=', 'pi.product_id');
        })->leftJoin('ui_categories as c', function ($join) {
            $join->on('c.id', '=', 'p.category_id');
        })->leftJoin('ui_customers as cus', function ($join) {
            $join->on('cus.id', '=', 'pi.seller_by');
        })->leftJoin('ui_salesbooth as sb', function ($join) {
            $join->on('sb.id', '=', 'pi.sales_booth_id');
        })->select('cus.full_name as seller_name', 'cus.id as seller_id',
            'p.id as product_id', 'c.id as category_id', 'c.name as category_name', 'c.slug as category_slug',
            'pi.id', 'pi.price', 'pi.price_sale', 'pi.slug',
            'pi.name', 'pi.sku', 'pi.quantity', 'pi.status', 'pi.images','sb.id as salesbooth_id','sb.name as salesbooth_name','sb.address as salesbooth_address',
            'pi.is_choose_agent as choose_agent'
        );
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $total = $query->count();

        return $query->get();
    }
    public static function getAllProductFeatured($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []): \Illuminate\Support\Collection
    {
        $query = app('db')->table('ui_products as p');
        $query = $query->leftJoin('ui_product_items as pi', function ($join) {
            $join->on('p.id', '=', 'pi.product_id');
        })->leftJoin('ui_categories as c', function ($join) {
            $join->on('c.id', '=', 'p.category_id');
        })->leftJoin('ui_customers as cus', function ($join) {
            $join->on('cus.id', '=', 'pi.seller_by');
        })->select('cus.full_name as seller_name', 'cus.id as seller_id',
            'p.id as product_id', 'c.id as category_id', 'c.name as category_name', 'c.slug as category_slug',
            'pi.id', 'pi.price', 'pi.price_sale', 'pi.slug',
            'pi.name', 'pi.sku', 'pi.quantity', 'pi.status', 'pi.images', 'pi.is_featured',
        );
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $total = $query->count();

        return $query->get();
    }

    public static function getProductsByProductsItem($id){
        if(!$id){
            return null;
        }
        $query = DB::table('ui_products');
        return $query->where('id', $id)->first();
    }


    public static function getByListSku($listSku, $listId = null)
    {
        $query = self::newQuery();
        if(!$listSku){
            $listSku = 0;
        }
        if(!empty($listId)){
            $query = $query->whereNotIn('id', $listId);
        }
        $query = $query->whereIn('sku', $listSku);
        return $query->get();
    }


    public static function getByListProductId($listId)
    {
        $query = self::newQuery();
        $query = $query->whereIn('id', $listId);
        return  $query->get();
    }

    public static function getProductItemIdAndNotSellerBoot($id)
    {
        $query = self::newQuery();
        $query = $query->where('sales_booth_id', 1);
        $query = $query->where('id', $id);
        return  $query->first();
    }

    public static function priceSaleWithoutDiscount($priceSale, $discount){
        return $priceSale/(1-($discount/100));
    }
}
