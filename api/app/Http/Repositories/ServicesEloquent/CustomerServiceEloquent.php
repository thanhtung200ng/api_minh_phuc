<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\BaseModel;
use App\Models\Customer;
use App\Models\CustomerWalletModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomerServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new Customer();
        parent::__construct($model);
    }

    public static function getById($id)
    {
        $query = self::newQuery();
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getByIdAndWith($id) {
        $query = self::newQuery();
        $query = $query->with(['wallets.wallet', 'wallets.totalMonthly' => function($q) {
            $q->orderBy(DB::raw("STR_TO_DATE(CONCAT('01-', time),'%d-%Y%m')"), 'DESC')->take(3);
        }, 'banks', 'city', 'district', 'ward', 'moneyRankMpMartTotalRevenue.rank']);
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getChildren($ref) {
        $query = self::newQuery();
        $query = $query->where('parent_referral_code', $ref);
        return $query->get();
    }
    public static function getlistChildren($ref) {
        $query = self::newQuery();
        $query = $query->with(['wallets.wallet', 'city', 'district', 'ward', 'moneyRankMpMartTotalRevenue.rank']);
        $query = $query->where('parent_referral_code', $ref);
        return $query;
    }

    public static function getByRef($id, $selects = [])
    {
        $query = self::newQuery();
        if ($selects) {
            $query = self::processSelect($query, $selects);
        }
        $query = $query->where('referral_code', $id);
        return $query->first();
    }

    public static function getBySellerRef($id, $selects = [])
    {
        $query = self::newQuery();
        if ($selects) {
            $query = self::processSelect($query, $selects);
        }
        $query = $query->where('seller_referral_code', $id);
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $selects = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $query = self::processSelect($query, $selects);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function getAllActive()
    {
        $query = self::newQuery();
        $query = $query->where('status', BaseModel::getStatusActive());
        $query = $query->whereNotNull('mpmart_rank');
        $query = $query->whereNotNull('city_id');
        $query = $query->whereNotNull('ward_id');
        $query = $query->whereNotNull('district_id');
        $query = $query->whereNotNull('street');
        return $query->get();
    }
    public static function getAllListActive($listId)
    {
        $query = self::newQuery();
        $query = $query->whereIn('id', $listId);
        $query = $query->where('status', BaseModel::getStatusActive());
        $query = $query->whereNotNull('mpmart_rank');
        $query = $query->whereNotNull('city_id');
        $query = $query->whereNotNull('ward_id');
        $query = $query->whereNotNull('district_id');
        $query = $query->whereNotNull('street');
        return $query->get();
    }

    public static function getAllCustomerByCty(){
        $listCustomer = self::newQuery()->where([['status', 1], ['is_mpmart', 1]])->select('full_name', 'account', 'id')->get();
        if(!$listCustomer){
            return null;
        }
        return $listCustomer;
    }

    public static function create($customer){
        $customer['created_at'] = time();
        return self::newQuery()->insertGetId($customer);
    }

    public static function update(Customer $obj, $before = null): Customer
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }
    public static function updateRank( $request, $id)
    {
        $rank = $request['rank']['id'];
        self::newQuery()->where('id', $id)->update([
            'mpmart_rank_total_revenue' => $rank,
        ]);
        $before = null;
        $obj = self::newQuery()->where('id', $id)->first();
        $after =$obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }

    public static function updateRankMpmat($ranks,$customer_id){
        $query = self::newQuery();
        $query->where('id', $customer_id)->update([
            'mpmart_rank' => json_encode($ranks),
        ]);
        return $query->where('id', $customer_id)->first();
    }

    public static function updateRankWareHouse($obj){
        $query = self::newQuery();
        $obj['updated_at'] = time();
        $before = $query->where('id', $obj['id'])->first();
        $query->where('id', $obj['id'])->update($obj);
        $after = $query->where('id', $obj['id'])->first();
        LogAdminService::create($obj['id'], ResourceCode::UPDATE,'ui_customers' , $obj, $before, $after);
        return $after;
    }
    public static function getTreeReverseByRefCode($referralCode, &$data)
    {
        $cur_item = self::newQuery()
            ->where('referral_code', $referralCode)->first();
        if(!empty($cur_item)) {
            $p_item = self::newQuery()
                ->where('referral_code', $cur_item->parent_referral_code)->first();
            $data[] = $cur_item;
            if(!empty($p_item)) {
                self::getTreeReverseByRefCode($p_item->referral_code, $data);
            }
        }
        return $data;
    }

}
