<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\BaseModel;
use App\Models\MoneyRankMpMartModel;
use App\Models\RankMpMartModel;
use Illuminate\Database\Eloquent\Model;

class MpMartRankServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new RankMpMartModel();
        parent::__construct($model);
    }

    public static function getById($id, $selects = [])
    {
        $query = self::newQuery();
        if ($selects) {
            $query = self::processSelect($query, $selects);
        }
        $query = $query->where('id', $id)->with(['moneyRank']);
        return $query->first();
    }

    public static function getAll($with = [],$offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $selects = [])
    {
        $query = self::newQuery();
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query = $query->where('status',BaseModel::getStatusActive());
        if (!empty($with)){
            $query = $query->with($with);
        }
        return $query->get();
    }
    public static function create($obj)
    {
        $query = self::newQuery();
        $id = $query->insertGetId($obj);
        LogAdminService::create($id, ResourceCode::CREATE, '__ranks_mpmart', $obj, [], $obj);
        return $id;
    }

    public static function update($obj, $before = null)
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, '__ranks_mpmart', $obj->getDirty(), $before, $after);
        return true;
    }

}
