<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\BaseModel;
use App\Models\ProductModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class ProductServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new ProductModel();
        parent::__construct($model);
    }

    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        if ($with) {
            $query = $query->with('variants', 'category', 'variants.productVariantByWallet', 'variants.salesBooth');
        }
        $query = $query->where(['id'=> $id]);
        return $query->first();
    }

    public static function getByIdAndWithNotWhere($id, $with = true)
    {
        $query = self::newQuery();
        if ($with) {
            $query = $query->with('variants', 'category', 'variants.productVariantByWallet', 'variants.salesBooth')->whereHas('variants', function ($q) {
                $q->orWhere('status', '!=', self::getStatusDeleted());
            });
        }
        $query = $query->where(['id'=> $id]);
        return $query->first();
    }

    public static function getByListId($lsid, $with = true)
    {
        $query = self::newQuery();
        if ($with) {
            $query = $query->with('variants', 'category', 'variants.productVariantByWallet', 'variants.salesBooth');
        }
            $query = $query->whereIn('id',$lsid);
        return $query->get();
    }


    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = $query->with('variants')->has('variants');
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query= $query->where('status','=',BaseModel::getStatusActive());
        return $query->get();
    }

    /**
     * Lấy danh sách sản phẩm ra ngoài trang chủ
     * @makeBy @kayn
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param array $orders
     * @param array $filters
     * @param array $selects
     * @return Collection
     */
    public static function getAllNotFeatured($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $selects = [])
    {
        $query = self::newQuery();
        $query = $query->select('id', 'status', 'category_id', 'views')->with('variants')->
        whereHas('variants', function($query) {
            $query->where('is_featured', '!=', 1);
        })->where('status', self::getStatusActive());
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function create($request){  // @todo nhớ tách ra để mà check quyền
        $id = self::newQuery()->insertGetId([
            'description' => $request->get('description'),
            'category_id' => $request->get('category_id'),
            'status' => $request->get('status', 0),
            'created_at' => time(),
            'created_by' => auth()->user()->id,
            'seo' => $request->get('seo') ?? '',
        ]);
        return self::newQuery()->where('id', $id)->first();
    }

    public static function update($request, $id){
        self::newQuery()->where('id', $id)->update([
            'description' => $request->get('description'),
            'category_id' => $request->get('category_id'),
            'status' => $request->get('status'),
            'created_at' => time(),
            'created_by' => auth()->user()->id,
            'seo' => $request->get('seo') ?? '',
        ]);
        return self::newQuery()->where('id', $id)->first();
    }
}
