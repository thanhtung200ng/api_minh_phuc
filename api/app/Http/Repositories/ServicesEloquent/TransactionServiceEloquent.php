<?php

namespace App\Http\Repositories\ServicesEloquent;

use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Services\LogAdminService;
use App\Models\DepositModel;
use App\Models\TransactionModel;
use Illuminate\Support\Facades\Auth;

class TransactionServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new TransactionModel();
        parent::__construct($model);
    }

    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = $query->with(['fromCustomer.moneyRankMpMartTotalRevenue.rank',
            'toCustomer.moneyRankMpMartTotalRevenue.rank',
            'fromCustomerWallet.wallet', 'toCustomerWallet.wallet', 'deposit']);
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    /**
     * @param array $transaction
     * @return int
     */
    public static function create(array $transaction): int
    {
        return self::newQuery()->insertGetId([
            'deposit_id' => $transaction['deposit_id'],
            'purchase_order_id' => @$transaction['purchase_order_id'],
            'ranks' => $transaction['ranks'],
            'first_money' => $transaction['first_money'],
            'last_money' => $transaction['last_money'],
            'before_balance_from_customer_wallet' => $transaction['before_balance_from_customer_wallet'],
            'after_balance_from_customer_wallet' => $transaction['after_balance_from_customer_wallet'],
            'before_balance_to_customer_wallet' => $transaction['before_balance_to_customer_wallet'],
            'after_balance_to_customer_wallet' => $transaction['after_balance_to_customer_wallet'],
            'discount' => $transaction['discount'],
            'fee' => $transaction['fee'],
            'from_customer_id' => $transaction['from_customer_id'],
            'to_customer_id' => $transaction['to_customer_id'],
            'from_customer_wallet_id' => $transaction['from_customer_wallet_id'],
            'to_customer_wallet_id' => $transaction['to_customer_wallet_id'],
            'transaction_code' => $transaction['transaction_code'],
            'status' => $transaction['status'],
            'member_created_at' => $transaction['member_created_at'],
            'member_created_by' => $transaction['member_created_by'],
            'approved_at' => @$transaction['approved_at'],
            'approved_by' => @$transaction['approved_by'],
        ]);
    }

    public static function update(DepositModel $obj, $before = null): DepositModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }
}
