<?php


namespace App\Http\Repositories\ServicesEloquent;


use App\Models\CategoryModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\DocBlock\Tags\See;

class CategoryServiceEloquent extends BaseServiceEloquent
{
    public function __construct()
    {
        $model = new CategoryModel();
        parent::__construct($model);
    }

    public static function getById($id, $selects = [])
    {
        $query = self::newQuery();
        if ($selects) {
            $query = self::processSelect($query, $selects);
        }
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function getBySlug($id, $selects = [])
    {
        $query = self::newQuery();
        if ($selects) {
            $query = self::processSelect($query, $selects);
        }
        $query = $query->where('status', self::getStatusActive())->where('slug', $id);
        return $query->first();
    }

    /**
     * @makeBy @kayn
     * @param int $parentId
     * @return Collection
     */
    public static function getAllSubCategories(int $parentId): Collection
    {
        $query = self::newQuery();
        return $query
            ->where('parent_id', $parentId)->where('status', self::getStatusActive())
            ->get();
    }

    public static function getAllFeatured($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $selects = [])
    {
        $query = self::newQuery();
        $query = $query->with(['products' => function ($p) {
            return $p->select('id', 'status', 'category_id', 'views')->with('variants')->
                whereHas('variants', function($query) {
                    $query->where('is_featured', 1);
                })->orderBy('id', 'DESC');
        }])->has('products')->where('is_featured', 1);
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        if ($selects) {
            $query = self::processSelect($query, $selects);
        }
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get()->map(function($request) {
            $request->setRelation('products', $request->products->take(21));
            return $request;
        });
    }
}
