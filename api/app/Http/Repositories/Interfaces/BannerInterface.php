<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Entities\Banner;
use Illuminate\Support\Collection;

interface BannerInterface
{

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return Banner[]|Collection
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Banner $banner
     * @return Banner $user
     */
    public static function create(Banner $banner): Banner;

    /**
     * @param Banner $banner
     * @return Banner $user
     */
    public static function update(Banner $banner): Banner;

    /**
     * @param int $bannerId
     * @return int
     */
    public static function delete(int $bannerId): int;
}
