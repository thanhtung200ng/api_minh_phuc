<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Location;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;

interface LocationInterface
{
    /**
     * @param string $type
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return Location[]|Collection
     */
    public static function getAll(string $type, &$total, $orders = [], $filters = []);
}
