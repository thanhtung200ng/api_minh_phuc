<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\ConfigWebsite;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;

interface ConfigWebsiteInterface
{
    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return ConfigWebsite[]|Collection
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param ConfigWebsite $config
     * @return ConfigWebsite $user
     */
    public static function create(ConfigWebsite $config): ConfigWebsite;

    /**
     * @param ConfigWebsite $config
     * @return ConfigWebsite $user
     */
    public static function update(ConfigWebsite $config): ConfigWebsite;

    /**
     * @param int $configId
     * @return int
     */
    public static function delete(int $configId): int;

}
