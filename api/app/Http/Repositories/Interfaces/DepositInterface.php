<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Deposit;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;

interface DepositInterface
{
    /**
     * @param int $id
     * @return Deposit|null
     */
    public static function getById(int $id): ?Deposit;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return Deposit[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Deposit $deposit
     * @return Deposit $user
     */
    public static function create($deposit);

    /**
     * @param array $objToSave
     * @param int $depositId
     * @return Deposit $user
     */
    public static function update(array $objToSave, int $depositId);

    /**
     * @param int $userId
     * @return int
     */
    public static function delete($userId);
}
