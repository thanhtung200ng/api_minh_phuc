<?php


namespace App\Http\Repositories\Interfaces;


interface WarehouseInterface
{
    /**
     * @param int $warehouseId
     */
    public static function getById(int $warehouseId);

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null);

    /**
     * @param  $warehouse
     */
    public static function create($warehouse);

    /**
     * @param  $warehouse
     */
    public static function update($warehouse, $id);

    /**
     * @param int $warehouseId
     * @return int
     */
    public static function delete(int $warehouseId): int;
}
