<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\BusinessContract;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;

interface BusinessContractInterface
{
    /**
     * @param int $BusinessContractId
     * @return BusinessContract|null
     */
    public static function getById(int $BusinessContractId): ?BusinessContract;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return BusinessContract[]|Collection
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param BusinessContract $BusinessContract
     * @return BusinessContract $user
     */
    public static function create(BusinessContract $BusinessContract): BusinessContract;

    /**
     * @param BusinessContract $BusinessContract
     * @return BusinessContract $user
     */
    public static function update(BusinessContract $BusinessContract): BusinessContract;

    /**
     * @param int $BusinessContractId
     * @return int
     */
    public static function delete(int $BusinessContractId): int;
}
