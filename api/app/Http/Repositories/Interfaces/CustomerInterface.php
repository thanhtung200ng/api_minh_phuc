<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Customer;

interface CustomerInterface
{
    /**
     * @param int $customerId
     * @return Customer|null
     */
    public static function getById(int $customerId): ?Customer;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return User[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Customer $user
     * @return Customer $user
     */
    public static function create($user);

    /**
     * @param array $objToSave
     * @param Customer $user
     * @return Customer $user
     */
    public static function update(array $objToSave, Customer $user);

    /**
     * @param int $userId
     * @return int
     */
    public static function delete($userId);

    public static function getDataViByAccount($id);
}
