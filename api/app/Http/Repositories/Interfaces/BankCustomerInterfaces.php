<?php

namespace App\Http\Repositories\Interfaces;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;
interface BankCustomerInterfaces
{
    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param  $customerBanks
     *
     */
    public static function create($customerBanks);

    /**
     * @param  $customerBanks
     * @param  $id
     */
    public static function update( $customerBanks, $id);

    /**
     * @param int $customerBank_id
     * @return int
     */
    public static function delete(int $customerBank_id): int;
}
