<?php


namespace App\Http\Repositories\Interfaces;



interface WarehouseProductsInterface
{
    /**
     * @param int $warehouseProductsId
     */
    public static function getById(int $warehouseProductsId);

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null);

    /**
     * @param  $warehouseProducts
     */
    public static function create($warehouseProducts);

    /**
     * @param  $warehouseProducts
     */
    public static function update($warehouseProducts, $id);

    /**
     * @param int $warehouseProductsId
     * @return int
     */
    public static function delete(int $warehouseProductsId): int;
}
