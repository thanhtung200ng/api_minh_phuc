<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\RankMpMart;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;

interface RankMpMartInterface
{
    /**
     * @param int $MpMartRank_id
     * @return RankMpMart|null
     */
    public static function getById(int $MpMartRank_id): ?RankMpMart;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return RankMpMart[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param RankMpMart $MpMartRank_id
     * @return RankMpMart $RankMpMart
     */
    public static function create(RankMpMart $MpMartRank_id);

    /**
     * @param RankMpMart $MpMartRank_id
     * @return RankMpMart $MpMartRank_id
     */
    public static function update(RankMpMart $MpMartRank_id);

    /**
     * @param int $MpMartRank_id
     * @return int
     */
    public static function delete($MpMartRank_id);
}
