<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\PurchaseOders;
use App\Models\PurchaseOrdersModel;

interface PurchaseOderInterface
{
    /**
     * @param int PurchaseOders_id
     * @return PurchaseOders|null
     */

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return User[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param PurchaseOders $PurchaseOders_id
     * @return PurchaseOders $PurchaseOders
     */
    public static function create(PurchaseOders $PurchaseOders);

    /**
     * @param PurchaseOrdersModel $obj
     * @param array|null $before
     *  @return PurchaseOrdersModel $obj
     */
    public static function update(PurchaseOrdersModel $obj, ?array $before);

    /**
     * @param int $PurchaseOders_id
     * @return int
     */
    public static function delete($PurchaseOders_id);
}
