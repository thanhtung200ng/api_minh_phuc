<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Deposit;
use App\Http\Repositories\Entities\Transaction;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;

interface TransactionInterface
{
    /**
     * @param int $id
     * @return Transaction|null
     */
    public static function getById(int $id): ?Transaction;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return Transaction[]|Collection
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param array $transaction
     * @return int
     */
    public static function create(array $transaction): int;

    /**
     * @param array $objToSave
     * @param int $id
     * @return Transaction $user
     */
    public static function update(array $objToSave, int $id): Transaction;

    /**
     * @param int $id
     * @return int
     */
    public static function delete(int $id);
}
