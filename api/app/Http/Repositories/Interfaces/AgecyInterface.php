<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Agecy;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;

interface AgecyInterface
{
    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Agecy Agecy
     * @return Agecy $agecy
     */
    public static function create(Agecy $agecy): Agecy;

    /**
     * @param Agecy $agecy
     * @param Agecy $id
     * @return Agecy $agecy
     */
    public static function update(Agecy $agecy, $id): Agecy;

    /**
     * @param int $agecyId
     * @return int
     */
    public static function delete(int $agecyId): int;
}
