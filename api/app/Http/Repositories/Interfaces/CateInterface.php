<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Category;

interface CateInterface
{
    /**
     * @param int $cateId
     * @return Category|null
     */
    public static function getById(int $cateId): ?Category;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return User[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Category $cate
     * @return Category $cate
     */
    public static function create(Category $cate);

    /**
     * @param Category $cate
     * @return Category $cate
     */
    public static function update(Category $cate);

    /**
     * @param int $cateId
     * @return int
     */
    public static function delete($cateId);
}
