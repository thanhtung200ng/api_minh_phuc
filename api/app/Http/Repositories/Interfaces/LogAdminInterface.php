<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\LogAdmin;
use App\Http\Repositories\Entities\Order;

interface LogAdminInterface
{
    /**
     * @param int $LogAdminId
     * @return LogAdmin|null
     */
    public static function getById(int $LogAdminId): ?LogAdmin;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return User[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param $objectId
     * @param $type
     * @param $after
     * @param $before
     * @param $objToSave
     * @param $table
     * @return bool
     */
    public static function create($objectId, $type, $table, $objToSave, $before, $after):bool;

}
