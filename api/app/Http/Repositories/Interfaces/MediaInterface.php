<?php


namespace App\Http\Repositories\Interfaces;
use App\Http\Repositories\Entities\Media;

interface MediaInterface
{
    /**
     * @param int $LogAdminId
     * @return LogAdmin|null
     */
    public static function getById(int $LogAdminId): ?Media;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return User[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param $obj
     * @param $type
     * @param $after
     * @param $before
     * @param $table
     * @return bool
     */
    public static function create($obj, $type, $after, $before, $table):bool;

    public static function getUploadPath($folder = '');

    public static function getUploadFolder($folder, $extra = TRUE);

    public static function buildImageLink($src, $no_image = '');

    public static function getImageSrc($src, $width = 0, $height = 0, $type = '');

    public static function getFileLink($link);

    public static function getFileExtension($file);

    public static function convertToAlias($str, $replace = "-");

    public static function removeAccent($mystring);

    static function url_slug($str, $options = []);

    static public function isLink($link);



}
