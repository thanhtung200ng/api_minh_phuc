<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Wallets;
use Illuminate\Support\Collection;

interface WalletsInterface
{
    /**
     * @param int $walletsId
     * @return Wallets|null
     */
    public static function getById(int $walletsId): ?Wallets;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @return Wallets[]|Collection
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null);

    /**
     * @param Wallets $wallet
     * @return Wallets $user
     */
    public static function create(Wallets $wallet): Wallets;

    /**
     * @param Wallets $wallet
     * @return Wallets $user
     */
    public static function update(Wallets $wallet): Wallets;

    /**
     * @param int $walletId
     * @return int
     */
    public static function delete(int $walletId): int;
}
