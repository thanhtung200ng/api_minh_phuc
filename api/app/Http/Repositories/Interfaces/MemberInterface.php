<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Member;

interface MemberInterface
{
    /**
     * @param int $memberId
     * @return Member|null
     */
    public static function getById(int $memberId): ?Member;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return User[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Member $user
     * @return Member $user
     */
    public static function create($user);

    /**
     * @param User $user
     * @return User $user
     */
    public static function update($user);

    /**
     * @param int $userId
     * @return int
     */
    public static function delete($userId);
}
