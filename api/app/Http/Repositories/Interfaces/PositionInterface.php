<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Entities\Position;
use Illuminate\Support\Collection;

interface PositionInterface
{
    /**
     * @param int $memberId
     * @return Position|null
     */
    public static function getById(int $memberId): ?Position;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return Position[]|Collection
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Position $position
     * @return Position $user
     */
    public static function create(Position $position): Position;

    /**
     * @param Position $position
     * @return Position $user
     */
    public static function update(Position $position): Position;

    /**
     * @param int $positionId
     * @return int
     */
    public static function delete(int $positionId): int;
}
