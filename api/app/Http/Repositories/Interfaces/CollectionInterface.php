<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Entities\Collections;
use Illuminate\Support\Collection;

interface CollectionInterface
{
    /**
     * @param int $collectionId
     * @return Collections|null
     */
    public static function getById(int $collectionId): ?Collections;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collections $orders
     * @param Filter[]|Collections $filters
     * @return Collections[]|Collections
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Collections $collection
     * @return Collections $user
     */
    public static function create(Collections $collection): Collections;

    /**
     * @param Collections $collection
     * @return Collections $user
     */
    public static function update(Collections $collection): Collections;

    /**
     * @param int $collectionId
     * @return int
     */
    public static function delete(int $collectionId): int;
}
