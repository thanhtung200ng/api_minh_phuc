<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Product;

interface RankBuyerInterface
{
    /**
     * @param int $product_id
     * @return Rank|null
     */
    public static function getById(int $product_id): ?Product;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return User[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Product $product_id
     * @return Product $product
     */
    public static function create(Product $product_id);

    /**
     * @param Product $product_id
     * @return Product $product_id
     */
    public static function update(Product $product_id);

    /**
     * @param int $product_id
     * @return int
     */
    public static function delete($product_id);
}
