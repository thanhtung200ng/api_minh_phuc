<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\SalesBooth;
use Illuminate\Support\Collection;

interface SalesBoothInterface
{
    /**
     * @param int $SalesBoothId
     * @return SalesBooth|null
     */
    public static function getById(int $SalesBoothId): ?SalesBooth;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters

     * @return SalesBooth[]|Collection
     */
    public static function getAll( $offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param SalesBooth $tag
     * @return SalesBooth $user
     */
    public static function create(SalesBooth $tag): SalesBooth;

    /**
     * @param SalesBooth $tag
     * @return SalesBooth $user
     */
    public static function update(SalesBooth $tag): SalesBooth;

    /**
     * @param int $tagId
     * @return int
     */
    public static function delete(int $tagId): int;
}
