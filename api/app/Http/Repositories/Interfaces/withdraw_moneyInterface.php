<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\withdraw_money;

interface withdraw_moneyInterface
{
    /**
     * @param int $customerId
     * @return withdraw_money|null
     */
    public static function getById(int $customerId): ?withdraw_money;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return User[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Customer $user
     * @return Customer $user
     */
    public static function create(withdraw_money $user);

    /**
     * @param id $id
     *

     */
    public static function update($id);

    /**
     * @param int $userId
     * @return int
     */
    public static function delete($userId);
}
