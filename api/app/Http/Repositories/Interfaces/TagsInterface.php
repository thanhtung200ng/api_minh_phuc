<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Tags;
use Illuminate\Support\Collection;

interface TagsInterface
{
    /**
     * @param int $productId
     * @return Tags|null
     */
    public static function getById(int $productId): ?Tags;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @return Tags[]|Collection
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null);

    /**
     * @param Tags $tag
     * @return Tags $user
     */
    public static function create(Tags $tag): Tags;

    /**
     * @param Tags $tag
     * @return Tags $user
     */
    public static function update(Tags $tag): Tags;

    /**
     * @param int $tagId
     * @return int
     */
    public static function delete(int $tagId): int;
}
