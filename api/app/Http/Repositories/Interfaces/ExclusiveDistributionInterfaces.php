<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;

interface ExclusiveDistributionInterfaces
{
    /**
     * @param int $exclusiveDistributionId
     */
    public static function getById(int $exclusiveDistributionId);

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param  $customer
     * @param  $products
     */
    public static function create($customer, $products);

    /**
     * @param  $id
     * @param  $customer
     * @param  $products
     */
    public static function update($id ,$customer, $products);

    /**
     * @param int $exclusiveDistribution
     * @return int
     */
    public static function delete($exclusiveDistribution);
}
