<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\EventSale;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;

interface EventSaleInterface
{
    /**
     * @param int $EventSaleId
     * @return EventSale|null
     */
    public static function getById(int $EventSaleId): ?EventSale;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return EventSale[]|Collection
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param EventSale $eventSale
     * @return EventSale $user
     */
    public static function create(EventSale $eventSale): EventSale;

    /**
     * @param EventSale $eventSale
     * @return EventSale $user
     */
    public static function update(EventSale $eventSale): EventSale;

    /**
     * @param int $eventSale
     * @return int
     */
    public static function delete($eventSale);
}
