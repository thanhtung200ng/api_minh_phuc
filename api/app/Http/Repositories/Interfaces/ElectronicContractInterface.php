<?php


namespace App\Http\Repositories\Interfaces;

use App\Http\Repositories\Entities\ElectronicContract;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use Illuminate\Support\Collection;

interface ElectronicContractInterface
{
    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param ElectronicContract ElectronicContractId
     * @return ElectronicContract $electronicContract
     */
    public static function create(ElectronicContract $electronicContract): ElectronicContract;


    public static function update(ElectronicContract $electronicContract, $id);

    /**
     * @param int $electronicContractId
     * @return int
     */
    public static function delete(int $electronicContractId): int;
}
