<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\PurchaseOdersItem;

interface PurchaseOderItemInterface
{
    /**
     * @param int PurchaseOders_id
     * @return PurchaseOdersItem|null
     */

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return User[]|Collection
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param PurchaseOdersItem $PurchaseOdersItem_id
     * @return PurchaseOdersItem $PurchaseOders
     */
    public static function create(PurchaseOdersItem $PurchaseOders);

    /**
     * @param PurchaseOdersItem $PurchaseOdersItem_id
     *  @return PurchaseOdersItem $PurchaseOders_id
     */
    public static function update(PurchaseOdersItem $PurchaseOdersItem_id);

    /**
     * @param int $PurchaseOdersItem_id
     * @return int
     */
    public static function delete($PurchaseOdersItem_id);
}
