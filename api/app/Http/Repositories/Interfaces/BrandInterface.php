<?php


namespace App\Http\Repositories\Interfaces;


use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Entities\Brand;
use Illuminate\Support\Collection;

interface BrandInterface
{
    /**
     * @param int $brandId
     * @return Brand|null
     */
    public static function getById(int $brandId): ?Brand;

    /**
     * @param int $offset
     * @param int $count
     * @param null $total
     * @param Order[]|Collection $orders
     * @param Filter[]|Collection $filters
     * @return Brand[]|Collection
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = []);

    /**
     * @param Brand $brand
     * @return Brand $user
     */
    public static function create(Brand $brand): Brand;

    /**
     * @param int $brandId
     * @return int
     */
    public static function delete($brandId);
}
