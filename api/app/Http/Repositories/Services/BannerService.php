<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Banner;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\BannerFactory;
use App\Http\Repositories\Interfaces\BannerInterface;

class BannerService extends BaseService implements BannerInterface
{
    protected $table = 'ui_banner';
    public static function getById(int $bannerId): ?Banner
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $bannerId)
            ->first();
        return $entity ? BannerFactory::make($entity) : null;
    }

    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset((int)$offset);
            $query->limit((int)$count);
        }
        $entities = $query->get();
        return BannerFactory::makeCollection($entities);
    }
     public  static function getfirstBaner(){
         $data = self::newQuery()->first();
         if(empty($data)){
             return null;
         }
        $data->images_main = explode(',',$data->images_main);
        $data->image_items = explode(',',$data->image_items);
        return $data;
     }
    public static function create(Banner $banner): Banner
    {
        $banner->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'images_main' => $banner->getImagesMain(),
            'images_bottom' => $banner->getImagesBottom(),
            'image_items' => $banner->getImageItems(),
            'created_at' => $banner->getCreatedAt()
        ]);
        $banner->setId($id);
        return $banner;
    }

    public static function update(Banner $banner): Banner
    {
        $instance = self::getInstance();
        $banner->setUpdatedAt(time());
        self::newQuery()
            ->where($instance->primaryKey, $banner->getId())
            ->update([
                'images_main' => $banner->getImagesMain(),
                'images_bottom' => $banner->getImagesBottom(),
                'image_items' => $banner->getImageItems(),
                'updated_at' => $banner->getUpdatedAt()
            ]);
        $bannerAfterUpdate = self::newQuery()
            ->where($instance->primaryKey, $banner->getId())->first();
        $banner->setCreatedAt($bannerAfterUpdate->created_at);
        return $banner;
    }

    /**
     * @inheritDoc
     */
    public static function delete(int $bannerId): int
    {
        $instance = self::getInstance();
        $bannerBeforeDelete = self::newQuery()
            ->where($instance->primaryKey, $bannerId)->first();
        if (!$bannerBeforeDelete){
            return 0;
        }
//        LogAdminService::create($bannerBeforeDelete, ResourceCode::DELETE, json_encode($bannerBeforeDelete),[],$instance->table);
        return self::newQuery()
            ->where($instance->primaryKey, $bannerId)
            ->delete();
    }
}
