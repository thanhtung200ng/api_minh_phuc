<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\EventSale;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Factories\EventSaleFactory;
use App\Http\Repositories\Interfaces\EventSaleInterface;
use Illuminate\Support\Collection;

class EventSaleService extends BaseService implements EventSaleInterface
{
    protected $table = 'ui_event_sale';
    /**
     * @inheritDoc
     */
    public static function getById(int $EventSaleId): ?EventSale
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $EventSaleId)
            ->first();
        return $entity ? EventSaleFactory::make($entity) : null;
    }

    /**
     * @inheritDoc
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();
        return EventSaleFactory::makeCollection($entities);
    }

    /**
     * @inheritDoc
     */
    public static function create(EventSale $eventSale): EventSale
    {
        $eventSale->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'title' => $eventSale->getTitle(),
            'images' => $eventSale->getImages(),
            'link' => $eventSale->getLink(),
            'price' => $eventSale->getPrice(),
            'price_sale' => $eventSale->getPriceSale(),
            'status' => $eventSale->getStatus(),
            'created_at' => $eventSale->getCreatedAt(),
            'created_by' => $eventSale->getCreatedBy(),
        ]);
        $eventSale->setId($id);
        return $eventSale;
    }

    /**
     * @inheritDoc
     */
    public static function delete($eventSale)
    {
        $brandBeforeDelete = self::newQuery()
            ->where('id', $eventSale)->first();
        if (!$brandBeforeDelete){
            return 0;
        }
        return self::newQuery()
            ->where('id', $eventSale)
            ->delete();
    }

    /**
     * @inheritDoc
     */
    public static function update(EventSale $eventSale): EventSale
    {
        $instance = self::getInstance();
        $eventSale->setUpdateAt(time());
        self::newQuery()
            ->where($instance->primaryKey, $eventSale->getId())
            ->update([
                'title' => $eventSale->getTitle(),
                'images' => $eventSale->getImages(),
                'link' => $eventSale->getLink(),
                'status' => $eventSale->getStatus(),
                'price' => $eventSale->getPrice(),
                'price_sale' => $eventSale->getPriceSale(),
            ]);
        return $eventSale;
    }
}
