<?php

namespace App\Http\Repositories\Services;
use App\Http\Repositories\Entities\Media;
use App\Http\Repositories\Factories\MediaFactory;
use App\Http\Repositories\Interfaces\MediaInterface;
use phpDocumentor\Reflection\Types\Self_;

class MediaService extends BaseService implements MediaInterface
{
    protected $table = 'ui_media';
    const TYPE_IS_IMAGE = 'image';
    const TYPE_IS_DOC = 'doc';
    private static $mediaUpload = [
         'folder' => 'upload/',
         'path'   => '',
     ];
    public static function getById(int $LogAdminId): ?Media
    {
        // TODO: Implement getById() method.
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        // TODO: Implement getAll() method.
    }

     public static function create($obj, $type, $after, $before, $table): bool
     {
         // TODO: Implement create() method.
     }

     public static function getUploadPath($folder = '')
     {
         return self::public_path(config('filesystems.media_folder') . "/upload/") . $folder;
     }

     public static function getUploadFolder($folder, $extra = TRUE)
     {
         if ($extra) {
             return self::$mediaUpload['folder'] . $folder;
         } else {
             return $folder;
         }
     }

     public static function buildImageLink($src, $no_image = '')
     {
         if (!$src) {
             if (!$no_image) {
                 return url('images/no-image.jpg?ver=2580524613');
             } else {
                 return $no_image;
             }
         }
         if (self::isLink($src)) {
             return $src;
         }

//        return config('filesystems.media_domain') . $src . '?ver=' . HtmlHelper::$clientVersion;
         /***
          * @param $MeoObj Sửa lại ConFig bên trên là Config cũ
          * */
//        return config('filesystems.media_domain') .'data/'. $src;
         return '/data/'. $src;
     }

     /***
      * @param $src
      * @param $width
      * @param $height
      * @param $type
      *
      * @note: todo: tạm thời gen ảnh thumb tại đây
      */
     public static function getImageSrc($src, $width = 0, $height = 0, $type = '')
     {
         if (!$src) {
             return url('/images/no-image.jpg') . '?ver=888';
         }
         if (self::isLink($src)) {
             return $src;
         }
         if ($width == 0 && $height == 0) {
             return self::buildImageLink($src);
         }
         $_src = explode('/', $src);
         $_src[0] = 'thumbs/' . $width . 'x' . $height;//replace thư mục images gốc thành thư mục thumbs/size
         $src = implode('/', $_src);
         return config('filesystems.media_domain') . $src;
     }

     public static function getFileLink($link)
     {
         if (self::isLink($link)) {
             return $link;
         }
         if(isset($link['src'])) {
             $url = $link['src'];
         }else {
             $url = $link;

         }
         return url('/' . $url);
     }

     public static function getFileExtension($file)
     {
         $pos = strrpos($file, '.');
         if (!$pos) {
             return FALSE;
         }
         $str = substr($file, $pos, strlen($file));

         return strtolower($str);
     }
     public static function convertToAlias($str, $replace = "-")
     {

         //$str = strtolower(self::removeAccent($str));
         $str = self::removeAccent($str);
         $str = self::url_slug($str,['delimiter'=>$replace]);

         return $str;
         //return self::slugify($str);

         $str = trim($str);

         $str = str_replace("   ", " ", $str);
         $str = str_replace("ū", "u", $str);
         $str = str_replace("沐", " ", $str);
         $str = str_replace("轶", " ", $str);
         $str = str_replace("沐轶", " ", $str);
         $str = str_replace("  ", " ", $str);
         $str = str_replace(" ", $replace, $str);

         return $str;
     }

     public static function removeAccent($mystring)
     {
         $marTViet = [
             // Chữ thường
             "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ",
             "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ",
             "ì", "í", "ị", "ỉ", "ĩ",
             "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ",
             "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
             "ỳ", "ý", "ỵ", "ỷ", "ỹ",
             "đ", "Đ", "'",
             // Chữ hoa
             "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
             "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
             "Ì", "Í", "Ị", "Ỉ", "Ĩ",
             "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
             "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
             "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
             "Đ", "Đ", "'", ".",
         ];
         $marKoDau = [
             /// Chữ thường
             "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
             "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
             "i", "i", "i", "i", "i",
             "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
             "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
             "y", "y", "y", "y", "y",
             "d", "D", "",
             //Chữ hoa
             "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
             "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
             "I", "I", "I", "I", "I",
             "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
             "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
             "Y", "Y", "Y", "Y", "Y",
             "D", "D", "", "-",
         ];

         return str_replace($marTViet, $marKoDau, $mystring);
     }

     static function url_slug($str, $options = [])
     {
         // Make sure string is in UTF-8 and strip invalid UTF-8 characters
         // $str = strtolower($str);
         $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());

         $defaults = [
             'delimiter'     => '-',
             'limit'         => NULL,
             'lowercase'     => TRUE,
             'replacements'  => [],
             'transliterate' => TRUE,
         ];

         // Merge options
         $options = array_merge($defaults, $options);

         $char_map = [
             // Latin
             'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
             'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
             'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
             'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
             'ß' => 'ss',
             'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
             'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
             'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
             'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
             'ÿ' => 'y',
             // Latin symbols
             '©' => '(c)',
             // Greek
             'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
             'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
             'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
             'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
             'Ϋ' => 'Y',
             'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
             'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
             'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
             'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
             'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
             // Turkish
             'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
             'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
             // Russian
             'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
             'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
             'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
             'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
             'Я' => 'Ya',
             'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
             'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
             'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
             'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
             'я' => 'ya',
             // Ukrainian
             'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
             'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
             // Czech
             'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
             'Ž' => 'Z',
             'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
             'ž' => 'z',
             // Polish
             'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
             'Ż' => 'Z',
             'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
             'ż' => 'z',
             // Latvian
             'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
             'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
             'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
             'š' => 's', 'ū' => 'u', 'ž' => 'z',
         ];

         // Make custom replacements
         $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

         // Transliterate characters to ASCII
         if ($options['transliterate']) {
             $str = str_replace(array_keys($char_map), $char_map, $str);

         }
         // Replace non-alphanumeric characters with our delimiter
         $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

         // Remove duplicate delimiters
         $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

         // Truncate slug to max. characters
         $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

         // Remove delimiter from ends
         $str = trim($str, $options['delimiter']);

         return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
     }

     static public function isLink($link)
     {
         return filter_var($link, FILTER_VALIDATE_URL);
     }

     /**
      * Get the path to the public folder.
      *
      * @param  string $path
      * @return string
      */
     static public function public_path($path = '')
     {
         return env('PUBLIC_PATH', base_path('public')) . ($path ? '/' . $path : $path);
     }

 }
