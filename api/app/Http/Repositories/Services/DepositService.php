<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Deposit;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\DepositFactory;
use App\Http\Repositories\Interfaces\DepositInterface;
use Illuminate\Support\Collection;


class DepositService extends BaseService implements DepositInterface
{

    protected $table = 'ui_deposits';

    public static function getById(int $id): Deposit
    {
        $query = self::newQuery();
        $query = $query->leftJoin('ui_customers as c_from', function ($join) {
            $join->on('c_from.id', '=', 'ui_deposits.from_customer_id');
        })->leftJoin('ui_customers as c_to', function ($join) {
            $join->on('c_to.id', '=', 'ui_deposits.to_customer_id');
        })->leftJoin('ui_customer_wallets as cw_from', function ($join) {
            $join->on('cw_from.id', '=', 'ui_deposits.from_customer_wallet_id');
        })->leftJoin('ui_customer_wallets as cw_to', function ($join) {
            $join->on('cw_to.id', '=', 'ui_deposits.to_customer_wallet_id');
        })->leftJoin('ui_wallets as w_from', function ($join) {
            $join->on('w_from.id', '=', 'cw_from.wallet_id');
        })->leftJoin('ui_wallets as w_to', function ($join) {
            $join->on('w_to.id', '=', 'cw_to.wallet_id');
        })->leftJoin('ui_members as mem_approved', function ($join) {
            $join->on('mem_approved.id', '=', 'ui_deposits.approved_by');
        })
            ->where('ui_deposits.id', $id)->select(
            'c_from.full_name as from_customer_full_name', 'c_from.account as from_customer_account',
            'c_from.status as from_customer_status',
            'c_to.full_name as to_customer_full_name', 'c_to.account as to_customer_account',
            'c_to.status as to_customer_status',
            'w_from.name as from_customer_wallet_name', 'cw_from.current_balance as from_customer_wallet_current_balance',
            'w_from.status as from_customer_wallet_status',
            'w_to.name as to_customer_wallet_name', 'cw_to.current_balance as to_customer_wallet_current_balance',
            'w_to.status as to_customer_wallet_status', 'mem_approved.last_name as approved_name',
            'ui_deposits.*',
        );
        $entities = $query->first();
        return $entities ? DepositFactory::make($entities) : app(Deposit::class);
    }



    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();

        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query = $query->leftJoin('ui_customers as c_from', function ($join) {
            $join->on('c_from.id', '=', 'ui_deposits.from_customer_id');
        })->leftJoin('ui_customers as c_to', function ($join) {
            $join->on('c_to.id', '=', 'ui_deposits.to_customer_id');
        })->leftJoin('ui_customer_wallets as cw_from', function ($join) {
            $join->on('cw_from.id', '=', 'ui_deposits.from_customer_wallet_id');
        })->leftJoin('ui_customer_wallets as cw_to', function ($join) {
            $join->on('cw_to.id', '=', 'ui_deposits.to_customer_wallet_id');
        })->leftJoin('ui_wallets as w_from', function ($join) {
            $join->on('w_from.id', '=', 'cw_from.wallet_id');
        })->leftJoin('ui_wallets as w_to', function ($join) {
            $join->on('w_to.id', '=', 'cw_to.wallet_id');
        })->leftJoin('ui_members as mem_approved', function ($join) {
            $join->on('mem_approved.id', '=', 'ui_deposits.approved_by');
        })
            ->select(
            'c_from.full_name as from_customer_full_name', 'c_from.account as from_customer_account',
            'cw_from.status as from_customer_wallet_status',
            'c_from.status as from_customer_status',
            'c_to.full_name as to_customer_full_name', 'c_to.account as to_customer_account',
            'c_to.status as to_customer_status',
            'cw_to.status as to_customer_wallet_status',
            'cw_to.current_balance as to_customer_wallet_current_balance',
            'w_from.name as from_customer_wallet_name',
            'w_from.name as from_customer_wallet_name', 'cw_from.current_balance as from_customer_wallet_current_balance',
            'w_to.name as to_customer_wallet_name',
            'mem_approved.last_name as approved_name',
            'ui_deposits.*',
        );
        $entities = $query->get();
        return DepositFactory::makeCollection($entities);
    }

    public static function create($deposit): Deposit
    {
        $deposit->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'money' => $deposit->getMoney(),
            'from_customer_id' => $deposit->getFromCustomerId(),
            'to_customer_id' => $deposit->getToCustomerId(),
            'from_customer_wallet_id' => $deposit->getFromCustomerWalletId(),
            'to_customer_wallet_id' => $deposit->getToCustomerWalletId(),
            'transaction_code' => $deposit->getTransactionCode(),
            'status' => $deposit->getStatus(),
            'created_at' => $deposit->getCreatedAt(),
            'created_by' => $deposit->getCreatedBy(),
        ]);

        $deposit->setId($id);
        return $deposit;
    }

    public static function update(array $objToSave, int $depositId)
    {
        $instance = self::getInstance();
        $before = self::newQuery()
            ->where($instance->primaryKey, $depositId)->first();
        self::newQuery()
            ->where($instance->primaryKey, $depositId)
            ->update($objToSave);
        $after = self::newQuery()
            ->where($instance->primaryKey, $depositId)->first();
        LogAdminService::create($depositId, ResourceCode::UPDATE, $instance->table, $objToSave, $before, $after);
    }

    public static function delete($userId)
    {
        // TODO: Implement delete() method.
    }
}
