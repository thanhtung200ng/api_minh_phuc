<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\ElectronicContract;
use App\Http\Repositories\Factories\ElectronicContractFactory;
use Illuminate\Support\Facades\DB;

class FavoriteProductService  extends BaseService
{
    protected $table = 'ui_favorite_product';


    public static function getById(int $id)
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $id)
            ->first();
        return $entity;
    }

    public static function create($request)
    {
      $id =  self::newQuery()->insertGetId([
            'products_id' => $request->products_id,
            'type' => $request->type,
            'customer_id' => $request->customer_id,
        ]);
        $products =  DB::table('ui_products')->where('id',$request->products_id)->first();
        $like = $products->like;
        if(empty($like)){
            $like = 0;
        }
       DB::table('ui_products')->where('id',$request->products_id)->update([
           'like' => $like + 1
       ]);
        return self::newQuery()->where('id',$id)->first();
    }

    public static function deleted($id)
    {
        $FavoriteProduct  =  self::newQuery()->where('id', $id)->first();
        $products =  DB::table('ui_products')->where('id',$FavoriteProduct->products_id)->first();
        DB::table('ui_products')->where('id', $FavoriteProduct->products_id)->update([
           'like' => $products->like - 1
        ]);
        return self::newQuery()->where('id', $id)->delete();
    }
}
