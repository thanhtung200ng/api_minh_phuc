<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Cities;
use App\Http\Repositories\Entities\Districts;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Entities\Wards;
use App\Http\Repositories\Factories\LocationFactory;
use App\Http\Repositories\Interfaces\LocationInterface;

class LocationService extends BaseService implements LocationInterface
{
    protected $table = '__cities';


    public static function getAll(string $type, &$total, $orders = [], $filters = [])
    {
        switch ($type) {
            case 'districts':
                $query = app('db')->table('__districts'); break;
            case 'wards':
                $query = app('db')->table('__wards'); break;
            default:
                $query = app('db')->table('__cities'); break;
        }

        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        $entities = $query->get();
        return LocationFactory::makeCollection($entities);
    }
}
