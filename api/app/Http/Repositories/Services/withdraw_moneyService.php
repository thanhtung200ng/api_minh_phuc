<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\withdraw_money;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\withdraw_moneyFactory;
use App\Http\Repositories\Interfaces\withdraw_moneyInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class withdraw_moneyService extends BaseService implements withdraw_moneyInterface
{
    protected $table = 'ui_withdraw';
    /**
     * @inheritDoc
     */
    public static function getById(int $walletsId): ?withdraw_money
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $walletsId)
            ->first();
        return $entity ? withdraw_moneyFactory::make($entity) : null;
    }

    /**
     * @inheritDoc
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();
        return $entities;
    }


    /**
     * @inheritDoc
     */


    public static function deduction($item,$money){
        $totalMoney = $item->getCurrentBalance() - $money;
        DB::table('ui_customer_wallets')->where('id', $item->getId())->update([
            'current_balance' => $totalMoney
        ]);
        $item->setCurrentBalance($totalMoney);
        return $item;
    }

    /**
     * @inheritDoc
     */
    public static function update($id)
    {
        self::newQuery()->whereIn('id',$id )->update(['status' => 1]);
        $data =  self::newQuery()->whereIn('id',$id )->get();
        return $data;
    }

    public function minWithdrawMoney(){
        return 50000;
    }
    public function walletLining(){
        return 50000;
    }
    public function transactionFee($money){
        if($money <= 3000000 ){
            return 2000;
        }if($money < 10000000){
            return 5000;
        }if($money < 25000000){
            return 10000;
        }if($money >= 25000000){
            return 15000;
        }
    }

    public static function incomeTax(){
        return 0.05;
    }

    /**
     * @inheritDoc
     */
    public static function delete($userId)
    {

    }
    public static function create($objToSave){
        return self::newQuery()->insertGetId($objToSave);
    }
}
