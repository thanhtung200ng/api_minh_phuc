<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Interfaces\WarehouseProductsInterface;
use App\Models\WarehouseProductsModel;

class WarehouseProductService extends BaseServiceEloquent implements WarehouseProductsInterface
{
    public function __construct()
    {
        $model = new WarehouseProductsModel();
        parent::__construct($model);
    }
    /**
     * @inheritDoc
     */
    public static function getById(int $warehouseProductsId)
    {
        // TODO: Implement getById() method.
    }

    /**
     * @inheritDoc
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null)
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @inheritDoc
     */
    public static function create($warehouseProducts)
    {
        $query = self::newQuery();
        $id = $query->insertGetId([
          'warehouse_id' => $warehouseProducts['warehouse_id'],
          'product_variant_id' => $warehouseProducts['products_item_id'],
        ]);
        return $query->where('id', $id)->first();
    }

    /**
     * @inheritDoc
     */
    public static function update($warehouseProducts, $id)
    {
        $query = self::newQuery();
        $query->where('warehouse_id', $id)->update([
            'warehouse_id' => $warehouseProducts['warehouse_id'],
            'product_variant_id' => $warehouseProducts['products_item_id'],
        ]);
        return $query->where('id', $id)->first();
    }

    /**
     * @inheritDoc
     */
    public static function delete(int $warehouseProductsId): int
    {
        // TODO: Implement delete() method.
    }
}
