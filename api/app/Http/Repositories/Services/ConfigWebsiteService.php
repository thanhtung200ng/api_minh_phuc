<?php


namespace App\Http\Repositories\Services;


class ConfigWebsiteService extends BaseService
{
    protected $table = '__config_website';

    /**
     * @inheritDoc
     */
    public static function getByType(int $type)
    {
        $query = self::newQuery();
        $config = $query->where('type', $type)->first();
        if ($config) {
            return $config;
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {

    }

    /**
     * @inheritDoc
     */
    public static function create($type, $configWebsite)
    {
        self::newQuery()->insertGetId([
            'type' =>  $type,
            'value' => $configWebsite
        ]);
        $data = self::newQuery()->where('type',$type)->first();
        return $data;
    }

    /**
     * @inheritDoc
     */
    public static function update($type, $configWebsite)
    {
        self::newQuery()->where('type', $type)->update([
            'type' =>  $type,
            'value' => $configWebsite
        ]);
        $data = self::newQuery()->where('type',$type)->first();
        return $data;
    }

    /**
     * @inheritDoc
     */
    public static function delete($cateId)
    {
        // TODO: Implement delete() method.
    }
}
