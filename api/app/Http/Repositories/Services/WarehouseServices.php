<?php


namespace App\Http\Repositories\Services;

use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Interfaces\WarehouseInterface;
use App\Models\PurchaseOrdersModel;
use App\Models\WarehouseModel;
use Illuminate\Support\Facades\Auth;

class WarehouseServices extends BaseServiceEloquent implements WarehouseInterface
{
    public function __construct()
    {
        $model = new WarehouseModel();
        parent::__construct($model);
    }

    /**
     * @inheritDoc
     */
    public static function getById(int $warehouseId)
    {
        $query = self::newQuery();
        $query = $query->where('id',$warehouseId);
        $query = $query->with(['customer','WarehouseProducts.productVariant',
          'city' => function($q){
            $q->select('id','name','code');
        },'district' => function($q){
            $q->select('id','name','code');
        },'ward' => function($q){
            $q->select('id','name','code');
        }]);
        return $query->first();
    }

    public static function getByCustomerId(int $warehouseId)
    {
        $query = self::newQuery();
        $query = $query->where('customer_id',$warehouseId);
        $query = $query->with(['WarehouseProducts.productVariant' => function($q){
            $q->select('id', 'name','product_id','sku','images','slug','status');             // check xem cái thằng cù lìn này trạng thái của sản phẩm nó thế nào ?
        },'customer' => function($q){
            $q->select('id', 'account');
        },'city' => function($q){
            $q->select('id','name','code');
        },'district' => function($q){
            $q->select('id','name','code');
        },'ward' => function($q){
            $q->select('id','name','code');
        }]);
        return $query->first();
    }

    public static function getWarehouseByCity($city_id){
        $query = self::newQuery();
        $query= $query->where('id', $city_id);
        return $query->first();
    }

    /**
     * @inheritDoc
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query =  $query->with(['customer','WarehouseProducts','city' => function($q){
            $q->select('id','name','code');
        },'district' => function($q){
            $q->select('id','name','code');
        },'ward' => function($q){
            $q->select('id','name','code');
        }]);
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get()->toArray();
    }

    /**
     * @inheritDoc
     */
    public static function create($warehouse)
    {
        $query = self::newQuery();
        $id = $query->insertGetId([
            'name' => $warehouse->get('name'),
            'customer_id' =>  $warehouse->get('customer')['id'],
            'sku' => $warehouse->get('sku'),
            'status' => $warehouse->get('status'),
            'city_id' => $warehouse->get('city')['id'],
            'district_id' => $warehouse->get('district')['id'],
            'ward_id' => $warehouse->get('ward')['id'],
            'street' => $warehouse->get('street'),
            'member_approved_by' => null,
            'member_approved_at' => null,
            'created_at' => time()
        ]);
        return $query->where('id', $id)->first();
    }

    /**
     * @inheritDoc
     */
    public static function update($warehouse, $id)
    {
        $query = self::newQuery();
        $query->where('id', $id)->update([
            'name' => $warehouse->get('name'),
            'customer_id' => $warehouse->get('customer_id'),
            'sku' => $warehouse->get('sku'),
            'status' => $warehouse->get('status'),
            'city_id' => $warehouse->get('city_id'),
            'district_id' => $warehouse->get('district_id'),
            'ward_id' => $warehouse->get('ward_id'),
            'street' => $warehouse->get('street'),
            'member_approved_by' => Auth()->user()->id,
            'member_approved_at' => time(),
        ]);
        return $query->where('id', $id)->first();
    }

    /**
     * @inheritDoc
     */
    public static function delete(int $warehouseId): int
    {
        // TODO: Implement delete() method.
    }


    public function createWareHouse($obj) {
        $query = self::newQuery();
        $obj['created_at'] = time();
        $id = $query->insertGetId($obj);
        $after = $query->where('id', $id)->first();
        LogAdminService::create($id, ResourceCode::CREATE, 'ui_warehouse', $obj, [], $after);
        return $after;
    }
}
