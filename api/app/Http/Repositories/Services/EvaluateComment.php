<?php


namespace App\Http\Repositories\Services;


use Illuminate\Support\Facades\DB;

class EvaluateComment extends BaseService
{
    protected $table = 'ui_evaluate_comment';


    public static function create($request)
    {
        $id =  self::newQuery()->insertGetId([
            'evaluate' => $request->evaluate,
            'name' => $request->name,
            'customer_id' => $request->customer_id,
            'email' => $request->email,
            'comment' => json_encode($request->comment),
            'products_id' => $request->products_id,
        ]);
        return self::newQuery()->where('id', $id)->first();
    }
}
