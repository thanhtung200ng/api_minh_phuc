<?php

namespace App\Http\Repositories\Services;

use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Interfaces\BankCustomerInterfaces;
use Illuminate\Support\Collection;

class BankCustomerServices extends BaseService implements BankCustomerInterfaces
{

    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        // TODO: Implement getAll() method.
    }

    public static function create($customerBanks)
    {
        // TODO: Implement create() method.
    }

    public static function update($customerBanks, $id)
    {
        // TODO: Implement update() method.
    }

    public static function delete(int $customerBank_id): int
    {
        // TODO: Implement delete() method.
    }
}
