<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\LogAdmin;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Interfaces\Collection;
use App\Http\Repositories\Interfaces\LogAdminInterface;
use App\Http\Repositories\Interfaces\User;

class LogAdminService extends BaseService implements LogAdminInterface
{
    protected $table = '__logs_admin';

    public static function getById(int $LogAdminId): ?LogAdmin
    {
        // TODO: Implement getById() method.
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        // TODO: Implement getAll() method.
    }

     public static function create($objectId, $type, $table, $objToSave, $before = [], $after = []):bool
    {
        $created_by = '';
        if (auth()->user()) {
            $created_by = json_encode([
                'id' => Auth()->user()->id,
                'account' => Auth()->user()->account,
                'name' => Auth()->user()->first_name . ' '. Auth()->user()->last_name,
            ]);
        }else if(auth('api_customer')->user()) {
            $created_by = json_encode([
                'id' => Auth('api_customer')->user()->id,
                'account' => Auth('api_customer')->user()->account,
                'name' => Auth('api_customer')->user()->first_name . ' '. Auth('api_customer')->user()->last_name,
            ]);
        }
        LogAdminService::newQuery()->insertGetId([
            'object_id' => $objectId,
            'type' => $type,
            'table' => $table,
            'obj_to_save' => json_encode($objToSave),
            'before' => json_encode($before),
            'after' => json_encode($after),
            'created_by' => $created_by,
            'client_info' => json_encode([
                'agent' => @$_SERVER['HTTP_USER_AGENT'],
                'referer' => @$_SERVER['HTTP_REFERER'],
                'ip' => $_SERVER['REMOTE_ADDR'],
            ]),
            'created_at' => time(),
        ]);
        return true;
    }


}
