<?php


namespace App\Http\Repositories\Services;


use App\Exceptions\Handler;
use App\Http\Repositories\Entities\Customer;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\CustomerFactory;
use App\Http\Repositories\Factories\CustomerWalletFactory;
use App\Http\Repositories\Interfaces\CustomerInterface;
use App\Models\BaseModel;
use App\Models\Member;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Http\Repositories\Enums\ContructsCode;


class CustomerService extends BaseService implements CustomerInterface
{
    protected $table = 'ui_customers';

    /**
     * @param int $customerId
     * @param array|null $queries
     * @return Customer|null
     */
    public static function getById(int $customerId, ?array $queries = []): ?Customer
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $customer = $query->where($instance->primaryKey, $customerId)->first();
        if ($customer) {
            $wallets = self::getAllWalletsByCustomerId($customer->id);
            if ($wallets) {
                $customer->wallets = $wallets->toArray();
            }
            return CustomerFactory::make($customer);
        }
        return null;
    }

    static function getAllWalletsByCustomerId(int $customerId)
    {
        $wallets = self::newQuery()
            ->leftJoin('ui_customer_wallets as cwa', function ($join) {
                $join->on('cwa.customer_id', '=', 'ui_customers.id');
            })
            ->leftJoin('ui_wallets as wa_root', function ($join) {
                $join->on('cwa.wallet_id', '=', 'wa_root.id');
            })

            ->where('wa_root.status', app(WalletsService::class)->getStatusActive())
            ->where('ui_customers.id', $customerId)
            ->select(
                'wa_root.name', 'wa_root.slug',
                'wa_root.permission_send as wallet_permission_send',
                'wa_root.permission_receive as wallet_permission_receive',
                'wa_root.group_transaction_code as wallet_group_transaction_code',
                'cwa.*'
            )->get();
        return $wallets ? CustomerWalletFactory::makeCollection($wallets) : null;
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();

        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }

        $entities = $query->get();
        return CustomerFactory::makeCollection($entities);
    }

    public static function getAllCustomerId()
    {
        $query = self::newQuery();
        $query = $query->where('status', BaseModel::getStatusActive())->pluck('id');
        return $query;
    }

    public static function getAgency($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->where('mpmart_agency', '>=', 1)->get();
        return $entities;
    }

    public static function updateRankTotalRevenue($id, $rankId){
        $query = self::newQuery();
        $query->where('id', $id)->update([
            'mpmart_rank_total_revenue' => $rankId
        ]);
        return $query->where('id', $id)->first();
    }

    /**
     * @param Customer $user
     * @return Customer $user
     */
    public static function create($user)
    {
        $user->setCreatedAt(time());
        try {
            $status = DB::transaction(function () use ($user) {
                $id = self::newQuery()->insertGetId([
                    'first_name' => $user->getFirstName(),
                    'last_name' => $user->getLastName(),
                    'full_name' => $user->getFirstName() . ' ' . $user->getLastName(),
                    'referral_code' => $user->getReferralCode(),
                    'parent_referral_code' => $user->getParentReferralCode(),
                    'account' => $user->getAccount(),
                    'mobile' => $user->getMobile(),
                    'email' => $user->getEmail(),
                    'password' => $user->getPassword(),
                    'address' => $user->getAddress(),
                    'is_mpmart' => 1,
                    'status' => $user->getStatus(),
                    'created_at' => $user->getCreatedAt()
                ]);
                CustomerWalletService::initWallet($id);
                $user->setId($id);
                $user->setIsMpMart(1);
                $user->setIsSeller(0);
                $user->setIsBuyer(0);

                return $user;
            });

            if ($status == null) {
                DB::rollBack();
                return null;
            }
        }catch (\Exception $e) {
            DB::rollBack();
        }
    }

    public static function update(array $objToSave, Customer $user)
    {
        $instance = self::getInstance();
        $user->setUpdatedAt(time());
        $before = self::newQuery()
            ->where($instance->primaryKey, $user->getId())->first();
        self::newQuery()
            ->where($instance->primaryKey, $user->getId())
            ->update($objToSave);
        $after = self::newQuery()
            ->where($instance->primaryKey, $user->getId())->first();
        LogAdminService::create($user->getId(), ResourceCode::UPDATE, $instance->table, $objToSave, $before, $after);

    }
    public static function updateExclusiveDistribution($id){
        $instance = self::getInstance();
        self::newQuery()
            ->where($instance->primaryKey, $id)
            ->update(['exclusive_distribution' => 1]);
        return  self::newQuery()
            ->where($instance->primaryKey, $id)->first();
    }
    public static function cancelExclusiveDistributionToCustomer($id){
        $instance = self::getInstance();
        self::newQuery()
            ->where($instance->primaryKey, $id)
            ->update(['exclusive_distribution' => 0]);
        return  self::newQuery()
            ->where($instance->primaryKey, $id)->first();
    }


    public static function delete($userId)
    {
        // TODO: Implement delete() method.
    }

    public static function getDataViByAccount($id){
        $dataVi = DB::table('ui_customer_wallets')
            ->join('ui_wallets', 'ui_customer_wallets.wallet_id', '=', 'ui_wallets.id')
            ->select('wallet_id','customer_id','current_balance','name','ui_customer_wallets.status as customer_wallet_status')
            ->where('ui_customer_wallets.status', 1)->where('customer_id', $id)->get();
        if(!empty($dataVi)){
            return $dataVi;
        }
        return null;
    }

    public static function getTreeReverseByRefCode($referralCode, &$data)
    {
        $cur_item = self::newQuery()
            ->where('referral_code', $referralCode)->first();
        if(!empty($cur_item)) {
            $p_item = self::newQuery()
                ->where('referral_code', $cur_item->parent_referral_code)->first();
            $wallets = self::getAllWalletsByCustomerId($cur_item->id);
            if ($wallets) {
                $cur_item->wallets = $wallets->toArray();
            }
            $data[] = $cur_item;
            if(!empty($p_item)) {
                self::getTreeReverseByRefCode($p_item->referral_code, $data);
            }
        }
        return $data;
    }
     public static function updateContract($id, $type){
        if($type == ContructsCode::typeIs_seller){
            self::newQuery()->where('id', $id)->update([
                'is_seller' => ContructsCode::typeIs_seller,
            ]);
        }elseif ($type == ContructsCode::typeIs_mpmart){
            self::newQuery()->where('id', $id)->update([
                'is_mpmart' => ContructsCode::typeIs_mpmart,
            ]);
        }
        return self::newQuery()->where('id', $id)->first();
     }
}
