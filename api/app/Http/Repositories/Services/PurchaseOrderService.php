<?php

namespace App\Http\Repositories\Services;
use App\Elibs\Helper;
use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Customer;
use App\Http\Repositories\Entities\CustomerWallet;
use App\Http\Repositories\Entities\PurchaseOders;
use App\Http\Repositories\Entities\Wallets;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Enums\TransactionCode;
use App\Http\Repositories\Factories\PurchaseOdersFactory;
use App\Http\Repositories\Interfaces\PurchaseOderInterface;
use App\Http\Repositories\Services\Transactions\TransactionService;
use App\Models\PurchaseOrderLogsModel;
use App\Models\PurchaseOrdersModel;
use Hamcrest\BaseDescription;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class PurchaseOrderService extends BaseServiceEloquent implements PurchaseOderInterface
{

    public function __construct()
    {
        $model = new PurchaseOrdersModel();
        parent::__construct($model);
    }

    public static function getById($id, $with = true)
    {
        $query = self::newQuery();
        if ($with) {
            self::with($query);
        }
        $query = $query->where('id', $id);
        return $query->first();
    }

    public static function with(&$query)
    {
        $query = $query->with('items.product', 'agent.customer',
            'agent.city', 'agent.district', 'agent.ward',
            'salesBooth.city', 'salesBooth.district', 'salesBooth.ward',
            'salesBooth.customer', 'customerWallet.wallet',
            'deliveryAddress.customer', 'deliveryAddress.city',
            'deliveryAddress.district', 'deliveryAddress.ward');
    }


    public static function create($order){
        $order['created_at'] = time();
        return self::newQuery()->insertGetId($order);
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []){
        $query = self::newQuery();
        $query = $query->with('agent.customer', 'salesBooth.customer', 'customerWallet',
            'deliveryAddress.customer', 'deliveryAddress.city',
            'deliveryAddress.district', 'deliveryAddress.ward');
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function getOrderByCustomer($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $id){
        $query = DB::table('ui_customers')->where('ui_customers.id', $id)
            ->join('ui_customer_wallets', 'ui_customers.id', '=', 'ui_customer_wallets.customer_id')
            ->join('ui_purchase_orders', 'ui_purchase_orders.customer_wallet_id', '=', 'ui_customer_wallets.id')
            ->join('ui_purchase_order_items', 'ui_purchase_order_items.purchase_order_id', '=' ,'ui_purchase_orders.id')
            ->join('ui_salesbooth', 'ui_salesbooth.id', '=' ,'ui_purchase_orders.sales_booth_id')
            ->select('ui_purchase_orders.*',  'ui_purchase_orders.id as purchase_orders_id',
                'ui_purchase_order_items.purchase_order_id',
                'ui_salesbooth.name as salesbooth_name', 'ui_customers.full_name as full_name', 'ui_customers.id as customer_id'
            )
        ;
        return $query->get();
    }

    public static function getAllOrderByCustomerId($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [], $id){
        $query = self::newQuery();
        $query = $query->with(
            ['salesBooth.customer' => function ($q) use ($id) {
                $q->where('id', $id);
        }, 'items','customerWallet']);
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public static function update(PurchaseOrdersModel $obj, $before = null): PurchaseOrdersModel
    {
        $obj->save();
        $after = $obj->toArray();
        LogAdminService::create($obj->getKey(), ResourceCode::UPDATE, $obj->getTable(), $obj->getDirty(), $before, $after);
        return $obj;
    }

    static function calcMoneyDelivered ($order) {
        if($order->agency_id){
            $agency = AgecyService::getById($order->agency_id);
            $Customer = CustomerService::getById($agency->getCustomerId());

        }

        $fromCustomerId = $order->customer_id;
        $toCustomerId = $order->customer_id_giver;
        $fromCustomer = CustomerService::getById($fromCustomerId);
        $toCustomer = CustomerService::getById($toCustomerId);

        $money = $order->ending_balancex;
        $from_customer_rank_mpmart = $fromCustomer->getRankMpMartToArray();
        $to_customer_rank_mpmart = $toCustomer->getRankMpMartToArray();
        #region Tính tiền thực tế của đại lý giao hàng
        $percentToCustomer = round(1 - 100/$to_customer_rank_mpmart['discount'], 2); // phần trăm thực tế của người giao hàng
        $moneyToCustomerAfterDelivered = $percentToCustomer*$order->ending_balancex; // số tiền thực tế của người giao hàng
        #endregion
        #
        ##region Tính tiền thực tế của người mua hàng
        $percentFromCustomer = round(1 - 100/$from_customer_rank_mpmart['discount'], 2); // phần trăm thực tế của người mua
        $moneyFromCustomerAfterDelivered = 1*$order->ending_balancex; // số tiền thực tế đơn hàng
        #endregion

        // Tính % chênh lệch
        $rangePercent = $percentToCustomer - $percentFromCustomer;
        $afterMoney = $rangePercent*$moneyFromCustomerAfterDelivered; // số tiền người giao hàng nhận
        $walletRose = $toCustomer->getWalletRose();
        $toCustomerWalletCurrentBalanceAfterChange = $afterMoney + $walletRose->getCurrentBalance(); // số dư ví thay đổi
        //$beforeToCustomerWalletCurrentBalance = $walletRose->getCurrentBalance(); // số dư ví trước khi thay đổi

        $staTranSql = DB::transaction(function () use ($toCustomer,
            $fromCustomer,
            $walletRose, $toCustomerWalletCurrentBalanceAfterChange) {
            self::updateBalanceMpMart($walletRose, $toCustomerWalletCurrentBalanceAfterChange);
            return true;
        });
        if ($staTranSql == null) {
            DB::rollBack();
        }
    }


    public static function updateBalanceMpMart ($toCustomerWallet, $toCustomerWalletCurrentBalanceAfterChange) {

        $objToSaveCustomerWallet = [
            'current_balance' => $toCustomerWalletCurrentBalanceAfterChange
        ];
        CustomerWalletService::update($objToSaveCustomerWallet, $toCustomerWallet->getId());

    }


    public static function delete($PurchaseOders)
    {
        // TODO: Implement delete() method.
    }


}
