<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Collections;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\CollectionFactory;
use App\Http\Repositories\Interfaces\CollectionInterface;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Description;

class CollectionService extends BaseService implements CollectionInterface
{
    protected $table = 'ui_collections';
    public static function getById(int $collectionId): ?Collections
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $collectionId)
            ->first();
        $dataCateByCollecion = self::getCateByCollection($collectionId);

        $entity->cates = $dataCateByCollecion;
        return $entity ? CollectionFactory::make($entity) : null;
    }
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->Join('ui_categories_collections as ui_cate_coll', function ($join) {
            $join->on('ui_cate_coll.collection_id', '=', 'ui_collections.id');
        });
        $cateId = $entities->pluck('categories_id');
        $entities = $entities->groupBy('ui_collections.id')->get();
        $data = DB::table('ui_categories')->whereIn('id',$cateId)->get()->toArray();
        foreach ($entities as $val){
            $val->cates = $data;
        }
        return CollectionFactory::makeCollection($entities);
    }

    public static function getCateByCollection($collectionId){
            return  DB::table('ui_categories')
                ->join('ui_categories_collections','ui_categories.id','=','ui_categories_collections.categories_id')
                ->join('ui_collections','ui_categories_collections.collection_id','=','ui_collections.id')
                ->where('ui_collections.id',$collectionId)->select('ui_categories.*')->get()->toArray();
    }

    public static function create(Collections $collection): Collections
    {
        $collection->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'name' => $collection->getName(),
            'slug' => $collection->getSlug(),
            'status' => $collection->getStatus(),
            'created_at' => $collection->getCreatedAt(),
            'created_by' => $collection->getCreatedBy(),
        ]);
        $collection->setId($id);
        if(!empty($collection->getCateId())){
            self::insertCateByCollection($collection->getCateId(),$collection->getId());
        }
        return $collection;
    }

    public static function update(Collections $collection): Collections
    {
        $instance = self::getInstance();

        $collection->setUpdatedAt(time());
        self::newQuery()
            ->where($instance->primaryKey, $collection->getId())
            ->update([
                'name' => $collection->getName(),
                'slug' => $collection->getSlug(),
                'status' => $collection->getStatus(),
            ]);
        $collectionAfterUpdate = self::newQuery()
            ->where($instance->primaryKey, $collection->getId())->first();
        $collection->setCreatedAt($collectionAfterUpdate->created_at);
        if(!empty($collection->getCateId())){
            self::updateCateByCollection($collection->getCateId(),$collection->getId());
        }
        return $collection;
    }

    public static function insertCateByCollection($collectionCate, $idCollection)
    {
        if ($collectionCate){
            foreach ($collectionCate as $item){
                DB::table('ui_categories_collections')->updateOrInsert(
                    [
                        'categories_id' =>$item ,
                        'collection_id' => $idCollection
                    ]
                );
            }
        }
    }
    public static function updateCateByCollection($collectionCate, $idCollection)
    {
        DB::table('ui_categories_collections')->where('collection_id',$idCollection)->delete();
        if ($collectionCate){
            foreach ($collectionCate as $item){
                DB::table('ui_categories_collections')->updateOrInsert(
                    [
                        'categories_id' =>$item ,
                        'collection_id' => $idCollection
                    ]
                );
            }
        }
    }
    public static function delete(int $collectionId): int
    {
        $instance = self::getInstance();
        $collectionBeforeDelete = self::newQuery()
            ->where($instance->primaryKey, $collectionId)->first();
        if (!$collectionBeforeDelete){
            return 0;
        }
//        LogAdminService::create($collectionBeforeDelete, ResourceCode::DELETE, json_encode($collectionBeforeDelete),[],$instance->table);
        return self::newQuery()
            ->where($instance->primaryKey, $collectionId)
            ->delete();
    }
    public static function getIdByCate($requestData){
        return collect($requestData)->pluck('id')->toArray();
    }
}
