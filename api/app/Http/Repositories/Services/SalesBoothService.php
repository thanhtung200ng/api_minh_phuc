<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\SalesBooth;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\SalesBoothFacetory;
use App\Http\Repositories\Interfaces\SalesBoothInterface;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\DocBlock\Description;

class SalesBoothService extends BaseService implements SalesBoothInterface
{
    protected $table = 'ui_salesbooth';
    /**
     * @inheritDoc
     */
    public static function getById(int $salesboothId): ?SalesBooth
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $salesboothId)
            ->first();
        return $entity ? SalesBoothFacetory::make($entity) : null;
    }
    public static function getByCustomerId(int $salesboothId)
    {
        $query = self::newQuery();
        $entity = $query
            ->where('customer_id','=', $salesboothId)->select('id')
            ->first();
        return $entity ? SalesBoothFacetory::makeId($entity) : null;
    }

    /**
     * @inheritDoc
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();

        return SalesBoothFacetory::makeCollection($entities);
    }

    /**
     * @inheritDoc
     */
    public static function create(SalesBooth $sale_booth): SalesBooth
    {
        $sale_booth->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'name' => $sale_booth->getName(),
            'address' => $sale_booth->getAddress(),
            'slug' => $sale_booth->getSlug(),
            'customer_id' => $sale_booth->getCustomerId(),
            'status' => $sale_booth->getStatus(),
            'email' => $sale_booth->getEmail(),
            'mobile' => $sale_booth->getMobile(),
            'created_at' => $sale_booth->getCreatedAt(),
            'updated_at' => $sale_booth->getUpdatedAt()
        ]);

        $sale_booth->setId($id);
        return $sale_booth;
    }

    /**
     * @inheritDoc
     */
    public static function update(SalesBooth $sale_booth): SalesBooth
    {
        $instance = self::getInstance();
        $sale_booth->setUpdatedAt(time());
        self::newQuery()
            ->where($instance->primaryKey, $sale_booth->getId())
            ->update([
                'name' => $sale_booth->getName(),
                'slug' => $sale_booth->getSlug(),
                'address' => $sale_booth->getAddress(),
                'email' => $sale_booth->getEmail(),
                'mobile' => $sale_booth->getMobile(),
                'status' => $sale_booth->getStatus(),
                'update_at' => $sale_booth->getUpdatedAt(),
            ]);
        return $sale_booth;
    }

    /**
     * @inheritDoc
     */
    public static function delete(int $tagId): int
    {
        $instance = self::getInstance();
        $tagBeforeDelete = self::newQuery()
            ->where($instance->primaryKey, $tagId)->first();
        if (!$tagBeforeDelete){
            return 0;
        }
//        LogAdminService::create($tagBeforeDelete, ResourceCode::DELETE, json_encode($tagBeforeDelete),[],$instance->table);
        return self::newQuery()
            ->where($instance->primaryKey, $tagId)
            ->delete();
    }
}
