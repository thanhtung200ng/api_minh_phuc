<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Customer;
use App\Http\Repositories\Entities\CustomerWallet;
use App\Http\Repositories\Entities\Wallets;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\CustomerFactory;
use App\Http\Repositories\Factories\CustomerWalletFactory;
use Illuminate\Support\Collection;

class CustomerWalletService extends BaseService
{
    protected $table = 'ui_customer_wallets';
    public $fKeyCustomer = 'customer_id';
    public $fKeyWallet = 'wallet_id';

    /**
     * @param int $id
     * @param array|null $queries
     * @return Customer|null
     */
    public static function getById(int $id, ?array $queries = []): CustomerWallet
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->leftJoin('ui_wallets as wa_root', function ($join) {
                $join->on('ui_customer_wallets.wallet_id', '=', 'wa_root.id');
            })
            ->where('ui_customer_wallets.id', $id)
            ->select('ui_customer_wallets.*', 'wa_root.name', 'wa_root.slug',
                'wa_root.permission_send as wallet_permission_send',
                'wa_root.permission_receive as wallet_permission_receive',
                'wa_root.group_transaction_code as wallet_group_transaction_code',
            )
            ->first();
        return $entity ? CustomerWalletFactory::make($entity) : app(CustomerWallet::class);
    }

    public static function getAllActiveWalletsByCustomerId(int $customerId): array
    {
        $instCW = self::getInstance();
        $inst = WalletsService::getInstance();
        $nameWJoin = $inst->fetchKeyJoin('name');
        $iconWJoin = $inst->fetchKeyJoin('icon');

        $statusCWJoin = $instCW->fetchKeyJoin('status');
        $customerIdCWJoin = $instCW->fetchKeyJoin('customer_id');
        $currentBalanceCWJoin = $instCW->fetchKeyJoin('current_balance');
        $walletIdJoin = $instCW->fetchKeyJoin('id');
        $createdAtJoin = $instCW->fetchKeyJoin('created_at');
        $groupTransactionCodeJoin = $instCW->fetchKeyJoin('group_transaction_code');


        return  WalletsService::newQuery()->where($statusCWJoin, $instCW->getStatusActive())
            ->where($statusCWJoin, $instCW->getStatusActive())
            ->where($customerIdCWJoin, $customerId)
            ->where($instCW->fKeyCustomer, $customerId)
            ->leftJoin($instCW->table, $instCW->fetchKeyJoin($instCW->fKeyWallet), '=',
                $inst->fetchKeyJoin($inst->primaryKey))
            ->select($nameWJoin, $walletIdJoin.' as wallet_id',
                $currentBalanceCWJoin, $customerIdCWJoin, $statusCWJoin, $iconWJoin, $createdAtJoin, $groupTransactionCodeJoin)
            ->get()->toArray();

        //return CustomerWalletFactory::makeCollection($entities);
    }

    /**
     * @param int $customerId
     * @param Wallets $wallet
     * @return CustomerWallet $customerWallet
     */
    public static function create(int $customerId, Wallets $wallet)
    {
        $wallet->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'wallet_id' => $wallet->getId(),
            'customer_id' => $customerId,
            'current_balance' => 0,
            'created_at' => $wallet->getCreatedAt(),
            'created_by' => auth()->user()->id,
            'group_transaction_code' => $wallet->getGroupTransactionCode(),
            'status' => $wallet->getStatus(),
        ]);
        return CustomerWalletService::getById($id);
    }

    public static function createNoEntity(int $customerId, $wallet)
    {
        $id = self::newQuery()->insertGetId([
            'wallet_id' => $wallet->id,
            'customer_id' => $customerId,
            'current_balance' => 0,
            'created_at' => time(),
            'created_by' => auth()->user()->id,
            'group_transaction_code' => $wallet->group_transaction_code,
            'status' => $wallet->status,
        ]);
        return CustomerWalletService::getById($id);
    }


    public static function update(array $objToSave, $customerWalletId)
    {
        $instance = self::getInstance();
        $before = self::newQuery()
            ->where($instance->primaryKey, $customerWalletId)->first();
        self::newQuery()
            ->where($instance->primaryKey, $customerWalletId)
            ->update($objToSave);
        $after = self::newQuery()
            ->where($instance->primaryKey, $customerWalletId)->first();
        LogAdminService::create($customerWalletId, ResourceCode::UPDATE, $instance->table, $objToSave, $before, $after);
    }

    /*
     * Khởi tạo các ví cho khách hàng
     * */
    static function initWallet(int $customerId) {
        $wallets = app('db')->table('ui_wallets')->get();
        foreach ($wallets as $wallet) {
            self::newQuery()->insertGetId([
                'wallet_id' => $wallet->id,
                'customer_id' => $customerId,
                'status' => 1,
                'created_at' => time(),
                'created_by' => 0,
                'init_by_root' => 1,
                'current_balance' => 0,
                'group_transaction_code' => $wallet->group_transaction_code,
            ]);
        }

    }

    static function createWalletTotalRevenue($id) {
        return self::newQuery()->insertGetId([
            'wallet_id' => 14,
            'customer_id' => $id,
            'status' => 1,
            'created_at' => time(),
            'created_by' => auth()->user()->id,
            'current_balance' => 0,
            'group_transaction_code' => '4',
        ]);
    }

    static function getWalletTotalRevenueByCustomerId(int $id) {
        $wallet = self::newQuery()->where('customer_id', $id)->where('group_transaction_code', 'LIKE', '4')->first();
        if ($wallet === null) {
            self::createWalletTotalRevenue($id);
        }else {
            return $wallet;
        }
        return self::getWalletTotalRevenueByCustomerId($id);
    }

    static function updateWalletTotalRevenueByCustomerId(int $id, $money) {
        $instance = self::getInstance();
        $wallet = self::getWalletTotalRevenueByCustomerId($id);
        $before = $wallet;
        $objToSave = [
            'current_balance' => $wallet->current_balance + $money
        ];
        self::newQuery()->where('id', $wallet->id)->update($objToSave);
        $after = self::newQuery()->where($instance->primaryKey, $wallet->id)->first();
        LogAdminService::create($wallet->id, ResourceCode::UPDATE, $instance->table, $objToSave, $before, $after);
        return $after;
    }

    static function createWalletRose($id) {
        $walletRose = app('db')->table('ui_wallets')->where('slug', 'vi-hoa-hong')->first();
        return self::newQuery()->insertGetId([
            'wallet_id' => $walletRose->id,
            'customer_id' => $id,
            'status' => 1,
            'created_at' => time(),
            'created_by' => auth()->user()->id,
            'current_balance' => 0,
            'group_transaction_code' => $walletRose->group_transaction_code,
        ]);
    }

    static function getWalletRoseByCustomerId(int $id) {
        $wallet = self::newQuery()->where('customer_id', $id)->where('group_transaction_code', 'LIKE', '%6%')->first();
        if ($wallet === null) {
            self::createWalletRose($id);
        }else {
            return $wallet;
        }
        return self::getWalletRoseByCustomerId($id);
    }

    static function updateWalletRoseByCustomerId(int $id, $money) {
        $instance = self::getInstance();
        $wallet = self::getWalletRoseByCustomerId($id);
        $before = $wallet;
        $objToSave = [
            'current_balance' => $wallet->current_balance + $money
        ];
        self::newQuery()->where('id', $wallet->id)->update($objToSave);
        $after = self::newQuery()->where($instance->primaryKey, $wallet->id)->first();
        LogAdminService::create($wallet->id, ResourceCode::UPDATE, $instance->table, $objToSave, $before, $after);
        return $after;
    }

    static function getAllWalletByCustomer($listIdCustomer){
        $query = self::newQuery();
        return $query->whereIn('customer_id', $listIdCustomer)->get()->groupBy('customer_id');
    }

}
