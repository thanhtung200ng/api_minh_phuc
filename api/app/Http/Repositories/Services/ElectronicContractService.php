<?php


namespace App\Http\Repositories\Services;

use App\Http\Repositories\Entities\ElectronicContract;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Factories\ElectronicContractFactory;
use App\Http\Repositories\Interfaces\ElectronicContractInterface;
use Illuminate\Support\Collection;

class ElectronicContractService extends BaseService implements ElectronicContractInterface
{
    protected $table = 'ui_contracts';
    /**
     * @inheritDoc
     */

    /**
     * @inheritDoc
     */
    public static function getById(int $id): ?ElectronicContract
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $id)
            ->first();
        return $entity ? ElectronicContractFactory::make($entity) : null;
    }

    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset((int)$offset);
            $query->limit((int)$count);
        }
        $entities = $query->get();
        return ElectronicContractFactory::makeCollection($entities);
    }

    /**
     * @inheritDoc
     */
    public static function create(ElectronicContract $contract): ElectronicContract
    {
        $contract->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'name' => $contract->getName(),
            'no' => $contract->getNo(),
            'status' => $contract->getStatus(),
            'created_at' => $contract->getCreatedAt(),
            'created_by' => $contract->getCreatedBy(),
            'type' => $contract->getType(),
            'files' => $contract->getFile(),
        ]);
        $contract->setId($id);
        return $contract;
    }

    /**
     * @inheritDoc
     */
    /**
     * @inheritDoc
     */
    public static function update(ElectronicContract $contract , $id)
    {
        //$contract->setUpdatedAt(time());
        self::newQuery()->where('id', $id)->update([
            'name' =>   $contract->getName(),
            'no' => $contract->getNo(),
            'status' => $contract->getStatus(),
            'created_by' => $contract->getCreatedBy(),
            'files' => $contract->getFile(),
            'type' => $contract->getType(),
        ]);
    }

    public static function getContracts($id){
        $contruct = self::newQuery()->where('id', $id)->first();
        return $contruct;
    }
    /**
     * @inheritDoc
     */
    public static function delete(int $electronicContractId): int
    {
        // TODO: Implement delete() method.
    }
}
