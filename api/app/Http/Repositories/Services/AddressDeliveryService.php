<?php


namespace App\Http\Repositories\Services;


class AddressDeliveryService extends BaseService
{
    protected $table = 'ui_delivery_address';

    /**
     * @inheritDoc
     */
    public static function getById(int $id)
    {
        $query = self::newQuery();
        $entity = $query
            ->where('ui_delivery_address.id', $id)
            ->join('__cities', 'ui_delivery_address.city_id','=','__cities.id')
            ->join('__districts', 'ui_delivery_address.district_id','=','__districts.id')
            ->join('__wards', 'ui_delivery_address.ward_id','=','__wards.id')
            ->select('ui_delivery_address.*', '__cities.name as city_name', '__districts.name as district_name', '__wards.name as ward_name')
            ->first();
        return $entity;
    }
    public static function getByCustomerId(int $cutomer_id, $id)
    {
        $query = self::newQuery();
        $entity = $query
            ->where('ui_delivery_address.customer_id', $cutomer_id)
            ->where('ui_delivery_address.id',$id)
            ->join('__cities', 'ui_delivery_address.city_id','=','__cities.id')
            ->join('__districts', 'ui_delivery_address.district_id','=','__districts.id')
            ->join('__wards', 'ui_delivery_address.ward_id','=','__wards.id')
            ->select('ui_delivery_address.*', '__cities.name as city_name', '__districts.name as district_name', '__wards.name as ward_name')
            ->first();
        return $entity;
    }

    public static function getByDefault($customer_id)
    {
        $query = self::newQuery();
        $entity = $query->where('ui_delivery_address.customer_id', $customer_id)
            ->where('ui_delivery_address.default' , 1)
            ->join('__cities', 'ui_delivery_address.city_id','=','__cities.id')
            ->join('__districts', 'ui_delivery_address.district_id','=','__districts.id')
            ->join('__wards', 'ui_delivery_address.ward_id','=','__wards.id')
            ->select('ui_delivery_address.*', '__cities.name as city_name', '__districts.name as district_name', '__wards.name as ward_name')
            ->first();
        return $entity;
    }


    /**
     * @inheritDoc
     */
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $query->join('__cities', 'ui_delivery_address.city_id','=','__cities.id')
            ->join('__districts', 'ui_delivery_address.district_id','=','__districts.id')
            ->join('__wards', 'ui_delivery_address.ward_id','=','__wards.id')
            ->select('ui_delivery_address.*', '__cities.name as city_name', '__districts.name as district_name', '__wards.name as ward_name');
        $total = $query->count();
        if ($count) {
            $query->offset((int)$offset);
            $query->limit((int)$count);
        }
        $entities = $query->get();
        return $entities;
    }
    public static function getAllAddress(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [], $id)
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $query->where('customer_id', $id)
            ->join('__cities', 'ui_delivery_address.city_id','=','__cities.id')
            ->join('__districts', 'ui_delivery_address.district_id','=','__districts.id')
            ->join('__wards', 'ui_delivery_address.ward_id','=','__wards.id')
            ->select('ui_delivery_address.*', '__cities.name as city_name', '__districts.name as district_name', '__wards.name as ward_name');
        $total = $query->count();
        if ($count) {
            $query->offset((int)$offset);
            $query->limit((int)$count);
        }
        $entities = $query->get();
        return $entities;
    }

    public static function create($request, $id)
    {
       $id =  self::newQuery()->insertGetId([
           'full_name' => $request->full_name,
           'mobile' => $request->mobile,
           'city_id' => $request->city_id,
           'district_id' => $request->district_id,
           'ward_id' => $request->ward_id,
           'street' => $request->street,
           'type' => $request->type,
           'status' => 1,
           'customer_id' => $id,
           'default' => $request->default,
        ]);
        return self::newQuery()->   where('id',$id)->first();
    }

    public static function update( $request , $id)
    {
          self::newQuery()->where('id', $id)->update([
            'full_name' => $request->full_name ,
            'mobile' => $request->mobile,
            'city_id' => $request->city_id,
            'district_id' => $request->district_id,
            'ward_id' => $request->ward_id,
            'street' => $request->street,
            'type' => $request->type,
            'status' => 1,
            'customer_id' => $request->customer_id,
            'default' => $request->default,
        ]);
        return self::newQuery()->where('id',$id)->first();
    }

    public static function resetDefalt(){
        self::newQuery()->update(['default' => 0]);
    }

    /**
     * @inheritDoc
     */
    public static function delete($cateId)
    {
        // TODO: Implement delete() method.
    }
}
