<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Brand;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\BrandFactory;
use App\Http\Repositories\Interfaces\BrandInterface;

class BrandService extends BaseService implements BrandInterface
{
    protected $table = 'ui_brands';
    public static function getById(int $brandId): ?Brand
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $brandId)
            ->first();
        return $entity ? BrandFactory::make($entity) : null;
    }

    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();
        return BrandFactory::makeCollection($entities);
    }

    public static function create(Brand $brand): Brand
    {
        $brand->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'name' => $brand->getName(),
            'images' => $brand->getImages(),
            'slug' => $brand->getSlug(),
            'status' => $brand->getStatus(),
            'created_at' => $brand->getCreatedAt(),
            'created_by' => $brand->getCreatedBy(),
        ]);
        $brand->setId($id);
        return $brand;
    }

    public static function update(Brand $brand): Brand
    {
        $instance = self::getInstance();
        $brand->setUpdatedAt(time());
        self::newQuery()
            ->where($instance->primaryKey, $brand->getId())
            ->update([
                'name' => $brand->getName(),
                'images' => $brand->getImages(),
                'slug' => $brand->getSlug(),
                'status' => $brand->getStatus(),
            ]);
        return $brand;
    }

    public static function delete($brandId)
    {
        $instance = self::getInstance();
        $brandBeforeDelete = self::newQuery()
            ->where('id', $brandId)->first();
        if (!$brandBeforeDelete){
            return 0;
        }
        //LogAdminService::create($brandBeforeDelete, ResourceCode::DELETE, json_encode($brandBeforeDelete),[],$instance->table);
        return self::newQuery()
            ->where('id', $brandId)
            ->delete();
    }
}
