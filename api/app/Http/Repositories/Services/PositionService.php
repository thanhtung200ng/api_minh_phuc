<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Position;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\PositionFactory;
use App\Http\Repositories\Interfaces\PositionInterface;

class PositionService extends BaseService implements PositionInterface
{
    protected $table = 'ui_positions';
    public static function getById(int $memberId): ?Position
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $memberId)
            ->first();
        return $entity ? PositionFactory::make($entity) : null;
    }

    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }

        $entities = $query->get();
        return PositionFactory::makeCollection($entities);
    }

    public static function create(Position $position): Position
    {
        $position->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'name' => $position->getName(),
            'slug' => $position->getSlug(),
            'status' => $position->getStatus(),
            'created_at' => $position->getCreatedAt()
        ]);
        $position->setId($id);
        return $position;
    }

    public static function update(Position $position): Position
    {
        $instance = self::getInstance();

        $position->setUpdatedAt(time());
        self::newQuery()
            ->where($instance->primaryKey, $position->getId())
            ->update([
                'name' => $position->getName(),
                'slug' => $position->getSlug(),
                'status' => $position->getStatus(),
                'updated_at' => $position->getUpdatedAt()
            ]);
        $positionAfterUpdate = self::newQuery()
            ->where($instance->primaryKey, $position->getId())->first();
        $position->setCreatedAt($positionAfterUpdate->created_at);
        return $position;
    }

    public static function delete(int $positionId): int
    {
        $instance = self::getInstance();
        $positionBeforeDelete = self::newQuery()
            ->where($instance->primaryKey, $positionId)->first();
        LogAdminService::create($positionBeforeDelete, ResourceCode::DELETE, $after = [], $before = $positionBeforeDelete, $instance->table);
        return self::newQuery()
            ->where($instance->primaryKey, $positionId)
            ->delete();
    }
}
