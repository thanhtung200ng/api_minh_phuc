<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Agecy;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Enums\agecyCode;
use App\Http\Repositories\Factories\AgecyFactory;
use App\Http\Repositories\Interfaces\AgecyInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class AgecyService extends BaseService implements AgecyInterface
{
    protected $table = 'ui_agents';

    /**
     * @inheritDoc
     */
    public static function getById(int $id): ?Agecy
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $id)
            ->first();
        return $entity ? AgecyFactory::make($entity) : null;
    }

    public static function getByCustomer($id){
        $query = self::newQuery();
        $entity = $query
            ->where('customer_id', $id)
            ->first();
        return $entity;
    }

    public static function getByListCustomer($listId): Collection
    {
        $query = self::newQuery();
        $query = $query->whereIn('customer_id', $listId);
        return $query->get();
    }
   public static function agentdDisabled($id){
       $query = self::newQuery();
       $query->where('id', $id)->update(['status' => agecyCode::DISABLED]);
   }
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset((int)$offset);
            $query->limit((int)$count);
        }
        $entities = $query->get();
        return AgecyFactory::makeCollection($entities);
    }

    /**
     * @inheritDoc
     */

    public static function  createAll($obj){
        $query = self::newQuery();
        $id = self::newQuery()->insertGetId($obj);
        return $query->where('id', $id)->first();
    }
    public static function create(Agecy $agecy): Agecy
    {
        $agecy->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'full_name' => $agecy->getFullName(),
            'customer_id' => $agecy->getCustomerId(),
            'account' => $agecy->getAccount(),
            'status' => $agecy->getStatus(),
            'created_at' => $agecy->getCreatedAt(),
            'city_id' => $agecy->getCityId(),
            'district_id' => $agecy->getDistrictId(),
            'ward_id' => $agecy->getWardId(),
            'street' => $agecy->getStreet(),
            'rank_agency' => $agecy->getRankAgecy(),
            'mobile' => $agecy->getMobile(),
        ]);
        $agecy->setId($id);
        return $agecy;
    }

    public function createAgencyByDeposit($customer,$rank){
           $address = $customer->getAddressToArray();
           self::newQuery()->insertGetId([
            'full_name' => $customer->getFullName(),
            'customer_id' => $customer->getId(),
            'account' => $customer->getAccount(),
            'status' => $customer->getStatus(),
            'created_at' => time(),
            'city_id' => $address['temporary_residence_address']['city_of_temporary_residence']['id'] ?? 0,
            'district_id' => $address['temporary_residence_address']['district_of_temporary_residence']['id'] ?? 0,
            'ward_id' => $address['temporary_residence_address']['ward_of_temporary_residence']['id'] ?? 0,
            'street' => $address['temporary_residence_address']['street_of_temporary_residence'] ?? 0,
            'rank_agency' => $rank,
            'mobile' => $customer->getMobile(),
        ]);

    }
    public function createAgencyByExclusiveDistributionV2($customer){
        $query = self::newQuery();
        $id = $query->insertGetId([
            'full_name' => $customer['full_name'],
            'customer_id' => $customer['id'],
            'account' => $customer['account'],
            'status' => $customer['status'],
            'created_at' => time(),
            'city_id' => $customer['city_id'] ?? 0,
            'district_id' => $customer['district_id'] ?? 0,
            'ward_id' => $customer['ward_id'] ?? 0,
            'street' => $customer['street'] ?? 0,
            'rank_agency' => 3,
            'mobile' => $customer['mobile'],
        ]);
        return self::newQuery()->where('id', $id)->first();
    }
    public function createAgencyByExclusiveDistribution($customer){
        $address = $customer->getAddressToArray();
        $id = self::newQuery()->insertGetId([
            'full_name' => $customer->getFullName(),
            'customer_id' => $customer->getId(),
            'account' => $customer->getAccount(),
            'status' => $customer->getStatus(),
            'created_at' => time(),
            'city_id' => $address['temporary_residence_address']['city_of_temporary_residence']['id'] ?? 0,
            'district_id' => $address['temporary_residence_address']['district_of_temporary_residence']['id'] ?? 0,
            'ward_id' => $address['temporary_residence_address']['ward_of_temporary_residence']['id'] ?? 0,
            'street' => $address['temporary_residence_address']['street_of_temporary_residence'] ?? 0,
            'rank_agency' => 3,
            'mobile' => $customer->getMobile(),
        ]);
        return self::newQuery()->where('id', $id)->first();
    }

    public static function updateAgencyByExclusiveDistribution($id){
        self::newQuery()->where('customer_id', $id)->update(['rank_agency' => 3]);
        return self::newQuery()->where('customer_id', $id)->first();
    }

    /**
     * @inheritDoc
     */
    public static function update(Agecy $agecy , $id): Agecy
    {
        $agecy->setUpdatedAt(time());
        self::newQuery()->where('id', $id)->update([
            'full_name' => $agecy->getFullName(),
            'customer_id' => $agecy->getCustomerId(),
            'account' => $agecy->getAccount(),
            'status' => $agecy->getStatus(),
            'city_id' => $agecy->getCityId(),
            'district_id' => $agecy->getDistrictId(),
            'ward_id' => $agecy->getWardId(),
            'street' => $agecy->getStreet(),
            'rank_agency' => $agecy->getRankAgecy(),
            'updated_at' => $agecy->getUpdatedAt(),
            'mobile' => $agecy->getMobile(),
        ]);
        return $agecy;
    }


    public static function updateAgencyByDeposit($id, $rank){
        self::newQuery()->where('customer_id', $id)->update([
            'rank_agency' => $rank
        ]);
    }

    /**
     * @inheritDoc
     */
    public static function delete(int $agencyId): int
    {
        self::newQuery()->where('id' , $agencyId)->update([
            'status' => -4
        ]);
        return self::newQuery()->where('id' , $agencyId)->first();
    }

    public function checkisRanks($code): bool
    {
        if ($code == agecyCode::agecy || $code == agecyCode::supermarket) {
            return true;
        }
        return false;
    }

    public function getAddress1($address){
        $agency = self::newQuery()
            ->rightJoin('ui_customers', 'ui_agents.customer_id', '=','ui_customers.id')
            ->where('ui_agents.city_id',$address)->where('ui_agents.status', 1)
            ->where('ui_customers.status', 1)
            ->orWhere('ui_customers.is_cty', 1)
            ->select('ui_agents.*', 'ui_customers.full_name as cty_name', 'ui_customers.account as cty_account')
            ->get();
        return $agency;
    }

    public function getAddress($address){
        $agency = self::newQuery()->where('ui_agents.city_id', $address)
            ->join('__cities', 'ui_agents.city_id','=','__cities.id')
            ->join('ui_customers', 'ui_agents.customer_id','=','ui_customers.id')
            ->select('ui_agents.*', '__cities.name as city_name')->orWhere('ui_agents.root', 1)
            ->get();
        return $agency;
    }

}
