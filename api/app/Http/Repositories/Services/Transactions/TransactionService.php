<?php


namespace App\Http\Repositories\Services\Transactions;


use App\Http\Repositories\Entities\Deposit;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Entities\Transaction;
use App\Http\Repositories\Factories\TransactionFactory;
use App\Http\Repositories\Interfaces\TransactionInterface;
use App\Http\Repositories\Services\BaseService;
use Illuminate\Support\Collection;

class TransactionService extends BaseService implements TransactionInterface
{
    protected $table = 'ui_transactions';

    public static function getById(int $id): ?Transaction
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        self::joinTemplate($query);
        $query = $query->where('_mr_mp_tr.id', $moneyRankId);
        $entity = $query->first();
        return $entity ? RankMpMartFactory::make($entity) : null;
    }

    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();

        $query = $query->leftJoin('ui_customers as c_from', function ($join) {
            $join->on('c_from.id', '=', 'ui_transactions.from_customer_id');
        })->leftJoin('ui_customers as c_to', function ($join) {
            $join->on('c_to.id', '=', 'ui_transactions.to_customer_id');
        })->leftJoin('ui_customer_wallets as cw_from', function ($join) {
            $join->on('cw_from.id', '=', 'ui_transactions.from_customer_wallet_id');
        })->leftJoin('ui_customer_wallets as cw_to', function ($join) {
            $join->on('cw_to.id', '=', 'ui_transactions.to_customer_wallet_id');
        })->leftJoin('ui_wallets as w_from', function ($join) {
            $join->on('w_from.id', '=', 'cw_from.wallet_id');
        })->leftJoin('ui_wallets as w_to', function ($join) {
            $join->on('w_to.id', '=', 'cw_to.wallet_id');
        })->leftJoin('ui_members as mem_approved', function ($join) {
            $join->on('mem_approved.id', '=', 'ui_transactions.approved_by');
        })->select(
            'c_from.full_name as from_customer_full_name', 'c_from.account as from_customer_account',
            'cw_from.status as from_customer_wallet_status',
            'c_from.status as from_customer_status',
            'c_to.full_name as to_customer_full_name', 'c_to.account as to_customer_account',
            'c_to.status as to_customer_status',
            'cw_to.status as to_customer_wallet_status',
            'w_from.name as from_customer_wallet_name',
            'w_to.name as to_customer_wallet_name',
            'mem_approved.last_name as approved_name',
            'ui_transactions.*'
        );
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();
        return TransactionFactory::makeCollection($entities);
    }

    /**
     * @param array $transaction
     * @return int
     */
    public static function create(array $transaction): int
    {
        return self::newQuery()->insertGetId([
            'deposit_id' => $transaction['deposit_id'],
            'ranks' => $transaction['ranks'],
            'first_money' => $transaction['first_money'],
            'last_money' => $transaction['last_money'],
            'before_balance_from_customer_wallet' => $transaction['before_balance_from_customer_wallet'],
            'after_balance_from_customer_wallet' => $transaction['after_balance_from_customer_wallet'],
            'before_balance_to_customer_wallet' => $transaction['before_balance_to_customer_wallet'],
            'after_balance_to_customer_wallet' => $transaction['after_balance_to_customer_wallet'],
            'discount' => $transaction['discount'],
            'fee' => $transaction['fee'],
            'from_customer_id' => $transaction['from_customer_id'],
            'to_customer_id' => $transaction['to_customer_id'],
            'from_customer_wallet_id' => $transaction['from_customer_wallet_id'],
            'to_customer_wallet_id' => $transaction['to_customer_wallet_id'],
            'transaction_code' => $transaction['transaction_code'],
            'status' => $transaction['status'],
            'member_created_at' => $transaction['member_created_at'],
            'member_created_by' => $transaction['member_created_by'],
        ]);
    }

    public static function update(array $objToSave, int $id): Transaction
    {
        // TODO: Implement update() method.
    }

    public static function delete(int $id)
    {
        // TODO: Implement delete() method.
    }
}
