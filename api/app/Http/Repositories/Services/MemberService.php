<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Member;
use App\Http\Repositories\Factories\MemberFactory;
use App\Http\Repositories\Interfaces\MemberInterface;
use App\Models\BaseModel;

class MemberService extends BaseService implements MemberInterface
{
    protected $table = 'ui_members';

    /**
     * @param int $memberId
     * @return Member|null
     */
    public static function getById(int $memberId): ?Member
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $memberId)
            ->first();
        return $entity ? MemberFactory::make($entity) : null;
    }
    public static function getByIdActive(int $memberId): ?Member
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        $entity = $query
            ->where('status', BaseModel::getStatusActive())
            ->where($instance->primaryKey, $memberId)
            ->first();
        return $entity ? MemberFactory::make($entity) : null;
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();

        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();
        return MemberFactory::makeCollection($entities);
    }

    /**
     * @param Member $user
     * @return Member $user
     */
    public static function create($user)
    {
        $user->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'full_name' => $user->getFullName(),
            'account' => $user->getAccount(),
            'mobile' => $user->getMobile(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'address' => $user->getAddress(),
            //'position_id' => $user->getPositionId(),
            'status' => $user->getStatus(),
            'created_at' => $user->getCreatedAt()
        ]);

        $user->setId($id);
        return $user;
    }

    public static function update($user)
    {
        // TODO: Implement update() method.
    }

    public static function delete($userId)
    {
        $instance = self::getInstance();
        $query = self::newQuery()->where($instance->primaryKey, $userId);
        $walletBeforeDelete = $query->first();
        if (!$walletBeforeDelete){
            return 0;
        }
//        LogAdminService::create($walletBeforeDelete, ResourceCode::DELETE, json_encode($walletBeforeDelete),[],$instance->table);
        return $query->update(['status' => 0]);

    }
}
