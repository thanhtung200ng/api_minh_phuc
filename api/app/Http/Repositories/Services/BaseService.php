<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class BaseService
{
    protected $primaryKey = 'id';
    protected $table = '';
    protected static $instance = false;
    protected static $instances = [];



    public static function getInstance()
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
        }

        return self::$instances[$cls];
    }

    /**
     * @return Builder
     */
    public static function newQuery(): Builder
    {
        $instance = self::getInstance();
        return app('db')->table($instance->table);
    }

    /**
     * @param Builder $query
     * @param Order[]|Collection $orders
     * @return Builder $query
     */
    protected static function processOrder(Builder $query, $orders): Builder
    {
        foreach ($orders as $order) {
            $query->orderBy($order->getColumnName(), $order->getValue());
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param Filter[]|Collection $filters
     * @return Builder $query
     */
    protected static function processFilter(Builder $query, $filters): Builder
    {
        foreach ($filters as $filter) {
            switch ($filter->getOperand()) {
                case 'IsEqualTo':
                    $query->having($filter->getColumnName(), '=', $filter->getValue());
                    break;
                case 'IsEqualToOrNull':
                    $query->where(function ($query) use ($filter) {
                        /** @var Builder $query */
                        $query->having($filter->getColumnName(), '=', $filter->getValue())
                            ->orWhereNull($filter->getColumnName());
                    });
                    break;
                case 'IsNull':
                    $query->whereNull($filter->getColumnName());
                    break;
                case 'IsNotEqualTo':
                    $query->having($filter->getColumnName(), '<>', $filter->getValue());
                    break;
                case 'IsNotNull':
                    $query->whereNotNull($filter->getColumnName());
                    break;
                case 'StartWith':
                    $query->having($filter->getColumnName(), 'LIKE', $filter->getValue() . '%');
                    break;
                case 'DoesNotContains':
                    $query->having($filter->getColumnName(), 'NOT LIKE', '%' . $filter->getValue() . '%');
                    break;
                case 'Contains':
                    $query->having($filter->getColumnName(), 'LIKE', '%' . $filter->getValue() . '%');
                    break;
                case 'EndsWith':
                    $query->having($filter->getColumnName(), 'LIKE', '%' . $filter->getValue());
                    break;
                case 'In':
                    $query->whereIn($filter->getColumnName(), $filter->getValue());
                    break;
                case 'NotIn':
                    $query->whereNotIn($filter->getColumnName(), $filter->getValue());
                    break;
                case 'Between':
                    $query->havingBetween($filter->getColumnName(), $filter->getValue());
                    break;
                case 'IsAfterThanOrEqualTo':
                case 'IsGreaterThanOrEqualTo':
                    $query->having($filter->getColumnName(), '>=', $filter->getValue());
                    break;
                case 'IsGreaterThanOrNull':
                    $query->where(function ($query) use ($filter) {
                        /** @var Builder $query */
                        $query->having($filter->getColumnName(), '>', $filter->getValue())
                            ->orWhereNull($filter->getColumnName());
                    });
                    break;
                case 'IsAfterThan':
                case 'IsGreaterThan':
                    $query->having($filter->getColumnName(), '>', $filter->getValue());
                    break;
                case 'IsBeforeThanOrEqualTo':
                case 'IsLessThanOrEqualTo':
                    $query->having($filter->getColumnName(), '<=', $filter->getValue());
                    break;
                case 'IsBeforeThan':
                case 'IsLessThan':
                    $query->having($filter->getColumnName(), '<', $filter->getValue());
                    break;
            }
        }
        return $query;
    }

    public static function getStatusActive(): int
    {
        return 1;
    }

    /**
     * @return int
     */

    protected static function getStatusInActive(): int
    {
        return 0;
    }

    /**
     * @return int
     */

    public static function getStatusDeleted(): int
    {
        return -4;
    }

    /**
     * @return int
     */

    protected static function getStatusDisabled(): int
    {
        return -1;
    }


    protected static function fetchKeyJoin(string $column): string
    {
        $instance = self::getInstance();
        return $instance->table.'.'.$column;
    }
}
