<?php


namespace App\Http\Repositories\Services;

use App\Http\Repositories\Enums\TransactionCode;
class  WalletBuyProductVariantServices extends BaseService
{
    protected $table = 'ui_allow_pay_product_variant_by_wallet';

    public function getListWalletBuyProduct($products, $customer_id, $walletIds = []){
        $query = self::newQuery();
        $list = $query
            ->join('ui_customer_wallets', 'ui_allow_pay_product_variant_by_wallet.wallet_id', '=', 'ui_customer_wallets.wallet_id')
            ->join('ui_wallets', 'ui_wallets.id', '=', 'ui_allow_pay_product_variant_by_wallet.wallet_id')
            ->whereIn('ui_allow_pay_product_variant_by_wallet.product_item_id', $products)
            ->where('ui_customer_wallets.customer_id', '=', $customer_id )
            ->whereIn('ui_customer_wallets.wallet_id', $walletIds)
            ->select('ui_customer_wallets.*', 'ui_wallets.name as wallet_name','ui_allow_pay_product_variant_by_wallet.product_item_id as productItemId')
            ->groupBy('wallet_id')
            ->get();
        return $list;
    }

    public static function create($products_item_id, $wallet)
    {
       $id =  self::newQuery()->insertGetId([
            'wallet_id' => $wallet,
            'product_item_id' => $products_item_id,
        ]);
        return self::newQuery()->where('id',$id )->first();
    }

    public static function update($products_item_id, $wallet, $count)
    {
        if($count === 0){
            self::newQuery()->where('product_item_id', $products_item_id)->delete();
        }
        $id =  self::newQuery()->insertGetId([
            'wallet_id' => $wallet,
            'product_item_id' => $products_item_id,
        ]);
        return self::newQuery()->where('id',$id )->first();
    }


}
