<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Category;
use App\Http\Repositories\Factories\CateFactory;
use App\Http\Repositories\Interfaces\CateInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CategoryService extends BaseService implements CateInterface
{
    protected $table = 'ui_categories';

    /**
     * @param int $cate_id
     * @return Category|null
     */
    public static function getById(int $cate_id): ?Category
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $cate_id)
            ->first();
        return $entity ? CateFactory::make($entity) : null;
    }

    public static function getBySlug(string $slug): ?Category
    {
        $query = self::newQuery();
        $entity = $query
            ->where('slug', $slug)
            ->first();
        return $entity ? CateFactory::make($entity) : null;
    }

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }

        $entities = $query
            ->orderBy('ui_categories.created_at','desc')
            ->get();
        return CateFactory::makeCollection($entities);
    }

    public static function getAllCategory($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get()->keyBy('id');
        return $entities;
    }

    /**
     * @param Category $cate
     * @return Category $cate
     */
    public static function create($cate)
    {
        $cate->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'name' => $cate->getName(),
            'slug' => $cate->getSlug(),
            'is_leaf' => $cate->getIsLeaf(),
            'parent_id' => $cate->getParentId(),
            'status' => $cate->isDisabled(),
            'created_at' => $cate->getCreatedAt(),
            'updated_at' => $cate->getUpdatedAt()
        ]);
        $cate->setId($id);
        return $cate;
    }

    public static function update($user)
    {
        // TODO: Implement update() method.
    }

    public static function delete($userId)
    {
        // TODO: Implement delete() method.
    }
    public static function get_category_tree_id_list()
    {
        $data = DB::table('ui_categories')->get();
        $gia_pha_dong_ho = collect($data)->keyBy('id');
        $flag = 0;
        $result = $gia_pha_dong_ho->map(function ($item) use ($gia_pha_dong_ho, $flag) {
            if($flag === 2) {
                return 0;
            }
            if (! ($parent = $gia_pha_dong_ho->get($item->parent_id))) {
                $item->extraClass = 'menu-item-has-children has-mega-menu';
                $item->subClass = 'sub-menu';
                $item->mega = true;
                return $item;
            }
            $flag++;

            $parent->megaContent[] = $item;
        })->filter();
        return $result;
    }

    /**
     * @makeBy @kayn
     * @param int $parentId
     * @return Collection
     */
    public static function getAllSubCategories(int $parentId): Collection
    {
        $query = self::newQuery();
        return $query
            ->where('parent_id', $parentId)
            ->get();
    }

    /**
     * @makeBy @kayn
     * @param $id
     * @param $data
     * @return array
     */
    public static function getTreeReverseById($id, &$data): array
    {
        $cur_item = self::newQuery()->where('status', self::getStatusActive())
            ->where('id', $id)->first();
        if(!empty($cur_item)) {
            $p_item = self::newQuery()
                ->where('id', $cur_item->parent_id)->first();
            $data[] = $cur_item;
            if(!empty($p_item)) {
                self::getTreeReverseById($p_item->id, $data);
            }
        }
        return $data;
    }


}
