<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\ProductVariants;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\ProductVariantsFactory;
use App\Models\ProductVariantModel;
use Illuminate\Support\Facades\DB;

class ProductVariantsService extends BaseService
{
    protected $table = 'ui_product_items';

    /**
     * @param string $slug
     * @return ProductVariants|null
     */
    public static function getBySlug(string $slug): ?ProductVariants
    {

        $query = self::newQuery();
        $entity = $query
            ->where('slug', $slug)
            ->first();
        return $entity ? ProductVariantsFactory::make($entity) : null;
    }

    public static function getById($id)
    {
        $query = self::newQuery();
        $entity  = $query
            ->where('id', $id)
            ->first();
        return $entity ? ProductVariantsFactory::make($entity) : null;
    }


    public static function getByIdGroupBy($id)
    {
        $query = self::newQuery();
        $entity  = $query
            ->where('id', $id)
            ->where('is_choose_agent',0)
            ->get()->groupBy('sales_booth_id');
        return $entity ? $entity : null;
    }



    /**
     * @param ProductVariants $variant
     * @return ProductVariants
     */

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query = $query->leftJoin('ui_customers as cus', function ($join) {
            $join->on('cus.id', '=', 'ui_product_items.seller_by');
        })->leftJoin('ui_salesbooth as sb', function ($join) {
            $join->on('sb.id', '=', 'ui_product_items.sales_booth_id');
        })->select('cus.full_name as seller_name', 'ui_product_items.*',
            'sb.id as salesbooth_id', 'sb.name as salesbooth_name', 'sb.slug as salesbooth_slug', 'sb.status as salesbooth_status', 'sb.customer_id as customer_id');
        $entities = $query->get();
        return ProductVariantsFactory::makeCollection($entities);
    }

     public static function getAllProductsByCty(){
         $listProducts = self::newQuery()->where([['status', 1], ['sales_booth_id', 1]])->select('name', 'id')->get();
         if(!$listProducts){
             return null;
         }
         return $listProducts;
     }
    public static function getAllProductItem($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get()->groupBy('product_id');
        return $entities;
    }


    public static function getAllProductHotSale($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query->where('ui_product_items.is_hot_sale', 1)->join('ui_products', 'ui_products.id' , '=' ,'ui_product_items.product_id');
        $entities = $query->get();
        return $entities;
    }

        public static function getAllProductHotOfCty($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query->where('status', 1)->orderBy('created_at', 'desc');
        $entities = $query->get();
        return $entities;
    }

    public static function create(ProductVariants &$variant){
        $variant->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'product_id' => $variant->getProductId(),
            'sku' => $variant->getSku(),
            'name' => $variant->getName(),
            'slug' => $variant->getSlug(),
            'price' => $variant->getPrice(),
            'price_sale' => $variant->getPriceSale(),
            'sales_booth_id' => $variant->getSalesBooth_id(),
            'quantity' => $variant->getQuantity(),
            'images' => $variant->getImagesToString(),
            'ranks' => $variant->getRanksToString(),
            'created_at' => $variant->getCreatedAt(),
            'seller_by' => $variant->getSellerBy(),
            'is_choose_agent' => $variant->getChooseAgent(),
            'discount_product_by_quantity' => $variant->getDiscountProductByQuantity(),
        ]);

        $variant->setId($id);
        return $variant;
    }

    public static function update($value,$id){
        $query = self::newQuery();
        $value->setCreatedAt(time());
        $query->where('product_id', $id)->where('id',$value->getId())->update([
             'name' => $value->getName(),
             'property_item_id' => $value->getPropertyItemId(),
             'product_id' => $value->getProductId(),
             'sku' => $value->getSku(),
             'images' => json_encode($value->getImages()),
             'updated_at' => $value->getUpdatedAt(),
             'price' => $value->getPrice(),
             'quantity' => $value->getQuantity(),
             'gtin' => $value->getGtin(),
             'slug' => $value->getSlug(),
             'ranks' => json_encode($value->getRanks()),
             'price_sale' => $value->getPriceSale(),
             'is_choose_agent' => $value->getChooseAgent(),
            'discount_product_by_quantity' => $value->getDiscountProductByQuantity(),
        ]);
    }

    public static function deleted($id)
    {
        $instance = self::getInstance();
        $BeforeDelete = self::newQuery()
            ->find($id);
        LogAdminService::create($id, ResourceCode::DELETE,$instance->table,null,$BeforeDelete,null);
        return $after = self::newQuery()
            ->where('product_id', $id)
            ->delete();
    }

    public static function getAllProductVariants($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []): \Illuminate\Support\Collection
    {
        $query = app('db')->table('ui_products as p');
        $query = $query->leftJoin('ui_product_items as pi', function ($join) {
            $join->on('p.id', '=', 'pi.product_id');
        })->leftJoin('ui_categories as c', function ($join) {
            $join->on('c.id', '=', 'p.category_id');
        })->leftJoin('ui_customers as cus', function ($join) {
            $join->on('cus.id', '=', 'pi.seller_by');
        })->leftJoin('ui_salesbooth as sb', function ($join) {
            $join->on('sb.id', '=', 'pi.sales_booth_id');
        })->select('cus.full_name as seller_name', 'cus.id as seller_id',
            'p.id as product_id', 'c.id as category_id', 'c.name as category_name', 'c.slug as category_slug',
            'pi.id', 'pi.price', 'pi.price_sale', 'pi.slug',
            'pi.name', 'pi.sku', 'pi.quantity', 'pi.status', 'pi.images','sb.id as salesbooth_id','sb.name as salesbooth_name','sb.address as salesbooth_address',
            'pi.is_choose_agent as choose_agent'
        );
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $total = $query->count();

        return $query->get();
    }
    public static function getAllProductFeatured($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []): \Illuminate\Support\Collection
    {
        $query = app('db')->table('ui_products as p');
        $query = $query->leftJoin('ui_product_items as pi', function ($join) {
            $join->on('p.id', '=', 'pi.product_id');
        })->leftJoin('ui_categories as c', function ($join) {
            $join->on('c.id', '=', 'p.category_id');
        })->leftJoin('ui_customers as cus', function ($join) {
            $join->on('cus.id', '=', 'pi.seller_by');
        })->select('cus.full_name as seller_name', 'cus.id as seller_id',
            'p.id as product_id', 'c.id as category_id', 'c.name as category_name', 'c.slug as category_slug',
            'pi.id', 'pi.price', 'pi.price_sale', 'pi.slug',
            'pi.name', 'pi.sku', 'pi.quantity', 'pi.status', 'pi.images', 'pi.is_featured',
        );
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $total = $query->count();

        return $query->get();
    }

    public static function getProductsByProductsItem($id){
        if(!$id){
            return null;
        }
        $query = DB::table('ui_products');
        return $query->where('id', $id)->first();
    }
}
