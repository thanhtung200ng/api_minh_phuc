<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Tags;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Factories\TagsFactory;
use App\Http\Repositories\Interfaces\TagsInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TagsService extends BaseService implements TagsInterface
{
    protected $table = 'ui_tags';
    /**
     * @inheritDoc
     */
    public static function getById(int $tagsId): ?Tags
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $tagsId)
            ->first();
        return $entity ? TagsFactory::make($entity) : null;
    }

    /**
     * @inheritDoc
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();

        return TagsFactory::makeCollection($entities);
    }

    public static function insertTagsProductPost($tagsRequest, $dataId)
    {
        $tag = DB::table('ui_tags')->find($tagsRequest);
        $data = [];
        $data['id_tags'] = $tagsRequest;
        if ($tag->type === 0){
            $data['id_post'] = $dataId;
        }
        elseif ($tag->type === 1){
            $data['id_product'] = $dataId;
        }
        return DB::table('ui_tags_product_post')->updateOrInsert($data);
    }
//    public static function updateTagsProductPost($tagsRequest, $dataId)
//    {
//        $tag = DB::table('ui_tags')->find($tagsRequest);
//        $data = [];
//        $data['id_tags'] = $tagsRequest;
//        if ($tag->type === 0){
//            $data = ['t'];
//        }
//        elseif ($tag->type === 1){
//            $data['id_product'] = $dataId;
//        }
//        return DB::table('ui_tags_product_post')->where('id_tags',$tagsRequest )->update($data);
//    }

    /**
     * @inheritDoc
     */
    public static function create(Tags $tag): Tags
    {
        $tag->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'name' => $tag->getName(),
            'slug' => $tag->getSlug(),
            'status' => $tag->getStatus(),
            'created_at' => $tag->getCreatedAt(),
            'created_by' => $tag->getCreatedBy(),
        ]);

        $tag->setId($id);
        return $tag;
    }

    /**
     * @inheritDoc
     */
    public static function update(Tags $tag): Tags
    {
        $instance = self::getInstance();
        $tag->setCreatedAt(time());
        self::newQuery()
            ->where($instance->primaryKey, $tag->getId())
            ->update([
                'name' => $tag->getName(),
                'slug' => $tag->getSlug(),
                'status' => $tag->getStatus(),
                'created_at' => $tag->getCreatedAt(),
                'created_by' => $tag->getCreatedBy()
            ]);
        $positionAfterUpdate = self::newQuery()
            ->where($instance->primaryKey, $tag->getId())->first();
        $tag->setCreatedAt($positionAfterUpdate->created_at);
        return $tag;
    }

    /**
     * @inheritDoc
     */
    public static function delete(int $tagId): int
    {
        $instance = self::getInstance();
        $tagBeforeDelete = self::newQuery()
            ->where($instance->primaryKey, $tagId)->first();
        if (!$tagBeforeDelete){
            return 0;
        }
//        LogAdminService::create($tagBeforeDelete, ResourceCode::DELETE, json_encode($tagBeforeDelete),[],$instance->table);
        return self::newQuery()
            ->where($instance->primaryKey, $tagId)
            ->delete();
    }
}
