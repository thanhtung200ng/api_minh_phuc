<?php


namespace App\Http\Repositories\Services;
use App\Http\Repositories\Entities\PurchaseOdersItem;
use App\Http\Repositories\Entities\PurchaseOders;
use App\Http\Repositories\Interfaces\PurchaseOderItemInterface;
use Dflydev\DotAccessData\Data;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Description;


class PurchaseOderItemService  extends BaseService implements PurchaseOderItemInterface
{

    protected $table = 'ui_purchase_order_items';

    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = []){

    }

    public static function create($order){
        return self::newQuery()->insertGetId($order);
    }

    public static function update($purchaseOrder){

    }

    public static function delete($PurchaseOdersItem_id){

    }

    public static function createInsertPurchaseOderItem(PurchaseOdersItem $data ){
            $data->setCreatedAt(time());
            $id = self::newQuery()->insertGetId([
                'total_money' => $data->getTotalMoney(),
                'quantity' => $data->getQuantity(),
                'product_item_id' => $data->getProductItemId(),
                'created_at' => $data->getCreatedAt(),
                'purchase_oders_id' => $data->getPurchaseOdersId(),
                'name_product' => $data->getNameProducts(),
                'price' => $data->getPrice(),
                'agency' => $data->getCustomerIdTraHang(),
            ]);
            $data->setId($id);
            return $data;
    }

    public static function ExceptForUserWallets($wallet_id, $moneyOrder)
    {
        $wallet = DB::table('ui_customer_wallets')->where('id', $wallet_id);
        $money = $wallet->first()->current_balance - $moneyOrder;
        $wallet->update(['current_balance' => $money]);
    }
}
