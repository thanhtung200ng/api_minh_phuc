<?php


namespace App\Http\Repositories\Services;


use App\Exceptions\Handler;
use App\Http\Repositories\Entities\RankMpMart;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Http\Repositories\Factories\RankMpMartFactory;

class RankMpMartTotalRevenueService extends RankService
{
    protected $table = '__ranks_mpmart_total_revenue';


    /**
     * @param int $moneyRankId
     * @param array|null $queries
     * @return RankMpMart|null
     */
    public static function getById(int $moneyRankId, ?array $queries = []): ?RankMpMart
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        self::joinTemplate($query);
        $query = $query->where('_mr_mp_tr.id', $moneyRankId);
        $entity = $query->first();
        return $entity ? RankMpMartFactory::make($entity) : null;
    }

    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        self::joinTemplate($query);
        $entities = $query->get();
        return RankMpMartFactory::makeCollection($entities);
    }

    public static function getByMaxMinMoney(float $money): RankMpMart
    {
        $query = self::newQuery();
        $query = $query
            ->where('_mr_mp_tr.min_money', '<=', $money)
            ->where('_mr_mp_tr.max_money', '>', $money);
        self::joinTemplate($query);
        $entities = $query->first();
        return $entities ? RankMpMartFactory::make($entities) : app(RankMpMart::class);
    }

    static function joinTemplate(&$query) {
        return $query->leftJoin('__money_rank_mpmart_total_revenue as _mr_mp_tr', function ($join) {
                $join->on('_mr_mp_tr.rank_total_revenue_id', '=', '__ranks_mpmart_total_revenue.id');
            })
            ->where('__ranks_mpmart_total_revenue.status', self::getStatusActive())
            ->where('_mr_mp_tr.status', self::getStatusActive())
            ->select(
                '_mr_mp_tr.rank_total_revenue_id as rank_id', '_mr_mp_tr.min_money', '_mr_mp_tr.max_money', '_mr_mp_tr.discount',
                '_mr_mp_tr.status', '_mr_mp_tr.created_at', '_mr_mp_tr.created_by', '_mr_mp_tr.sort',
                '_mr_mp_tr.id','__ranks_mpmart_total_revenue.name',
                '__ranks_mpmart_total_revenue.slug',
            );
    }


}
