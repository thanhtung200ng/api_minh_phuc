<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Customer;
use App\Http\Repositories\Entities\CustomerWallet;
use App\Http\Repositories\Entities\RankUser;
use App\Http\Repositories\Factories\CustomerFactory;
use App\Http\Repositories\Factories\CustomerWalletFactory;
use App\Http\Repositories\Factories\RankFactory;
use Illuminate\Support\Collection;

class RankService extends BaseService
{


    /**
     * @param int $customerId
     * @param array|null $queries
     * @return RankUser|null
     */
    public static function getById(int $customerId, ?array $queries = []): ?RankUser
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        $query = $query
            ->where($instance->primaryKey, $customerId);
        $entity = $query->first();
        return $entity ? RankFactory::make($entity) : null;
    }

    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();
        return RankFactory::makeCollection($entities);
    }






}
