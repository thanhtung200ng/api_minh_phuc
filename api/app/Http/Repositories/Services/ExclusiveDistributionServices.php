<?php


namespace App\Http\Repositories\Services;



use App\Http\Repositories\Interfaces\ExclusiveDistributionInterfaces;
use App\Models\ExclusiveDistribution;
use Illuminate\Support\Facades\DB;

class ExclusiveDistributionServices extends BaseServiceEloquent implements ExclusiveDistributionInterfaces
{
    public function __construct()
    {
        $model = new ExclusiveDistribution();
        parent::__construct($model);
    }


    public static function getById(int $id)
    {
        $query = self::newQuery();
        $entity = $query
            ->where('id', $id)
            ->first();
        return $entity->toArray();
    }

    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query =  $query->with('customer','productsItem');
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get()->toArray();
    }


    public static function create($customer, $products){
        $id = self::newQuery()->insertGetId([
          'customer_id' => $customer,
          'products_item_id' => $products,
        ]);
        return self::newQuery()->where('id', $id)->first();
    }

    public static function update($id,$customer,$products){
         self::newQuery()->where('id', $id)->update([
              'customer_id' => $customer,
              'products_item_id' => $products
          ]);
        return self::newQuery()->where('id', $id)->first();
    }

    public static function delete($exclusiveDistribution){

    }


}
