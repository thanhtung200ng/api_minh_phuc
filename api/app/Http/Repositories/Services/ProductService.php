<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Product;
use App\Http\Repositories\Enums\HttpStatusCode;
use App\Http\Repositories\Factories\ProductFactory;
use App\Http\Repositories\Factories\ProductVariantsFactory;
use App\Http\Repositories\Interfaces\ProductInterface;
use Hamcrest\BaseDescription;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductService extends BaseService implements ProductInterface
{
    protected $table = 'ui_products';

    /**
     * @param int $id
     * @return Product|null
     */
    public static function getById(int $id): ?Product
    {
        $instance = self::getInstance();

        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $id)
            ->first();
        return $entity ? ProductFactory::make($entity) : null;
    }


    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();
        return ProductFactory::makeCollection($entities);
    }
    public static function getAllListProducts($offset = 0, &$count = 0, &$total = 0, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $query = $query->leftJoin('ui_product_items', function ($join) {
            $join->on('ui_product_items.product_id', '=', 'ui_products.id');
        })->leftJoin('ui_categories', function ($join) {
            $join->on('ui_categories.id', '=', 'ui_products.category_id');
        })->select(
            'ui_categories.name as cate_name',
            'ui_products.id',
            'ui_product_items.name',
            'ui_product_items.slug',
            'ui_product_items.price',
            'ui_product_items.price_sale',
            'ui_product_items.quantity',
            'ui_product_items.created_at',
            'ui_product_items.updated_at',
            'ui_product_items.images',
            'ui_product_items.sales_booth_id',
            'ui_product_items.status'
        );
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();
        foreach ($entities as $value){
            $value->images = json_decode($value->images);
        }
        return $entities;
    }
    public static function getAllProducts($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get()->groupBy('category_id');
        return $entities;
    }


    public static function getListProducts(?array $query){
       $listObj = DB::table('ui_products')
            ->join('ui_categories','ui_products.categoty_id','=','ui_categories.id')
            ->join('ui_product_items','ui_products.id', '=' ,'ui_product_items.product_id')
           ->select('ui_product_items.id','ui_product_items.images','ui_product_items.images',
               'ui_categories.name as cateName', 'ui_product_items.created_at','ui_product_items.price','ui_product_items.inventory',
               'ui_product_items.status','ui_product_items.name as name')->orderBy('ui_products.created_at');
       if (@$query['whereIn']['column']) {
           $listObj = $listObj->whereIn($query['whereIn']['column'], $query['whereIn']['value']);
       }
        $listObj = $listObj->get();
       if(!empty($listObj)){
           return $listObj;
       }
       return null;
    }

    public static function geListProductNotJoin($listProductsId){
        $listObj = DB::table('ui_products')->whereIn('id', $listProductsId)->get()->keyBy('id');
        if(!$listObj){
            return null;
        }
        return $listObj;
    }

    public static function getlistProductsHot($cateId){
        $lsObj = DB::table('ui_categories')->where('id', $cateId)->select('id','name','slug','created_at')->first();
        if(!empty($lsObj)){
            $lsProducts = DB::table('ui_products')->where('ui_products.categoty_id', $lsObj->id)
                ->join('ui_product_items','ui_products.id', '=' ,'ui_product_items.product_id')
                ->where('ui_product_items.status','=',1)->limit(10)
                ->get();
//            @todo phần này làm ảnh nhiều theo mảng nhé
            foreach ($lsProducts as $value){
//                $value->images = json_decode($value->images);
                $value->created_at = date('m/d/Y', $value->created_at);
            }
            $lsObj->products = $lsProducts;
            return $lsObj;
        }
        return null;
    }
    public static function getlistBoxShowProducts($cateId = [])
    {
        $lsObj = DB::table('ui_categories')->whereIn('id', $cateId)
            ->select('id', 'name', 'slug', 'created_at')->get();
        if (!empty($lsObj)) {
            foreach ($lsObj as $value) {
                $k = self::queryBoxShow($value->id);
                $value->created_at = date('m/d/Y', $value->created_at);
                $value->products = $k;
                $data[] = $value;
            }
            return $data;
        }
        return null;
    }
    public static function queryBoxShow($id){
        $lsProducts = DB::table('ui_products')->where('ui_products.categoty_id',$id )
            ->join('ui_product_items','ui_products.id', '=' ,'ui_product_items.product_id')
            ->join('ui_customers','ui_customers.id','=','ui_product_items.customer_id')
            ->where('ui_product_items.status','=',1)->limit(10)
            ->get(['ui_products.*','ui_product_items.*','ui_customers.full_name']);
        foreach ($lsProducts as $val){
            $val->created_at = date('m/d/Y', $val->created_at);
        }
        return $lsProducts;
    }

    public static function getlistProductsNewArrivals(){
        $lsObj = DB::table('ui_categories')->select('id','name','slug','created_at')
            ->where('ui_categories.id','=',1)
//            ->inRandomOrder()
            ->first();
        if(!empty($lsObj)){
            $lsProducts = DB::table('ui_products')->where('ui_products.categoty_id', $lsObj->id)
                ->join('ui_product_items','ui_products.id', '=' ,'ui_product_items.product_id')
                ->where('ui_product_items.status','=',1)->limit(20)->inRandomOrder()
                ->get();
//            @todo phần này làm ảnh nhiều theo mảng nhé
            foreach ($lsProducts as $value){
//                $value->images = json_decode($value->images);
                $value->created_at = date('m/d/Y', $value->created_at);
            }
            $lsObj->products = $lsProducts;
            return $lsObj;
        }
        return null;
    }

    /**
     * @param Product $product
     * @return Product
     */

    public static function create(Product $product){
        $product->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'description' => $product->getDescription(),
            'category_id' => $product->getCategoryId(),
            'status' => $product->getStatus(),
            'created_at' => $product->getCreatedAt(),
            'created_by' => $product->getCreatedBy(),
            'seller_by' => $product->getSellerBy(),
            'seo' => $product->getSeo()
        ]);
        $product->setId($id);
        return $product;
    }


    public static function UpdateMetaData($data)
    {
       $metaTitle =  $data->getMeta_title();
       $metaDescription =  $data->getMeta_description();
       $metaKeyword =  $data->getMeta_keyword();
       $meta = json_encode(['metaTitle' => $metaTitle,'metaDescription' => $metaDescription,'metaKeyword' => $metaKeyword]);
       if(!empty($meta)){
           $updateMeta = DB::table('ui_products')->where('id',$data->getId())->update(['meta_seo' => $meta]);
           if($updateMeta == 1){
               $dataMeta = DB::table('ui_products')->where('id',$data->getId())->first();
               $dataMeta->meta_seo = json_decode( $dataMeta->meta_seo);
               if(!empty($dataMeta)){
                   return $dataMeta;
               }
           }
       }
       return null;
         //$updateMeta
    }

    public static function createByProductItem($product)
    {
        $product->setCreatedAt(time());
        $id = DB::table('ui_product_items')->insertGetId([
            'property_item_id' => $product->getPropertyItemId(),
            'product_id' => $product->getProductId(),
            'name' => $product->getName(),
            'sku' => $product->getSku(),
            'slug' => $product->getSlug(),
            'price' => $product->getPrice(),
            'price_sale' => $product->getPriceSale(),
//            'gtin' => $product->getGtin(),
            'images' => $product->getImages(),
            'inventory' => $product->getQuatity(),
            'status' => $product->isDisabled(),
            'ranks' => $product->getRank(),
            'created_at' => $product->getCreatedAt()
        ]);
        $product->setId($id);
        return $product;
    }

    public static function create_cha($product)
    {
        $product->setCreatedAt(time());
        $id = DB::table('ui_products')->insertGetId([
            'categoty_id' => $product->getCateId(),
            'description' => $product->getDescription(),
            'content' => $product->getContent(),
            'status' => $product->getStatus(),
            'created_at' => $product->getCreatedAt()
        ]);
        $product->setId($id);
        return $product;
    }

    public static function update($products,$id )
    {
        $products->setUpdatedAt(time());
        DB::table('ui_products')->where('id', $id)->update([
            'category_id' => $products->getCategoryId(),
            'description' => $products->getDescription(),
            'content' => $products->getContent(),
            'status' => $products->getStatus(),
            'seo' => $products->getSeo(),
            'updated_at' => $products->getUpdatedAt(),
            'created_by' => $products->getCreatedBy(),
            'seller_by' => $products->getSellerBy(),
        ]);
    }

    public static function delete($id)
    {
        $query = self::newQuery();
        $query->where('id', $id)->update(
            [
                'status' => self::getStatusDeleted(),
                'updated_at' => time(),
            ]
        );
    }

     public static function checkIdcate($cateId){
         $data = DB::table('ui_categories')
             ->where('id', $cateId)->first();
        if(empty($data)){
            return 0;
        }
        return 1;
     }


     public static function update_cha($cateId){
            $a = $cateId['refer'];
            $b = end($a);
         $id= DB::table('ui_products')->where('id',$cateId['id'] )->update([
             //'categoty_id' => $b,
             'description' => $cateId['description'],
             'content' => $cateId['content'],
//             'update_at' => time() ?? '',
         ]);
         if($id == 1){
             $data = DB::table('ui_products')->where('id',$cateId['id'])->first();
             return $data;
         }
         return null;
     }
     public static function updateDataStep2($data){
        $updateData = DB::table('ui_product_items')->where('id', $data['id'])->update([
            'name' => @$data['name'].'2132eeeeeeee13',
            'sku' => @$data['sku'].'kj',
            'price' => @$data['price'],
            'property_item_id' => @$data['property_item_id'],
            'product_id' => @$data['product_id'],
            'inventory' => @$data['inventory'],
            'status' => $data['status'] ,
//            'gtin' => @$data['gtin'],
            'images' => @$data['imgs'][0],
            'ranks' => @$data['ranks'],
        ]);
             $data = DB::table('ui_product_items')->where('id',$data['id'])->first();
             return $data;
     }
     public static function getCatePropertyProducts($idCate){
         $data = DB::table('ui_categories')->where('status', 1)->where('ui_categories.id',$idCate)
             ->join('ui_item_category','ui_categories.id','=','ui_item_category.category_id')
             ->join('ui_properties','ui_item_category.property_id','=','ui_properties.id')
             ->select('ui_properties.id','ui_properties.name')->get();
         if(!empty($data)){
             return $data;
         }
         return null;
     }
     public static function getPropertyProducts($idCate){
         $data = DB::table('ui_item_category')->where('ui_item_category.category_id',$idCate)
             ->join('ui_items_property_key','ui_item_category.property_id','=','ui_items_property_key.property_id')
             ->join('ui_items_property','ui_items_property_key.property_item_id','=','ui_items_property.id')
             ->select('ui_items_property_key.property_id','ui_items_property.id','ui_items_property.name')->get();
         if(!empty($data)){
             return $data;
         }
         return null;
     }

     public  static function getListProductsItem($id){
        if($id){
            $data = DB::table('ui_product_items')->where('status', BaseService::getStatusActive())->whereIn('id', $id)->get()->keyBy('id')->toArray();
            if(!empty($data)){
                return $data;
            }
            return null;
        }
        return null;
     }

     static function templateJoin(&$query) {
         $query->leftJoin('ui_product_items as pi', function ($join) {
             $join->on('p.id', '=', 'pi.product_id');
         })->leftJoin('ui_categories as c', function ($join) {
             $join->on('c.id', '=', 'p.category_id');
         })->leftJoin('ui_customers as cus', function ($join) {
             $join->on('cus.id', '=', 'pi.seller_by');
         })->select('cus.full_name as seller_name', 'cus.id as seller_id',
             'p.id as product_id', 'c.id as category_id', 'c.name as category_name', 'c.slug as category_slug',
             'pi.id', 'pi.price', 'pi.price_sale', 'pi.slug',
             'pi.name', 'pi.sku', 'pi.quantity', 'pi.status', 'pi.images',
         );
     }
    public static function setView($id){
        $query = self::newQuery();
        $products  = $query
            ->where('id', $id)
            ->first();
        $view = $products->views;
        if(empty($view)){
            $view = 0;
        }
        return $query->where('id', $id)->update(['views' => $view + 1]);
    }
}
