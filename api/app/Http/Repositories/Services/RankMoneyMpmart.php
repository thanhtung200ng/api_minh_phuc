<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Factories\RankFactory;

class RankMoneyMpmart extends RankService
{
    protected $table = '__money_rank_mpmart';

    public static function getAllMoneyRanksMpmart(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $query->join('__ranks_mpmart', '__ranks_mpmart.id', '=', '__money_rank_mpmart.rank_id');
        return $query->get();
    }
}
