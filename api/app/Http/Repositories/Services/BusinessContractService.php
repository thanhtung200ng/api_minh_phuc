<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\BusinessContract;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Factories\BusinessContractFactory;
use App\Http\Repositories\Interfaces\BusinessContractInterface;
use Illuminate\Support\Collection;

class BusinessContractService extends BaseService implements BusinessContractInterface
{
    protected $table = 'ui_business_contract';

    public static function getById(int $id): ?BusinessContract
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $id)
            ->first();
        return $entity ? BusinessContractFactory::make($entity) : null;
    }

    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $query->join('ui_customers', 'ui_customers.id', '=' ,'ui_business_contract.customer_id')
        ->join('ui_contracts', 'ui_contracts.id', '=', 'ui_business_contract.contract_id');
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();
        return $entities;
    }

    public static function create(BusinessContract $businessContract): BusinessContract
    {
        $businessContract->setCreatedAt(time());
        $id = self::newQuery()->insertGetId([
            'name' => $businessContract->getName(),
            'customer_id' => $businessContract->getCustomerId(),
            'contract_id' => $businessContract->getContractId(),
            'documents' => $businessContract->getDocuments(),
            'status' => $businessContract->getStatus(),
            'created_at' => $businessContract->getCreatedAt()
        ]);

        $businessContract->setId($id);
        return $businessContract;
    }

    public static function update(BusinessContract $businessContract): BusinessContract
    {
        // TODO: Implement update() method.
    }

    public static function delete(int $businessContractId): int
    {
        // TODO: Implement delete() method.
    }
}
