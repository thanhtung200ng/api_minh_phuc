<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Wallets;
use App\Http\Repositories\Enums\ResourceCode;
use App\Http\Repositories\Enums\TransactionCode;
use App\Http\Repositories\Factories\WalletsFactory;
use App\Http\Repositories\Interfaces\WalletsInterface;
use App\Models\WalletModel;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class WalletsService extends BaseService implements WalletsInterface
{
    protected $table = 'ui_wallets';
    /**
     * @inheritDoc
     */
    public static function getById(int $walletsId): ?Wallets
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $walletsId)
            ->first();
        return $entity ? WalletsFactory::make($entity) : null;
    }

    /**
     * @inheritDoc
     */
    public static function getAll($offset = 0, $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        $entities = $query->get();
        return $entities;
    }

    public static function countWallet()
    {
        $query = self::newQuery();
        return $query->count();
    }

    public static function create(Wallets $wallet): Wallets
    {

    }

    /**
     * @inheritDoc
     */


    /**
     * @inheritDoc
     */
    public static function update(Wallets $wallet): Wallets
    {

    }

    /**
     * @inheritDoc
     */
    public static function delete(int $walletsId): int
    {

    }

    public static function getWalletPermissionBuyProducts(){
        $query = self::newQuery();
        $listWallet = $query->where('status' , 1)
            ->whereIn('id' , [
                WalletModel::getInstance()->getViTieuDung(),
                WalletModel::getInstance()->getViKhoDiem(),
                WalletModel::getInstance()->getViHopTac(),
                WalletModel::getInstance()->getViHoaHong(),
                WalletModel::getInstance()->getViLoiNhuan(),
            ])->get();
        return $listWallet;
    }

}
