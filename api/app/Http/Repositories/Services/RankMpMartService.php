<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\Deposit;
use App\Http\Repositories\Entities\RankMpMart;
use App\Http\Repositories\Factories\RankMpMartFactory;

class RankMpMartService extends RankService
{
    protected $table = '__ranks_mpmart';

    public static function getByMaxMinMoney(float $money): RankMpMart
    {
        $query = self::newQuery();
        $query = $query
        ->leftJoin('__money_rank_mpmart as _mr_mp', function ($join) {
            $join->on('_mr_mp.rank_id', '=', '__ranks_mpmart.id');
        })
        ->where('_mr_mp.min_money', '<=', $money)
        ->where('_mr_mp.max_money', '>', $money)
        ->where('__ranks_mpmart.status', self::getStatusActive())
        ->where('_mr_mp.status', self::getStatusActive())
            ->select(
            '_mr_mp.rank_id', '_mr_mp.min_money', '_mr_mp.max_money', '_mr_mp.discount',
            '_mr_mp.status', '_mr_mp.created_at', '_mr_mp.created_by', '_mr_mp.sort',
            '_mr_mp.id','__ranks_mpmart.name',
            '__ranks_mpmart.slug',
        );
        $entities = $query->first();
        return $entities ? RankMpMartFactory::make($entities) : app(RankMpMart::class);
    }
}
