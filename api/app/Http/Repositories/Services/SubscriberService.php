<?php


namespace App\Http\Repositories\Services;


use App\Http\Repositories\Entities\SalesBooth;

class SubscriberService extends BaseService
{
    protected $table = 'ui_subscriber';

    public static function getById(int $id)
    {
        $instance = self::getInstance();
        $query = self::newQuery();
        $entity = $query
            ->where($instance->primaryKey, $id)
            ->first();
        return $entity;
    }

    public static function getEmail($email){
       return self::newQuery()->where('email', $email)->first();
    }

    public static function updateEmail($id, $email){
        self::newQuery()->where('id', $id)->update([
            'email' => $email
        ]);
        return self::newQuery()->where('id', $id)->first();
    }

    public static function create($email){
        $id =  self::newQuery()->insertGetId([
            'email' => $email
        ]);
        return self::newQuery()->where('id', $id)->first();
    }
    public static function getAll(int $offset = 0, int $count = 0, &$total = null, $orders = [], $filters = [])
    {
        $query = self::newQuery();
        $query = self::processOrder($query, $orders);
        $query = self::processFilter($query, $filters);
        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query->get();
    }

    public function delete($id){
       return self::newQuery()->where('id',$id)->delete();
    }

}
