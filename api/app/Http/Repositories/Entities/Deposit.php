<?php


namespace App\Http\Repositories\Entities;


use App\Elibs\Date;
use App\Http\Repositories\Enums\TransactionCode;

class Deposit extends BaseEntity
{
    /**
     * @var int
     */
    private $id;
    private $from_customer_id;
    private $to_customer_id;
    private $form_customer_wallet_id;
    private $to_customer_wallet_id;
    private $approved_at;
    private $approved_by;




    /**
     * @var int
     */
    private $created_at;

    /**
     * @var string
     */
    private $created_by;

    /**
     * @var int
     */
    protected $status;
    protected $from_customer_status;
    public function getFromCustomerStatus(): int
    {
        return $this->from_customer_status;
    }

    /**
     * @param int $from_customer_status
     */
    public function setFromCustomerStatus(int $from_customer_status)
    {
        $this->from_customer_status = $from_customer_status;
    }

    /**
     * @return bool
     */
    public function isFromCustomerStatusActive(): bool
    {
        return $this->from_customer_status === $this->getStatusActive();
    }

    /**
     * @return bool
     */
    public function isFromCustomerStatusDisabled(): bool
    {
        return $this->from_customer_status === $this->getStatusDisabled();
    }

    protected $to_customer_status;
    public function getToCustomerStatus(): int
    {
        return $this->to_customer_status;
    }

    /**
     * @param int $to_customer_status
     */
    public function setToCustomerStatus(int $to_customer_status)
    {
        $this->to_customer_status = $to_customer_status;
    }

    /**
     * @return bool
     */
    public function isToCustomerStatusActive(): bool
    {
        return $this->to_customer_status === $this->getStatusActive();
    }

    /**
     * @return bool
     */
    public function isCustomerStatusDisabled(): bool
    {
        return $this->to_customer_status === $this->getStatusDisabled();
    }
    /**
     * @var double
     */
    private $money;



    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    private $rank;
    /**
     * @return string
     */
    public function getRank(): string
    {
        return $this->rank;
    }

    /**
     * @param string $rank
     */
    public function setRank(string $rank)
    {
        $this->rank = $rank;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    public function getCreatedBy(): ?int
    {
        return $this->created_by;
    }

    /**
     * @param int $created_by
     */
    public function setCreatedBy(int $created_by)
    {
        $this->created_by = $created_by;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getStatusToArray(): array
    {
        return $this->getCode($this->status, true);
    }

    /**
     * @return string
     */
    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    public function getMoney(): float
    {
        return $this->money;
    }

    /**
     * @param float $money
     */
    public function setMoney(float $money)
    {
        $this->money = $money;
    }

    public function getFromCustomerId(): int
    {
        return $this->from_customer_id;
    }

    /**
     * @param int $from_customer_id
     */
    public function setFromCustomerId(int $from_customer_id)
    {
        $this->from_customer_id = $from_customer_id;
    }

    public function getToCustomerId(): int
    {
        return $this->to_customer_id;
    }

    /**
     * @param int $to_customer_id
     */
    public function setToCustomerId(int $to_customer_id)
    {
        $this->to_customer_id = $to_customer_id;
    }

    public function getFromCustomerWalletId(): ?int
    {
        return $this->form_customer_wallet_id;
    }

    /**
     * @param int|null $form_customer_wallet_id
     */
    public function setFromCustomerWalletId(?int $form_customer_wallet_id)
    {
        $this->form_customer_wallet_id = $form_customer_wallet_id;
    }

    public function getToCustomerWalletId(): int
    {
        return $this->to_customer_wallet_id;
    }

    /**
     * @param int $to_customer_wallet_id
     */
    public function setToCustomerWalletId(int $to_customer_wallet_id)
    {
        $this->to_customer_wallet_id = $to_customer_wallet_id;
    }

    public function getApprovedAt(): ?int
    {
        return $this->approved_at;
    }

    /**
     * @param int|null $approved_at
     */
    public function setApprovedAt(?int $approved_at)
    {
        $this->approved_at = $approved_at;
    }

    public function getApprovedBy(): ?int
    {
        return $this->approved_by;
    }

    /**
     * @param int|null $approved_by
     */
    public function setApprovedBy(?int $approved_by)
    {
        $this->approved_by = $approved_by;
    }

    private $approved_name;
    public function getApprovedByName(): ?string
    {
        return $this->approved_name;
    }

    /**
     * @param string|null $approved_name
     */
    public function setApprovedByName(?string $approved_name)
    {
        $this->approved_name = $approved_name;
    }

    private $transaction_code;
    public function getTransactionCode(): int
    {
        return $this->transaction_code;
    }

    public function getTransactionCodeToArray(): array
    {
        return TransactionCode::getCode($this->transaction_code, true);
    }

    /**
     * @param int $transaction_code
     */
    public function setTransactionCode(int $transaction_code)
    {
        $this->transaction_code = $transaction_code;
    }


    /**
     * @var string
     */
    private $from_customer_full_name;
    public function getFromCustomerFullName(): string
    {
        return $this->from_customer_full_name;
    }

    /**
     * @param string $from_customer_full_name
     */
    public function setFromCustomerFullName(string $from_customer_full_name)
    {
        $this->from_customer_full_name = $from_customer_full_name;
    }

    /**
     * @var string
     */
    private $from_customer_account;

    public function getFromCustomerAccount(): string
    {
        return $this->from_customer_account;
    }

    /**
     * @param string $from_customer_account
     */
    public function setFromCustomerAccount(string $from_customer_account)
    {
        $this->from_customer_account = $from_customer_account;
    }

    /**
     * @var string
     */
    private $to_customer_full_name;

    public function getToCustomerFullName(): string
    {
        return $this->to_customer_full_name;
    }

    /**
     * @param string $to_customer_full_name
     */
    public function setToCustomerFullName(string $to_customer_full_name)
    {
        $this->to_customer_full_name = $to_customer_full_name;
    }

    /**
     * @var string
     */
    private $to_customer_account;

    public function getToCustomerAccount(): string
    {
        return $this->to_customer_account;
    }

    /**
     * @param string $to_customer_account
     */
    public function setToCustomerAccount(string $to_customer_account)
    {
        $this->to_customer_account = $to_customer_account;
    }

    /**
     * @var string
     */
    private $from_customer_wallet_name;

    public function getFromCustomerWalletName(): ?string
    {
        return $this->from_customer_wallet_name;
    }

    /**
     * @param string|null $from_customer_wallet_name
     */
    public function setFromCustomerWalletName(?string $from_customer_wallet_name)
    {
        $this->from_customer_wallet_name = $from_customer_wallet_name;
    }

    /**
     * @var string
     */
    private $to_customer_wallet_name;

    public function getToCustomerWalletName(): ?string
    {
        return $this->to_customer_wallet_name;
    }

    /**
     * @param string|null $to_customer_wallet_name
     */
    public function setToCustomerWalletName(?string $to_customer_wallet_name)
    {
        $this->to_customer_wallet_name = $to_customer_wallet_name;
    }


    protected $from_customer_wallet_status;
    public function getFromCustomerWalletStatus(): ?int
    {
        return $this->from_customer_wallet_status;
    }

    /**
     * @param int|null $from_customer_wallet_status
     */
    public function setFromCustomerWalletStatus(?int $from_customer_wallet_status)
    {
        $this->from_customer_wallet_status = $from_customer_wallet_status;
    }

    /**
     * @return bool
     */
    public function isFromCustomerWalletStatusActive(): bool
    {
        return $this->getFromCustomerWalletStatus() === $this->getStatusActive();
    }

    /**
     * @return bool
     */
    public function isFromCustomerWalletStatusDisabled(): bool
    {
        return $this->getFromCustomerWalletStatus() === $this->getStatusDisabled();
    }

    protected $to_customer_wallet_status;

    public function  getToCustomerWalletStatus()
    {
        return $this->to_customer_wallet_status;
    }

    /**
     * @param int|null $to_customer_wallet_status
     */
    public function setToCustomerWalletStatus(?int $to_customer_wallet_status)
    {
        $this->to_customer_wallet_status = $to_customer_wallet_status;
    }

    /**
     * @return bool
     */
    public function isToCustomerWalletStatusActive(): bool
    {
        return $this->getToCustomerWalletStatus() === $this->getStatusActive();
    }

    /**
     * @return bool
     */
    public function isToCustomerWalletStatusDisabled(): bool
    {
        return $this->getToCustomerWalletStatus() === $this->getStatusDisabled();
    }


    private $from_customer_wallet_current_balance;
    public function getFromCustomerWalletCurrentBalance()
    {
        return $this->from_customer_wallet_current_balance;
    }

    /**
     * @param int|null $from_customer_wallet_current_balance
     */
    public function setFromCustomerWalletCurrentBalance(?int $from_customer_wallet_current_balance)
    {
        $this->from_customer_wallet_current_balance = $from_customer_wallet_current_balance;
    }

    private $to_customer_wallet_current_balance;
    public function getToCustomerWalletCurrentBalance()
    {
        return $this->to_customer_wallet_current_balance;
    }

    /**
     * @param int|null $to_customer_wallet_current_balance
     */
    public function setToCustomerWalletCurrentBalance(?int $to_customer_wallet_current_balance)
    {
        $this->to_customer_wallet_current_balance = $to_customer_wallet_current_balance;
    }

    public function getCode($selected = false, $return = false): array {
        $listStatus = [
            ['id' => $this->getStatusActive(), 'style' => 'success', 'name' => 'Đã duyệt',],
            ['id' => $this->getStatusInActive(), 'style' => 'primary', 'name' => 'Đơn mới'],
            ['id' => $this->getStatusDeleted(), 'style' => 'danger', 'name' => 'Đã xoá'],
        ];
        if (is_numeric($selected)) {
            foreach ($listStatus as $status) {
                if ($status['id'] === $selected) {
                    $status['checked'] = 'checked';
                    if($return) {
                        return $status;
                    }
                }
            }
        }

        return $listStatus;
    }

}
