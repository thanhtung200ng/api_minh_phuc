<?php


namespace App\Http\Repositories\Entities;


use App\Elibs\Date;

class ProductVariants extends BaseEntity
{

    private $id;
    /**
     * @var int|null
     */
    private $is_product_company;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }


    /**
     * @var string
     */
    private $sku;

    public function setSku(string $sku) {
        $this->sku = $sku;
    }

    public function getSku (): string
    {
        return $this->sku;
    }

    /**
     * @var string
     */
    private $gtin;

    public function setGtin (?string $sku) {
        $this->gtin = $sku;
    }

    public function getGtin (): ?string
    {
        return $this->gtin;
    }

    /**
     * @var string
     */
    private $name;

    public function setName (?string $name) {
        $this->name = $name;
    }

    public function getName (): ?string
    {
        return $this->name;
    }

    /**
     * @var float
     */
    private $price;

    public function setPrice(float $price) {
        $this->price = $price;
    }

    public function getPrice (): float
    {
        return $this->price;
    }

    /**
     * @var float
     */
    private $price_sale;

    public function setPriceSale(float $price) {
        $this->price_sale = $price;
    }

    public function getPriceSale (): float
    {
        return $this->price_sale;
    }

    /**
     * @var int
     */
    private $product_id;

    public function setProductId(int $productId) {
        $this->product_id = $productId;
    }

    public function getProductId (): int
    {
        return $this->product_id;
    }

    /**
     * @var int
     */
    private $quantity;

    public function setQuantity(int $quantity) {
        $this->quantity = $quantity;
    }

    public function getQuantity (): int
    {
        return $this->quantity;
    }
    /**
     * @var int
     */
    private $sales_booth_id;

    public function setSalesBooth_id(int $sales_booth_id) {
        $this->sales_booth_id = $sales_booth_id;
    }

    public function getSalesBooth_id(): int
    {
        return $this->sales_booth_id;
    }

    /**
     * @var string
     */
    private $sales_booth_name;

    public function setSalesBooth_name(string $sales_booth_name) {
        $this->sales_booth_name = $sales_booth_name;
    }

    public function getSalesBooth_name (): string
    {
        return $this->sales_booth_name;
    }




    public function add($key, $name, $value)
    {
        $this->$key[$name] = $value;
        return $this;
    }

    /**
     * @var bool
     */
    private $is_mpmart;

    public function setIsMpmart (bool $is_mpmart) {
        $this->is_mpmart = $is_mpmart;
    }

    public function getIsMpmart (): bool
    {
        return $this->is_mpmart;
    }

    /**
     * @var string
     */
    private $seller_by;
    /**
     * @return int
     */
    public function getSellerBy(): ?int
    {
        return $this->seller_by;
    }

    /**
     * @param int|null $createdBy
     */
    public function setSellerBy(?int $createdBy)
    {
        $this->seller_by = $createdBy;
    }

    /**
     * @var string
     */
    private $seller_name;
    /**
     * @return string
     */
    public function getSellerName(): ?string
    {
        return $this->seller_name;
    }

    /**
     * @param string|null $sellerName
     */
    public function setSellerName(?string $sellerName)
    {
        $this->seller_name = $sellerName;
    }

    /**
     * @var string
     */
    private $created_at;

    public function setCreatedAt (int $created_at) {
        $this->created_at = $created_at;
    }

    public function getCreatedAt (): int
    {
        return $this->created_at;
    }

    /**
     * @var string
     */
    private $created_by;
    /**
     * @return int
     */
    public function getCreatedBy(): ?int
    {
        return $this->created_by;
    }

    /**
     * @param int|null $createdBy
     */
    public function setCreatedBy(?int $createdBy)
    {
        $this->created_by = $createdBy;
    }

    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }

    /**
     * @var string
     */
    private $updated_at;

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * @var string
     */
    private $property_item_id;

    /**
     * @return string
     */
    public function getPropertyItemId(): ?string
    {
        return $this->property_item_id;
    }

    /**
     * @param string|null $property_item_id
     */
    public function setPropertyItemId(?string $property_item_id)
    {
        $this->property_item_id = $property_item_id;
    }
    /**
     * @var string
     */
    private $slug;
    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @var array
     */
    private $images;

    public function setImages(?array $images) {
        $this->images = $images;
    }

    public function getImages (): ?array
    {
        return $this->images;
    }

    public function getImagesToString (): ?string
    {
        return $this->getImages() ? json_encode($this->getImages()) : null;
    }

    public function setImagesDecode (?string $images)
    {
        $this->setImages($images ? (array)json_decode($images) : []);
    }

    /**
     * @var array
     */
    private $ranks;

    public function setRanksDecode (string $ranks) {
        $this->setRanks($ranks ? (array)json_decode($ranks) : []);
    }

    public function setRanks (?array $ranks) {
        $this->ranks = $ranks;
    }

    public function getRanks (): ?array
    {
        return $this->ranks;
    }
    public function setIsProductCompany (?int $ranks) {
        $this->is_product_company = $ranks;
    }

    public function getIsProductCompany (): ?int
    {
        return $this->is_product_company;
    }

    public function getRanksToString (): ?string
    {
        return $this->getRanks() ? json_encode($this->getRanks()) : null;
    }

    public function checkQuantity(): bool
    {
        return $this->quantity >= 0;
    }
    private $customer_id_tra_hang;
    /**
     * @return int
     */
    public function getCustomerIdTraHang(): int
    {
        return $this->customer_id_tra_hang;
    }

    /**
     * @param int|null $customer_id_tra_hang
     */
    public function setCustomerIdTraHang(?int $customer_id_tra_hang)
    {
        $this->customer_id_tra_hang = $customer_id_tra_hang;
    }

    /**
     * @var string
     */
    private $is_choose_agent;
    /**
     * @return int
     */
    public function getChooseAgent(): ?int
    {
        return $this->is_choose_agent;
    }

    /**
     * @param int $is_choose_agent
     */
    public function setChooseAgent(?int $is_choose_agent)
    {
        $this->is_choose_agent = $is_choose_agent;
    }

    public function isChooseAgent(): bool
    {
        return $this->getChooseAgent() === 1;
    }

    /**
     * @var array
     */
    private $select_wallets;
    /**
     * @return array
     */
    public function getWalletByProducts(): ?array
    {
        return $this->select_wallets;
    }

    /**
     * @param array $select_wallets
     */
    public function setWalletByProducts(?array $select_wallets)
    {
        $this->select_wallets = $select_wallets;
    }

    /**
     * @var SalesBooth
     */
    private $sales_booth;

    public function setSalesBooth (SalesBooth $salesBooth) {
        $this->sales_booth = $salesBooth;
    }

    public function getSalesBooth (): SalesBooth
    {
        return $this->sales_booth;
    }

    private  $discount_product_by_quantity;

    public function setDiscountProductByQuantity ( int $discount_product_by_quantity) {
        $this->discount_product_by_quantity = $discount_product_by_quantity;
    }

    public function getDiscountProductByQuantity () : int
    {
        return $this->discount_product_by_quantity;
    }
}
