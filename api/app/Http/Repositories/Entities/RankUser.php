<?php


namespace App\Http\Repositories\Entities;


class RankUser extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @return bool
     */
    public function hasId():bool
    {
        return (bool)$this->id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }


    /**
     * @var int
     */
    private $sort;

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort)
    {
        $this->sort = $sort;
    }

    /**
     * @var string
     */
    private $slug;

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @var int
     */
    protected $created_at;

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @return int
     */
    public function getCreatedBy(): int
    {
        return $this->created_by;
    }

    /**
     * @param int $createdBy
     */
    public function setCreatedBy(int $createdBy)
    {
        $this->created_by = $createdBy;
    }

    public function getStatusToArray(): array
    {
        return $this->getCode($this->status, true);
    }

    public function getCode($selected = false, $return = false): array {
        $listStatus = [
            ['id' => $this->getStatusActive(), 'style' => 'success', 'name' => 'Đã duyệt',],
            ['id' => $this->getStatusInActive(), 'style' => 'primary', 'name' => 'Đơn mới'],
            ['id' => $this->getStatusDeleted(), 'style' => 'danger', 'name' => 'Đã xoá'],
        ];
        if (is_numeric($selected)) {
            foreach ($listStatus as $status) {
                if ($status['id'] === $selected) {
                    $status['checked'] = 'checked';
                    if($return) {
                        return $status;
                    }
                }
            }
        }

        return $listStatus;
    }


}
