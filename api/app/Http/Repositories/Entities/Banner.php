<?php


namespace App\Http\Repositories\Entities;


use App\Elibs\Date;

class Banner extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var string
     */
    protected $status;
    /**
     * @return int
     */
    private $images_main;
    /**
     * @var string
     */
    private $images_bottom;
    /**
     * @var string
     */
    private $image_items;


    /**
     * @return int
     * @var int
     */

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param int $status
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updated_at
     */
    public function setUpdatedAt(?int $updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return string
     */
    public function getImagesMain(): ?string
    {
        return $this->images_main;
    }

    /**
     * @param string|null $images_main
     */
    public function setImagesMain(?string $images_main)
    {
        $this->images_main = $images_main;
    }
    /**
     * @return string
     */
    public function getImagesBottom(): ?string
    {
        return $this->images_bottom;
    }

    /**
     * @param string|null $images_bottom
     */
    public function setImagesBottom(?string $images_bottom)
    {
        $this->images_bottom = $images_bottom;
    }
    /**
     * @return string
     */
    public function getImageItems(): ?string
    {
        return $this->image_items;
    }

    /**
     * @param string|null $image_items
     */
    public function setImageItems(?string $image_items)
    {
        $this->image_items = $image_items;
    }


    /**
     * @return string
     */
    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }

}
