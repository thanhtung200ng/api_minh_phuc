<?php


namespace App\Http\Repositories\Entities;


use App\Elibs\Date;

class SalesBooth extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var string
     */
    private $slug;
    /**
     * @var int|int|null
     */
    private $created_by;
    private $updated_by;
    /**
     * @var string
     */
    private $images;
    /**
     * @var string|string|null
     */
    private $email;
    /**
     * @var string
     */
    private $customer_id;
    /**
     * @var string|int
     */
    private $address;
    /**
     * @var string|string|null
     */
    private $mobile;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param int $status
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getCreatedBy(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedBy(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getImages(): ?string
    {
        return $this->images;
    }
    /**
     * @param string images
     */
    public function setImages(?string $images)
    {
        $this->images = $images;
    }
    /**
     * @return string
     */
    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }
    public function getUpdatedAtToString(): string
    {
        return Date::dateFormat($this->updated_at, 'd/m/Y H:i:s');
    }
    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email)
    {
        $this->email = $email;
    }
    /**
     * @return string
     */
    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile(?string $mobile)
    {
        $this->mobile = $mobile;
    }
    /**
     * @return string
     */
    public function getCustomerId(): int
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId(int $customer_id)
    {
        $this->customer_id = $customer_id;
    }
    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param int $address
     */
    public function setAddress(?string $address)
    {
        $this->address = $address;
    }
}
