<?php


namespace App\Http\Repositories\Entities;


class PurchaseOders extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var int
     */
    private $customer_id;

    /**
     * @var int
     */
    private $wallets_id;

    /**
     * @var int
     */
    private $customer_id_tra_hang;

    /**
     * @var int
     */
    private $address_customer_id_mua_hang;

    /**
     * @var int
     */
    private $ending_balancex;


    /**
     * @return int
     */
    public function getEndingBalancex(): int
    {
        return $this->ending_balancex;
    }

    /**
     * @param int $ending_balancex
     */
    public function setEndingBalancex(int $ending_balancex)
    {
        $this->ending_balancex = $ending_balancex;
    }


    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param int $status
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }


    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customer_id;
    }

    /**
     * @param int|null $customer_id
     */
    public function setCustomerId(?int $customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return int
     */
    public function getCustomerIdTraHang(): int
    {
        return $this->customer_id_tra_hang;
    }

    /**
     * @param int|null $customer_id_tra_hang
     */
    public function setCustomerIdTraHang(?int $customer_id_tra_hang)
    {
        $this->customer_id_tra_hang = $customer_id_tra_hang;
    }


    /**
     * @return int
     */
    public function getWalletsId(): int
    {
        return $this->wallets_id;
    }

    /**
     * @param int|null $wallets_id
     */
    public function setWalletsId(?int $wallets_id)
    {
        $this->wallets_id = $wallets_id;
    }

    /**
     * @return int
     */
    public function getAddressCustomerIdMuaHang(): int
    {
        return $this->address_customer_id_mua_hang;
    }

    /**
     * @param int|null $address_customer_id_mua_hang
     */
    public function setAddressCustomerIdMuaHang(?int $address_customer_id_mua_hang)
    {
        $this->address_customer_id_mua_hang = $address_customer_id_mua_hang;
    }
    /**
     * @var string
     */
    private $name;

    public function setName (?string $name) {
        $this->name = $name;
    }

    public function getName (): ?string
    {
        return $this->name;
    }
    /**
     * @var string
     */
    private $slug;
    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }
}
