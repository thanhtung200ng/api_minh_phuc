<?php


namespace App\Http\Repositories\Entities;


use App\Elibs\Date;
use App\Http\Repositories\Enums\TransactionCode;

class CustomerWallet extends BaseEntity
{
    /**
     * @var int
     */
    private $id;
    private $wallet_id;
    private $customer_id;
    private $name;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $created_by;

    /**
     * @var string
     */
    private $group_transaction_code;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var double
     */
    private $current_balance;



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    private $slug;
    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    public function getCreatedBy(): ?int
    {
        return $this->created_by;
    }

    /**
     * @param int|null $created_by
     */
    public function setCreatedBy(?int $created_by)
    {
        $this->created_by = $created_by;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    public function getCurrentBalance(): float
    {
        return $this->current_balance;
    }

    /**
     * @param float $current_balance
     */
    public function setCurrentBalance(float $current_balance)
    {
        $this->current_balance = $current_balance;
    }

    public function getWalletId(): int
    {
        return $this->wallet_id;
    }

    /**
     * @param int $walletId
     */
    public function setWalletId(int $walletId)
    {
        $this->wallet_id = $walletId;
    }

    public function getCustomerId(): ?int
    {
        return $this->customer_id;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId(int $customerId)
    {
        $this->customer_id = $customerId;
    }

    public function getGroupTransactionCode(): ?string
    {
        return $this->group_transaction_code;
    }

    public function getGroupTransactionCodeToArray(): ?array
    {
        return $this->getGroupTransactionCode() ? explode(',', $this->getGroupTransactionCode()) : [];
    }

    /**
     * @param string|null $group_transaction_code
     */
    public function setGroupTransactionCode(?string $group_transaction_code)
    {
        $this->group_transaction_code = $group_transaction_code;
    }

    public function isNoDiscountWholesaleWallet(): bool
    {
        if (in_array(TransactionCode::noDiscountWholesale, $this->getGroupTransactionCodeToArray())) {
            return true;
        }
        return false;
    }

    public function isWalletRose(): bool
    {
        if (in_array(TransactionCode::recruitmentCommission, $this->getGroupTransactionCodeToArray())) {
            return true;
        }
        return false;
    }

    public function isWalletTotalRevenue(): bool
    {
        if (in_array(TransactionCode::totalRevenue, $this->getGroupTransactionCodeToArray())) {
            return true;
        }
        return false;
    }

    public function isPurchaseMoney(): bool
    {
        if (in_array(TransactionCode::purchaseMoney, $this->getGroupTransactionCodeToArray())) {
            return true;
        }
        return false;
    }

    public function isWalletWithdrawal(): bool
    {
        if (in_array(TransactionCode::withdrawal, $this->getGroupTransactionCodeToArray())) {
            return true;
        }
        return false;
    }

    public function isPurchase(): bool
    {
        if (in_array(TransactionCode::purchase, $this->getGroupTransactionCodeToArray())) {
            return true;
        }
        return false;
    }



}
