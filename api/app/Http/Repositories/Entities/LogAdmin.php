<?php


namespace App\Http\Repositories\Entities;


class LogAdmin extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $object_id;

    /**
     * @var int
     */
    private $type;

    /**
     * @var string
     */
    private $table;

    /**
     * @var string
     */
    private $before;

    /**
     * @var string
     */
    private $after;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $created_by_member_id;

    /**
     * @var string
     */
    private $client_info;
}
