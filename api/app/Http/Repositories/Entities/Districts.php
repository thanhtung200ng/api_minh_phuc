<?php


namespace App\Http\Repositories\Entities;


class Districts extends Location
{
    /**
     * @var int
     */
    private $city_id;

    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->city_id;
    }

    /**
     * @param int $id
     */
    public function setCityId(int $id)
    {
        $this->city_id = $id;
    }
}
