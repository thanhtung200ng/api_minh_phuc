<?php


namespace App\Http\Repositories\Entities;


use App\Elibs\Date;

class Wallets extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var string
     */
    private $created_by;


    /**
     * @var int
     */
    private $update_at;

    /**
     * @var int
     */
    private $update_by;
    /**
     * @var int
     */
    protected $status;

     /**
     * @var string
     */
    private $icon;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdateAt(): ?int
    {
        return $this->update_at;
    }

    /**
     * @param int|null $updateAt
     */
    public function setUpdateAt(?int $updateAt)
    {
        $this->update_at = $updateAt;
    }

    /**
     * @return int
     */
    public function getUpdateBy(): ?int
    {
        return $this->update_by;
    }

    /**
     * @param int|null $update_at
     */
    public function setUpdateBy(?int $update_at)
    {
        $this->update_at = $update_at;
    }
    /**
     * @return string
     */

    public function getCreatedBy(): ?int
    {
        return $this->created_by;
    }

    /**
     * @param int|null $update_at
     */
    public function setCreatedBy(?int $created_by)
    {
        $this->created_by = $created_by;
    }
    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }


    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @param int $status
     */
    public function getStatus(): int
    {
        return $this->status;
    }
    /**
     * @return string
     */
    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }
    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(?string $icon)
    {
        $this->icon = $icon;
    }

    private $group_transaction_code;
    public function getGroupTransactionCode(): ?string
    {
        return $this->group_transaction_code;
    }

    public function getGroupTransactionCodeToArray(): ?array
    {
        return $this->getGroupTransactionCode() ? explode(',', $this->getGroupTransactionCode()) : [];
    }

    /**
     * @param string|null $group_transaction_code
     */
    public function setGroupTransactionCode(?string $group_transaction_code)
    {
        $this->group_transaction_code = $group_transaction_code;
    }




}
