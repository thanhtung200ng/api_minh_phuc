<?php


namespace App\Http\Repositories\Entities;



use App\Elibs\Date;

class Member extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $first_name;

    /**
     * @var string
     */
    private $last_name;

    /**
     * @var string
     */
    private $account;

    /**
     * @var int
     */
    private $created_by;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var string
     */
    private $birthday;

    /**
     * @var string
     */
    private $address;

    /**
     * @var int
     */
    private $position_id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->first_name = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->last_name = $lastName;
    }

    /**
     * @return string
     */
    public function getAccount(): string
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount(string $account)
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile(string $mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getBirthday(): string
    {
        return $this->birthday;
    }

    /**
     * @return string
     */
    public function getBirthdayFormat(): string
    {
        return Date::getInstance()->dateFormat($this->birthday);
    }

    /**
     * @param string $birthday
     */
    public function setBirthday(string $birthday)
    {
        $this->birthday = Date::getInstance()->getTimestampFromVNDate($birthday);
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getAddressToArray(): array
    {
        return $this->getAddress() ? json_decode($this->getAddress(), true) : [];
    }



    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->status === -1;
    }


    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @return string
     */
    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * @return int
     */
    public function getPositionId(): ?int
    {
        return $this->position_id;
    }

    /**
     * @param int|null $positionId
     */
    public function setPositionId(?int $positionId)
    {
        $this->position_id = $positionId;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->getFirstName()  . ' ' . $this->getLastName();
    }

    public static function passwordHash($password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function setCreatedBy(?int $createdBy)
    {
        $this->created_by = $createdBy;
    }
}
