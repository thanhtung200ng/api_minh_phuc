<?php


namespace App\Http\Repositories\Entities;


use App\Elibs\Date;
use App\Http\Repositories\Enums\agecyCode;
use Illuminate\Support\Str;

class Agecy extends BaseEntity
{
    private $id;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    protected $name;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var string
     */
    private $fullname;

    /**
     * @var string
     */
    private $account;

    /**
     * @var int
     */
    private $customer_id;

    /**
     * @var int
     */
    private $rank_agency;

    /**
     * @var int
     */
    private $city_id;

    /**
     * @var int
     */
    private $district_id;

    /**
     * @var int
     */
    private $ward_id;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @return string
     */
    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile(?string $mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $city_id
     */
    public function setCityId(int $city_id)
    {
        $this->city_id = $city_id;
    }

    /**
     * @return int
     */
    public function getDistrictId(): int
    {
        return $this->district_id;
    }

    /**
     * @param int $district_id
     */
    public function setDistrictId(int $district_id)
    {
        $this->district_id = $district_id;
    }

    /**
     * @return int
     */
    public function getWardId(): int
    {
        return $this->ward_id;
    }

    /**
     * @param int $ward_id
     */
    public function setWardId(int $ward_id)
    {
        $this->ward_id = $ward_id;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(?string $street)
    {
        $this->street = $street;
    }


    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->city_id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }


    /**
     * @return int
     */
    public function getRankAgecy(): ?int
    {
        return $this->rank_agency;
    }

    /**
     * @param int|null $rank_agency
     */
    public function setRankAgecy(?int $rank_agency)
    {
        $this->rank_agency = $rank_agency;
    }


    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId(int $customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullname;
    }

    /**
     * @param string $fullname
     */
    public function setFullName(string $fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return string
     */
    public function getAccount(): string
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount(?string $account)
    {
        $this->account = Str::lower($account);
    }


    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @return string
     */
    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }



}
