<?php


namespace App\Http\Repositories\Entities;

use App\Http\Repositories\Resources\Products\ProductVariantsResourcesCollection;
use App\Elibs\Date;

class Product extends BaseEntity
{

    private $id;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    private $cate_name;
    /**
     * @return string
     */
    public function getCateName(): ?string
    {
        return $this->cate_name;
    }

    /**
     * @param string $cate_name
     */
    public function setCateName(?string $cate_name)
    {
        $this->cate_name = $cate_name;
    }
    /**
     * @var string
     */
    private $sku;

    public function setSku(string $sku) {
        $this->sku = $sku;
    }

    public function getSku (): string
    {
        return $this->sku;
    }

    /**
     * @var string
     */
    private $gtin;

    public function setGtin (?string $sku) {
        $this->gtin = $sku;
    }

    public function getGtin (): ?string
    {
        return $this->gtin;
    }

    /**
     * @var string
     */
    private $name;

    public function setName (?string $name) {
        $this->name = $name;
    }

    public function getName (): ?string
    {
        return $this->name;
    }

    /**
     * @var float
     */
    private $price;

    public function setPrice(float $price) {
        $this->price = $price;
    }

    public function getPrice (): float
    {
        return $this->price;
    }

    /**
     * @var float
     */
    private $price_sale;

    public function setPriceSale(float $price) {
        $this->price_sale = $price;
    }

    public function getPriceSale (): float
    {
        return $this->price_sale;
    }

    /**
     * @var int
     */
    private $product_id;

    public function setProductId(int $productId) {
        $this->product_id = $productId;
    }

    public function getProductId (): int
    {
        return $this->product_id;
    }

    /**
     * @var int
     */
    private $quantity;

    public function setQuantity(int $quantity) {
        $this->quantity = $quantity;
    }

    public function getQuantity (): int
    {
        return $this->quantity;
    }

    /**
     * @var array
     */
    private $images;

    public function setImages(?array $images) {
        $this->images = $images;
    }

    public function getImages (): ?array
    {
        return $this->images;
    }

    public function getImagesToString (): ?string
    {
        return $this->getImages() ? json_encode($this->getImages()) : null;
    }

    public function setImagesDecode (?string $images)
    {
        $this->setImages($images ? (array)json_decode($images) : []);
    }

    /**
     * @var array
     */
    private $ranks;

    public function setRanksDecode (string $ranks) {
        $this->setRanks($ranks ? (array)json_decode($ranks) : []);
    }

    public function setRanks (array $ranks) {
        $this->ranks = $ranks;
    }

    public function getRanks (): array
    {
        return $this->ranks;
    }

    public function getRanksToString (): ?string
    {
        return $this->getRanks() ? json_encode($this->getRanks()) : null;
    }

    public function add($key, $name, $value)
    {
        $this->$key[$name] = $value;
        return $this;
    }



    /**
     * @var string
     */
    private $seller_by;
    /**
     * @return int
     */
    public function getSellerBy(): ?int
    {
        return $this->seller_by;
    }

    /**
     * @param int|null $createdBy
     */
    public function setSellerBy(?int $createdBy)
    {
        $this->seller_by = $createdBy;
    }

    /**
     * @var string
     */
    private $created_at;

    public function setCreatedAt (int $created_at) {
        $this->created_at = $created_at;
    }

    public function getCreatedAt (): int
    {
        return $this->created_at;
    }

    /**
     * @var string
     */
    private $created_by;
    /**
     * @return int
     */
    public function getCreatedBy(): ?int
    {
        return $this->created_by;
    }

    /**
     * @param int|null $createdBy
     */
    public function setCreatedBy(?int $createdBy)
    {
        $this->created_by = $createdBy;
    }

    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }

    /**
     * @var string
     */
    private $updated_at;

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    private $category_id;

    public function setCategoryId (?int $category_id) {
        $this->category_id = $category_id;
    }

    public function getCategoryId (): ?int
    {
        return $this->category_id;
    }

    private $tags_id;
    public function setTagsId (?array $tags_id) {
        $this->tags_id = $tags_id;
    }

    public function getTagsId (): ?array
    {
        return $this->tags_id;
    }

    private $description;

    public function setDescription (string $description) {
        $this->description = $description;
    }

    public function getDescription (): string
    {
        return $this->description;
    }
    /**
     * @var string
     */
    private $content;
    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     */
    public function setContent(?string $content)
    {
        $this->content = $content;
    }

    /**
     * @var string
     */
    private $seo;
    /**
     * @return int
     */
    public function getSeo(): ?int
    {
        return $this->seo;
    }

    /**
     * @param int|null $seo
     */
    public function setSeo(?int $seo)
    {
        $this->seo = $seo;
    }

    /**
     * @var array
     */
    public $variants = [];

    public function setVariants (array $variants) {
        $this->variants = $variants;
    }

    public function getVariants (): array
    {
        return $this->variants;
    }

    public function getVariantsResource (): array
    {
        return ProductVariantsResourcesCollection::toArray($this->getVariants());
    }


    /**
     * @var string
     */
    private $like;
    /**
     * @return int
     */
    public function getlike(): ?int
    {
        return $this->like;
    }

    /**
     * @param int $seo
     */
    public function setlike(?int $like)
    {
        $this->like = $like;
    }



    /**
     * @var string
     */
    private $views;
    /**
     * @return int
     */
    public function getviews(): ?int
    {
        return $this->views;
    }

    /**
     * @param int $views
     */
    public function setviews(?int $views)
    {
        $this->views = $views;
    }


    private $is_choose_agent;
    /**
     * @return int
     */
    public function getChooseAgent(): ?int
    {
        return $this->is_choose_agent;
    }

    /**
     * @param int $is_choose_agent
     */
    public function setChooseAgent(?int $is_choose_agent)
    {
        $this->is_choose_agent = $is_choose_agent;
    }

    public function isChooseAgent(): bool
    {
        return $this->getChooseAgent() === 1;
    }

}
