<?php
namespace App\Http\Repositories\Entities;


use App\Elibs\Date;

abstract class BaseEntity
{
    // contain originals value of attributes
    private $originals = [];
    protected $status;

    public function __construct()
    {

    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $function = camel_case('set_' . snake_case($name));
            $this->$function($value);
        }
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            $function = camel_case('get_' . snake_case($name));
            return $this->$function();
        }
    }

    public function __isset($name)
    {
        return property_exists($this, $name);
    }

    /**
     * get an Array of current Attributes value
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }


    /**
     * store an array of attributes original value
     */
    public function storeOriginals()
    {
        $this->originals = $this->toArray();
    }

    /**
     * get an Array of Changed Attributes
     * @return array
     */
    public function changedAttributesName(): array
    {
        $changedAttributes = [];
        $attributes = $this->toArray();
        foreach ($attributes as $key => $value) {
            if (array_key_exists($key, $this->originals)) {
                if ($value != $this->originals[$key] && !((is_array($this->originals[$key]) || is_object($this->originals[$key])))) {
                    $changedAttributes[] = $key;
                }
            }
        }
        return $changedAttributes;
    }

    /**
     * get an Array of Changed Attributes with new values
     * @return array
     */
    public function getDirty(): array
    {
        $dirty = [];
        $attributes = $this->toArray();

        foreach ($this->changedAttributesName() as $key) {
            $dirty[$key] = $attributes[$key];
        }

        return $dirty;
    }

    /**
     * get an Array of Changed Attributes with original values
     * @return array
     */
    public function getChanges(): array
    {
        $changes = [];

        foreach ($this->changedAttributesName() as $key) {
            $changes[$key] = $this->originals[$key];
        }

        return $changes;
    }

    /**
     * is any attribute changed?
     * @return bool
     */
    public function isDirty(): bool
    {
        if (count($this->changedAttributesName()) > 0) return true;

        return false;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * @return int
     */

    public function getStatusActive(): int
    {
        return 1;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === $this->getStatusActive();
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->status === $this->getStatusDisabled();
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->status === $this->getStatusDeleted();
    }

    /**
     * @return bool
     */
    public function isInActive(): bool
    {
        return $this->status === $this->getStatusInActive();
    }

    /**
     * @return int
     */

    public function getStatusInActive(): int
    {
        return 0;
    }

    /**
     * @return int
     */

    public function getStatusDeleted(): int
    {
        return -4;
    }

    /**
     * @return int
     */

    public function getStatusDisabled(): int
    {
        return -1;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatus():int
    {
        return $this->status;
    }

}
