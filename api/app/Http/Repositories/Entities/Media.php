<?php

namespace App\Http\Repositories\Entities;

class Media
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var string
     */
    private $links;


    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }
}
