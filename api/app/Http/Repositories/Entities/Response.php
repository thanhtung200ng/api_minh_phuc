<?php

namespace App\Http\Repositories\Entities;


use App\Elibs\ApiHelper;
use App\Http\Repositories\Enums\HttpStatusCode;

class Response
{
    public function __construct()
    {
        $this->error = new Error();
        $this->value = new Value();
        $this->success = false;
        ApiHelper::saveApiRequest();

    }

    /**
     * @var integer $code Http Status Code
     */
    public $code;

    /**
     * @var bool $success
     */
    public $success;

    /**
     * @var string $message
     */
    public $message;

    /**
     * @var Error $error
     */
    public $error;

    /**
     * @var Value $value
     */
    public $value;

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return bool
     */
    public function getSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return Value
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param Value $value
     */
    public function setValue(Value $value)
    {
        $this->value = $value;
    }

    public function resetValue()
    {
        $this->value = new Value();
    }

    /**
     * @return Error
     */
    public function getError(): Error
    {
        return $this->error;
    }

    /**
     * @param Error $error
     */
    public function setError(Error $error)
    {
        $this->error = $error;
    }

    public function toArray(): array
    {
        if ($this->error->count() || !$this->getSuccess()) {
            $this->resetValue();
        }

        return [
            'message' => $this->getMessage(),
            'errors' => $this->error->count() ? $this->error->toArray() : null,
            'value' => $this->value->count() ? $this->value->toArray() : null,
            'success' => $this->getSuccess()
        ];
    }

    public function json($meta = [] , $code): \Illuminate\Http\JsonResponse
    {
        $this->setCode($code);
        $this->setMessage($meta['message']);
        $this->setSuccess($meta['success']);
        ApiHelper::saveApiResponse($this->toArray(), $this->getCode());
        return response()->json($this->toArray(), $this->getCode());
    }
}
