<?php


namespace App\Http\Repositories\Entities;


class EventSale extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $images;

//    /**
//     * @var int
//     */
//    protected $status;

    /**
     * @var int|null
     */
    private $created_at;

    /**
     * @var int|null
     */
    private $update_at;

    /**
     * @var int|null
     */
    private $price;
    /**
     * @var int|null
     */
    private $price_sale;

    /**
     * @var string
     */
    private $created_by;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

//    /**
//     * @return int|null
//     */
//    public function getStatus(): int
//    {
//        return $this->status;
//    }
//
//    /**
//     * @param int $status
//     */
//    public function setStatus(int $status)
//    {
//        $this->status = $status;
//    }

    /**
     * @return int|null
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link)
    {
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getPriceSale(): int
    {
        return $this->price_sale;
    }

    /**
     * @param int $price_sale
     */
    public function setPriceSale(int $price_sale)
    {
        $this->price_sale = $price_sale;
    }

    /**
     * @return string|null
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param string $created_by
     */
    public function setCreatedBy(string $created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * @return string
     */
    public function getImages(): string
    {
        return $this->images;
    }

    /**
     * @param string $images
     */
    public function setImages(string $images)
    {
        $this->images = $images;
    }

    /**
     * @return int|null
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param int $updateAt
     */
    public function setUpdateAt(int $updateAt)
    {
        $this->update_at = $updateAt;
    }

}
