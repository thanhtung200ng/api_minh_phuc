<?php


namespace App\Http\Repositories\Entities;


class Wards extends Location
{
    /**
     * @var int
     */
    private $district_id;

    /**
     * @return int
     */
    public function getDistrictId(): int
    {
        return $this->district_id;
    }

    /**
     * @param int $id
     */
    public function setDistrictId(int $id)
    {
        $this->district_id = $id;
    }
}
