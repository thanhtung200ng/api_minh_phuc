<?php


namespace App\Http\Repositories\Entities;


class BusinessContract extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    private $customer_id;

    /**
     * @var int
     */
    private $contract_id;

    /**
     * @var string
     */
    private $documents;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->status === -1;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customer_id;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId(int $customerId)
    {
        $this->customer_id = $customerId;
    }

    /**
     * @return int
     */
    public function getContractId(): int
    {
        return $this->contract_id;
    }

    /**
     * @param int $contractId
     */
    public function setContractId(int $contractId)
    {
        $this->contract_id = $contractId;
    }

    /**
     * @return string
     */
    public function getDocuments(): string
    {
        return $this->contract_id;
    }

    /**
     * @param string $documents
     */
    public function setDocuments(string $documents)
    {
        $this->documents = $documents;
    }

    public function getStatusToArray(): array
    {
        return $this->getCode($this->status, true);
    }

    public function getCode($selected = false, $return = false): array {
        $listStatus = [
            ['id' => $this->getStatusActive(), 'style' => 'success', 'name' => 'Đã duyệt',],
            ['id' => $this->getStatusInActive(), 'style' => 'danger', 'name' => 'Chờ duyệt'],
        ];
        if (is_numeric($selected)) {
            foreach ($listStatus as $status) {
                if ($status['id'] === $selected) {
                    $status['checked'] = 'checked';
                    if($return) {
                        return $status;
                    }
                }
            }
        }

        return $listStatus;
    }
}
