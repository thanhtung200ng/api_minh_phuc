<?php


namespace App\Http\Repositories\Entities;



use App\Elibs\Date;

class Category extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var int
     */
    protected $status;
    /**
     * @var int
     */
    private $parent_id;

    /**
     * @var int
     */
    private $is_leaf;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }
    /**
     * @return int
     */
    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    /**
     * @param string $parent_id
     */
    public function setParentId(?int $parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->status === -1;
    }

    /**
     * @param int $status
     */
    public function getStatus(): int
    {
        return $this->status;
    }
    /**
     * @return string
     */
    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return bool
     */
    public function getIsLeaf(): bool
    {
        return $this->is_leaf;
    }

    /**
     * @param int $isLeaf
     * @return bool
     */
    public function setIsLeaf(int $isLeaf): bool
    {
        return $this->is_leaf = (bool)$isLeaf;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

}
