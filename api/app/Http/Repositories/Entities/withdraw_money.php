<?php


namespace App\Http\Repositories\Entities;


class withdraw_money extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $customer_name;

    /**
     * @var string
     */
    private $wallet_name;

    /**
     * @var string
     */
    private $bank_name;

    /**
     * @var int
     */
    private $customer_id;

    /**
     * @var int
     */
    private $wallet_id;

    /**
     * @var int
     */
    private $bank_account;

    /**
     * @var int
     */
    private $money;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $transaction_fee;

    /**
     * @var int
     */
    private $updated_at;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }


    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId(int $customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return int
     */
    public function getWallestId(): int
    {
        return $this->wallet_id;
    }

    /**
     * @param int $wallet_id
     */
    public function setWallestId(int $wallet_id)
    {
        $this->wallet_id = $wallet_id;
    }

    /**
     * @return int
     */
    public function getBankAccount(): int
    {
        return $this->bank_account;
    }

    /**
     * @param int $bank_account
     */
    public function setBankAccount(int $bank_account)
    {
        $this->bank_account = $bank_account;
    }

    /**
     * @return int
     */
    public function getMoney(): int
    {
        return $this->money;
    }

    /**
     * @param int $money
     */
    public function setMoney(int $money)
    {
        $this->money = $money;
    }


    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }


    public function getTransactionFee(): int
    {
        return $this->transaction_fee;
    }

    /**
     * @param int $transaction_fee
     */
    public function setTransactionFee(int $transaction_fee)
    {
        $this->transaction_fee = $transaction_fee;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * @return string
     */
    public function getNameCustomer(): string
    {
        return $this->customer_name;
    }

    /**
     * @param string $customer_name
     */
    public function setNameCustomer(string $customer_name)
    {
        $this->customer_name = $customer_name;
    }

    /**
     * @return string
     */
    public function getNameBank(): string
    {
        return $this->bank_name;
    }

    /**
     * @param string $bank_name
     */
    public function setNameBank(string $bank_name)
    {
        $this->bank_name = $bank_name;
    }

    /**
     * @return string
     */
    public function getWallestName(): string
    {
        return $this->wallet_name;
    }

    /**
     * @param string $wallet_name
     */
    public function setWallestName(string $wallet_name)
    {
        $this->wallet_name = $wallet_name;
    }


}
