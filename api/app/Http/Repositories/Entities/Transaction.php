<?php


namespace App\Http\Repositories\Entities;


class Transaction extends Deposit
{
    /**
     * @var int
     */
    private $id;

    /**
     * @return bool
     */
    public function hasId():bool
    {
        return (bool)$this->id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @var int
     */
    protected $created_at;

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @return int
     */
    public function getCreatedBy(): int
    {
        return $this->created_by;
    }

    /**
     * @param int $createdBy
     */
    public function setCreatedBy(int $createdBy)
    {
        $this->created_by = $createdBy;
    }

    public function getStatusToArray(): array
    {
        return $this->getCode($this->status, true);
    }

    public function getCode($selected = false, $return = false): array {
        $listStatus = [
            ['id' => $this->getStatusActive(), 'style' => 'success', 'name' => 'Đã duyệt',],
            ['id' => $this->getStatusInActive(), 'style' => 'primary', 'name' => 'Đơn mới'],
            ['id' => $this->getStatusDeleted(), 'style' => 'danger', 'name' => 'Đã xoá'],
        ];
        if (is_numeric($selected)) {
            foreach ($listStatus as $status) {
                if ($status['id'] === $selected) {
                    $status['checked'] = 'checked';
                    if($return) {
                        return $status;
                    }
                }
            }
        }

        return $listStatus;
    }

    private $group_transaction_code;

    public function getGroupTransactionCode(): ?string
    {
        return $this->group_transaction_code;
    }

    public function getGroupTransactionCodeToArray(): ?array
    {
        return $this->getGroupTransactionCode() ? explode(',', $this->getGroupTransactionCode()) : [];
    }

    /**
     * @param string|null $group_transaction_code
     */
    public function setGroupTransactionCode(?string $group_transaction_code)
    {
        $this->group_transaction_code = $group_transaction_code;
    }

    private $first_money;

    public function getFirstMoney(): string
    {
        return $this->first_money;
    }


    /**
     * @param string $first_money
     */
    public function setFirstMoney(string $first_money)
    {
        $this->first_money = $first_money;
    }

    private $last_money;

    public function getLastMoney(): string
    {
        return $this->last_money;
    }


    /**
     * @param string $last_money
     */
    public function setLastMoney(string $last_money)
    {
        $this->last_money = $last_money;
    }

    //
    private $before_balance_from_customer_wallet;

    public function getBeforeFromCustomerWalletCurrentBalance(): ?float
    {
        return $this->before_balance_from_customer_wallet;
    }


    /**
     * @param float|null $before_balance_from_customer_wallet
     */
    public function setBeforeFromCustomerWalletCurrentBalance(?float $before_balance_from_customer_wallet)
    {
        $this->before_balance_from_customer_wallet = $before_balance_from_customer_wallet;
    }

    private $after_balance_from_customer_wallet;

    public function getAfterFromCustomerWalletCurrentBalance(): ?float
    {
        return $this->after_balance_from_customer_wallet;
    }


    /**
     * @param float|null $after_balance_from_customer_wallet
     */
    public function setAfterFromCustomerWalletCurrentBalance(?float $after_balance_from_customer_wallet)
    {
        $this->after_balance_from_customer_wallet = $after_balance_from_customer_wallet;
    }

    private $before_balance_to_customer_wallet;

    public function getBeforeToCustomerWalletCurrentBalance(): ?float
    {
        return $this->before_balance_to_customer_wallet;
    }


    /**
     * @param float $before_balance_to_customer_wallet
     */
    public function setBeforeToCustomerWalletCurrentBalance(float $before_balance_to_customer_wallet)
    {
        $this->before_balance_to_customer_wallet = $before_balance_to_customer_wallet;
    }

    private $after_balance_to_customer_wallet;

    public function getAfterToCustomerWalletCurrentBalance(): float
    {
        return $this->after_balance_to_customer_wallet;
    }


    /**
     * @param float $after_balance_to_customer_wallet
     */
    public function setAfterToCustomerWalletCurrentBalance(float $after_balance_to_customer_wallet)
    {
        $this->after_balance_to_customer_wallet = $after_balance_to_customer_wallet;
    }

    private $discount;

    public function getDiscount(): ?float
    {
        return $this->discount;
    }


    /**
     * @param float|null $discount
     */
    public function setDiscount(?float $discount)
    {
        $this->discount = $discount;
    }

    private $fee;

    public function getFee(): float
    {
        return $this->fee;
    }


    /**
     * @param float $fee
     */
    public function setFee(float $fee)
    {
        $this->fee = $fee;
    }

    private $ranks;

    public function getRanks(): ?string
    {
        return $this->ranks;
    }

    public function getRanksToArray(): ?array
    {
        return $this->getRanks() ? (array)\GuzzleHttp\json_decode($this->getRanks()) : [];
    }


    /**
     * @param string|null $rank
     */
    public function setRanks(?string $rank)
    {
        $this->ranks = $rank;
    }
}
