<?php


namespace App\Http\Repositories\Entities;


use App\Elibs\Date;

class Collections extends BaseEntity
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    /**
     * @var int
     */
    protected $status;
    private $name;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var string
     */
    private $slug;
    /**
     * @var int|int|null
     */
    private $created_by;

    private $categories_id;


    private $updated_by;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param int $status
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }


    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }
    public function getCreatedBy(): ?int
    {
        return $this->created_by;
    }

    /**
     * @param int|null $createdBy
     */
    public function setCreatedBy(?int $createdBy)
    {
        $this->created_by = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getCateId()
    {
        return $this->categories_id;
    }

    /**
     * @param mixed $cate_id
     */
    public function setCateId( ?array $cate_id)
    {
        $this->categories_id = $cate_id;
    }

}
