<?php


namespace App\Http\Repositories\Entities;


use App\Elibs\Date;

class ElectronicContract extends BaseEntity
{
    private $id;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var int
     */
    private $created_by;

    /**
     * @var int
     */
    private $customer_id;

    /**
     * @var int
     */
    protected $name;


    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $file;


    /**
     * @var string
     */
    protected $no;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId(int $customer_id)
    {
        $this->customer_id = $customer_id;
    }
    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @return string
     */
    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getCreatedBy(): int
    {
        return $this->created_by;
    }

    /**
     * @param string $created_by
     */
    public function setCreatedBy(string $created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }


    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->created_by;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @return array
     */
    public function getFileDecode(): array
    {
        return $this->getFile() ? (array)json_decode($this->getFile()) : [];
    }

    /**
     * @param string $file
     */
    public function setFile(string $file)
    {
        $this->file = $file;
    }


    /**
     * @return string
     */
    public function getNo(): string
    {
        return $this->no;
    }

    /**
     * @param string $no
     */
    public function setNo(string $no)
    {
        $this->no = $no;
    }
    public function getStatusToArray(): array
    {
        return $this->getCode($this->status, true);
    }

    public function getCode($selected = false, $return = false): array {
        $listStatus = [
            ['id' => $this->getStatusActive(), 'style' => 'success', 'name' => 'Đã duyệt',],
            ['id' => $this->getStatusInActive(), 'style' => 'danger', 'name' => 'Chờ duyệt'],
        ];
        if (is_numeric($selected)) {
            foreach ($listStatus as $status) {
                if ($status['id'] === $selected) {
                    $status['checked'] = 'checked';
                    if($return) {
                        return $status;
                    }
                }
            }
        }

        return $listStatus;
    }

}
