<?php


namespace App\Http\Repositories\Entities;


class PurchaseOdersItem extends BaseEntity
{
    private $id;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    protected $name;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var int
     */
    private $product_item_id;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var int
     */
    private $total_money;

    /**
     * @var int
     */
    private $purchase_oders_id;

    /**
     * @var int
     */
    private $price;


    /**
     * @var string
     */
    private $ranks;

    /**
     * @var string
     */
    private $images;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }


    /**
     * @param int $price
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
    private $customer_id_tra_hang;
    /**
     * @return int
     */
    public function getCustomerIdTraHang(): int
    {
        return $this->customer_id_tra_hang;
    }

    /**
     * @param int|null $customer_id_tra_hang
     */
    public function setCustomerIdTraHang(?int $customer_id_tra_hang)
    {
        $this->customer_id_tra_hang = $customer_id_tra_hang;
    }

    /**
     * @param string $ranks
     */
    public function setRanks(string $ranks)
    {
        $this->ranks = $ranks;
    }

    public function getRanks(): ?string
    {
        return $this->ranks;
    }

    /**
     * @param string $images
     */
    public function setImages(?string $images)
    {
        $this->images = $images;
    }

    public function getImages(): ?string
    {
        return $this->images;
    }



    /**
     * @return string
     */
    public function getNameProducts(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setNameProducts(?string $name)
    {
        $this->name = $name;
    }

    /**
     * @param int $status
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }


    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }


    /**
     * @return int
     */
    public function getProductItemId(): int
    {
        return $this->product_item_id;
    }

    /**
     * @param int|null $product_item_id
     */
    public function setProductItemId(?int $product_item_id)
    {
        $this->product_item_id = $product_item_id;
    }


    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity(?int $quantity)
    {
        $this->quantity = $quantity;
    }


    public function getTotalMoney(): ?int
    {
        return $this->total_money;
    }

    /**
     * @param int|null $total_money
     */
    public function setTotalMoney(?int $total_money)
    {
        $this->total_money = $total_money;
    }


    public function getPurchaseOdersId(): int
    {
        return $this->purchase_oders_id;
    }

    /**
     * @param int|null $purchase_oders_id
     */
    public function setPurchaseOdersId(?int $purchase_oders_id)
    {
        $this->purchase_oders_id = $purchase_oders_id;
    }


}
