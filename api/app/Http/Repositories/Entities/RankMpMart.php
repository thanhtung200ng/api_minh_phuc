<?php


namespace App\Http\Repositories\Entities;


class RankMpMart extends RankUser
{

    /**
     * @var float
     */
    private $min_money;

    public function getMinMoney(): float
    {
        return $this->min_money;
    }

    /**
     * @param float $min_money
     */
    public function setMinMoney(float $min_money)
    {
        $this->min_money = $min_money;
    }

    /**
     * @var float
     */
    private $max_money;

    public function getMaxMoney(): float
    {
        return $this->max_money;
    }

    /**
     * @param float $max_money
     */
    public function setMaxMoney(float $max_money)
    {
        $this->max_money = $max_money;
    }

    /**
     * @var float
     */
    private $discount;

    public function getDiscount(): float
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount(float $discount)
    {
        $this->discount = $discount;
    }


    /**
     * @var int
     */
    protected $created_at;

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @return int
     */
    public function getCreatedBy(): int
    {
        return $this->created_by;
    }

    /**
     * @param int $createdBy
     */
    public function setCreatedBy(int $createdBy)
    {
        $this->created_by = $createdBy;
    }

    public function getStatusToArray(): array
    {
        return $this->getCode($this->status, true);
    }


    public function getCode($selected = false, $return = false): array {
        $listStatus = [
            ['id' => $this->getStatusActive(), 'style' => 'success', 'name' => 'Đã duyệt',],
            ['id' => $this->getStatusInActive(), 'style' => 'primary', 'name' => 'Đơn mới'],
            ['id' => $this->getStatusDeleted(), 'style' => 'danger', 'name' => 'Đã xoá'],
        ];
        if (is_numeric($selected)) {
            foreach ($listStatus as $status) {
                if ($status['id'] === $selected) {
                    $status['checked'] = 'checked';
                    if($return) {
                        return $status;
                    }
                }
            }
        }

        return $listStatus;
    }
}
