<?php


namespace App\Http\Repositories\Entities;



use App\Elibs\Date;
use Illuminate\Support\Str;

class Customer extends BaseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $first_name;

    /**
     * @var string
     */
    private $last_name;

    /**
     * @var string
     */
    private $account;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @var string
     */
    private $bio;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    private $created_at;

    /**
     * @var int
     */
    private $updated_at;

    /**
     * @var string
     */
    private $birthday;

    /**
     * @var string
     */
    private $address;

    /**
     * @var int
     */
    private $position_id;

    /**
     * @var string
     */
    private $parent_referral_code;
    /**
     * @var string
     */
    private $referral_code;

    /**
     * @var string
     */
    private $mpmart_agency;
    /**
     * @var int|string
     */
    private $district;
    private $ward;
    private $street;
    private $city;

    /**
     * @var int|string
     */

    /**
     * @return int
     */
    public function getMpmartAgency(): int
    {
        return $this->mpmart_agency;
    }

    /**
     * @param int $mpmart_agency
     */
    public function setMpmartAgency(int $mpmart_agency)
    {
        $this->mpmart_agency = $mpmart_agency;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }
    public function getCityToArray(): array
    {
        return $this->getCity() ? json_decode($this->getCity(), true) : [];
    }
    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }
    /**
     * @return string
     */
    public function getDistrict(): string
    {
        return $this->district;
    }
    public function getDistrictToArray(): array
    {
        return $this->getDistrict() ? json_decode($this->getDistrict(), true) : [];
    }
    /**
     * @param string $district
     */
    public function setDistrict(string $district)
    {
        $this->district = $district;
    }
    /**
     * @return string
     */
    public function getWard(): string
    {
        return $this->ward;
    }
    public function getWardToArray(): array
    {
        return $this->getWard() ? json_decode($this->getWard(), true) : [];
    }
    /**
     * @param string $ward
     */
    public function setWard(string $ward)
    {
        $this->ward = $ward;
    }
    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->first_name = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->last_name = $lastName;
    }

    /**
     * @return string
     */
    public function getAccount(): string
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount(string $account)
    {
        $this->account = Str::lower($account);
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile(string $mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string|null
     */
    public function getBio(): ?string
    {
        return $this->bio;
    }

    /**
     * @param string|null $bio
     */
    public function setBio(?string $bio)
    {
        $this->bio = $bio;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    /**
     * @return string
     */
    public function getBirthdayFormat(): ?string
    {
        return ($this->birthday) ? Date::dateFormat($this->birthday, 'd/m/Y'): null;
    }

    /**current_balance
     * @param int|null $birthday
     */
    public function setBirthday(?int $birthday)
    {
        $this->birthday = $birthday ? $birthday : null;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getAddressToArray(): array
    {
        return $this->getAddress() ? json_decode($this->getAddress(), true) : [];
    }

    /**
     * @param string $address
     */
    public function setAddress(?string $address)
    {
        $this->address = $address;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->status === -1;
    }




    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @return string
     */
    public function getCreatedAtToString(): string
    {
        return Date::dateFormat($this->created_at, 'd/m/Y H:i:s');
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updated_at;
    }

    /**
     * @param int|null $updatedAt
     */
    public function setUpdatedAt(?int $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * @return int
     */
    public function getPositionId(): ?int
    {
        return $this->position_id;
    }

    /**
     * @param int|null $positionId
     */
    public function setPositionId(?int $positionId)
    {
        $this->position_id = $positionId;
    }

    /**
     * @return string
     */
    public function getParentReferralCode(): ?string
    {
        return $this->parent_referral_code;
    }

    /**
     * @param string|null $parentReferralCode
     */
    public function setParentReferralCode(?string $parentReferralCode)
    {
        $this->parent_referral_code = $parentReferralCode;
    }

    /**
     * @return string
     */
    public function getReferralCode(): ?string
    {
        return $this->referral_code;
    }

    /**
     * @param string $referralCode
     */
    public function setReferralCode(string $referralCode)
    {
        $this->referral_code = Str::lower($referralCode);
    }

    private $mpmart_rank;
    /**
     * @return string
     */
    public function getRankMpMart(): ?string
    {
        return $this->mpmart_rank;
    }
    /**
     * @return array
     */
    public function getRankMpMartToArray(): array
    {
        return $this->getRankMpMart() ? (array)\GuzzleHttp\json_decode($this->getRankMpMart()) : [];
    }

    /**
     * @param string|null $mpmartRank
     */
    public function setRankMpMart(?string $mpmartRank)
    {
        $this->mpmart_rank = $mpmartRank;
    }

    private $mpmart_rank_total_revenue;
    /**
     * @return int
     */
    public function getRankMpMartTotalRevenue(): ?int
    {
        return $this->mpmart_rank_total_revenue;
    }
    /**
     * @return array
     */
    public function getRankMpMartTotalRevenueToArray(): array
    {
        return $this->getRankMpMartTotalRevenue() ? (array)\GuzzleHttp\json_decode($this->getRankMpMartTotalRevenue()) : [];
    }

    /**
     * @param int|null $mpmart_rank_total_revenue
     */
    public function setRankMpMartTotalRevenue(?int $mpmart_rank_total_revenue)
    {
        $this->mpmart_rank_total_revenue = $mpmart_rank_total_revenue;
    }

    private $change_new_mpmart_rank_total_revenue;
    /**
     * @param bool $change_new_mpmart_rank_total_revenue
     */
    public function changeRankMpMartTotalRevenue(bool $change_new_mpmart_rank_total_revenue = false)
    {
        $this->change_new_mpmart_rank_total_revenue = $change_new_mpmart_rank_total_revenue;
    }

    public function isUpdateNewRankMpMartTotalRevenue()
    {
        return $this->change_new_mpmart_rank_total_revenue;
    }

    private $is_mpmart;

    /**
     * @return int
     */
    public function isMpMart(): int
    {
        return $this->is_mpmart;
    }

    /**
     * @param int $is_mpmart
     */
    public function setIsMpMart(int $is_mpmart)
    {
        $this->is_mpmart = $is_mpmart;
    }

    private $is_seller;

    /**
     * @return int
     */
    public function isSeller(): int
    {
        return $this->is_seller;
    }

    /**
     * @param int $is_seller
     */
    public function setIsSeller(int $is_seller)
    {
        $this->is_seller = $is_seller;
    }

    private $is_buyer;

    /**
     * @return int
     */
    public function isBuyer(): int
    {
        return $this->is_buyer;
    }

    /**
     * @param int $is_buyer
     */
    public function setIsBuyer(int $is_buyer)
    {
        $this->is_buyer = $is_buyer;
    }

    private $wallets;
    /**
     * @param array $wallets
     */
    public function setWallets(array $wallets)
    {
        $this->wallets = $wallets;
    }

    /**
     * @return array
     */
    public function getWallets(): array
    {
        return $this->wallets;
    }

    public function hasWallet($id): bool
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->getId() === $id) {
                return true;
            }
        }
        return false;
    }

    public function countWallet(): int
    {
        return count($this->wallets);
    }

    public function getWallet($id)
    {
        if ($this->hasWallet($id)) {
            foreach ($this->wallets as $wallet) {
                if ($wallet->getId() === $id) {
                    return $wallet;
                }
            }
        }
        return null;
    }

    public function hasWalletRose(): bool
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->isWalletRose()) {
                return true;
            }
        }
        return false;
    }

    public function getWalletRose()
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->isWalletRose()) {
                return $wallet;
            }
        }
        return null;
    }

    public function hasNoDiscountWholesaleWallet(): bool
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->isNoDiscountWholesaleWallet()) {
                return true;
            }
        }
        return false;
    }

    public function getNoDiscountWholesaleWallet()
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->isNoDiscountWholesaleWallet()) {
                return $wallet;
            }
        }
        return null;
    }

    public function getAllWalletWithdrawal()
    {
        $listWallet = [];
        foreach ($this->wallets as $wallet) {
            if($wallet->isWalletWithdrawal()){
                $listWallet[] = $wallet;
            }
        }
        return $listWallet;
    }

    public function getAllWalletBuyProducts()
    {
        $listWallet = [];
        foreach ($this->wallets as $wallet) {
            if($wallet->isPurchase()){
                $listWallet[] = $wallet;
            }
        }
        return $listWallet;
    }


    public function hasPurchaseMoney(): bool
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->isPurchaseMoney()) {
                return true;
            }
        }
        return false;
    }

    public function getPurchaseMoney()
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->isPurchaseMoney()) {
                return $wallet;
            }
        }
        return null;
    }

    public function hasWalletTotalRevenue(): bool
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->isWalletTotalRevenue()) {
                return true;
            }
        }
        return false;
    }

    public function getWalletTotalRevenue()
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->isWalletTotalRevenue()) {
                return $wallet;
            }
        }
        return null;
    }

    public function hasPurchase(): bool
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->isPurchase()) {
                return true;
            }
        }
        return false;
    }

    public function getPurchase()
    {
        foreach ($this->wallets as $wallet) {
            if ($wallet->isPurchase()) {
                return $wallet;
            }
        }
        return null;
    }



    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->getFirstName()  . ' ' . $this->getLastName();
    }

    public static function passwordHash($password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function getStatusToArray(): array
    {
        return $this->getCode($this->status, true);
    }

    public function checkMpmartAgency(): bool
    {
        if ($this->getMpmartAgency() > 0) {
            return true;
        }
        return false;
    }


    public function getCode($selected = false, $return = false): array {
        $listStatus = [
            ['id' => $this->getStatusActive(), 'style' => 'success', 'name' => 'Hoạt động',],
            ['id' => $this->getStatusInActive(), 'style' => 'primary', 'name' => 'Chưa kích hoạt'],
            ['id' => $this->getStatusDeleted(), 'style' => 'danger', 'name' => 'Đã xoá'],
        ];
        if (is_numeric($selected)) {
            foreach ($listStatus as $status) {
                if ($status['id'] === $selected) {
                    $status['checked'] = 'checked';
                    if($return) {
                        return $status;
                    }
                }
            }
        }

        return $listStatus;
    }
}
