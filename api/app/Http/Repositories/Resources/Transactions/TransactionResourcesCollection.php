<?php


namespace App\Http\Repositories\Resources\Transactions;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Transaction;
use App\Http\Repositories\Resources\ResourcesCollectionInterface;
use Illuminate\Support\Collection;

class TransactionResourcesCollection implements ResourcesCollectionInterface
{

    /**
     * @param Transaction[]|Collection $entities
     * @return array []
     */
    public static function toArray($entities): array
    {
        $list = [];

        foreach ($entities as $obj){
            $list[] = [
                'id' => $obj->getId(),
                'first_money' => $obj->getFirstMoney(),
                'last_money' => $obj->getLastMoney(),
                'before_balance_from_customer_wallet' => $obj->getBeforeFromCustomerWalletCurrentBalance(),
                'after_balance_from_customer_wallet' => $obj->getAfterFromCustomerWalletCurrentBalance(),
                'before_balance_to_customer_wallet' => $obj->getBeforeToCustomerWalletCurrentBalance(),
                'after_balance_to_customer_wallet' => $obj->getAfterToCustomerWalletCurrentBalance(),
                'discount' => $obj->getDiscount(),
                'fee' => $obj->getFee(),
                'ranks' => $obj->getRanksToArray(),
                'from_customer_full_name' => $obj->getFromCustomerFullName(),
                'from_customer_id' => $obj->getFromCustomerId(),
                'from_customer_account' => $obj->getFromCustomerAccount(),
                'from_customer_status' => $obj->getFromCustomerStatus(),
                'from_customer_wallet_id' => $obj->getFromCustomerWalletId(),
                'from_customer_wallet_name' => $obj->getFromCustomerWalletName(),
                'to_customer_full_name' => $obj->getToCustomerFullName(),
                'to_customer_id' => $obj->getToCustomerId(),
                'to_customer_account' => $obj->getToCustomerAccount(),
                'to_customer_status' => $obj->getToCustomerStatus(),
                'to_customer_wallet_id' => $obj->getToCustomerWalletId(),
                'to_customer_wallet_name' => $obj->getToCustomerWalletName(),
                'transaction_code' => $obj->getTransactionCodeToArray(),
                'disabled' => $obj->isDisabled(),
                'status' => $obj->getStatusToArray(),
                'created_at' => $obj->getCreatedAtToString(),
                'created_by' => $obj->getCreatedBy(),
                'approved_at' => $obj->getApprovedAt(),
                'approved_by' => $obj->getApprovedBy(),
                'approved_name' => $obj->getApprovedByName()
            ];
        }

        return $list;
    }
}
