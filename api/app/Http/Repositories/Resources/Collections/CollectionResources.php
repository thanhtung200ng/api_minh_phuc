<?php


namespace App\Http\Repositories\Resources\Collections;


use App\Http\Repositories\Entities\Collections;
use App\Http\Repositories\Resources\ResourcesInterface;

class CollectionResources implements ResourcesInterface
{

    /**
     * @param Collections $collections
     * @return array
     */
    public static function toArray($collections): ?array
    {
        return $collections ? [
            'id' => $collections->getId(),
            'name' => $collections->getName(),
            'slug' => $collections->getSlug(),
            'status' => $collections->getStatus(),
            'categories' => $collections->getCateId(),
            'created_at' => $collections->getCreatedAt(),
            'updated_at' => $collections->getUpdatedAt()
        ] : null;
    }
}
