<?php


namespace App\Http\Repositories\Resources\Collections;


use App\Http\Repositories\Entities\Collections;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class CollectionResourcesCollection implements ResourcesInterface
{
    /**
     * @param Collections[]|Collections $collections
     * @return []
     */
    public static function toArray($collections): array
    {
        $list = [];

        foreach ($collections as $collection){
            $list[] = [
                'id' => $collection->getId(),
                'name' => $collection->getName(),
                'slug' => $collection->getSlug(),
                'status' => $collection->getStatus(),
                'created_at' => $collection->getCreatedAtToString(),
                'updated_at' => $collection->getUpdatedAt()
            ];
        }

        return $list;
    }
}
