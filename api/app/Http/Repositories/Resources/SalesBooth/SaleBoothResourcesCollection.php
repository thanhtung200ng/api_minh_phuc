<?php


namespace App\Http\Repositories\Resources\Categories;


use App\Http\Repositories\Entities\SalesBooth;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class SaleBoothResourcesCollection implements ResourcesInterface
{
    /**
     * @param SalesBooth[]|Collection $cates
     * @return []
     */
    public static function toArray($cates)
    {
        $list = [];
        foreach ($cates as $sale_booth){
            $list[] = [
                'id' => $sale_booth->getId(),
                'name' => $sale_booth->getName(),
                'address' => $sale_booth->getAddress(),
                'slug' => $sale_booth->getSlug(),
                'customer_id' => $sale_booth->getCustomerId(),
                'status' => $sale_booth->getStatus(),
                'email' => $sale_booth->getEmail(),
                'mobile' => $sale_booth->getMobile(),
                'created_at' => $sale_booth->getCreatedAtToString(),
                'updated_at' => $sale_booth->getUpdatedAt()
            ];
        }
        return $list;
    }
}
