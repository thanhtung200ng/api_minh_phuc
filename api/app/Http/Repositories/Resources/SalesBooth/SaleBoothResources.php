<?php


namespace App\Http\Repositories\Resources\SalesBooth;


use App\Http\Repositories\Entities\SalesBooth;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class SaleBoothResources implements ResourcesInterface
{

    /**
     * @param SalesBooth $sale_booth
     * @return array
     */
    public static function toArray($sale_booth): ?array
    {
        return $sale_booth ? [
            'id' => $sale_booth->getId(),
            'name' => $sale_booth->getName(),
            'address' => $sale_booth->getAddress(),
            'slug' => $sale_booth->getSlug(),
            'customer_id' => $sale_booth->getCustomerId(),
            'status' => $sale_booth->getStatus(),
            'email' => $sale_booth->getEmail(),
            'mobile' => $sale_booth->getMobile(),
            'created_at' => $sale_booth->getCreatedAtToString(),
            'updated_at' => $sale_booth->getUpdatedAtToString(),
        ] : null;
    }
    public static function toArrayId($sale_booth): ?array
    {
        return $sale_booth ? [
            'id' => $sale_booth->getId(),
        ] : null;
    }
}
