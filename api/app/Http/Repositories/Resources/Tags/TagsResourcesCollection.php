<?php


namespace App\Http\Repositories\Resources\Tags;


use App\Http\Repositories\Entities\Tags;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class TagsResourcesCollection implements ResourcesInterface
{
    /**
     * @param Tags[]|Collection $tags
     * @return []
     */
    public static function toArray($tags): array
    {
        $list = [];

        foreach ($tags as $tag){
            $list[] = [
                'id' => $tag->getId(),
                'name' => $tag->getName(),
                'slug' => $tag->getSlug(),
                'status' => $tag->getStatus(),
                'created_at' => $tag->getCreatedAtToString(),
            ];
        }

        return $list;
    }
}
