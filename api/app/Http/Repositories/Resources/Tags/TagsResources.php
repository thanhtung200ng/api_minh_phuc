<?php


namespace App\Http\Repositories\Resources\Tags;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Tags;
use App\Http\Repositories\Resources\ResourcesInterface;

class TagsResources implements ResourcesInterface
{

    /**
     * @param Tags $tags
     * @return array
     */
    public static function toArray($tags)
    {
        return $tags ? [
            'id' => $tags->getId(),
            'name' => $tags->getName(),
            'slug' => $tags->getSlug(),
            'status' => $tags->getStatus(),
            'created_at' => $tags->getCreatedAt(),
            'created_by' => $tags->getCreatedBy()
        ] : null;
    }
}
