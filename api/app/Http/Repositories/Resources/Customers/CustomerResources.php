<?php


namespace App\Http\Repositories\Resources\Customers;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Customer;
use App\Http\Repositories\Entities\Member;
use App\Http\Repositories\Resources\ResourcesInterface;

class CustomerResources implements ResourcesInterface
{

    /**
     * @param Customer $user
     * @return array
     */
    public static function toArray($user): ?array
    {
        return $user ? [
            'id' => $user->getId(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'full_name' => $user->getFullName(),
            'birthday' => $user->getBirthdayFormat(),
            'account' => $user->getAccount(),
            'bio' => $user->getBio(),
            'mobile' => $user->getMobile(),
            'email' => $user->getEmail(),
            'disabled' => $user->isDisabled(),
            'status' => $user->getStatus(),
            'address' => $user->getAddressToArray(),
            'city' => $user->getCityToArray(),
            'district' => $user->getDistrictToArray(),
            'ward' => $user->getAddressToArray(),
            'street' => $user->getAddressToArray(),
            'created_at' => $user->getCreatedAtToString(),
            'updated_at' => $user->getUpdatedAt(),
            'is_seller' => $user->isSeller(),
            'is_mpmart' => $user->isMpMart(),
            'is_buyer' => $user->isBuyer()
        ] : null;
    }
}
