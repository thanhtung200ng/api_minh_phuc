<?php


namespace App\Http\Repositories\Resources\Customers;


use App\Http\Repositories\Entities\Customer;
use App\Http\Repositories\Entities\Member;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class CustomerResourcesCollection implements ResourcesInterface
{
    /**
     * @param Customer[]|Collection $members
     * @return []
     */
    public static function toArray($members)
    {
        $list = [];

        foreach ($members as $member){
            $list[] = [
                'id' => $member->getId(),
                'account' => $member->getAccount(),
                'first_name' => $member->getFirstName(),
                'last_name' => $member->getLastName(),
                'full_name' => $member->getFullName(),
                'mobile' => $member->getMobile(),
                'email' => $member->getEmail(),
                'disabled' => $member->isDisabled(),
                'created_at' => $member->getCreatedAtToString(),
                'updated_at' => $member->getUpdatedAt(),
                'status' => $member->getStatusToArray(),
                'rank_name' => $member->isMpMart() ? 'MP' : $member->isBuyer() ? 'Buyer' : 'Seller',
            ];
        }

        return $list;
    }
}
