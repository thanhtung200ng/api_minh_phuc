<?php


namespace App\Http\Repositories\Resources\Brands;


use App\Http\Repositories\Entities\Brand;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class BrandResourcesCollection implements ResourcesInterface
{
    /**
     * @param Brand[]|Collection $brands
     * @return []
     */
    public static function toArray($brands): array
    {
        $list = [];

        foreach ($brands as $brand){
            $list[] = [
                'id' => $brand->getId(),
                'name' => $brand->getName(),
                'images' => json_decode($brand->getImages()),
                'slug' => $brand->getSlug(),
                'status' => $brand->getStatus(),
                'created_at' => $brand->getCreatedAtToString(),
                'updated_at' => $brand->getUpdatedAt()
            ];
        }
        return $list;
    }
}
