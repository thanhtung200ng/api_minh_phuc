<?php


namespace App\Http\Repositories\Resources\Brands;


use App\Http\Repositories\Entities\Brand;
use App\Http\Repositories\Resources\ResourcesInterface;

class BrandResources implements ResourcesInterface
{

    /**
     * @param Brand $brand
     * @return array
     */
    public static function toArray($brand): ?array
    {
        return $brand ? [
            'id' => $brand->getId(),
            'name' => $brand->getName(),
            'images' => $brand->getImages(),
            'slug' => $brand->getSlug(),
            'status' => $brand->getStatus(),
            'created_at' => $brand->getCreatedAtToString(),
            'updated_at' => $brand->getUpdatedAt()
        ] : null;
    }
}
