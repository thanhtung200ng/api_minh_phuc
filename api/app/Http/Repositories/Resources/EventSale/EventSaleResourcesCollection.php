<?php


namespace App\Http\Repositories\Resources\EventSale;


use App\Http\Repositories\Entities\EventSale;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class EventSaleResourcesCollection implements ResourcesInterface
{
    /**
     * @param EventSale[]|Collection $event
     * @return array []
     */
    public static function toArray($event): array
    {
        $list = [];

        foreach ($event as $eventSale){
            $list[] = [
                'id' => $eventSale->getId(),
                'title' => $eventSale->getTitle(),
                'images' => json_decode($eventSale->getImages()),
                'link' => $eventSale->getLink(),
                'price' => $eventSale->getPrice(),
                'price_sale' => $eventSale->getPriceSale(),
                'status' => $eventSale->getStatus(),
                'created_at' => $eventSale->getCreatedAtToString(),
                'updated_at' => $eventSale->getUpdatedAt()
            ];
        }

        return $list;
    }

}
