<?php


namespace App\Http\Repositories\Resources\EventSale;


use App\Http\Repositories\Entities\EventSale;
use App\Http\Repositories\Resources\ResourcesInterface;

class EventSaleResources implements ResourcesInterface
{
    /**
     * @param EventSale $event
     * @return array
     */
    public static function toArray($event): ?array
    {
        return $event ? [
            'id' => $event->getId(),
            'title' => $event->getTitle(),
            'link' => $event->getLink(),
            'images' => $event->getImages(),
            'price' => $event->getPrice(),
            'price_sale' => $event->getPriceSale(),
            'created_at' => $event->getCreatedAt(),
            'update_at' => $event->getUpdateAt()
        ] : null;
    }
}
