<?php


namespace App\Http\Repositories\Resources\Agecy;


use App\Http\Repositories\Entities\Agecy;
use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Resources\ResourcesInterface;

class AgecyResourcesCollection implements ResourcesInterface
{

    /**
     * @param Agecy $agecy
     * @return array
     */
    public static function toArray($entity) : ?array
    {
        $list = [];
        foreach ($entity as $agecy){
            $list[] = [
                'id' => $agecy->getId(),
                'fullname' => $agecy->getFullName(),
                'account' => $agecy->getAccount(),
                'status' => $agecy->getStatus(),
                'created_at' => $agecy->getCreatedAt(),
                'mobile' => $agecy->getMobile(),
                'ward_id' => $agecy->getWardId(),
                'district_id' => $agecy->getDistrictId(),
                'city_id' => $agecy->getCityId(),
                'street' => $agecy->getStreet(),
                'customer_id' =>  $agecy->getCustomerId(),
                'rank_agency' => $agecy->getRankAgecy(),
            ];
        }
        return $list;
    }
}
