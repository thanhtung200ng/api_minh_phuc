<?php


namespace App\Http\Repositories\Resources\Agecy;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Agecy;
use App\Http\Repositories\Resources\ResourcesInterface;

class AgecyResources implements ResourcesInterface
{

    /**
     * @param Agecy $agecy
     * @return array
     */
    public static function toArray($entity) :?array
    {
        return $entity ? [
            'id' => $entity->getId(),
            'full_name' => $entity->getFullName(),
            'customer_id' => $entity->getCustomerId(),
            'account' => $entity->getAccount(),
            'status' => $entity->getStatus(),
            'city_id' => $entity->getCityId(),
            'street' => $entity->getStreet(),
            'district_id' => $entity->getDistrictId(),
            'ward_id' => $entity->getWardId(),
            'rank_agency' => $entity->getRankAgecy(),
            'mobile' => $entity->getMobile()
        ] : null;
    }
}
