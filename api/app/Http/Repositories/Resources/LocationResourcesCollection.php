<?php


namespace App\Http\Repositories\Resources;


use App\Http\Repositories\Entities\Location;
use Illuminate\Support\Collection;

class LocationResourcesCollection implements ResourcesInterface
{

    /**
     * @param Location[]|Collection $locations
     * @return array []
     */
    public static function toArray($locations): array
    {
        $list = [];

        foreach ($locations as $postion){
            $list[] = [
                'id' => $postion->getId(),
                'name' => $postion->getName(),
                'code' => $postion->getCode(),
            ];
        }

        return $list;
    }
}
