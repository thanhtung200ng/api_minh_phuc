<?php


namespace App\Http\Repositories\Resources\PurchaseOrder;


use App\Http\Repositories\Entities\PurchaseOders;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class PurchaseOrderResourcesCollection implements ResourcesInterface
{
    /**
     * @param PurchaseOders[]|Collection $PurchaseOders
     * @return []
     */
    public static function toArray($PurchaseOders)
    {
        $list = [];
        foreach ($PurchaseOders as $PurchaseOder){
            $list[] = [

            ];
        }

        return $list;
    }

    public static function toArrayItem($PurchaseOders)
    {
        $list = [];
        foreach ($PurchaseOders as $PurchaseOder){
            $list[] = [
                'status' => $PurchaseOder->getStatus(),
                'total_money' => $PurchaseOder->getTotalMoney(),
                'quantity' => $PurchaseOder->getQuantity(),
                'products_item_id' => $PurchaseOder->getProductItemId(),
                'purchase_oder_id' => $PurchaseOder->getId(),
            ];
        }
        return $list;
    }
}
