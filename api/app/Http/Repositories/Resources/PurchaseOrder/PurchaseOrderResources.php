<?php


namespace App\Http\Repositories\Resources\PurchaseOrder;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\PurchaseOders;
use App\Http\Repositories\Resources\ResourcesInterface;

class PurchaseOrderResources implements ResourcesInterface
{

    /**
     * @param PurchaseOders $PurchaseOders_id
     * @return array
     */
    public static function toArray($PurchaseOders_id): ?array
    {
        return $PurchaseOders_id ? [
            'customer_id' => $PurchaseOders_id->getCustomerId(),
            'id' => $PurchaseOders_id->getId(),
            'created_at' => $PurchaseOders_id->getCreatedAt(),
            'customer_wallet_id' => $PurchaseOders_id->getWalletsId(),
            'delivery_address_id' => $PurchaseOders_id->getAddressCustomerIdMuaHang(),
            'total' => $PurchaseOders_id->getEndingBalancex(),
            'status' => $PurchaseOders_id->getStatus(),
        ] : null;
    }
}
