<?php


namespace App\Http\Repositories\Resources\ElectronicContract;

use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\ElectronicContract;
use App\Http\Repositories\Resources\ResourcesInterface;

class ElectronicContractResources implements ResourcesInterface
{

    /**
     * @param ElectronicContract $entity
     * @return array
     */
    public static function toArray($entity) : ?array
    {
        return $entity ? [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'no' => $entity->getNo(),
            'status' => $entity->getStatus(),
            'created_by' => $entity->getCreatedBy(),
            'type' => $entity->getType(),
            'files' => $entity->getFileDecode(),
        ] : null;
    }
}
