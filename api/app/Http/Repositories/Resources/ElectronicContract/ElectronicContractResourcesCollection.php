<?php


namespace App\Http\Repositories\Resources\ElectronicContract;

use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\ElectronicContract;
use App\Http\Repositories\Resources\ResourcesInterface;

class ElectronicContractResourcesCollection implements ResourcesInterface
{

    /**
     * @inheritDoc
     */
    public static function toArray($entities)
    {
        $list = [];
        foreach ($entities as $entity){
            $list[] = [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'no' => $entity->getNo(),
            'status' => $entity->getStatus(),
            'created_at' => $entity->getCreatedAt(),
            'created_by' => $entity->getCreatedBy(),
            'type' => $entity->getType(),
            'files' => $entity->getFileDecode(),
        ];
        }
        return $list;
    }
}
