<?php


namespace App\Http\Repositories\Resources\BussinessContracts;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\BusinessContract;
use App\Http\Repositories\Resources\ResourcesInterface;

class BusinessContractResource implements ResourcesInterface
{
    /**
     * @param BusinessContract $entity
     * @return array
     */
    public static function toArray($entity)
    {
        return $entity ? [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'status' => $entity->isDisabled(),
        ] : null;
    }
}
