<?php


namespace App\Http\Repositories\Resources\Members;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Member;
use App\Http\Repositories\Resources\ResourcesInterface;

class MemberResources implements ResourcesInterface
{

    /**
     * @param Member $user
     * @return array
     */
    public static function toArray($user): ?array
    {
        return $user ? [
            'id' => $user->getId(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
//            'full_name' => $user->getFullName(),
            'account' => $user->getAccount(),
            'mobile' => $user->getMobile(),
            'email' => $user->getEmail(),
            'status' => $user->isDisabled(),
            'address' => $user->getAddressToArray(),
            'created_at' => $user->getCreatedAtToString(),
            'updated_at' => $user->getUpdatedAt()
        ] : null;
    }
}
