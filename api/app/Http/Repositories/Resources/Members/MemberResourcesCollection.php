<?php


namespace App\Http\Repositories\Resources\Members;


use App\Http\Repositories\Entities\Member;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class MemberResourcesCollection implements ResourcesInterface
{
    /**
     * @param Member[]|Collection $members
     * @return []
     */
    public static function toArray($members)
    {
        $list = [];

        foreach ($members as $member){
            $list[] = [
                'id' => $member->getId(),
                'first_name' => $member->getFirstName(),
                'last_name' => $member->getLastName(),
                'full_name' => $member->getFullName(),
                'mobile' => $member->getMobile(),
                'email' => $member->getEmail(),
                'disabled' => $member->isDisabled(),
                'created_at' => $member->getCreatedAtToString(),
                'status' => $member->getStatus(),
                'updated_at' => $member->getUpdatedAt()
            ];
        }

        return $list;
    }
}
