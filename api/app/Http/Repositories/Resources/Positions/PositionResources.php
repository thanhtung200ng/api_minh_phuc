<?php


namespace App\Http\Repositories\Resources\Positions;


use App\Http\Repositories\Entities\Position;
use App\Http\Repositories\Resources\ResourcesInterface;

class PositionResources implements ResourcesInterface
{

    /**
     * @param Position $postion
     * @return array
     */
    public static function toArray($postion): ?array
    {
        return $postion ? [
            'id' => $postion->getId(),
            'name' => $postion->getName(),
            'slug' => $postion->getSlug(),
            'status' => $postion->getStatus(),
            'created_at' => $postion->getCreatedAt(),
            'updated_at' => $postion->getUpdatedAt()
        ] : null;
    }
}
