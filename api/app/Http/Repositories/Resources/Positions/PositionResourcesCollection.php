<?php


namespace App\Http\Repositories\Resources\Positions;


use App\Http\Repositories\Entities\Position;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class PositionResourcesCollection implements ResourcesInterface
{
    /**
     * @param Position[]|Collection $postions
     * @return []
     */
    public static function toArray($postions): array
    {
        $list = [];

        foreach ($postions as $postion){
            $list[] = [
                'id' => $postion->getId(),
                'name' => $postion->getName(),
                'slug' => $postion->getSlug(),
                'status' => $postion->getStatus(),
                'created_at' => $postion->getCreatedAt(),
                'updated_at' => $postion->getUpdatedAt()
            ];
        }

        return $list;
    }
}
