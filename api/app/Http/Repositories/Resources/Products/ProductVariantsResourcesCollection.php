<?php


namespace App\Http\Repositories\Resources\Products;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\ProductVariants;
use App\Http\Repositories\Resources\ResourcesInterface;

class ProductVariantsResourcesCollection implements ResourcesInterface
{

    /**
     * @param ProductVariants[]|Collection $entities
     * @return []
     */
    public static function toArray($entities)
    {
        $list = [];
        foreach ($entities as $obj){
            $list[] = [
                'rating' => 5,
                'product_id' => $obj->getProductId(),
                'id' => $obj->getId(),
                'seller_name' => $obj->getSellerName(),
                'seller_by' => $obj->getSellerBy(),
                'name' => $obj->getName(),
                'sku' => $obj->getSku(),
                'slug' => $obj->getSlug(),
                'price' => $obj->getPrice(),
                'ranks' => $obj->getRanks(),
                'price_sale' => $obj->getPriceSale(),
                'gtin' => $obj->getGtin(),
                'images' => $obj->getImages(),
                'is_choose_agent' => $obj->getChooseAgent(),
                'quantity' => $obj->getQuantity(),
                'status' => $obj->isDisabled(),
                'created_at' => $obj->getCreatedAtToString(),
                'updated_at' => $obj->getUpdatedAt(),
                'sales_booth_id' => $obj->getSalesBooth_id(),
                'select_wallets' => $obj->getWalletByProducts(),
                'discount_product_by_quantity' => $obj->getDiscountProductByQuantity(),
            ];
        }
        return $list;
    }
}
