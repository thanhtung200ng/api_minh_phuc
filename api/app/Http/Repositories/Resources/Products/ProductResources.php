<?php


namespace App\Http\Repositories\Resources\Products;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Product;
use App\Http\Repositories\Resources\ResourcesInterface;

class ProductResources implements ResourcesInterface
{

    /**
     * @param Product $product
     * @return array
     */
    public static function toArray($product): ?array
    {
        return $product ? [
            'id' => $product->getId(),
            'category_id' => $product->getCategoryId(),
            'variants' => $product->getVariantsResource(),
            'seller_by' => $product->getSellerBy(),
            'description' => $product->getDescription(),
            'status' => $product->getStatus(),
            'created_at' => $product->getCreatedAtToString(),
            'views' => $product->getviews(),
            'like' => $product->getlike(),
        ] : null;
    }
    public static function Product_father_toArray($product_id): ?array
    {
        return $product_id ? [
            'id' => $product_id->getId(),
            'categoty_id' => $product_id->getCateId(),
            'content' => $product_id->getContent(),
            'description' => $product_id->getDescription(),
            'status' => $product_id->isDisabled(),
            'created_at' => $product_id->getCreatedAtToString(),
            'updated_at' => $product_id->getUpdatedAt()
        ] : null;
    }
}
