<?php


namespace App\Http\Repositories\Resources\Products;


use App\Http\Repositories\Entities\Product;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class ProductResourcesCollection implements ResourcesInterface
{
    /**
     * @param Product[]|Collection $product
     * @return []
     */
    public static function toArray($products)
    {
        $list = [];
        foreach ($products as $product){
            $list[] = [
                'id' => $product->getId(),
                'cate_name' => $product->getCateName(),
                'name' => $product->getName(),
                'slug' => $product->getSlug(),
                'price' => $product->getPrice(),
                'price_sale' => $product->getPriceSale(),
                'images' => $product->getImages(),
                'quantity' => $product->getQuantity(),
                'status' => $product->isDisabled(),
                'created_at' => $product->getCreatedAtToString(),
                'updated_at' => $product->getUpdatedAt()
            ];
        }
        return $list;
    }
}
