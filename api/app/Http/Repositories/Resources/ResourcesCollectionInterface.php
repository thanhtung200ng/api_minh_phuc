<?php


namespace App\Http\Repositories\Resources;


use App\Http\Repositories\Entities\BaseEntity;
use Illuminate\Support\Collection;

interface ResourcesCollectionInterface
{
    /**
     * @param BaseEntity[]|Collection $entities
     * @return mixed
     */
    public static function toArray($entities);
}
