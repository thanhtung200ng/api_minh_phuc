<?php


namespace App\Http\Repositories\Resources\PurchaseOrderItem;


use App\Http\Repositories\Entities\PurchaseOdersItem;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class PurchaseOrderItemResourcesCollection implements ResourcesInterface
{
    /**
     * @param PurchaseOdersItem[]|Collection $PurchaseOders
     * @return []
     */

    public static function toArray($PurchaseOders)
    {
        $list = [];
        foreach ($PurchaseOders as $PurchaseOder){
            $list[] = [
                'id' => $PurchaseOder->getId(),
                'total_money' => $PurchaseOder->getTotalMoney(),
                'quantity' => $PurchaseOder->getQuantity(),
                'products_item_id' => $PurchaseOder->getProductItemId(),
                'purchase_oder_id' => $PurchaseOder->getPurchaseOdersId(),
                'name_product' => $PurchaseOder->getNameProducts(),
                'images' => $PurchaseOder->getImages(),
                'ranks' => $PurchaseOder->getRanks(),
                'price' => $PurchaseOder->getPrice(),
                'created_at' => $PurchaseOder->getCreatedAt(),
            ];
        }
        return $list;
    }
}
