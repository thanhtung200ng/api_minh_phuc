<?php


namespace App\Http\Repositories\Resources\PurchaseOrderItem;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\PurchaseOdersItem;
use App\Http\Repositories\Resources\ResourcesInterface;

class PurchaseOrderItemResources implements ResourcesInterface
{

    /**
     * @param PurchaseOdersItem $PurchaseOder
     * @return array
     */
    public static function toArray($PurchaseOder): ?array
    {
        return $PurchaseOder ? [
            'id' => $PurchaseOder->getId(),
            'status' => $PurchaseOder->getStatus(),
            'total_money' => $PurchaseOder->getTotalMoney(),
            'quantity' => $PurchaseOder->getQuantity(),
            'products_item_id' => $PurchaseOder->getProductItemId(),
            'purchase_oder_id' => $PurchaseOder->getPurchaseOdersId(),
            'name_product' => $PurchaseOder->getNameProducts(),
            'images' => $PurchaseOder->getImages(),
            'ranks' => $PurchaseOder->getRanks(),
            'price' => $PurchaseOder->getPrice(),
            'created_at' => $PurchaseOder->getCreatedAt(),
        ] : null;
    }
}
