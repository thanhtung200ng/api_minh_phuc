<?php


namespace App\Http\Repositories\Resources\ConfigWebsite;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\ConfigWebsite;
use App\Http\Repositories\Resources\ResourcesInterface;

class ConfigWebsiteResources implements ResourcesInterface
{

    /**
     * @param ConfigWebsite $config
     * @return array
     */
    public static function toArray($config)
    {
        return $config ? [
            'id' => $config->getId(),
            'type' => $config->getType(),
            'value' => $config->getValue(),
        ] : null;
    }
}
