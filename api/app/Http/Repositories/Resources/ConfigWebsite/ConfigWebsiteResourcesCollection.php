<?php


namespace App\Http\Repositories\Resources\ConfigWebsite;


use App\Http\Repositories\Entities\ConfigWebsite;
use App\Http\Repositories\Resources\ResourcesCollectionInterface;
use Illuminate\Support\Collection;

class ConfigWebsiteResourcesCollection implements ResourcesCollectionInterface
{

    /**
     * @param ConfigWebsite[]|Collection $config
     * @return array []
     */
    public static function toArray($config)
    {
        $list = [];

        foreach ($config as $configWebsite){
            $list[] = [
                'id' => $configWebsite->getId(),
                'type' => $configWebsite->getType(),
                'value' => $configWebsite->getValue(),
            ];
        }

        return $list;
    }
}
