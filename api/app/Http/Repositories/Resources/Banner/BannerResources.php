<?php


namespace App\Http\Repositories\Resources\Banner;


use App\Http\Repositories\Entities\Banner;
use App\Http\Repositories\Resources\ResourcesInterface;

class BannerResources implements ResourcesInterface
{

    /**
     * @param Banner $banner
     * @return array
     */
    public static function toArray($banner): ?array
    {
        return $banner ? [
            'id' => $banner->getId(),
            'images_main' => $banner->getImagesMain(),
            'images_bottom' => $banner->getImagesBottom(),
            'image_items' => $banner->getImageItems(),
            'created_at' => $banner->getCreatedAt(),
            'updated_at' => $banner->getUpdatedAt()
        ] : null;
    }

}
