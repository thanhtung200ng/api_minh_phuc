<?php


namespace App\Http\Repositories\Resources\Banner;


use App\Http\Repositories\Entities\Banner;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class BannerResourcesCollection implements ResourcesInterface
{
    /**
     * @param Banner[]|Collection $banners
     * @return array []
     */
    public static function toArray($banners): array
    {
        $list = [];
        foreach ($banners as $banner){
            $list[] = [
                'id' => $banner->getId(),
                'images_main' => $banner->getImagesMain(),
                'images_bottom' => $banner->getImagesBottom(),
                'image_item' => $banner->getImageItems(),
                'created_at' => $banner->getCreatedAtToString(),
                'updated_at' => $banner->getUpdatedAt()
            ];
        }
        return $list;
    }

}
