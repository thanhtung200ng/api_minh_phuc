<?php


namespace App\Http\Repositories\Resources\Wallets;


use App\Http\Repositories\Entities\CustomerWallet;
use App\Http\Repositories\Entities\Wallets;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class WalletsResourcesCollection implements ResourcesInterface
{
    /**
     * @param CustomerWallet[]|Collection $wallets
     * @return []
     */
    public static function toArray($wallets)
    {
        $list = [];
        dd($list);
        foreach ($wallets as $wallet){
            $list[] = [
                'id' => $wallet->getId(),
                'name' => $wallet->getName(),
                'customer_id' => $wallet->getCustomerId(),
                'current_balance' => $wallet->getCurrentBalance(),
                'group_transaction_code' => $wallet->getGroupTransactionCodeToArray(),
                'wallet_id' => $wallet->getWalletId(),
                'status' => $wallet->getStatus(),
                'created_at' => $wallet->getCreatedAtToString(),
            ];
        }
        return $list;
    }
}
