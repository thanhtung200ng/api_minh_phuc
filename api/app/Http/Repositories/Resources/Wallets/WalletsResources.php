<?php


namespace App\Http\Repositories\Resources\Wallets;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Wallets;
use App\Http\Repositories\Resources\ResourcesInterface;

class WalletsResources implements ResourcesInterface
{

    /**
     * @param Wallets $wallet
     * @return array
     */
    public static function toArray($wallet)
    {
        return $wallet ? [
            'id' => $wallet->getId(),
            'name' => $wallet->getName(),
            'slug' => $wallet->getSlug(),
            'status' => $wallet->getStatus(),
            'created_at' => $wallet->getCreatedAt(),
            'update_at' => $wallet->getUpdateAt(),
            'icon' => $wallet->getIcon()
        ] : null;
    }

    public static function toArrayWalletCustomer($wallet)
    {
        return $wallet ? [
            'id' => $wallet->getId(),
            'name' => $wallet->getName(),
            'slug' => $wallet->getSlug(),
            'status' => $wallet->getStatus(),
            'created_at' => $wallet->getCreatedAt(),
        ] : null;
    }

}
