<?php


namespace App\Http\Repositories\Resources;


use App\Http\Repositories\Entities\BaseEntity;

interface ResourcesInterface
{
    /**
     * @param BaseEntity $entity
     * @return mixed
     */
    public static function toArray($entity);
}
