<?php


namespace App\Http\Repositories\Resources\withdraw_money;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\withdraw_money;
use App\Http\Repositories\Resources\ResourcesInterface;

class withdraw_moneyResources implements ResourcesInterface
{

    /**
     * @param Member $user
     * @return array
     */
    public static function toArray($withdraw): ?array
    {
        return $withdraw ? [
            'id' => $withdraw->getId(),
            'customer_id' => $withdraw->getCustomerId(),
            'customer_name' => $withdraw->getNameCustomer(),
            'wallet_id' => $withdraw->getWallestId(),
            'wallet_name' => $withdraw->getWallestName(),
//            'bank_account' => $withdraw->getBankAccount(),
//            'bank_name' => $withdraw->getNameBank(),
            'status' => $withdraw->getStatus(),
            'money' => $withdraw->getMoney(),
            'created_at' => $withdraw->getCreatedAt(),
            'transaction_fee' => $withdraw->getTransactionFee()
        ] : null;
    }

    public static function toArrayWithdraw($deduction): ?array
    {
        return $deduction ? [
            'id' => $deduction->getId(),
            'customer_id' => $deduction->getCustomerId(),
            'wallet_id' => $deduction->getWalletId(),
            'wallet_name' => $deduction->getName(),
            'status' => $deduction->getStatus(),
            'group_transaction_code' => $deduction->getGroupTransactionCode(),
            'created_at' => $deduction->getCreatedAt(),
            'current_balance' => $deduction->getCurrentBalance()
        ] : null;
    }
}
