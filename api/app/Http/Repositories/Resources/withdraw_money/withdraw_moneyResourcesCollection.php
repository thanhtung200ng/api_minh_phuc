<?php


namespace App\Http\Repositories\Resources\withdraw_moneyResourcesCollection;


use App\Http\Repositories\Entities\withdraw_money;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class withdraw_moneyResourcesCollection implements ResourcesInterface
{
    /**
     * @param Member[]|Collection $members
     * @return []
     */
    public static function toArray($members)
    {
        $list = [];

        foreach ($members as $member){
            $list[] = [
                'id' => $member->getId(),
                'first_name' => $member->getFirstName(),
                'last_name' => $member->getLastName(),
                'full_name' => $member->getFullName(),
                'mobile' => $member->getMobile(),
                'email' => $member->getEmail(),
                'disabled' => $member->isDisabled(),
                'created_at' => $member->getCreatedAtToString(),
                'updated_at' => $member->getUpdatedAt()
            ];
        }

        return $list;
    }
}
