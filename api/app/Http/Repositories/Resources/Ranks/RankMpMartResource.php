<?php


namespace App\Http\Repositories\Resources\Ranks;


use App\Http\Repositories\Entities\RankMpMart;
use App\Http\Repositories\Resources\ResourcesInterface;

class RankMpMartResource implements ResourcesInterface
{

    /**
     * @param RankMpMart $entity
     * @return array|null
     */
    public static function toArray($entity): ?array
    {

        return $entity ? [
            'min_money' => $entity->getMinMoney(),
            'max_money' => $entity->getMaxMoney(),
            'discount' => $entity->getDiscount(),
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'slug' => $entity->getSlug(),
            'created_at' => $entity->getCreatedAt(),
        ] : null;
    }
}
