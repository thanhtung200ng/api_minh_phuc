<?php


namespace App\Http\Repositories\Resources\Ranks;


use App\Http\Repositories\Entities\Product;
use App\Http\Repositories\Entities\RankUser;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class RankResourcesCollection implements ResourcesInterface
{
    /**
     * @param RankUser[]|Collection $entities
     * @return array []
     */
    public static function toArray($entities): array
    {
        $list = [];

        foreach ($entities as $entity){
            $list[] = [
                'id' => $entity->getId(),
                'name' => $entity->getName(),
                'sort' => $entity->getSort(),
                'slug' => $entity->getSlug(),
                'done' => $entity->isActive(),
                'status' => $entity->getStatusToArray(),
                'created_by' => $entity->getCreatedBy(),
            ];
        }

        return $list;
    }
}
