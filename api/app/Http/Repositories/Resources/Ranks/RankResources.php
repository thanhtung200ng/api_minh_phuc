<?php


namespace App\Http\Repositories\Resources\Products;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Product;
use App\Http\Repositories\Resources\ResourcesInterface;

class RankResources implements ResourcesInterface
{

    /**
     * @param Product $product_id
     * @return array
     */
    public static function toArray($product_id): ?array
    {
        return $product_id ? [
            'property_item_id' => json_decode($product_id->getPropertyItemId()),
            'product_id' => $product_id->getProductId(),
            'id' => $product_id->getId(),
            'name' => $product_id->getName(),
            'sku' => $product_id->getSku(),
            'slug' => $product_id->getSlug(),
            'price' => $product_id->getPrice(),
            'price_sale' => $product_id->getPriceSale(),
            'gtin' => $product_id->getGtin(),
            'quatity' => $product_id->getQuatity(),
            'content' => $product_id->getContent(),
            'images' => $product_id->getImages(),
            'description' => $product_id->getDescription(),
            'avatar' => $product_id->getAvatar(),
            'status' => $product_id->isDisabled(),
            'created_at' => $product_id->getCreatedAtToString(),
            'updated_at' => $product_id->getUpdatedAt()
        ] : null;
    }
    public static function Product_father_toArray($product_id): ?array
    {
        return $product_id ? [
            'id' => $product_id->getId(),
            'categoty_id' => $product_id->getCateId(),
            'content' => $product_id->getContent(),
            'description' => $product_id->getDescription(),
            'status' => $product_id->isDisabled(),
            'created_at' => $product_id->getCreatedAtToString(),
            'updated_at' => $product_id->getUpdatedAt()
        ] : null;
    }
}
