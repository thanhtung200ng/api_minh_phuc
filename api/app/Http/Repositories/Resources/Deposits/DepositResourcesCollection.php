<?php


namespace App\Http\Repositories\Resources\Deposits;


use App\Http\Repositories\Entities\Deposit;
use App\Http\Repositories\Entities\Member;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class DepositResourcesCollection implements ResourcesInterface
{
    /**
     * @param Deposit[]|Collection $deposits
     * @return []
     */
    public static function toArray($deposits)
    {
        $list = [];

        foreach ($deposits as $deposit){
            $list[] = [
                'id' => $deposit->getId(),
                'from_customer_full_name' => $deposit->getFromCustomerFullName(),
                'from_customer_id' => $deposit->getFromCustomerId(),
                'from_customer_account' => $deposit->getFromCustomerAccount(),
                'from_customer_status' => $deposit->getFromCustomerStatus(),
                'from_customer_wallet_id' => $deposit->getFromCustomerWalletId(),
                'from_customer_wallet_name' => $deposit->getFromCustomerWalletName(),
                'to_customer_full_name' => $deposit->getToCustomerFullName(),
                'to_customer_id' => $deposit->getToCustomerId(),
                'to_customer_account' => $deposit->getToCustomerAccount(),
                'to_customer_status' => $deposit->getToCustomerStatus(),
                'to_customer_wallet_id' => $deposit->getToCustomerWalletId(),
                'to_customer_wallet_name' => $deposit->getToCustomerWalletName(),
                'money' => $deposit->getMoney(),
                'transaction_code' => $deposit->getTransactionCodeToArray(),
                'disabled' => $deposit->isDisabled(),
                'status' => $deposit->getStatusToArray(),
                'created_at' => $deposit->getCreatedAtToString(),
                'created_by' => $deposit->getCreatedBy(),
                'approved_at' => $deposit->getApprovedAt(),
                'approved_by' => $deposit->getApprovedBy()
            ];
        }

        return $list;
    }
}
