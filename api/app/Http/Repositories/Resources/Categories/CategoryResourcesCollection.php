<?php


namespace App\Http\Repositories\Resources\Categories;


use App\Http\Repositories\Entities\Category;
use App\Http\Repositories\Resources\ResourcesInterface;
use Illuminate\Support\Collection;

class CategoryResourcesCollection implements ResourcesInterface
{
    /**
     * @param Category[]|Collection $cates
     * @return []
     */
    public static function toArray($cates)
    {
        $list = [];

        foreach ($cates as $cate_id){
            $list[] = [
                'id' => $cate_id->getId(),
                'name' => $cate_id->getName(),
                'slug' => $cate_id->getSlug(),
                'parent_id' => $cate_id->getParentId(),
                'status' => $cate_id->isDisabled(),
                'created_at' => $cate_id->getCreatedAtToString(),
                'updated_at' => $cate_id->getUpdatedAt(),
                'is_leaf' => $cate_id->getIsLeaf()
            ];
        }
        return $list;
    }
}
