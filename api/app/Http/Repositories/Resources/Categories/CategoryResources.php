<?php


namespace App\Http\Repositories\Resources\Categories;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Category;
use App\Http\Repositories\Resources\ResourcesInterface;

class CategoryResources implements ResourcesInterface
{

    /**
     * @param Category $cate_id
     * @return array
     */
    public static function toArray($cate_id): ?array
    {
        return $cate_id ? [
            'id' => $cate_id->getId(),
            'name' => $cate_id->getName(),
            'parent_id' => $cate_id->getParentId(),
            'slug' => $cate_id->getSlug(),
            'is_leaf' => $cate_id->getIsLeaf(),
            'status' => $cate_id->getStatus(),
            'created_at' => $cate_id->getCreatedAtToString(),
            'updated_at' => $cate_id->getUpdatedAt()
        ] : null;
    }
}
