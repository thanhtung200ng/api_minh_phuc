<?php


namespace App\Http\Repositories\Enums;


abstract class RankMpMartCode
{
    const KH = 1;
    const CTV = 2;
    const CTV_I = 3;
    const DAILY = 4;
    const DAILY_I = 5;
    const DAILY_II = 6;
    const SIEUTHI = 7;
    const SIEUTHI_I = 8;
    const SIEUTHI_II = 9;
    const TONGKHO = 10;

    public static function isCTV (int $code): bool
    {
        return self::CTV === $code || self::CTV_I === $code;
    }

    public static function isDaiLy (int $code): bool
    {
        return self::DAILY === $code || self::DAILY_I === $code || self::DAILY_II === $code;
    }

    public static function isSieuThi (int $code): bool
    {
        return self::SIEUTHI === $code || self::SIEUTHI_I === $code || self::SIEUTHI_II === $code;
    }

    public static function isTongKho (int $code): bool
    {
        return self::TONGKHO === $code;
    }

    public static function getGroupCTV (): array
    {
        return [
            ['id' => self::CTV, 'name' => 'Cộng tác viên'],
            ['id' => self::CTV_I, 'name' => 'Cộng tác viên VIP I'],
        ];
    }

    public static function getGroupDaiLy (): array
    {
        return [
            ['id' => self::DAILY, 'name' => 'Đại Lý'],
            ['id' => self::DAILY_I, 'name' => 'Đại Lý VIP I'],
            ['id' => self::DAILY_II, 'name' => 'Đại Lý VIP II'],
        ];
    }

    public static function getGroupSieuThi (): array
    {
        return [
            ['id' => self::SIEUTHI, 'name' => 'Siêu Thị'],
            ['id' => self::SIEUTHI_I, 'name' => 'Siêu Thị VIP I'],
            ['id' => self::SIEUTHI_II, 'name' => 'Siêu Thị VIP II'],
        ];
    }

    public static function getGroupTongKho (): array
    {
        return [
            ['id' => self::TONGKHO, 'name' => 'Tổng kho'],
        ];
    }



}
