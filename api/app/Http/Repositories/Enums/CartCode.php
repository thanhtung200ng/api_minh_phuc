<?php

namespace App\Http\Repositories\Enums;


abstract class CartCode
{
    const TYPE_AGENT = 1; // giỏ hàng của cty
    const TYPE_FUGO = 3; // giỏ hàng fugo
    const TYPE_NOODLE = 4; // giỏ hàng mì
    const TYPE_SELLER = 2; // giỏ hàng của seller
    const STATUS_SHOPPING = 0; // đang mua
    const STATUS_PAY_SUCCESS = 1; // đang mua

    static function getStatusShopping(): int
    {
        return self::STATUS_SHOPPING;
    }

    static function getStatusPaySuccess(): int
    {
        return self::STATUS_PAY_SUCCESS;
    }

    static function getGroupCarts(): array
    {
        return [
          ['name' => 'Giỏ mua sỉ', 'id' => self::TYPE_AGENT, 'code' => 'wholesale'],
          ['name' => 'Giỏ hàng fugo', 'id' => self::TYPE_FUGO, 'code' => 'fugo'],
          ['name' => 'Giỏ hàng mì', 'id' => self::TYPE_NOODLE, 'code' => 'noodle'],
          ['name' => 'Giỏ mua lẻ', 'id' => self::TYPE_SELLER, 'code' => 'retail'],
        ];
    }

    static function getTypeCart($type): int
    {
        $groups = self::getGroupCarts();
        foreach ($groups as $group) {
            if ($group['code'] === $type) {
                return $group['id'];
            }
        }
        return -1;
    }
}
