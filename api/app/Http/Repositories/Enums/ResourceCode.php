<?php


namespace App\Http\Repositories\Enums;


abstract class  ResourceCode
{
    const CREATE = 1;
    const UPDATE = 2;
    const DELETE = -4;
    const RESTORE = -15;
}
