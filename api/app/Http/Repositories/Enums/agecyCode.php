<?php


namespace App\Http\Repositories\Enums;


use function MongoDB\BSON\toRelaxedExtendedJSON;

abstract class agecyCode
{
    const ctv = 1;
    const agecy = 2;
    const supermarket = 3;
    const DISABLED = 3;
}
