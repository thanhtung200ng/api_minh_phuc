<?php


namespace App\Http\Repositories\Enums;


abstract class PurchaseOrderCode
{
    const STATUS_PROCESSING = 0; // đang xử lý
    const STATUS_TO_PACK = 1; // đóng gói và bàn giao
    const STATUS_TO_SHIP = 2; // chờ lấy hàng
    const STATUS_SHIPPING = 3; // đang giao hàng
    const STATUS_COMPLETED = 9; // đã giao hàng
    const STATUS_CANCELLED = 4; // đã hủy do khách ko đặt nữa, chỉ đc hủy khi chưa giao hàng
    const STATUS_RETURNED = 5; // khách feedback trả lại khi hàng đã giao do ko đúng yêu cầu
    const STATUS_FAIL_DELIVERY = 6; // Giao hàng ko thành công (do thay đổi địa chỉ hoặc người nhận boom)
    const STATUS_DELETED = -4; // Giao hàng ko thành công (do thay đổi địa chỉ hoặc người nhận boom)
    const STATUS_CHANGE = 7; // Đơn trong trạng thái thay sửa

    const PAYMENT_VITIEUDUNG = 'payment_vitieudung'; // khách feedback trả lại khi hàng đã giao do ko đúng yêu cầu
    const PAYMENT_KHODIEM = 'payment_khodiem';

    static function getStatus($selected = false, $return = false, $groupAction = true): array
    {
        $listStatus = [
            self::STATUS_PROCESSING => [
                'id' => self::STATUS_PROCESSING, 'alias' => 'pending',
                'style' => 'info',
                'icon' => 'mdi mdi-comment-processing-outline',
                'name' => 'Chờ xử lý',

                'actions' => ($groupAction === true) ? [
                    self::STATUS_SHIPPING => self::getStatus(self::STATUS_SHIPPING, true, false),
                    self::STATUS_CANCELLED => self::getStatus(self::STATUS_CANCELLED, true, false),
                ] : []
            ],
            self::STATUS_TO_PACK => [
                'id' => self::STATUS_TO_PACK, 'alias' => 'pending',
                'style' => 'warning',
                'icon' => 'mdi mdi-package-variant',
                'name' => 'Đóng gói và bàn giao',
                'actions' => ($groupAction === true) ? [
                    self::STATUS_TO_SHIP => self::getStatus(self::STATUS_TO_SHIP, true, false),
                    self::STATUS_PROCESSING => self::getStatus(self::STATUS_PROCESSING, true, false),
                    self::STATUS_CANCELLED => self::getStatus(self::STATUS_CANCELLED, true, false),
                    self::STATUS_DELETED => self::getStatus(self::STATUS_DELETED, true, false),
                ] : []
            ],
            self::STATUS_TO_SHIP => [
                'id' => self::STATUS_TO_SHIP, 'alias' => 'ready_ship',
                'style' => 'info',
                'icon' => 'mdi mdi-react',
                'name' => 'Sẵn sàng giao hàng',
                'actions' => ($groupAction === true) ? [
                    self::STATUS_TO_PACK => self::getStatus(self::STATUS_TO_PACK, true, false),
                    self::STATUS_SHIPPING => self::getStatus(self::STATUS_SHIPPING, true, false),
                    self::STATUS_CANCELLED => self::getStatus(self::STATUS_CANCELLED, true, false),
                    self::STATUS_DELETED => self::getStatus(self::STATUS_DELETED, true, false),
                ] : []
            ],
            self::STATUS_SHIPPING => [
                'id' => self::STATUS_SHIPPING, 'alias' => 'shiped',
                'style' => 'info',
                'icon' => 'mdi mdi-truck-fast-outline',
                'name' => 'Đang giao hàng',
                'actions' => ($groupAction === true) ?[
                    self::STATUS_PROCESSING => self::getStatus(self::STATUS_PROCESSING, true, false),
                    self::STATUS_COMPLETED => self::getStatus(self::STATUS_COMPLETED, true, false),
                    self::STATUS_CANCELLED => self::getStatus(self::STATUS_CANCELLED, true, false),
                ] : []
            ],
            self::STATUS_COMPLETED => [
                'id' => self::STATUS_COMPLETED, 'alias' => 'delivered',
                'style' => 'info',
                'icon' => 'mdi mdi-check-decagram',
                'name' => 'Đã giao hàng',
                'text-action' => 'Đã giao hàng',
                'actions' => []
            ],
            self::STATUS_CANCELLED => [
                'id' => self::STATUS_CANCELLED, 'alias' => 'cancelled',
                'style' => 'secondary',
                'icon' => 'mdi mdi-cancel',
                'name' => 'Huỷ đơn',
                'text-action' => 'Đã hủy',
                'actions' => ($groupAction === true) ? [
                    self::STATUS_DELETED => self::getStatus(self::STATUS_DELETED, true, false),
                ] : []
            ],
            self::STATUS_RETURNED => [
                'id' => self::STATUS_RETURNED, 'alias' => 'returned',
                'style' => 'secondary',
                'icon' => 'mdi mdi-undo-variant',
                'name' => 'Trả hàng',
                'text-action' => 'Trả hàng',
                'actions' => ($groupAction === true) ? [
                    self::STATUS_DELETED => self::getStatus(self::STATUS_DELETED, true, false),
                ] : []
            ],
            self::STATUS_FAIL_DELIVERY  => [
                'id' => self::STATUS_FAIL_DELIVERY, 'alias' => 'fail-delivery',
                'style' => 'secondary',
                'icon' => 'mdi mdi-alert-decagram-outlinet',
                'name' => 'Giao hàng không thành công',
                'text-action' => 'Giao hàng không thành công',
                'actions' => ($groupAction === true) ? [
                    self::STATUS_DELETED => self::getStatus(self::STATUS_DELETED, true, false),
                ] : []
            ],
            self::STATUS_DELETED => [
                'id' => self::STATUS_DELETED, 'alias' => 'deleted',
                'style' => 'secondary',
                'icon' => 'mdi mdi-trash-can-outline',
                'name' => 'Đã xóa',
                'text-action' => 'Đã xóa',
                'actions' => []
            ],
        ];
        if ($selected !== false && isset($listStatus[$selected])) {
            $listStatus[$selected]['checked'] = 'checked';
            if($return) {
                return $listStatus[$selected];
            }
        }

        if($return) {
            return [
                'id' => 0, 'style' => 'secondary',
                'icon' => 'mdi mdi-trash-can-outline',
                'name' => '---',
                'actions' => []
            ];
        }

        return $listStatus;
    }

    static function isRefund(int $code): bool
    {
        return $code === PurchaseOrderCode::STATUS_CANCELLED || $code === PurchaseOrderCode::STATUS_RETURNED
            || $code === PurchaseOrderCode::STATUS_FAIL_DELIVERY || $code === PurchaseOrderCode::STATUS_DELETED;
    }

    static function groupCodeRefund(): array
    {
        return [PurchaseOrderCode::STATUS_CANCELLED, PurchaseOrderCode::STATUS_RETURNED,
            PurchaseOrderCode::STATUS_FAIL_DELIVERY, PurchaseOrderCode::STATUS_DELETED];
    }
}
