<?php


namespace App\Http\Repositories\Enums;


abstract class TransactionCode
{
    const recharge = 1; // nạp điểm
    const transfer = 2; // chuyển khoản
    const purchase = 3; // dùng để mua hàng
    const totalRevenue = 4; // tính tổng doanh số
    const withdrawal = 5; // rút tiền
    const recruitmentCommission = 6; // hoa hồng tuyển dụng
    const purchaseMoney = 7; // nhận tiền mua hàng
    const noDiscountWholesale = 8; // nhận nạp tiền bán buôn không chiết khấu
    const deliveryCommission = 9; // hoa hồng giao hàng
    const salesProfit = 10; // lợi nhuận thu về của công ty khi seller bán thành công
    const refundPO = 11; // hoàn trả tiền đơn hàng
    const profitPaymentPeriod = 12; // kỳ thanh toán lợi nhuận
    const donatePoint = 13; // tặng điểm
    const refunDiscountPO = 15; // hoàn tiền chiết khấu
    const refundOnException = 1313; // hoàn tiền ví khi xử lý lỗi/ nhầm tiền
    const refundInWrongTransfer = 3131; // hoàn tiền ví khi duyệt nhầm
    const transferWallet = 14;

    public static function isSalesProfit ($code): bool
    {
        return self::salesProfit === $code;
    }

    public static function isDeliveryCommission ($code): bool
    {
        return self::deliveryCommission === $code;
    }

    public static function isPurchaseMoney ($code): bool
    {
        return self::purchaseMoney === $code;
    }

    public static function getCode($selected = false, $return = false): array {
        $listStatus = [
            self::recharge => ['id' => self::recharge,
                'style' => 'danger', 'name' => 'Nạp điểm',],
            self::transfer => ['id' => self::transfer,
                'style' => 'secondary', 'name' => 'Chuyển tiền'],
            self::totalRevenue => ['id' => self::totalRevenue,
                'style' => 'secondary', 'name' => 'Tổng doanh số'],
            self::recruitmentCommission => ['id' => self::recruitmentCommission,
                'style' => 'secondary', 'name' => 'HH tuyển dụng'],
            self::purchaseMoney => ['id' => self::purchaseMoney,
                'style' => 'secondary', 'name' => 'Kho điểm'],
            self::salesProfit => ['id' => self::salesProfit,
                'style' => 'secondary', 'name' => 'Lợi nhuận PO'],
            self::deliveryCommission => ['id' => self::deliveryCommission,
                'style' => 'secondary', 'name' => 'HH giao hàng'],
            self::refunDiscountPO => ['id' => self::refunDiscountPO,
                'style' => 'secondary', 'name' => 'Hoàn tiền chiết khấu mua hàng'],
            self::refundPO => ['id' => self::refundPO,
                'style' => 'secondary', 'name' => 'Hoàn trả tiền đơn hàng huỷ/lỗi'],
            self::profitPaymentPeriod => ['id' => self::profitPaymentPeriod,
                'style' => 'secondary', 'name' => 'Thanh toán lợi nhuận'],
            self::donatePoint => ['id' => self::donatePoint,
                'style' => 'secondary', 'name' => 'Tặng điểm'],
            self::refundOnException => ['id' => self::refundOnException,
                'style' => 'secondary', 'name' => 'Hoàn tiền trường hợp xử lý lỗi hệ thống'],
            self::transferWallet => ['id' => self::transferWallet,
                'style' => 'secondary', 'name' => 'Chuyển điểm ví'],
        ];
        if ($selected && isset($listStatus[$selected])) {
            $listStatus[$selected]['checked'] = 'checked';
            if($return) {
                return $listStatus[$selected];
            }
        }

        if($return) {
            return [
                'id' => 0, 'style' => 'danger',
                'icon' => 'mdi mdi-trash-can-outline',
                'name' => '---',
                'actions' => []
            ];
        }

        return $listStatus;
    }
}
