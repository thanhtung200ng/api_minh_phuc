<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\RankMpMart;
use App\Http\Repositories\Entities\RankUser;

class RankMpMartFactory extends RankFactory
{
    /**
     * @param $entity
     * @return RankMpMart
     */
    public static function make($entity)
    {
        $obj = new RankMpMart();
        $obj->setId($entity->id);
        $obj->setName($entity->name);
        $obj->setSort($entity->sort);
        $obj->setMinMoney($entity->min_money);
        $obj->setMaxMoney($entity->max_money);
        $obj->setSlug($entity->slug);
        $obj->setDiscount($entity->discount);
        $obj->setStatus($entity->status);
        $obj->setCreatedAt($entity->created_at);
        $obj->setCreatedBy($entity->created_by);
        return $obj;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $position = collect();

        foreach ( $entities as $entity){
            $position->push(self::make($entity));
        }

        return $position;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
