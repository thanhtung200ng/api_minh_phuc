<?php


namespace App\Http\Repositories\Factories;

use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Product;
use App\Http\Repositories\Entities\ProductVariants;
use App\Http\Repositories\Entities\SalesBooth;

class ProductVariantsFactory implements InterfaceFactory
{

    public static function make($entity): ProductVariants
    {
        $product = new ProductVariants();
        $product->setProductId($entity->product_id);
        $product->setId($entity->id);
        $product->setSku($entity->sku);
        $product->setName($entity->name);
        if (isset($entity->seller_name)){
        $product->setSellerName($entity->seller_name);
        }
        $product->setSellerBy($entity->seller_by);
        $product->setPrice($entity->price);
        $product->setSlug($entity->slug);
        $product->setPriceSale($entity->price_sale);
        $product->setQuantity($entity->quantity);
        $product->setPriceSale($entity->price_sale);
        if(isset($entity->ranks)){
            $product->setRanksDecode($entity->ranks);
        }
        $product->setImagesDecode($entity->images);
        if(isset($entity->salesbooth_name)) {
            $product->setSalesBooth_name($entity->salesbooth_name);
        }
        if(isset($entity->sales_booth_id)) {
            $product->setSalesBooth_id($entity->sales_booth_id);
        }
        $product->setChooseAgent($entity->is_choose_agent);
        $product->setStatus($entity->status);
        $product->setCreatedAt($entity->created_at);
        $product->setUpdatedAt($entity->updated_at);
        $product->setPropertyItemId($entity->property_item_id);
        $product->setDiscountProductByQuantity($entity->discount_product_by_quantity);


        return $product;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $cate = collect();
        foreach ($entities as $entity) {
            $product = self::make($entity);
            $salesBooth = new SalesBooth();
            $salesBooth->setId($entity->sales_booth_id);
            $salesBooth->setName($entity->salesbooth_name);
            $salesBooth->setStatus($entity->salesbooth_status);
            $salesBooth->setSlug($entity->salesbooth_slug);
            $salesBooth->setCustomerId($entity->customer_id);
            $product->setSalesBooth($salesBooth);
            $cate->push($product);
        }
        return $cate;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
