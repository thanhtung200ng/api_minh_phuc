<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\ConfigWebsite;


class ConfigWebsiteFactory implements InterfaceFactory
{

    /**
     * @inheritDoc
     */
    public static function make($entity)
    {
        $config = new ConfigWebsite();
        $config->setId($entity->id);
        $config->setType($entity->type);
        $config->setValue($entity->values);
        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    /**
     * @inheritDoc
     */
    public static function makeCollection($entities)
    {
        $config = collect();
        foreach ( $entities as $entity){
            $config->push(self::make($entity));
        }
        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
