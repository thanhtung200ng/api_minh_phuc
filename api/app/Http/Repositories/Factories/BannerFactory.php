<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\Banner;
use Illuminate\Support\Collection;

class BannerFactory implements InterfaceFactory
{

    public static function make($entity)
    {
        $brand = new Banner();
        $brand->setId($entity->id);
        $brand->setImagesMain($entity->images_main);
        $brand->setImagesBottom($entity->images_bottom);
        $brand->setImageItems($entity->image_items);
        $brand->setCreatedAt($entity->created_at);
        $brand->setUpdatedAt($entity->updated_at);
        return $brand;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $brand = collect();
        foreach ( $entities as $entity){
            $brand->push(self::make($entity));
        }
        return $brand;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
