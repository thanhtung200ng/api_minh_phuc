<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\EventSale;

class EventSaleFactory implements InterfaceFactory
{
    public static function make($entity)
    {
        $event = new EventSale();
        $event->setId($entity->id);
        $event->setTitle($entity->title);
        $event->setImages($entity->images);
        $event->setLink($entity->link);
        $event->setPrice($entity->price);
        $event->setPriceSale($entity->price_sale);
        $event->setCreatedBy($entity->created_by);
        $event->setStatus($entity->status);
        $event->setCreatedAt($entity->created_at);
        return $event;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $event= collect();
        foreach ( $entities as $entity){
            $event->push(self::make($entity));
        }
        return $event;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }

}
