<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Deposit;

class DepositFactory implements InterfaceFactory
{
    public static function make($entity)
    {
        $deposit = new Deposit();
        $deposit->setId($entity->id);
        $deposit->setMoney($entity->money);
        $deposit->setFromCustomerId($entity->from_customer_id);
        $deposit->setFromCustomerFullName($entity->from_customer_full_name);
        $deposit->setFromCustomerAccount($entity->from_customer_account);
        $deposit->setToCustomerId($entity->to_customer_id);
        $deposit->setToCustomerFullName($entity->to_customer_full_name);
        $deposit->setToCustomerAccount($entity->to_customer_account);
        $deposit->setFromCustomerWalletId($entity->from_customer_wallet_id);
        $deposit->setToCustomerWalletId($entity->to_customer_wallet_id);
        $deposit->setFromCustomerWalletName($entity->from_customer_wallet_name);
        $deposit->setToCustomerWalletName($entity->to_customer_wallet_name);
        $deposit->setFromCustomerStatus($entity->from_customer_status);
        $deposit->setToCustomerStatus($entity->to_customer_status);
        $deposit->setFromCustomerWalletStatus($entity->from_customer_wallet_status);
        $deposit->setToCustomerWalletStatus($entity->to_customer_wallet_status);
        $deposit->setFromCustomerWalletCurrentBalance($entity->from_customer_wallet_current_balance);
        $deposit->setToCustomerWalletCurrentBalance($entity->to_customer_wallet_current_balance);
        $deposit->setApprovedAt($entity->approved_at);
        $deposit->setApprovedBy($entity->approved_by);
        $deposit->setApprovedByName($entity->approved_name);
        $deposit->setStatus($entity->status);
        $deposit->setCreatedAt($entity->created_at);
        $deposit->setCreatedBy($entity->created_by);
        $deposit->setTransactionCode($entity->transaction_code);
        return $deposit;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $position = collect();

        foreach ( $entities as $entity){
            $position->push(self::make($entity));
        }

        return $position;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
