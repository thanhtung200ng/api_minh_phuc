<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\RankUser;
use Illuminate\Support\Collection;

class RankFactory implements InterfaceFactory
{

    public static function make($entity)
    {
        $obj = new RankUser();
        $obj->setId($entity->id);
        $obj->setName($entity->name);
        $obj->setSort($entity->sort);
        $obj->setSlug($entity->slug);
        $obj->setStatus($entity->status);
        $obj->setCreatedAt($entity->created_at);
        $obj->setCreatedBy($entity->created_by);
        return $obj;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $position = collect();

        foreach ( $entities as $entity){
            $position->push(self::make($entity));
        }

        return $position;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
