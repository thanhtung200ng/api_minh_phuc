<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\Collections ;
use Illuminate\Support\Collection;

class CollectionFactory implements InterfaceFactory
{

    public static function make($entity)
    {
        $collection = new Collections();
        $collection->setId($entity->id);
        $collection->setName($entity->name);
        $collection->setSlug($entity->slug);
        $collection->setStatus($entity->status);
        $collection->setCreatedAt($entity->created_at);
        $collection->setCateId($entity->cates);
        return $collection;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $collection = collect();
        foreach ( $entities as $entity){
            $collection->push(self::make($entity));
        }
        return $collection;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
