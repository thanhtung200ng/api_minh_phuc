<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\ElectronicContract;
use App\Http\Repositories\Entities\Filter;
use App\Http\Repositories\Entities\Order;
use App\Http\Repositories\Interfaces\ElectronicContractInterface;

class ElectronicContractFactory implements InterfaceFactory
{
    /**
     * @inheritDoc
     */
    public static function make($entity)
    {
        $contract = new ElectronicContract();
        $contract->setId($entity->id);
        $contract->setName($entity->name);
        $contract->setStatus($entity->status);
        $contract->setCreatedAt($entity->created_at);
        $contract->setCreatedBy($entity->created_by);
        $contract->setType($entity->type);
        $contract->setFile($entity->files);
        $contract->setNo($entity->no);
        return $contract;
    }

    /**
     * @inheritDoc
     */
    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    /**
     * @inheritDoc
     */
    public static function makeCollection($entities)
    {
        $brand = collect();
        foreach ( $entities as $entity){
            $brand->push(self::make($entity));
        }
        return $brand;
    }

    /**
     * @inheritDoc
     */
    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }

}
