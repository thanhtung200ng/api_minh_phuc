<?php


namespace App\Http\Repositories\Factories;

use App\Http\Repositories\Entities\Product;

class ProductFactory implements InterfaceFactory
{

    public static function make($entity): Product
    {
        $product = new Product();
        $product->setId($entity->id);
        $product->setCategoryId($entity->category_id);
        $product->setDescription($entity->description);
        $product->setContent($entity->content);
        $product->setSeo($entity->seo);
        $product->setStatus($entity->status);
        $product->setCreatedAt($entity->created_at);
        $product->setUpdatedAt($entity->updated_at);
        $product->setCreatedBy($entity->created_by);
        $product->setSellerBy($entity->seller_by);
        $product->setlike($entity->like);
        $product->setviews($entity->views);
        return $product;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $cate = collect();
        foreach ($entities as $entity) {
            $cate->push(self::make($entity));
        }
        return $cate;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
