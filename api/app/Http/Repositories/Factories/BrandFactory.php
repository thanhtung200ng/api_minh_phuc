<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\Brand;
use Illuminate\Support\Collection;

class BrandFactory implements InterfaceFactory
{

    public static function make($entity)
    {
        $brand = new Brand();
        $brand->setId($entity->id);
        $brand->setName($entity->name);
        $brand->setImages($entity->images);
        $brand->setSlug($entity->slug);
        $brand->setStatus($entity->status);
        $brand->setCreatedAt($entity->created_at);
        return $brand;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $brand = collect();
        foreach ( $entities as $entity){
            $brand->push(self::make($entity));
        }
        return $brand;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
