<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\Position;
use Illuminate\Support\Collection;

class PositionFactory implements InterfaceFactory
{

    public static function make($entity)
    {
        $position = new Position();
        $position->setId($entity->id);
        $position->setName($entity->name);
        $position->setSlug($entity->slug);
        $position->setStatus($entity->status);
        $position->setCreatedAt($entity->created_at);
        $position->setUpdatedAt($entity->updated_at);
        return $position;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $position = collect();

        foreach ( $entities as $entity){
            $position->push(self::make($entity));
        }

        return $position;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
