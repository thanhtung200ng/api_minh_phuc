<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\SalesBooth;

class SalesBoothFacetory implements InterfaceFactory
{
    public static function make($entity)
    {
        $sale_booth = new SalesBooth();
        $sale_booth->setId($entity->id);
        $sale_booth->setName($entity->name);
        $sale_booth->setSlug($entity->slug);
        $sale_booth->setAddress($entity->address);
        $sale_booth->setStatus($entity->status);
        $sale_booth->setEmail($entity->email);
        $sale_booth->setCustomerId($entity->customer_id);
        $sale_booth->setMobile($entity->mobile);
        $sale_booth->setCreatedAt($entity->created_at);
        return $sale_booth;
    }
    public static function makeId($entity)
    {
        $sale_booth = new SalesBooth();
        $sale_booth->setId($entity->id);
        return $sale_booth;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $sale_booth = collect();

        foreach ($entities as $entity) {
            $sale_booth->push(self::make($entity));
        }

        return $sale_booth;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
