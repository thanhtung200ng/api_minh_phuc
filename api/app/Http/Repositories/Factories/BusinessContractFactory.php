<?php


namespace App\Http\Repositories\Factories;


use App\Data\Entities\BaseEntity;
use App\Http\Repositories\Entities\BusinessContract;
use Illuminate\Support\Collection;

class BusinessContractFactory implements InterfaceFactory
{
    public static function make($entity)
    {
        $businessContract = new BusinessContract();
        $businessContract->setId($entity->id);
        $businessContract->setCustomerId($entity->customer_id);
        $businessContract->setName($entity->name);
        $businessContract->setContractId($entity->contract_id);
        $businessContract->setStatus($entity->status);
        $businessContract->setDocuments($entity->documents);
        $businessContract->setCreatedAt($entity->created_at);
        return $businessContract;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $brand = collect();
        foreach ( $entities as $entity){
            $brand->push(self::make($entity));
        }
        return $brand;
    }


    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
