<?php


namespace App\Http\Repositories\Factories;

use App\Http\Repositories\Entities\Agecy;
use App\Http\Repositories\Entities;
use Illuminate\Support\Collection;

class AgecyFactory implements InterfaceFactory
{

    /**
     * @inheritDoc
     */
    public static function make($entity)
    {
        $agecy = new Agecy();
        $agecy->setId($entity->id);
        $agecy->setAccount($entity->account);
        $agecy->setFullName($entity->full_name);
        $agecy->setStatus($entity->status);
        $agecy->setCreatedAt($entity->created_at);
        $agecy->setMobile($entity->mobile);
        $agecy->setWardId($entity->ward_id);
        $agecy->setDistrictId($entity->district_id);
        $agecy->setCityId($entity->city_id);
        $agecy->setStreet($entity->street);
        $agecy->setCustomerId($entity->customer_id);
        $agecy->setRankAgecy($entity->rank_agency);
        return $agecy;
    }

    /**
     * @inheritDoc
     */
    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    /**
     * @inheritDoc
     */
    public static function makeCollection($entities)
    {
        $brand = collect();
        foreach ( $entities as $entity){
            $brand->push(self::make($entity));
        }
        return $brand;
    }

    /**
     * @inheritDoc
     */
    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
