<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\PurchaseOdersItem;

class PurchaseOdersItemFactory implements InterfaceFactory
{
    public static function make($entity)
    {
        $PurchaseOders = new PurchaseOdersItem();
        $PurchaseOders->setId($entity->id);
        $PurchaseOders->setStatus($entity->status);
        $PurchaseOders->setCreatedAt($entity->created_at);
        $PurchaseOders->setUpdatedAt($entity->updated_at);
        return $PurchaseOders;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $PurchaseOders = collect();
        foreach ( $entities as $entity){
            $PurchaseOders->push(self::make($entity));
        }
        return $PurchaseOders;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
