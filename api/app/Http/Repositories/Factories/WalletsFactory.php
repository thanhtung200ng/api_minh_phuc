<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\Wallets;

class WalletsFactory implements InterfaceFactory
{
    public static function make($entity)
    {
        $wallet = new Wallets();
        $wallet->setId($entity->id);
        $wallet->setName($entity->name);
        $wallet->setStatus($entity->status);
        $wallet->setCreatedAt($entity->created_at);
        $wallet->setCreatedBy($entity->created_by);
        $wallet->setGroupTransactionCode($entity->group_transaction_code);
        $wallet->setSlug($entity->slug);
        $wallet->setIcon($entity->icon);
        return $wallet;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $position = collect();

        foreach ( $entities as $entity){
            $position->push(self::make($entity));
        }

        return $position;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
