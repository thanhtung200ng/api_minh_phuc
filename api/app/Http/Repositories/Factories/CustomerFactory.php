<?php


namespace App\Http\Repositories\Factories;

use App\Http\Repositories\Entities\BaseEntity;
use App\Http\Repositories\Entities\Customer;

class CustomerFactory implements InterfaceFactory
{

    public static function make($entity): Customer
    {
        $user = new Customer();
        $user->setId($entity->id);
        $user->setFirstName($entity->first_name);
        $user->setLastName($entity->last_name);
        $user->setBio($entity->bio);
        $user->setBirthday($entity->birthday);
        $user->setMobile($entity->mobile);
        $user->setEmail($entity->email);
        $user->setAccount($entity->account);
        $user->setPassword($entity->password);
        $user->setStatus($entity->status);
        $user->setCreatedAt($entity->created_at);
        $user->setUpdatedAt($entity->updated_at);
        $user->setAddress($entity->address);
        $user->setParentReferralCode($entity->parent_referral_code);
        $user->setReferralCode($entity->referral_code);
        $user->setRankMpMart($entity->mpmart_rank);
        $user->setRankMpMartTotalRevenue($entity->mpmart_rank_total_revenue);
        $user->setIsBuyer($entity->is_buyer);
        $user->setIsSeller($entity->is_seller);
        $user->setIsMpMart($entity->is_mpmart);
        $user->setMpmartAgency($entity->mpmart_agency);
        if (isset($entity->wallets)){
            $user->setWallets($entity->wallets);
        }
        return $user;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $customer = collect();

        foreach ( $entities as $entity){
            $customer->push(self::make($entity));
        }

        return $customer;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
