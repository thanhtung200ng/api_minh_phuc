<?php


namespace App\Http\Repositories\Factories;

use App\Http\Repositories\Entities\Member;

class MemberFactory implements InterfaceFactory
{

    public static function make($entity): Member
    {
        $user = new Member();

        $user->setId($entity->id);
        $user->setFirstName($entity->first_name);
        $user->setLastName($entity->last_name);
        $user->setMobile($entity->mobile);
        $user->setEmail($entity->email);
        $user->setAccount($entity->account);
        $user->setPassword($entity->password);
        $user->setStatus($entity->status);
        $user->setCreatedAt($entity->created_at);
        $user->setUpdatedAt($entity->updated_at);

        return $user;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $members = collect();

        foreach ( $entities as $entity){
            $members->push(self::make($entity));
        }

        return $members;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
