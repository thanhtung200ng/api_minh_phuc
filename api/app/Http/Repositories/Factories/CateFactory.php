<?php


namespace App\Http\Repositories\Factories;

use App\Http\Repositories\Entities\Category;

class CateFactory implements InterfaceFactory
{

    public static function make($entity): Category
    {
        $cate = new Category();
        $cate->setId($entity->id);
        $cate->setName($entity->name);
        $cate->setParentId($entity->parent_id);
        $cate->setSlug($entity->slug);
        $cate->setStatus($entity->status);
        $cate->setCreatedAt($entity->created_at);
        $cate->setUpdatedAt($entity->updated_at);
        $cate->setIsLeaf($entity->is_leaf);
        return $cate;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $cate = collect();
        foreach ( $entities as $entity){
            $cate->push(self::make($entity));
        }

        return $cate;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
