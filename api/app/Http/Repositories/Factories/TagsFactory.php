<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\Tags;

class TagsFactory implements InterfaceFactory
{
    public static function make($entity)
    {
        $tag = new Tags();
        $tag->setId($entity->id);
        $tag->setName($entity->name);
        $tag->setSlug($entity->slug);
        $tag->setStatus($entity->status);
        $tag->setCreatedAt($entity->created_at);
        $tag->setCreatedBy($entity->created_by);
        return $tag;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $position = collect();

        foreach ( $entities as $entity){
            $position->push(self::make($entity));
        }

        return $position;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
