<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\withdraw_money;
use Illuminate\Support\Collection;

class withdraw_moneyFactory implements InterfaceFactory
{

    public static function make($entity)
    {
        $brand = new withdraw_money();
        $brand->setId($entity->id);
        $brand->setName($entity->name);
        $brand->setSlug($entity->slug);
        $brand->setStatus($entity->status);
        $brand->setCreatedAt($entity->created_at);
        $brand->setUpdatedAt($entity->updated_at);
        return $brand;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $brand = collect();

        foreach ( $entities as $entity){
            $brand->push(self::make($entity));
        }

        return $brand;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
