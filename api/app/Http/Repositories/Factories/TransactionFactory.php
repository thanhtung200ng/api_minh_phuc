<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\Transaction;

class TransactionFactory implements InterfaceFactory
{
    public static function make($entity): Transaction
    {
        $obj = new Transaction();
        $obj->setId($entity->id);
        $obj->setFirstMoney($entity->first_money);
        $obj->setLastMoney($entity->last_money);
        $obj->setFromCustomerId($entity->from_customer_id);
        $obj->setFromCustomerFullName($entity->from_customer_full_name);
        $obj->setFromCustomerAccount($entity->from_customer_account);
        $obj->setToCustomerId($entity->to_customer_id);
        $obj->setToCustomerFullName($entity->to_customer_full_name);
        $obj->setToCustomerAccount($entity->to_customer_account);
        $obj->setFromCustomerWalletId($entity->from_customer_wallet_id);
        $obj->setToCustomerWalletId($entity->to_customer_wallet_id);
        $obj->setFromCustomerWalletName($entity->from_customer_wallet_name);
        $obj->setToCustomerWalletName($entity->to_customer_wallet_name);
        $obj->setFromCustomerStatus($entity->from_customer_status);
        $obj->setToCustomerStatus($entity->to_customer_status);
        $obj->setFromCustomerWalletStatus($entity->from_customer_wallet_status);
        $obj->setToCustomerWalletStatus($entity->to_customer_wallet_status);
        $obj->setBeforeFromCustomerWalletCurrentBalance($entity->before_balance_from_customer_wallet);
        $obj->setAfterFromCustomerWalletCurrentBalance($entity->after_balance_from_customer_wallet);
        $obj->setBeforeToCustomerWalletCurrentBalance($entity->before_balance_to_customer_wallet);
        $obj->setAfterToCustomerWalletCurrentBalance($entity->after_balance_to_customer_wallet);
        $obj->setDiscount($entity->discount);
        $obj->setFee($entity->fee);
        $obj->setRanks($entity->ranks);
        $obj->setApprovedAt($entity->approved_at);
        $obj->setApprovedBy($entity->approved_by);
        $obj->setApprovedByName($entity->approved_name);
        $obj->setStatus($entity->status);
        $obj->setCreatedAt($entity->created_at);
        $obj->setCreatedBy($entity->created_by);
        $obj->setTransactionCode($entity->transaction_code);
        return $obj;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $position = collect();

        foreach ( $entities as $entity){
            $position->push(self::make($entity));
        }
        return $position;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
