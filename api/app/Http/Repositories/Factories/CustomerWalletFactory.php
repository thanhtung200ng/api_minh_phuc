<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\CustomerWallet;

class CustomerWalletFactory implements InterfaceFactory
{
    public static function make($entity)
    {
        $wallet = new CustomerWallet();
        $wallet->setId($entity->id);
        $wallet->setCurrentBalance($entity->current_balance);
        $wallet->setName($entity->name);
        $wallet->setSlug($entity->slug);
        $wallet->setWalletId($entity->wallet_id);
        $wallet->setCustomerId($entity->customer_id);
        $wallet->setStatus($entity->status);
        $wallet->setCreatedAt($entity->created_at);
        $wallet->setCreatedBy($entity->created_by);
        $wallet->setGroupTransactionCode($entity->group_transaction_code);
        return $wallet;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $position = collect();

        foreach ( $entities as $entity){
            $position->push(self::make($entity));
        }

        return $position;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
