<?php


namespace App\Http\Repositories\Factories;


use App\Http\Repositories\Entities\Cities;
use App\Http\Repositories\Entities\Districts;
use App\Http\Repositories\Entities\Wards;
use Illuminate\Support\Collection;

class LocationFactory implements InterfaceFactory
{

    public static function make($entity)
    {
        if(isset($entity->district_id)) {
            $location = new Wards();
        }elseif (isset($entity->city_id)) {
            $location = new Districts();
        }else {
            $location = new Cities();
        }
        $location->setId($entity->id);
        $location->setName($entity->name);
        $location->setCode($entity->code);
        return $location;
    }

    public static function makeFromArray($entity)
    {
        // TODO: Implement makeFromArray() method.
    }

    public static function makeCollection($entities)
    {
        $locations = collect();

        foreach ( $entities as $entity){
            $locations->push(self::make($entity));
        }

        return $locations;
    }

    public static function makeCollectionFromArray($entities)
    {
        // TODO: Implement makeCollectionFromArray() method.
    }
}
