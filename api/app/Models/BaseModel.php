<?php


namespace App\Models;


use App\Elibs\Date;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    protected static $instances = [];
    public static function &getInstance()
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
        }
        return self::$instances[$cls];
    }

    public static function getStatusActive(): int
    {
        return 1;
    }

    /**
     * @return int
     */

    public static function getStatusInActive(): int
    {
        return 0;
    }

    /**
     * @return int
     */

    public static function getStatusDeleted(): int
    {
        return -4;
    }

    /**
     * @return int
     */

    public static function getStatusDisabled(): int
    {
        return -1;
    }

    /**
     * @return int
     */

    public static function getStatusInvalid(): int
    {
        return -13;
    }

    public function getCreatedAtAttribute()
    {
        return Date::dateFormat($this->attributes['created_at'], 'd/m/Y H:i:s');
    }
    public function getActive(): int
    {
        return 1;
    }

    public function getUpdatedAtAttribute(){
        return Date::dateFormat($this->attributes['updated_at'], 'd/m/Y H:i:s');
    }

}
