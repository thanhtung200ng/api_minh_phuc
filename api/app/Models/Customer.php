<?php

namespace App\Models;

use App\Elibs\Date;
use App\Http\Repositories\ServicesEloquent\CustomerWalletServiceEloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Customer extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = 'ui_customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'email', 'last_name', 'account', 'mobile'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];


    public static function getStatusActive(): int
    {
        return 1;
    }

    public function getBirthdayAttribute()
    {
        return Date::dateFormat($this->attributes['birthday'], 'd/m/Y');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public $new_mpmart_rank_total_revenue = false;

    function isChangeRankMpMartTotalRevenue () :bool {
        return $this->new_mpmart_rank_total_revenue === $this->attributes['mpmart_rank_total_revenue'];
    }

    public function moneyRankMpMartTotalRevenue(): \Illuminate\Database\Eloquent\Relations\hasOne
    {
        return $this->hasOne(MoneyRankMpMartTotalRevenueModel::class, 'id', 'mpmart_rank_total_revenue');
    }

    public function wallets(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CustomerWalletModel::class, 'customer_id', 'id');
    }

    public function wallet($id): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CustomerWalletModel::class, 'customer_id', 'id')->where('id', $id);
    }

    public function walletCode($code): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CustomerWalletModel::class, 'customer_id', 'id')->where('group_transaction_code', 'LIKE', '%'.$code.'%');
    }

    public function social(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CustomerSocialModel::class, 'customer_id', 'id');
    }

    public function agent(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(AgentModel::class, 'customer_id', 'id');
    }
    public function sales_booth(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(SalesBoothModel::class, 'customer_id', 'id');
    }

    public function inventoryExport(): \Illuminate\Database\Eloquent\Relations\hasMany
    {
        return $this->hasMany(InventoryExportModel::class, 'customer_id', 'id');
    }

    public function inventoryImport(): \Illuminate\Database\Eloquent\Relations\hasMany
    {
        return $this->hasMany(InventoryImportModel::class, 'customer_id', 'id');
    }

    public function banks(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->HasMany(BankCustomerModel::class, 'customer_id', 'id');
    }

    public function hasSocialLinked($service): bool
    {
        return (bool) $this->social->where('service', $service)->count();
    }

    // lấy ví theo code

    function getViTieuDung()
    {
        return $this->wallets->where('wallet_id', app(WalletModel::class)->getViTieuDung())->first();
    }

    function getViHopTac()
    {
        return $this->wallets->where('wallet_id', app(WalletModel::class)->getViHopTac())->first();
    }

    function getViHoaHong()
    {
        return $this->wallets->where('wallet_id', app(WalletModel::class)->getViHoaHong())->first();
    }

    function getViSi()
    {
        return $this->wallets->where('wallet_id', app(WalletModel::class)->getViSi())->first();
    }

    function getViLoiNhuan()
    {
        return $this->wallets->where('wallet_id', app(WalletModel::class)->getViLoiNhuan())->first();
    }

    function getViKyQuy()
    {
        return $this->wallets->where('wallet_id', app(WalletModel::class)->getViKyQuy())->first();
    }

    function getViTongDoanhThu()
    {
        return $this->wallets->where('wallet_id', app(WalletModel::class)->getViTongDoanhThu())->first();
    }

    function getViTangDiem()
    {
        $wallet  = $this->wallets->where('wallet_id', app(WalletModel::class)->getViTangDiem())->first();
        if (!$wallet) {
            CustomerWalletServiceEloquent::initNewModel($wallet, $this->getId());
        }
        return $wallet;
    }

    function getViKhoDiem()
    {
        return $this->wallets->where('wallet_id', app(WalletModel::class)->getViKhoDiem())->first();
    }

    function getViDauTu()
    {
        return $this->wallets->where('wallet_id', app(WalletModel::class)->getViDauTu())->first();
    }

    public function city(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(City::class,'id','city_id');
    }
    public function district(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(District::class,'id','district_id');
    }
    public function ward() : \Illuminate\Database\Eloquent\Relations\HasOne {
        return $this->hasOne(Ward::class,'id','ward_id');
    }

    public function warehouse(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(WarehouseModel::class, 'customer_id', 'id');
    }

    function getId () {
        return $this->id;
    }

    function isActive (): bool {
        return $this->status === 1;
    }

    function isInActive (): bool {
        return $this->status === 0;
    }

    function isDisabled (): bool {
        return $this->status === -1;
    }

    function isMpMart (): bool {
        return $this->is_mpmart == 1;
    }
    function isSeller (): bool {
        return $this->is_seller == 1;
    }
    function isInvestor (): bool {
        return $this->is_investor == 1;
    }

    function getIsMpMart (): int {
        return 1;
    }

    function getIsSeller (): int {
        return 1;
    }

    function getInvestor (): int {
        return 1;
    }

    function isCty (): bool
    {
        return $this->is_cty === 1;
    }

    function getAccount () {
        return $this->account;
    }

    function setMpmartRankAttribute($rank)
    {
        $this->attributes['mpmart_rank'] = json_encode($rank);
    }

    function setBirthdayAttribute($birthday)
    {
        $this->attributes['birthday'] = Date::getTimestampFromVNDate($birthday);
    }

    function getMpmartRankAttribute(): array
    {
        return (array)json_decode($this->attributes['mpmart_rank']);
    }

    function getIsMpmartAttribute(): bool
    {
        return $this->attributes['is_mpmart'];
    }

    function getIsSellerAttribute(): bool
    {
        return $this->attributes['is_seller'];
    }

    function getIsBuyerAttribute(): bool
    {
        return $this->attributes['is_buyer'];
    }
    function getParentReferralCode()
    {
        return $this->parent_referral_code;
    }

    function getAddressAttribute(): array {
        return (array)json_decode($this->attributes['address']);
    }
    function getCityAttribute(): array {
        return (array)json_decode($this->attributes['city']);
    }
    function getDistrictAttribute(): array {
        return (array)json_decode($this->attributes['district']);
    }
    function getWardAttribute(): array {
        return (array)json_decode($this->attributes['ward']);
    }

    static function get_cty() {
        return self::where('is_cty', 1)->first();
    }

    static function genToken($customer): string
    {
        $prefix = 'MINHPHUCMART';
        $suffix = md5('$zouk*Toscanello');
        return md5($prefix . $customer['account'] . $customer['mobile'] . $suffix);
    }

    static function genRef($customer): string
    {
        $prefix = md5('Chỉ còn lại món lạc rang');
        $suffix = md5('captain@black');
        $ref = md5($prefix . $customer['account'] . 'MINHPHUCMART' .$customer['mobile'] . $suffix);
        return substr($ref, 0, 15);
    }

    static function genSellerRef($customer): string
    {
        $prefix = md5('Hôm nay khá hơn');
        $suffix = md5('captain@black');
        $ref = md5($prefix . $customer['account'] . 'SELLER_MINHPHUCMART' .$customer['mobile'] . $suffix);
        return substr($ref, 0, 15);
    }

    public function can($abilities, $arguments = [])
    {
        // TODO: Implement can() method.
    }
}
