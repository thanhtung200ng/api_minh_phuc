<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Member extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $table = 'ui_members';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'email', 'last_name', 'account', 'mobile'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public static function getStatusActive(): int
    {
        return 1;
    }
    public static function getStatusInActive(): int
    {
        return 0;
    }

    /**
     * @return int
     */

    public static function getStatusDeleted(): int
    {
        return -4;
    }

    static function getStatus($selected = false, $return = false,  $groupAction = true): array
    {
        $listStatus = [
            self::getStatusActive() => [
                'id' => self::getStatusActive(),
                'style' => 'success',
                'icon' => 'mdi mdi-check-all',
                'name' => 'Đã duyệt',
            ],
            self::getStatusInActive() => [
                'id' => self::getStatusInActive(),
                'style' => 'warning',
                'icon' => 'mdi mdi-circle-double',
                'name' => 'Chưa kích hoạt',
                'actions' => ($groupAction === true) ? [
                    self::getStatusActive() => self::getStatus(self::getStatusActive(), true, false),
                ] : []
            ],
            self::getStatusDeleted() => [
                'id' => self::getStatusDeleted(),
                'style' => 'danger',
                'icon' => 'mdi mdi-trash-can-outline',
                'name' => 'Đã xóa',
                'text-action' => 'Đã xóa',
            ],
        ];
        if ($selected !== false && isset($listStatus[$selected])) {
            $listStatus[$selected]['checked'] = 'checked';
            if($return) {
                return $listStatus[$selected];
            }
        }

        if($return) {
            return [
                'id' => -13, 'style' => 'danger',
                'icon' => 'mdi mdi-trash-can-outline',
                'name' => '---',
                'actions' => []
            ];
        }

        return $listStatus;
    }

}
