<?php


namespace App\Elibs;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class ApiHelper
{
    static $requestId;
    static $dbConnection = 'mongodb';
    static $dbTable = '_api_logs';
    static $client;
    static $noLog = false;
    /**
     * @api {get} {url} {content}
     * @apiName saveApiRequest
     * @apiDescription Mô tả.
     * @apiGroup  App\Elibs
     *
     * @apiParam {Type} {name} {des}.
     *
     */
    static function saveApiRequest($connection = 'mongodb')
    {
        return false;
        try {
            if(self::$noLog){
                return false;
            }
            $fullUrl = request()->fullUrl();
            $method = request()->method();
            $all = request()->all();
            $query = request()->query();
            $created_at = Helper::getMongoDateTime();
            $headers = request()->headers->all();
            $hostname = request()->getHost();
            $path = request()->path();
            $objToSave = [
                'path' => $path,
                'headers' => $headers,
                'hostName' => $hostname,
                "fullUrl" => $fullUrl,
                "method" => $method,
                "request" => $all,
                "query" => $query,
                "created_at" => $created_at,
            ];
            self::$dbConnection = $connection;
            self::$requestId = DB::connection($connection)->table('_api_nuxt_logs')->insertGetId($objToSave);
        }catch (\Exception $e) {
            Debug::pushNotification('Lỗi tạo log: '.$e->getMessage());
        }
    }

    static function saveApiResponse($response, $code, $table = '_api_nuxt_logs')
    {
        return false;
        try {
            if (!self::$requestId) {
                return false;
            }
            DB::connection(self::$dbConnection)->table($table)->where('_id', self::$requestId)->update([
                'response' => $response,
                'code' => $code,
                'updated_at' => Helper::getMongoDateTime(),
            ]);
        }catch (\Exception $e) {
            Debug::pushNotification('Lỗi cập nhật log: '.$e->getMessage());
        }

    }



}
