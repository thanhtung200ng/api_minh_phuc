<?php


namespace App\Elibs;


use App\Models\TokenSocialModel;
use Illuminate\Support\Str;

class Zalo
{
    public function __construct()
    {
        abort(403, 'Tạm khoá chức năng gửi thông báo zalo địa chỉ này.');
    }

    static function graphApi (string $url, $type = 'GET', $data = [], $header = ['Content-Type: application/json'])
    {
        $ch = curl_init();
        if ($type === 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        // Set the URL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        // Return the output instead of displaying it directly
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // Execute the curl session
        $result = curl_exec($ch);

        if (curl_error($ch)) {
            return false;
        }



        // Close the curl session
        curl_close($ch);
        // Return the output as a variable
        return json_decode($result);
    }

    static function generatingAppAccessToken(): string
    {
        $state = self::generate_state_param(); // for CSRF prevention
        // Generate the code verifier and code challenge
        $codes = self::generate_pkce_codes();
        // Store the request state to be checked in auth.php
        $_SESSION["zalo_auth_state"] = $state;
        // Store the code verifier to be used in auth.php
        $_SESSION["zalo_code_verifier"] = $codes["verifier"];

        return env('ZALO_PERMISSION_URL')
            . "?" . http_build_query( array(
                "app_id" => env('ZALO_CLIENT_ID'),
                "redirect_uri" => env('APP_AUTH_URL'),
                "code_challenge" => $codes["challenge"],
                "state" => $state, // <- prevent CSRF
            ) );
    }

    static function urlGenAccessTokenByRefreshToken($token): string
    {
        return 'https://oauth.zaloapp.com/v4/oa/access_token?refresh_token='.$token.'&app_id='.env('ZALO_CLIENT_ID').'&grant_type=refresh_token';
    }

    static function urlUserInfo($id, $token): string
    {
        return 'https://openapi.zalo.me/v2.0/oa/getprofile?data={"user_id":"'.$id.'"}&access_token='.$token;
    }

    static function urlMessText(): string
    {
        return 'https://openapi.zalo.me/v2.0/oa/message';
    }

    static function base64url_encode($text) {
        $base64 = base64_encode($text);
        $base64 = trim($base64, "=");
        return strtr($base64, "+/", "-_");
    }


    static function generate_state_param() {
        // a random 8 digit hex, for instance
        return bin2hex(openssl_random_pseudo_bytes(4));
    }

    static function generate_pkce_codes() {
        $random = bin2hex(openssl_random_pseudo_bytes(32)); // a random 64-digit hex
        $code_verifier = base64url_encode(pack('H*', $random));
        $code_challenge = base64url_encode(pack('H*', hash('sha256', $code_verifier)));
        return array(
            "verifier" => $code_verifier,
            "challenge" => $code_challenge
        );
    }

    static function sendMessage($messageZalo, $socialId) {
        $service = 'zalo';
        $reToken = TokenSocialModel::where('service', $service)->where('status', '!=', 1)->first();
        if ($reToken) {
            $reToken->status = 1;
            $result = Zalo::graphApi(Zalo::urlMessText(), 'POST', [
                'recipient' => ['user_id' => $socialId],
                'message' => [
                    'text' => $messageZalo
                ],
            ], array(
                'Content-Type: application/json',
                'access_token: '. $reToken['access_token']
            ));
            if ($result->error != 0) {
                $access_token = Zalo::graphApi(Zalo::urlGenAccessTokenByRefreshToken($reToken['refresh_token']), 'POST', [], array(
                    'Content-Type: application/json',
                    'secret_key: '. env('ZALO_CLIENT_SECRET')
                ));
                if (@$access_token->access_token) {
                    $reToken->save();
                    TokenSocialModel::insert(['service' => $service, 'refresh_token' => $access_token->refresh_token, 'access_token' => $access_token->access_token]);
                    Zalo::graphApi(Zalo::urlMessText(), 'POST', [
                        'recipient' => ['user_id' => $socialId],
                        'message' => [
                            'text' => $messageZalo
                        ],
                    ], array(
                        'Content-Type: application/json',
                        'access_token: '. $reToken['access_token']
                    ));
                }
            }
        }
    }

    static function messagePo($social, $po) {
        $messageZalo = 'Chúc mừng bạn thanh toán thành công đơn hàng số #' . $po['id'] . ' tại minhphucmart.vn.
Xin cám ơn quý khách đã tin tưởng và mua hàng tại Quy Hương Mart (https://minhphucmart.vn/).
Giờ đây, Quý khách hàng cũng có thể theo dõi đơn hàng bằng cách đăng nhập và theo dõi đơn hàng trên website của chúng tôi.
Thông tin chi tiết đơn hàng xem tại đây: https://minhphucmart.vn/account/tracking/order';
        Zalo::sendMessage($messageZalo, $social->social_id);
    }

    static function messagePoSeller($social, $po) {
        $messageZalo = 'Chúc mừng bạn có đơn hàng mới số #' . $po['id'] . ' tại minhphucmart.vn.
Vui lòng kiểm tra đơn hàng bằng cách đăng nhập và theo dõi đơn hàng trên website của chúng tôi.';
        Zalo::sendMessage($messageZalo, $social->social_id);
    }
}
