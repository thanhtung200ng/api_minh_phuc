<?php


namespace App\Elibs;


use function Symfony\Component\String\s;

class TeleNotice
{

    const agent_seller = 'agent_seller';
    const po = 'po';
    const customers = 'customers';
    const deposit = 'deposit';
    private static $chatId= 'mall_agent_seller_notice';
    private static $bot = [
      self::po => ['token' => '5131952374:AAEJOEIPx6AL091UhORQr6ZytkkhJ1lW7HI', 'chat_id' => 'mall_po'],
        self::agent_seller => ['token' => '5255303049:AAFuh7YnQHmAdiIGh-p238JpQ2PzaNeWBsI', 'chat_id' => 'mall_agent_seller_notice'],
        self::customers => ['token' => '5128104426:AAGMFVbA9QtMCy8g-b0_4oM6nShw-ClGk6U', 'chat_id' => 'mall_customers'],
        self::deposit => ['token' => '5219729852:AAF2BHzZXHetkji03a_pq2T0crM2TNInZmc', 'chat_id' => 'mall_deposit_notice'],
    ];
    static function pushNotification($bot, $msg=''): bool
    {
        if (env('APP_ENV') == 'local') {
            return true;
        }
        return true;
        $ch = curl_init();
        // Set the URL
        curl_setopt($ch, CURLOPT_URL, 'https://api.telegram.org/bot'.self::$bot[$bot]['token'].'/sendMessage?chat_id=@'.self::$bot[$bot]['chat_id'].'&text=' . urlencode($msg).'&parse_mode=html');
        // Removes the headers from the output
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // Return the output instead of displaying it directly
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // Execute the curl session
        curl_exec($ch);
        // Close the curl session
        curl_close($ch);
        // Return the output as a variable
        return true;
    }

    static function seller($mess = ''): string
    {
        $user = auth('api_customer')->user();
        $msg = $mess."\n-------------" ;
        $msg .= "\nEmail: ". $user->email;
        $msg .= "\nSĐT: ". $user->mobile;
        $msg .= "\nHọ tên: ". $user->last_name;
        $msg .= "\nAccount: ". $user->account;
        $msg .= "\n-------------\nURL: " . "<a href='".@$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI']."'>".@$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI']. "</a>";
        $msg .= "\nREMOTE_ADDR: " . @$_SERVER['REMOTE_ADDR'];
        $msg .= "\nHTTP_USER_AGENT: " . @$_SERVER['HTTP_USER_AGENT'];
        $msg .= "\nHTTP_REFERER: " . @$_SERVER['HTTP_REFERER'];
        $msg .= "\nREQUEST_METHOD: " . @$_SERVER['REQUEST_METHOD'];
        $msg .= "\nSERVER_NAME: " . @$_SERVER['SERVER_NAME'];
        $msg .= "\nHTTP_HOST: " . @$_SERVER['HTTP_HOST'];
        return self::pushNotification(self::agent_seller, $msg);
    }

    static function agent($msg = ''): string
    {
        return self::seller($msg);
    }

    static function po($mess = ''): string
    {
        $user = auth('api_customer')->user();
        $msg = $mess."\n-------------" ;
        $msg .= "\nEmail: ". $user->email;
        $msg .= "\nSĐT: ". $user->mobile;
        $msg .= "\nHọ tên: ". $user->last_name;
        $msg .= "\nAccount: ". $user->account;
        $msg .= "\n-------------\nURL: " . "<a href='".@$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI']."'>".@$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI']. "</a>";
        $msg .= "\nREMOTE_ADDR: " . @$_SERVER['REMOTE_ADDR'];
        $msg .= "\nHTTP_USER_AGENT: " . @$_SERVER['HTTP_USER_AGENT'];
        $msg .= "\nHTTP_REFERER: " . @$_SERVER['HTTP_REFERER'];
        $msg .= "\nREQUEST_METHOD: " . @$_SERVER['REQUEST_METHOD'];
        $msg .= "\nSERVER_NAME: " . @$_SERVER['SERVER_NAME'];
        $msg .= "\nHTTP_HOST: " . @$_SERVER['HTTP_HOST'];
        return self::pushNotification(self::po, $msg);
    }

    static function customer($mess = '', $user): string
    {
        $msg = $mess."\n-------------" ;
        $msg .= "\nEmail: ". $user['email'];
        $msg .= "\nSĐT: ". $user['mobile'];
        $msg .= "\nHọ tên: ". $user['last_name'];
        $msg .= "\nAccount: ". $user['account'];
        $msg .= "\nCấp trên: ". @$user['parent_referral_code'];
        $msg .= "\n-------------\nURL: " . "<a href='".@$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI']."'>".@$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI']. "</a>";
        $msg .= "\nREMOTE_ADDR: " . @$_SERVER['REMOTE_ADDR'];
        $msg .= "\nHTTP_USER_AGENT: " . @$_SERVER['HTTP_USER_AGENT'];
        $msg .= "\nHTTP_REFERER: " . @$_SERVER['HTTP_REFERER'];
        $msg .= "\nREQUEST_METHOD: " . @$_SERVER['REQUEST_METHOD'];
        $msg .= "\nSERVER_NAME: " . @$_SERVER['SERVER_NAME'];
        $msg .= "\nHTTP_HOST: " . @$_SERVER['HTTP_HOST'];
        return self::pushNotification(self::customers, $msg);
    }

    static function deposit($mess = '', $user): string
    {
        $msg = $mess."\n-------------" ;
        $msg .= "\nEmail: ". $user['email'];
        $msg .= "\nSĐT: ". $user['mobile'];
        $msg .= "\nHọ tên: ". $user['last_name'];
        $msg .= "\nAccount: ". $user['account'];
        $msg .= "\nMã đơn: #". $user['deposit_id'];
        $msg .= "\nSố điểm nạp: ". @$user['money'];
        $msg .= "\n-------------\nURL: " . "<a href='".@$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI']."'>".@$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI']. "</a>";
        $msg .= "\nREMOTE_ADDR: " . @$_SERVER['REMOTE_ADDR'];
        $msg .= "\nHTTP_USER_AGENT: " . @$_SERVER['HTTP_USER_AGENT'];
        $msg .= "\nHTTP_REFERER: " . @$_SERVER['HTTP_REFERER'];
        $msg .= "\nREQUEST_METHOD: " . @$_SERVER['REQUEST_METHOD'];
        $msg .= "\nSERVER_NAME: " . @$_SERVER['SERVER_NAME'];
        $msg .= "\nHTTP_HOST: " . @$_SERVER['HTTP_HOST'];
        return self::pushNotification(self::deposit, $msg);
    }

}
