<?php


namespace App\Elibs;


use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterAlreadyOpenedException;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Excel
{
    private static $instance = false;

    function __construct()
    {
        self::$instance = &$this;

    }

    static function &getInstance()
    {

        if (!self::$instance) {
            $excel = self::$instance = new self();
            return $excel;
        }

        return self::$instance;
    }

    /**
     * @throws WriterNotOpenedException
     * @throws WriterAlreadyOpenedException
     * @throws IOException
     */
    function export($fileName, $cells, $rows) {
        $subFolder = date('Y/md/');
        $dir = Excel::getInstance()->getExportPath($subFolder);
        if (! \File::exists($dir)) {
            \File::makeDirectory($dir, 755, true);
        }
        $writer = WriterEntityFactory::createXLSXWriter();
        // $writer = WriterEntityFactory::createODSWriter();
        // $writer = WriterEntityFactory::createCSVWriter();
        $writer->setShouldUseInlineStrings(true); // default (and recommended) value

        $zebraBlackStyle = (new StyleBuilder())
            ->setBackgroundColor(Color::LIGHT_BLUE)
            ->setFontColor(Color::WHITE)
            ->setFontBold()
            ->setFontSize(13)
            ->build();
        $path = $dir.$fileName;

        $writer->openToFile($path); // write data to a file or to a PHP stream
//        $writer->openToBrowser($fileName); // stream data directly to the browser

        foreach ($cells as $key => $cell) {
            $cells[$key] = WriterEntityFactory::createCell($cell);
        }
        /** add a row at a time */
        $singleRow = WriterEntityFactory::createRow($cells, $zebraBlackStyle);
        $writer->addRow($singleRow);

        foreach ($rows as $key => $row) {
            $rowFromValues = WriterEntityFactory::createRowFromArray($row);
            $writer->addRow($rowFromValues);
        }
        $writer->close();
        return route('getFile', ['token' => Crypt::encryptString($this->getExportFolder().$subFolder.$fileName)]);
    }


    function getExportPath($folder = ''): string
    {
        return public_path($this->getExportFolder()) . $folder;
    }

    function getExportFolder(): string
    {
        return '/export/';
    }

    function makeDir($path): bool
    {
        return is_dir($path) || mkdir($path);
    }
}
