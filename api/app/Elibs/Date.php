<?php


namespace App\Elibs;


class Date
{
    private static $instance = FALSE;

    function __construct()
    {
        self::$instance = &$this;

    }

    static function &getInstance()
    {

        if (!self::$instance) {
            new self();
        }

        return self::$instance;
    }

    public static function dateFormat($time = 0, $format = 'd/m - H:i', $vietnam = false, $show_time = false){
        if(!is_int($time)){
            $time = date_create($time)->getTimestamp();
        }
        $return = date($format,$time);
        if ($vietnam){
            $return = ($show_time ? date('H:i - ',$time) : '') . self::$days[date('D',$time)] . ', ngày ' . date('d/m/Y',$time);
        }
        return $return;
    }

    public static function getTimestampFromVNDate($str_date = '', $end = false){
        $time_str = str_replace('/', '-', $str_date);
        if($end){
            $time_str .= " 23:59:59";
        }
        return strtotime($time_str);
    }

    static function get_time_ago( $time )
    {
        $time_difference = time() - $time;

        if( $time_difference < 1 ) { return 'less than 1 second ago'; }
        $condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
            30 * 24 * 60 * 60       =>  'month',
            24 * 60 * 60            =>  'day',
            60 * 60                 =>  'hour',
            60                      =>  'minute',
            1                       =>  'second'
        );

        foreach( $condition as $secs => $str )
        {
            $d = $time_difference / $secs;

            if( $d >= 1 )
            {
                $t = round( $d );
                return 'about ' . $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
            }
        }
    }
}
