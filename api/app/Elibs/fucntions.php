<?php

use Illuminate\Http\Request;

if (! function_exists('checkRequestInputFieldEmptyOrNot')) {
    function checkRequestInputFieldEmptyOrNot(Request $request, $field): bool
    {
        if ($request->has($field) && !empty($request->input($field))) {
            return true;
        } else {
            return false;
        }
    }
}

if (! function_exists('convertRequestStringToArray')) {
    function convertRequestStringToArray(Request $request, $field): array
    {
        $q = $request->get($field);
        if (is_array($q)) {
            $results = $q;
            foreach ($results as $k => $s) {
                $results[$k] = $s;
            }
        }else {
            $results[] = $q;
        }

        return $results;
    }
}

if (! function_exists('base64url_encode')) {
    function base64url_encode($data)
    {
        // First of all you should encode $data to Base64 string
        $b64 = base64_encode($data);

        // Make sure you get a valid result, otherwise, return FALSE, as the base64_encode() function do
        if ($b64 === false) {
            return false;
        }

        // Convert Base64 to Base64URL by replacing “+” with “-” and “/” with “_”
        $url = strtr($b64, '+/', '-_');

        // Remove padding character from the end of line and return the Base64URL result
        return rtrim($url, '=');
    }
}

if (! function_exists('base64url_decode')) {

    /**
     * Decode data from Base64URL
     * @param string $data
     * @param boolean $strict
     * @return boolean|string
     */
    function base64url_decode($data, $strict = false)
    {
        // Convert Base64URL to Base64 by replacing “-” with “+” and “_” with “/”
        $b64 = strtr($data, '-_', '+/');

        // Decode Base64 string and return the original data
        return base64_decode($b64, $strict);
    }
}

if (! function_exists('calcDiscount')) {

    function calcDiscount($finalPrice, $regularPrice)
    {
        $finalPrice = (int)$finalPrice;
        $regularPrice = (int)$regularPrice;
        if(!$finalPrice || !$regularPrice){
            return 0;
        }

        return round(100 - ($finalPrice/$regularPrice*100));
    }
}
