<?php


namespace App\Elibs;


use Carbon\Carbon;
use Illuminate\Http\Request;

class Helper
{
    /***
     * @param $number
     *
     * @return int
     * @note: validate số điện thoại di động
     */
    static function isMobileNumber($number): int
    {
        return preg_match("/^(\+84|0[3|5|7|8|9])+([0-9]{8})$/i", trim($number));
    }

    static function calcMoneyRankDiscount ($money, $discount) {
        return $money * $discount/100;
    }


    /**
     * Function that groups an array of associative arrays by some key.
     *
     * @param {String} $key Property to sort by.
     * @param {Array} $data Array that stores multiple associative arrays.
     */
    static function groupBy($key, $data): array
    {
        $result = array();

        foreach ($data as $val) {
            if (array_key_exists($key, $val)) {
                $result[$val[$key]][] = $val;
            } else {
                $result[""][] = $val;
            }
        }

        return $result;
    }

    static function keyBy($key, $data): array
    {
        foreach ($data as $val) {
            if (array_key_exists($key, $val)) {
                $result[$val[$key]] = $val;
            } else {
                $result[""] = $val;
            }
        }

        return $result;
    }



    public static function formatMoney($stringNumber, $sep = '.', $₫ = ' ₫') {
        if(!$stringNumber) {
            return 0;
        }
        $stringNumber = (double)$stringNumber;
        if(!$stringNumber){
            return $stringNumber;
        }
        return number_format($stringNumber, 0, ',', $sep).$₫;
    }

    static function getMongoDateTime($date_time_string = FALSE, $format = 'd/m/Y H:i')
    {
        if (!$date_time_string) {
            return new \MongoDB\BSON\UTCDateTime(strtotime('now') * 1000);
        }
        $date = strtotime(Carbon::createFromFormat($format, $date_time_string)->toDateTimeString());
        return new \MongoDB\BSON\UTCDateTime($date * 1000);
    }

}
