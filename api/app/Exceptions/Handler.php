<?php

namespace App\Exceptions;

use App\Elibs\Debug;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];


    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    static $__countEx = false;
    public function report(Throwable $exception)
    {
        $statusCode = 0;
        if($exception instanceof AuthenticationException) {
            return response()->json(['message' => 'Tài khoản của bạn hết phiên đăng nhập. Vui lòng đăng nhập để tiếp tục'], 401);
        }

        if (!self::$__countEx ) {
            self::$__countEx = true;
            $msg = "User:======" ;
            $msg .= "\nAccount Email: ";
            $msg .= "\nAccount Name: ";
            $msg .= "\nAccount: ";
            $msg .= "\nUser:======" ;
            $msg .= "\n\n<b>Message</b>: " . "<i>".$exception->getMessage(). "</i>";
            $msg .= "\nStatusCode: " . $statusCode;
            $msg .= "\nFile: " . $exception->getFile() . ':' . $exception->getLine();
            $msg .= "\nURL: " . "<a href='".@$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI']."'>".@$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI']. "</a>";
            $msg .= "\nREMOTE_ADDR: " . @$_SERVER['REMOTE_ADDR'];
            $msg .= "\nHTTP_USER_AGENT: " . @$_SERVER['HTTP_USER_AGENT'];
            $msg .= "\nHTTP_REFERER: " . @$_SERVER['HTTP_REFERER'];
            $msg .= "\nREQUEST_METHOD: " . @$_SERVER['REQUEST_METHOD'];
            $msg .= "\nSERVER_NAME: " . @$_SERVER['SERVER_NAME'];
            $msg .= "\nHTTP_HOST: " . @$_SERVER['HTTP_HOST'];
            Debug::pushNotification($msg);
        }
        if ($this->shouldReport($exception) && app()->bound('sentry')) {
            app('sentry')->captureException($exception);
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }


}
